#pragma once

#include <gromacs/fileio/warninp.h>  // warninp_t
#include <gromacs/gmxpreprocess/grompp_impl.h>  // MoleculeInformation
#include <gromacs/mdtypes/inputrec.h>  // t_inputrec
#include <gromacs/topology/topology.h>  // gmx_mtop_t

/**
 * Take the scattering parameters (Cromer-Mann and NSL) from the parameter list in t_molinfo and
 *  - put them into mtop->scattTypes, keeping each scatting type only once
 *  - store the scattering type for each atom in atoms->atom[].nsltype or  atoms->atom[].cmtype
 */
void add_scattering_params(t_inputrec* ir, gmx_mtop_t* sys, std::vector<MoleculeInformation>& mi, warninp_t wi);
