#pragma once

#include <gromacs/fileio/readinp.h>  // t_inpfile
#include <gromacs/fileio/warninp.h>  // warninp_t
#include <gromacs/mdtypes/inputrec.h>  // t_inputrec
#include <gromacs/topology/topology.h>  // gmx_groups_t
#include <gromacs/utility/basedefinitions.h>  // gmx_bool
#include <gromacs/utility/real.h>  // real

void do_index_waxs(t_inputrec* ir, warninp_t wi, gmx_bool useReferenceTemperature, real tau_min, int natoms,
        gmx_bool bVerbose, SimulationGroups* groups, t_blocka* grps, int restnm, char** gnames);

void get_waxs_ir(std::vector<t_inpfile>* inp, t_inputrec* ir, warninp_t wi);
