#include "./readir.h"

#include <stdlib.h>

#include <gromacs/fileio/readinp.h>  // printStringNewline
#include <gromacs/fileio/warninp.h>  // warninp_t
#include <gromacs/gmxpreprocess/readir.h>  // gmx_groups_t, t_blocka
#include <gromacs/mdtypes/inputrec.h>  // t_inputrec
#include <gromacs/utility/cstringutil.h>  // gmx_strcasecmp
#include <gromacs/utility/fatalerror.h>  // FARGS, gmx_fatal
#include <gromacs/utility/smalloc.h>  // snew
#include <gromacs/utility/stringutil.h>  // splitString

#include <waxs/gmxlib/names.h>  // waxs*, escatter*, *TYPE

enum {
    egrptpALL,         /* All particles have to be a member of a group.     */
    egrptpALL_GENREST, /* A rest group with name is generated for particles *
                        * that are not part of any group.                   */
    egrptpPART,        /* As egrptpALL_GENREST, but no name is generated    *
                        * for the rest group.                               */
    egrptpONE          /* Merge all selected groups into one group,         *
                        * make a rest group for the remaining particles.    */
};

static char waxs_coupl_types[STRLEN], waxs_solute[STRLEN], waxs_solvent[STRLEN], waxs_rotfit[STRLEN],
        waxs_pbcatom[STRLEN], waxs_ensemble_init_w[STRLEN], waxs_fc[STRLEN], waxs_nq[STRLEN],
        waxs_start_q[STRLEN], waxs_end_q[STRLEN], waxs_deuter_conc[STRLEN];

void do_index_waxs(t_inputrec* ir, warninp_t wi, const gmx_bool useReferenceTemperature, const real tau_min, const int natoms,
        const gmx_bool bVerbose, SimulationGroups* groups, t_blocka* grps, const int restnm, char** gnames) {
    /** WAXS input processing */

    char warn_buf[STRLEN];

    ir->waxs_nTypes = gmx::splitString(waxs_coupl_types).size();

    std::vector<std::string> waxs_coupl_types_split = gmx::splitString(waxs_coupl_types);

    snew(ir->escatter, ir->waxs_nTypes);

    int nNeutron = 0;

    if (ir->waxs_nTypes > 0) {
        for (int i = 0; i < ir->waxs_nTypes; i++) {
            if (!gmx_strcasecmp(waxs_coupl_types_split[i].c_str(), "xray")) {
                ir->escatter[i] = escatterXRAY;
            } else if (!gmx_strcasecmp(waxs_coupl_types_split[i].c_str(), "neutron")) {
                ir->escatter[i] = escatterNEUTRON;
                nNeutron++;
            } else {
                sprintf(warn_buf, "Invalid scattering type %d of %d: %s\n", i, ir->waxs_nTypes,
                        waxs_coupl_types_split[i].c_str());
                warning_error(wi, warn_buf);
            }
        }

        if (ir->cutoff_scheme == ecutsVERLET) {
            /*warning_error(wi, "WAXS calculations are not currently implemented with Verlet cutoffs!\n"
              "Please use Groups cut-off scheme for WAXS calculations.\n");*/
        }

        if (ir->waxs_nstcalc <= 0) {
            sprintf(warn_buf, "waxs_nstcalc should be larger than 0 (found %d)\n", ir->waxs_nstcalc);
            warning_error(wi, warn_buf);
        }

        /* Solvent Density weights requires a non-zero relative error */
        if (ir->ewaxs_weights > 2 and ir->waxs_denssolventRelErr <= 0) {
            sprintf(warn_buf, "The weighting scheme for WAXS Potentials include solvent density, but waxs-solvdens-uncert is set to %g.\n",
                    ir->waxs_denssolventRelErr);
            warning_error(wi, warn_buf);
        }

        if (ir->ewaxs_weights < 3 and ir->waxs_denssolventRelErr > 0) {
            sprintf(warn_buf, "The weighting scheme for WAXS Potentials do not include solvent density, but waxs-solvdens-uncert is set to %g. Setting it to 0 to avoid unneeded calculations!\n",
                    ir->waxs_denssolventRelErr);
            warning(wi, warn_buf);
            ir->waxs_denssolventRelErr = 0;
        }

        if (useReferenceTemperature and
                (ir->waxs_tau > 0. and ((tau_min > 0.5 * ir->waxs_tau) or !EI_RANDOM(ir->eI))))
        {
            sprintf(warn_buf, "When coupling to an averaged WAXS intensity (waxs_tau=%g), a stochastic/brownian "
                    "dynamics integrator with a shorter time constant (tau_t) is advised (found %g). Otherwise, "
                    "you may pump energy into the system\n", ir->waxs_tau, tau_min);
            warning(wi, warn_buf);
        }
    }

    int nuser_solute = gmx::splitString(waxs_solute).size();

    /* If nuser_solute == 0, this will generate a tpr for the pure-water box */
    // if (ir->escatter and nuser != 1) {
    //     gmx_fatal(FARGS, "WAXS mdp error: one solute group must be specified (found %d).\n", nuser);
    // }

    do_numbering(natoms, groups, gmx::splitString(waxs_solute), grps, gnames, SimulationAtomGroupType::egcWAXSSolute, restnm,
            egrptpALL_GENREST, bVerbose, wi);

    int nuser_solvent = gmx::splitString(waxs_solvent).size();

    do_numbering(natoms, groups, gmx::splitString(waxs_solvent), grps, gnames, SimulationAtomGroupType::egcWAXSSolvent, restnm,
            egrptpALL_GENREST, bVerbose, wi);

    /* nuser_solvent is symmetric to nuser_solute, and thus can allow for multiple groups. */
    // if (ir->escatter != escatterNO and nuser_solvent>1) {
    //     gmx_fatal(FARGS, "Only one group allowed as solvent in WAXS analysis (found %d).\n", nuser_solvent);
    // }

    if (ir->waxs_nTypes > 0 and nuser_solute == 0 and nuser_solvent == 0) {
        gmx_fatal(FARGS, "Error in WAXS mdp options: you must provide a solute group, a solvent group, or both.\n"
                "(found %d and %d groups for solute and solvent, respectively.\n\n"
                "For generating a tpr for the pure-solvent box (to get the excluded-solvent scatting),"
                " provide only a group for waxs-solvent and leave waxs-solute empty.",
                nuser_solute, nuser_solvent);
    }

    // fprintf(stderr, "In readir.c, nuser_solute: %d, nuser_solvent: %d.\n ", nuser_solute, nuser_solvent);

    /* Convert the pbc-atom input into a list of pbcatoms. */
    ir->waxs_npbcatom = gmx::splitString(waxs_pbcatom).size();
    std::vector<std::string> waxs_pbcatom_split = gmx::splitString(waxs_pbcatom);

    if (ir->waxs_nTypes > 0 and ir->waxs_npbcatom == 0) {
        warning_error(wi, "Scattering calculations require a PBC atom (waxs-pbcatom");
    }

    if (ir->waxs_nTypes > 0) {
        snew(ir->waxs_pbcatoms, ir->waxs_npbcatom);

        for (int i = 0; i < ir->waxs_npbcatom; i++) {
            /* Do out of bounds checking here */
            /* Switch to intra-gmx numbering starting from zero */
            ir->waxs_pbcatoms[i] = atoi(waxs_pbcatom_split[i].c_str()) - 1;

            if (natoms <= ir->waxs_pbcatoms[i]) {
                sprintf(warn_buf, "A pbcatom for WAXS calculations is greater than the number of atoms in the system!\n"
                        "Please check waxs-pbcatom settings!\n");
                warning_error(wi, warn_buf);
            }
        }
    } else {
        ir->waxs_npbcatom = 0;
        ir->waxs_pbcatoms = nullptr;
    }

    if (ir->waxs_nTypes > 0 and nuser_solute != 0 and ir->waxs_npbcatom == 1 and ir->waxs_pbcatoms[0] == -1) {
        sprintf(warn_buf, "Scattering calculations require the solute to be whole.\n"
                "The default number-wise centre (waxs_npbcatom = 0) is inaccurate and can abort\n"
                "the simulation. Consider giving specific atom-indices near the geometric centre.\n");
        warning_note(wi, warn_buf);
    }

    int nuser_rotfit = gmx::splitString(waxs_rotfit).size();

    if (nuser_rotfit == 0 and nuser_solute != 0) {
        if (ir->waxs_nTypes > 0) {
            sprintf(warn_buf, "No selection found for rotational-fitting. Will use the default \n"
                    "(all waxs-solute atoms), which can take a long time to calculate!\n");
            warning_note(wi, warn_buf);
        }

        /** Copy the group from the WAXS-solute over to the fitting group.*/
        do_numbering(natoms, groups, gmx::splitString(waxs_solute), grps, gnames, SimulationAtomGroupType::egcROTFIT, restnm,
                egrptpALL_GENREST, bVerbose, wi);
    } else {
        do_numbering(natoms, groups, gmx::splitString(waxs_rotfit), grps, gnames, SimulationAtomGroupType::egcROTFIT, restnm,
                egrptpALL_GENREST, bVerbose, wi);
    }

    /* WAXS/Neutron force constant, nq, startq, endq: need 1 parameter per xray or neutron */
    snew(ir->waxs_fc,          ir->waxs_nTypes);
    snew(ir->waxs_nq,          ir->waxs_nTypes);
    snew(ir->waxs_start_q,     ir->waxs_nTypes);
    snew(ir->waxs_end_q,       ir->waxs_nTypes);
    snew(ir->waxs_deuter_conc, ir->waxs_nTypes);

    if (ir->waxs_nTypes > 0) {
        /** Picking WAXS/NEUTRON force constants */
        int nuser = gmx::splitString(waxs_fc).size();

        std::vector<std::string> waxs_fc_split = gmx::splitString(waxs_fc);

        if (nuser != ir->waxs_nTypes) {
            gmx_fatal(FARGS, "Expected %d valus for mdp option waxs-fc (found %d)\n", ir->waxs_nTypes, nuser);
        }

        for (int i = 0; i < ir->waxs_nTypes; i++) {
            ir->waxs_fc[i] = atof(waxs_fc_split[i].c_str());
        }

        /** Picking WAXS/NEUTRON number of q points */
        nuser = gmx::splitString(waxs_nq).size();
        std::vector<std::string> waxs_nq_split = gmx::splitString(waxs_nq);

        if (nuser != ir->waxs_nTypes) {
            gmx_fatal(FARGS, "Expected %d valus for mdp option waxs-nq (found %d)\n", ir->waxs_nTypes, nuser);
        }

        for (int i = 0; i < ir->waxs_nTypes; i++) {
            ir->waxs_nq[i] = atoi(waxs_nq_split[i].c_str());
        }

        /** Picking WAXS/NEUTRON q-start */
        nuser = gmx::splitString(waxs_start_q).size();
        std::vector<std::string> waxs_start_q_split = gmx::splitString(waxs_start_q);

        if (nuser != ir->waxs_nTypes) {
            gmx_fatal(FARGS, "Expected %d valus for mdp option waxs-startq (found %d)\n", ir->waxs_nTypes, nuser);
        }

        for (int i = 0; i < ir->waxs_nTypes; i++) {
            ir->waxs_start_q[i] = atof(waxs_start_q_split[i].c_str());
        }

        /* Picking WAXS/NEUTRON q-end */
        nuser = gmx::splitString(waxs_end_q).size();
        std::vector<std::string> waxs_end_q_split = gmx::splitString(waxs_end_q);

        if (nuser != ir->waxs_nTypes) {
            gmx_fatal(FARGS, "Expected %d valus for mdp option waxs-endq (found %d)\n", ir->waxs_nTypes, nuser);
        }

        for (int i = 0; i < ir->waxs_nTypes; i++) {
            ir->waxs_end_q[i] = atof(waxs_end_q_split[i].c_str());
        }

        /* Picking deuterium concentration */
        nuser = gmx::splitString(waxs_deuter_conc).size();
        std::vector<std::string> wax_deuter_conc_split = gmx::splitString(waxs_deuter_conc);

        if (nuser != nNeutron) {
            gmx_fatal(FARGS, "Expected %d valus for mdp option waxs-deuter-conc, one for each neutron group (found %d)\n",
                    nNeutron, nuser);
        }

        int iNeutron = 0;

        for (int i = 0; i < ir->waxs_nTypes; i++) {
            /* waxs_deuter_conc has size waxs_nTypes, but ptr1 has size nNeutron */
            if (ir->escatter[i] == escatterNEUTRON) {
                ir->waxs_deuter_conc[i] = atof(wax_deuter_conc_split[iNeutron++].c_str());

                if (ir->waxs_deuter_conc[i] < 0 or ir->waxs_deuter_conc[i] > 1) {
                    warning_error(wi, "Deuterium fraction in solvent (waxs-deuter-conc) should be between 0 and 1.");
                }
            } else {
                ir->waxs_deuter_conc[i] = -1;
            }
        }

        if (ir->ewaxs_weights == ewaxsWeightsUNIFORM and ir->ewaxs_potential == ewaxsPotentialLINEAR) {
            warning_note(wi, "In case you use this tpr for a SAXS-driven MD, you should not combine a "
                         "linear SAXS potential (waxs-potential) with a uniform weights (waxs-weights), "
                         "since will lead to huge forces. Uniform weights are meant for potential on a log "
                         "scale.");
        }
    }

    if (ir->ewaxs_ensemble_type == ewaxsEnsemble_maxentEnsemble) {
        warning_error(wi, "Ensmelbe type maxent-ensemble is not yet supported\n");
    }

    if (ir->waxs_nTypes > 0 and ir->waxs_ensemble_nstates > 0 and ir->ewaxs_ensemble_type == ewaxsEnsemble_BayesianOneRefined) {
        if (ir->waxs_ensemble_nstates == 1 or ir->waxs_ensemble_nstates >= 50) {
            gmx_fatal(FARGS, "waxs-ensemble-nstate should be either 0 (no ensemble refinement), >= 2, and < 50 (found %d)\n",
                    ir->waxs_ensemble_nstates);
        }

        /** Reading initial weights for states of the refined ensemble */

        int nuser = gmx::splitString(waxs_ensemble_init_w).size();
        std::vector<std::string> waxs_ensemble_init_w_split = gmx::splitString(waxs_ensemble_init_w);

        if (ir->waxs_ensemble_nstates != nuser and nuser > 0) {
            gmx_fatal(FARGS, "The number of initial weithts for WAXS ensemble states is wrong. Expected %d values, found %d !\n",
                    ir->waxs_ensemble_nstates, nuser);
        }

        snew(ir->waxs_ensemble_init_w, ir->waxs_ensemble_nstates);

        if (nuser == 0) {
            sprintf(warn_buf, "No initial weights given for states in SAXS ensemble refinement - using equal weights (%g)\n",
                    1. / ir->waxs_ensemble_nstates);
            warning_note(wi, warn_buf);

            for (int i = 0; i < ir->waxs_ensemble_nstates; i++) {
                ir->waxs_ensemble_init_w[i] = 1. / ir->waxs_ensemble_nstates;
            }
        } else {
            real sum_w = 0;

            for (int i = 0; i < ir->waxs_ensemble_nstates; i++) {
                if (atof(waxs_ensemble_init_w_split[i].c_str()) < 0) {
                    gmx_fatal(FARGS, "Found negative weight of state in SWAXS refinement (%g)!\n",
                            atof(waxs_ensemble_init_w_split[i].c_str()));
                }

                sum_w += atof(waxs_ensemble_init_w_split[i].c_str());
            }

            for (int i = 0; i < ir->waxs_ensemble_nstates; i++) {
                ir->waxs_ensemble_init_w[i] = atof(waxs_ensemble_init_w_split[i].c_str()) / sum_w;
            }
        }

        fprintf(stderr, "Initial weights of states in SAXS refinement:");

        for (int i = 0; i < ir->waxs_ensemble_nstates; i++) {
            fprintf(stderr, " %g", ir->waxs_ensemble_init_w[i]);
        }

        fprintf(stderr, "\n\n");
    } else {
        ir->waxs_ensemble_init_w = nullptr;
        ir->waxs_ensemble_nstates = 0;
    }
}

void get_waxs_ir(std::vector<t_inpfile>* inp, t_inputrec* ir, warninp_t wi) {
    /* Wide/Small-angle X-ray scattering and SANS stuff */
    printStringNewline(inp, "Scattering coupling stuff: xray and/or neutron (multiple neutron possible)");
    // ir->escatter = get_eeenum(inp, "scatt-coupl", escatter_names, wi);
    setStringEntry(inp, "scatt-coupl",  waxs_coupl_types, nullptr);
    printStringNoNewline(inp, "Selection of solute, solvent and fit group");
    setStringEntry(inp, "waxs-solute", waxs_solute, nullptr);
    setStringEntry(inp, "waxs-solvent", waxs_solvent, nullptr);
    setStringEntry(inp, "waxs-rotfit", waxs_rotfit, nullptr);
    printStringNoNewline(inp, "WAXS pbc atom list near center of solute (uses global indices, 0 = number-wise center, -1 = used atomic distances)");
    setStringEntry(inp, "waxs-pbcatom", waxs_pbcatom, nullptr);
    printStringNoNewline(inp, "Coupling time constant (ps). (0=no averaging; -1=non-weighted average)");
    ir->waxs_tau = get_ereal(inp, "waxs-tau", 100, wi);
    printStringNoNewline(inp, "Gradual switch target SWAXS curve from current to experimental curve within this time (ps)");
    ir->waxs_t_target = get_ereal(inp, "waxs-t-target", 0, wi);
    printStringNoNewline(inp, "WAXS coupling potential type (linear or log)");
    ir->ewaxs_potential = get_eeenum(inp, "waxs-potential", waxspotential_names, wi);
    printStringNoNewline(inp, "WAXS coupling weights (uniform, exp, exp+calc, exp+solvdens, exp+calc+solvdens)");
    ir->ewaxs_weights = get_eeenum(inp, "waxs-weights", waxsweights_names, wi);
    printStringNoNewline(inp, "Extra overall force constant (one per scatt-coupl type)");
    setStringEntry(inp, "waxs-fc", waxs_fc, nullptr);
    printStringNoNewline(inp, "Frequency of spectrum calculation (steps)");
    ir->waxs_nstcalc = get_eint(inp, "waxs-nstcalc", 500, wi);
    printStringNoNewline(inp, "Number of pure solvent structure factors to use (-1 = take from xtc)");
    ir->waxs_nfrsolvent = get_eint(inp, "waxs-nfrsolvent", -1, wi);
    printStringNoNewline(inp, "Frequency of output (steps)");
    ir->waxs_nstlog = get_eint(inp, "waxs-nstlog", 500, wi);
    printStringNoNewline(inp, "Nr of q values, starting and ending q (one per scatt-coupl type)");
    setStringEntry(inp, "waxs-nq", waxs_nq, nullptr);
    setStringEntry(inp, "waxs-startq", waxs_start_q, nullptr);
    setStringEntry(inp, "waxs-endq", waxs_end_q, nullptr);
    printStringNoNewline(inp, "Nr of point for spherical quadrature (0 = auto)");
    ir->waxs_J = get_eint(inp, "waxs-nsphere", 0, wi);
    printStringNoNewline(inp, "Fit I(exp) on the fly to I(calc): no / scale-and-offset /scale");
    ir->ewaxs_Iexp_fit = get_eeenum(inp, "waxs-Iexp-fit", waxs_Iexp_fit_names, wi);
    printStringNoNewline(inp, "Electron density of solvent (0 = use xtc, 334 = experiment)");
    ir->waxs_denssolvent = get_ereal(inp, "waxs-solvdens", 0, wi);
    printStringNoNewline(inp, "Relative uncertainty of solvent density (e.g., 0.01 means 1% uncertainty)");
    ir->waxs_denssolventRelErr = get_ereal(inp, "waxs-solvdens-uncert", 0, wi);
    printStringNoNewline(inp, "Bayesian sampling of solvent density uncertainty (no/yes)");
    ir->ewaxs_solvdensUnsertBayesian = get_eeenum(inp, "waxs-solvdens-uncert-bayesian", waxssolvdensunsertbayesian_names, wi);
    printStringNoNewline(inp, "Buffer subtraction reduced by solute volume (no/yes)");
    ir->ewaxs_correctbuff = get_eeenum(inp, "waxs-correct-buffer", waxscorrectbuff_names, wi);
    printStringNoNewline(inp, "Deuterium concentration (0 - 1), for each neutron group");
    setStringEntry(inp, "waxs-deuter-conc", waxs_deuter_conc, nullptr);
    printStringNoNewline(inp, "Scale I(q=0) to target pattern while coupling");
    ir->ewaxs_bScaleI0 = get_eeenum(inp, "waxs-scale-i0", waxs_bScaleI0_names, wi);
    printStringNoNewline(inp, "Warn if solvation layer is thinner than:");
    ir->waxs_warn_lay = get_ereal(inp, "waxs-warnlay", 0.3, wi);
    printStringNoNewline(inp, "WAXS ensemble type (none, bayesian-one-refined, maxent-ensemble)");
    ir->ewaxs_ensemble_type = get_eeenum(inp, "waxs-ensemble-type", waxsensemble_names, wi);
    printStringNoNewline(inp, "Ensemble refinement - number of states (will read files intentisty_stateXX.dat)");
    ir->waxs_ensemble_nstates = get_eint(inp, "waxs-ensemble-nstates", 0, wi);
    printStringNoNewline(inp, "Ensemble refinement - initial weights and force constant");
    setStringEntry(inp, "waxs-ensemble-init-w", waxs_ensemble_init_w, nullptr);
    ir->waxs_ensemble_fc = get_ereal(inp, "waxs-ensemble-fc", 0.0, wi);
}
