#include "./grompp.h"  // t_inputrec, gmx_mtop_t, t_molinfo, warninp_t

#include <gromacs/fileio/warninp.h>  // warninp_t
#include <gromacs/gmxpreprocess/grompp_impl.h>  // MoleculeInformation
#include <gromacs/gmxpreprocess/notset.h>  // NOTSET
#include <gromacs/mdtypes/inputrec.h>  // t_inputrec
#include <gromacs/topology/topology.h>  // gmx_mtop_t
#include <gromacs/utility/cstringutil.h>  // STRLEN
#include <gromacs/utility/fatalerror.h>  // FARGS, gmx_fatal

#include <waxs/gmxlib/sftypeio.h>  // countGroups
#include <waxs/types/enums.h>  // escatter*

/**
 * Take the scattering parameters (Cromer-Mann and NSL) from the parameter list in t_molinfo and
 *  - put them into mtop->scattTypes, keeping each scatting type only once
 *  - store the scattering type for each atom in atoms->atom[].nsltype or  atoms->atom[].cmtype
 */
void add_scattering_params(t_inputrec* ir, gmx_mtop_t* sys, std::vector<MoleculeInformation>& mi, warninp_t wi) {
    /** The total number of unique atoms is nmoltypes * natoms_in_eas_type. */
    int isum = 0;

    int nSoluteGroups = countGroups(sys->groups, SimulationAtomGroupType::egcWAXSSolute);
    int nSolventGroups = countGroups(sys->groups, SimulationAtomGroupType::egcWAXSSolvent);

    gmx_bool bXray    = FALSE;
    gmx_bool bNeutron = FALSE;

    for (int i = 0; i < ir->waxs_nTypes; i++) {
        bXray    |= (ir->escatter[i] == escatterXRAY);
        bNeutron |= (ir->escatter[i] == escatterNEUTRON);
    }

    /*
    nSoluteGroups=isys->groups.grps[egcWAXSSolute].nr - 1 ;
    nSolventGroups=sys->groups.grps[egcWAXSSolvent].nr - 1 ;

     Initiate superfluous tests to distinguish between zero groups and one group. Happens when entinre system is assigned.
    if (nSoluteGroups == 0)
    {
        fprintf(stderr,"Lone solute group name: %s",*(sys->groups.grpname[sys->groups.grps[egcWAXSSolute].nm_ind[0]]) );
        if ( strncmp("rest",*(sys->groups.grpname[sys->groups.grps[egcWAXSSolute].nm_ind[0]]),4) != 0 )
        {
            nSoluteGroups++;
            fprintf(stderr,".\n");
        } else
             fprintf(stderr," - this is a fictitious group.\n");
    }
    if (nSolventGroups == 0)
    {
        fprintf(stderr,"Lone solvent group name: %s",*(sys->groups.grpname[sys->groups.grps[egcWAXSSolvent].nm_ind[0]]) );
        if ( strncmp("rest",*(sys->groups.grpname[sys->groups.grps[egcWAXSSolvent].nm_ind[0]]),4) != 0 )
        {
            nSolventGroups++;
            fprintf(stderr,".\n");
        } else
            fprintf(stderr," - this is a fictitious group.\n");
    }
    */

    fprintf(stderr, "Atomic-scattering indices: there are %d solute groups and %d solvent groups\n",
            nSoluteGroups, nSolventGroups);

    if (nSoluteGroups == 0 and nSolventGroups == 0) {
        warning_note(wi, "You have elected to record scattering but have not given any groups for selection. Is this correct?\n");
    }

    /** For each molecule 'type' in the topology, e.g. Protein, solvent. */

    for (std::size_t mb = 0; mb < sys->molblock.size(); mb++) {
        t_atoms* atoms = &sys->moltype[sys->molblock[mb].type].atoms;

        auto& xray_interactions = mi[sys->molblock[mb].type].interactions[F_XRAY_COUPLE];
        auto& neutron_interactions = mi[sys->molblock[mb].type].interactions[F_NEUTRON_COUPLE];

        /* loop over all atoms in this block. */
        for (int iatom = 0; iatom < atoms->nr; iatom++) {
            /* Enable index conversion between the systematic atom index used in the custom input and the molecular index. */
            int i_tot = iatom + isum;

            gmx_bool bSolute = (nSoluteGroups > 0)
                    and (getGroupType(sys->groups, SimulationAtomGroupType::egcWAXSSolute, i_tot) < nSoluteGroups);
            gmx_bool bSolvent = (nSolventGroups > 0)
                    and (getGroupType(sys->groups, SimulationAtomGroupType::egcWAXSSolvent, i_tot) < nSolventGroups);

            /** Init cmtype and nsl type to unset */
            atoms->atom[iatom].cmtype = NOTSET;
            atoms->atom[iatom].nsltype = NOTSET;

            /* Debugging. */

            //if (iatom < 5)
            //    fprintf(stderr,"Atom (%d,%d) group membership: %d,%d. cmtype: %d -- nsltype = %d - bSolute %d - bSolvent %d\n",i_tot+1, iatom,
            //            getGroupType(sys->groups, SimulationAtomGroupType::egcWAXSSolute, i_tot),
            //            getGroupType(sys->groups, SimulationAtomGroupType::egcWAXSSolvent, i_tot),
            //            atoms->atom[iatom].cmtype, atoms->atom[iatom].nsltype, bSolute, bSolvent);

            if (bSolute or bSolvent) {
                /** Check for AND case */
                if (bSolute and bSolvent) {
                    char warn_buf[STRLEN];

                    sprintf(warn_buf, "Atom nr %d (name %s) is in both solvent and solute groups. This should not be allowed.\n",
                            i_tot + 1, *(atoms->atomname[iatom]));

                    warning_error(wi, warn_buf);
                }

                /* search for this atom in parameter list */
                /* Note: Even if you do only neutron scattering, we need Cromer-Mann parameters to compute the solvent density */
                if (bXray or bNeutron) {
                    auto& interactions = xray_interactions;

                    int np = interactions.size();
                    int ip = -1;

                    // fprintf(stderr, "Looking for atom nr %d in interactions\n",iatom);

                    for (int i = 0; i < np; i++) {
                        if (interactions.interactionTypes[i].ai() == iatom) {
                            ip = i;

                            break;
                        }
                    }

                    if (ip == -1) {
                        gmx_fatal(FARGS,"Atom nr %d (name %s) is in WAXS scattering group %s, but no\n"
                                  "Cromer-Mann parameters are defined for this atom.\n\n"
                                  "Use g_genscatt to generate an itp file with Cromer-Mann definitions,\n"
                                  "and include it into the molecule type definition in the topology.\n\n"
                                  "Note: Even if you do only neutron scattering, we need Cromer-Mann\n"
                                  "      parameters to compute the solvent density.",
                                  i_tot + 1, *(atoms->atomname[iatom]), bSolute ? "Solute" : "Solvent");
                    }

                    t_cromer_mann cm;

                    for (int i = 0; i < 4; i++) {
                        cm.a[i] = interactions.interactionTypes[ip].forceParam()[i];
                        cm.b[i] = interactions.interactionTypes[ip].forceParam()[i + 4];
                    }

                    cm.c = interactions.interactionTypes[ip].forceParam()[8];

                    int isf = search_scattTypes_by_cm(&sys->scattTypes, &cm);

                    if (isf == NOTSET) {
                        isf = add_scatteringType(&sys->scattTypes, &cm, nullptr);
                    }

                    atoms->atom[iatom].cmtype = isf;

                    // fprintf(stderr,"isf (ato %3d, %s) = %d, (%d total)\n",iatom,
                    //        *(atoms->atomname[iatom]), isf, sys->scattTypes.ncm);
                }

                if (bNeutron) {
                    auto& interactions = neutron_interactions;

                    int np = interactions.size();
                    int ip = -1;

                    // fprintf(stderr, "Looking for atom nr %d in interactions\n",iatom);

                    for (int i = 0; i < np; i++) {
                        if (interactions.interactionTypes[i].ai() == iatom) {
                            ip = i;
                            break;
                        }
                    }

                    if (ip == -1) {
                        gmx_fatal(FARGS,"Atom nr %d (name %s) is in NEUTRON scattering group %s, but no\n"
                                  "Neutron Sattering Length (NSL) is defined for this atom.\n\n"
                                  "Use g_genscatt to generate an itp file with NSL definitions,\n"
                                  "and include it into the molecule type definition in the topology.",
                                  i_tot + 1, *(atoms->atomname[iatom]), bSolute ? "Solute" : "Solvent");
                    }

                    t_neutron_sl nslType;

                    nslType.cohb = interactions.interactionTypes[ip].forceParam()[0];

                    int isf = search_scattTypes_by_nsl(&sys->scattTypes, &nslType);

                    if (isf == NOTSET) {
                        isf = add_scatteringType(&sys->scattTypes, nullptr, &nslType);
                    }

                    atoms->atom[iatom].nsltype = isf;
                }
            }
        }

        isum += atoms->nr * sys->molblock[mb].nmol;
    }

    fprintf(stderr, "Generated list of %2d unique Cromer-Mann types\n", sys->scattTypes.nCromerMannParameters);
    fprintf(stderr, "Generated list of %2d unique NSL         types\n", sys->scattTypes.nNeutronScatteringLengths);
}
