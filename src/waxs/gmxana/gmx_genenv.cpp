#include <algorithm>  // std::max
#include <cmath>  // norm2, sqr
#include <cstring>  // strlen, strcpy

#include <gromacs/gmxana/gmx_ana.h>  // Required to register this file as a CLI tool

#include <gromacs/commandline/pargs.h>  // etBOOL, etINT, etREAL, etRVEC, t_pargs, PCA_BE_NICE, PCA_CAN_TIME
#include <gromacs/fileio/confio.h>  // write_sto_conf_indexed
#include <gromacs/fileio/tpxio.h>  // read_tps_conf, read_tpx, t_tpxheader
#include <gromacs/fileio/trxio.h>  // get_index, gmx_conect, read_next_x, t_trxstatus
#include <gromacs/fileio/xvgr.h>  // xvgropen
#include <gromacs/math/do_fit.h>  // do_fit, reset_x
#include <gromacs/math/functions.h>  // square
#include <gromacs/math/vec.h>  // clear_rvec, copy_rvec, rvec_dec, rvec_inc, rvec_sub, norm2, sqr, dsqr
#include <gromacs/pbcutil/pbc.h>  // ecenterTRIC, PbcType, set_pbc, TRICLINIC
#include <gromacs/pbcutil/rmpbc.h>  // gmx_rmpbc_init, gmx_rmpbc_t
#include <gromacs/topology/atomprop.h> // epropVDW
#include <gromacs/topology/index.h>  // get_index
#include <gromacs/utility/arraysize.h>  // asize
#include <gromacs/utility/cstringutil.h>  // STRLEN
#include <gromacs/utility/arrayref.h>  // ArrayRef
#include <gromacs/utility/fatalerror.h>  // gmx_fatal, gmx_incons
#include <gromacs/utility/futil.h>  // gmx_ffopen, gmx_ffclose
#include <gromacs/utility/gmxomp.h>  // gmx_omp_get_thread_num, gmx_omp_get_max_threads
#include <gromacs/utility/smalloc.h>  // snew, srenew, sfree

#include <waxs/envelope/density.h>  // swaxs::envelope_unit_ft
#include <waxs/envelope/envelope.h>  // gmx_envelope
#include <waxs/envelope/serialization.h>  // envelope_read_from_file, envelope_write_to_file, envelope_write_pymol_cgo, envelope_write_vmd_cgo
#include <waxs/envelope/volume.h>  // swaxs::envelope_volume, swaxs::envelope_calculate_volume
#include <waxs/gmxlib/bounding_sphere.h>  // get_bounding_sphere
#include <waxs/gmxlib/gmx_envelope.h>
#include <waxs/gmxlib/waxsmd.h>  // gen_qvecs_map
#include <waxs/gmxlib/waxsmd_utils.h>  // check_prot_box_distance, swaxs::countElectronsPerAtom, mv_cog_to_rvec, read_fit_reference

static void mv_bounding_sphere_to_origin(gmx::ArrayRef<gmx::RVec> x, int nat, int nsolute, int* index) {
    rvec cent = { 0, 0, 0 };
    real R = 0;
    static int bFirst = TRUE;

    get_bounding_sphere(x, index, nsolute, cent, &R, bFirst);

    /* shift geomtric center to orgin */
    for (int i = 0; i < nat; i++) {
        rvec_dec(x[i], cent);
    }

    bFirst = FALSE;
}

#define WAXS_WARN_BOX_DIST 0.5

static void solvent_rdf_inside_envelope(int nd, real dmin, real dmax, gmx::ArrayRef<gmx::RVec> x_solute_allFrames,
        int* index_solute_allFrames, int isize_solute_allFrames, int* index_solute, int nsolute,
        real* vdwRadii, int nRadii,
        rvec* xref, real* w_rls, int nfit, int* ifit, gmx_bool bFit, gmx_bool bCentBoundingSphere,
        int nAtomsTotal, real sigma_smooth, t_trxstatus* status, gmx_output_env_t* oenv, t_topology* top,
        const char* ndxfile, int nrec, const char* solvent_rdf_file, PbcType pbcType,
        real rdf_max, int nbins) {

    if (nd != 2) {
        gmx_fatal(FARGS, "To compute the solvent RDF within the envelope, you must specify the inner and outer "
                  "distance from the solute with -nd 2 -dmin DMIN and -dmax DMAX. Only atoms within this distance "
                  "range are used to compute the RDF (found nd = %d instead of 2)\n", nd);
    }

    /* Build inner and outer envelope */
    gmx_envelope* envelope_inner = gmx_envelope_init(nrec, TRUE);
    gmx_envelope* envelope_outer = gmx_envelope_init(nrec, TRUE);
    gmx_envelope_buildEnvelope_omp(envelope_inner, x_solute_allFrames, index_solute_allFrames,
                                   isize_solute_allFrames, dmin, sigma_smooth, FALSE, vdwRadii, nRadii);
    gmx_envelope_buildEnvelope_omp(envelope_outer, x_solute_allFrames, index_solute_allFrames,
                                   isize_solute_allFrames, dmax, sigma_smooth, FALSE, vdwRadii, nRadii);

    /* Read index group of atom type for RDF calculation */
    printf("\nComputing the RDF of solvent inside the envelope...\n");
    printf("\nChoose a group for one atom type of the solvent:\n");
    int nsolvent = 0;
    int* isolvent = nullptr;
    char* solventname = nullptr;
    get_index(&top->atoms, ndxfile, 1, &nsolvent, &isolvent, &solventname);

    /* Read first frame of the xtc file */
    matrix box;
    rvec* xread = nullptr;
    snew(xread, nAtomsTotal);
    real t;
    read_next_x(oenv, status, &t, xread, box);

    int nframes = 0;
    int nWarn = 0;
    const int nWarnMax = 20;

    real rdf_max2 = rdf_max*rdf_max;
    real dbin = rdf_max / nbins;

    /* Alloc RDF array for each OpenMP thread */
    int nThreads = gmx_omp_get_max_threads();
    real** rdf_loc;
    snew(rdf_loc, nThreads);
    for (int t = 0; t < nThreads; t++) {
        snew(rdf_loc[t], nbins);
    }

    int nSolventAlloc = 0;
    int* indexInEnvelope = nullptr;
    double nSolventAverage = 0;

    do {
        /**
         * First of all, get the solvent around the protein.
         * So move the protein to the center of the box and put the solvent into the compact unitcell
         */

        /* 1) get box center */
        rvec boxcenter;
        calc_box_center(ecenterTRIC, box, boxcenter);

        auto xreadRef = gmx::arrayRefFromArray(reinterpret_cast<gmx::RVec*>(xread), nAtomsTotal);

        /* 2) move protein to box center */
        mv_cog_to_rvec(nAtomsTotal, xreadRef, index_solute, nsolute, boxcenter, nullptr);

        /* 3) put all atoms into the compact unit cell */
        put_atoms_in_compact_unitcell(pbcType, ecenterTRIC, box, xreadRef);

        /* 4) Check if the protein has a reasonable distance from the box surface */
        real boxdist;
        check_prot_box_distance(xreadRef, index_solute, nsolute, box, nullptr, FALSE, &boxdist);

        if (boxdist < WAXS_WARN_BOX_DIST and nWarn < nWarnMax) {
            printf("\nWARNING, solute getting close to the box surface (%g nm). Your solute is probably not whole. Fix this with trjconv,\n"
                   "e.g. with options -pbc mol or -pbc nojump or -pbc cluster before calling g_genenv.\n", boxdist);

            nWarn++;
        }

        if (bFit) {
            rvec xbefore;
            copy_rvec(xread[0], xbefore);

            /* shift to zero (required for do_fit) */
            reset_x(nfit, ifit, nAtomsTotal, nullptr, xread, w_rls);

            rvec xdiff;
            rvec_sub(xbefore, xread[0], xdiff);

            do_fit(nAtomsTotal, w_rls, xref, xread);

            for (int j = 0; j < nAtomsTotal; j++) {
                /* shift back */
                rvec_inc(xread[j], xdiff);
            }
        }

        if (bCentBoundingSphere) {
            /* This option is deprecated - we now always shift the COG to the origin */
            mv_bounding_sphere_to_origin(xreadRef, nAtomsTotal, nsolute, index_solute);
        } else {
            rvec origin = {0,0,0};
            mv_cog_to_rvec(nAtomsTotal, xreadRef, index_solute, nsolute, origin, nullptr);
        }

        /* Find all the solvent atoms inside the envelope */
        int nSolvent = 0;
        for (int j = 0; j < nsolvent; j++) {
            /* First check if atom is really between the largest and the smallest envelope */
            if (gmx_envelope_isInside(envelope_outer, xread[isolvent[j]])
                && !gmx_envelope_isInside(envelope_inner, xread[isolvent[j]])) {

                /* Extend solvent index group, if needed */
                if (nSolventAlloc <= nSolvent) {
                    nSolventAlloc += 1000;
                    srenew(indexInEnvelope, nSolventAlloc);
                }

                indexInEnvelope[nSolvent] = isolvent[j];
                nSolvent++;
            }
        }
        // printf("Frame %3d: %5d solvent atoms in envelope, %d allocated\n", nframes, nSolvent, nSolventAlloc);
        nSolventAverage += nSolvent;

        #pragma omp parallel
        {
            int threadID = gmx_omp_get_thread_num();

            #pragma omp for
            for (int i = 0; i < nSolvent; i++) {
                for (int j = i+1; j < nSolvent; j++) {
                    rvec diff;
                    rvec_sub(xread[indexInEnvelope[i]], xread[indexInEnvelope[j]], diff);

                    real r2 = norm2(diff);
                    if (r2 < rdf_max2) {
                        real r = sqrt(r2);
                        int ibin = floor(r / dbin);
                        if (ibin < nbins) {
                            rdf_loc[threadID][ibin] += 1.0;
                        }
                    }
                }
            }
        }

        nframes++;
    } while (read_next_x(oenv, status, &t, xread, box));

    nSolventAverage /= nframes;
    printf("\nAverage number of %s atoms inside envelope: %g\n", solventname, nSolventAverage);

    /* Now collect RDFs from all local RDFs, and normalzie by nr of frames */
    real* rdf;
    snew(rdf, nbins);
    for (int i = 0; i < nbins; i++) {
        for (int t = 0; t < nThreads; t++) {
            rdf[i] += rdf_loc[t][i];
        }
        rdf[i] /= nframes;
    }

    /* Do not use this normalization for now, since it is not really justified.
       Probably, we should instead test how many neighbors the solvent atom
       COULD have inside the envelope, and then normalize by that. */
    gmx_bool bNormRDF = false;
    real normfactor = 1;
    if (bNormRDF) {
        /* Normalize RDF such that it is close to 1 at large distance
           To this end, consider the range
           [rdf_max - largeDistWidth, rdf_max ]
           as large distance:
        */
        const real largeDistWidth = 0.1;
        real rdfLargeDist = 0;
        real distLargeDist = 0;
        int nBinsLargeDist = largeDistWidth/dbin;
        for (int i = nbins-nBinsLargeDist; i < nbins; i++) {
            rdfLargeDist += rdf[i];
            distLargeDist += (i+0.5)*dbin;
        }
        rdfLargeDist /= nBinsLargeDist;
        distLargeDist /= nBinsLargeDist;
        normfactor = distLargeDist/rdfLargeDist;
        printf("distLargeDist = %g -- rdfLargeDist = %g -- normfactor = %g\n", distLargeDist, rdfLargeDist, normfactor);
    }

    FILE* fp = xvgropen(solvent_rdf_file, "Solvent RDF", "R (nm)", "RDF", oenv);
    fprintf(fp, "@TYPE xy\n");
    for (int i = 0; i < nbins; i++) {
        if (bNormRDF)
        {
            real r = (i+0.5)*dbin;
            fprintf(fp, "%12g %12g\n", (i+0.5)*dbin, rdf[i] * normfactor / r);
        } else {
            fprintf(fp, "%12g %12g\n", (i+0.5)*dbin, rdf[i]);
        }
    }
    gmx_ffclose(fp);

    /* Clear memory */
    for (int t = 0; t < nThreads; t++) {
        sfree(rdf_loc[t]);
    }
    sfree(rdf_loc);
    sfree(rdf);
    if (indexInEnvelope) {
        sfree(indexInEnvelope);
    }

    printf("\nComputed RDF of atoms %s inside envelope from %d frames\n", solventname, nframes);
    printf("\nWrote solvent RDF to %s\n", solvent_rdf_file);
}


static void solvent_density_scan(int nd, real dmin, real dmax, gmx::ArrayRef<gmx::RVec> x_solute_allFrames,
        int* index_solute_allFrames, int isize_solute_allFrames, int* index_solute, int nsolute, real* vdwRadii,
        int nRadii, rvec* xref, real* w_rls, int nfit, int* ifit, gmx_bool bFit, gmx_bool bCentBoundingSphere,
        int nAtomsTotal, real sigma_smooth, t_trxstatus* status, gmx_output_env_t* oenv, t_topology* top,
        const char* ndxfile, int nrec, const char* tprfile, const char* solvdensfile, PbcType pbcType) {
    if (nd < 2) {
        gmx_fatal(FARGS, "Need at least 2 distances (option -nd)\n");
    }

    gmx_envelope** envelopes = nullptr;
    snew(envelopes, nd);

    printf("\nComputing solvent density around solute, building %d envelopes...\n", nd);
    printf("\nChoose a group for the solvent\n");

    int nsolvent = 0;
    int* isolvent = nullptr;
    char* solventname = nullptr;
    get_index(&top->atoms, ndxfile, 1, &nsolvent, &isolvent, &solventname);

    real dd = (dmax - dmin) / (nd - 1);

    for (int iEnv = 0; iEnv < nd; iEnv++) {
        real d = dmin + dd * iEnv;

        printf("CREATING ENVELOPE %d of %d -- d = %g\n", iEnv, nd, d);

        envelopes[iEnv] = gmx_envelope_init(nrec, TRUE);

        gmx_envelope_buildEnvelope_omp(envelopes[iEnv], x_solute_allFrames, index_solute_allFrames,
                isize_solute_allFrames, d, sigma_smooth, FALSE, vdwRadii, nRadii);
    }

    /* Read mtop from tpr file and make list of number of electrons for all atoms */
    TpxFileHeader tpx = readTpxHeader(tprfile, true);

    rvec* top_x;
    snew(top_x, tpx.natoms);

    if (tpx.natoms != nAtomsTotal) {
        gmx_incons("Atom number mismatch in solvent_density_scan()\n");
    }

    matrix box;
    int natoms_tpr;
    gmx_mtop_t mtop;
    read_tpx(tprfile, nullptr, box, &natoms_tpr, top_x, nullptr, &mtop);

    /* Read first frame of the xtc file */
    rvec* xread = nullptr;
    snew(xread, nAtomsTotal);
    real t;
    read_next_x(oenv, status, &t, xread, box);

    double* nElectronsPerAtom = swaxs::countElectronsPerAtom(&mtop);

    int nlayers = nd - 1;
    int nThreads = gmx_omp_get_max_threads();

    double *nElecInLayer = nullptr;
    double *nElecInLayer_sqr = nullptr;
    double *nElec_loc = nullptr;
    snew(nElecInLayer, nlayers);
    snew(nElecInLayer_sqr, nlayers);
    snew(nElec_loc, nlayers * nThreads);

    rvec origin;
    clear_rvec(origin);

    int nframes = 0;
    int nWarn = 0;
    const int nWarnMax = 20;

    do {
        /**
         * First of all, get the solvent around the protein.
         * So move the protein to the center of the box and put the solvent into the compact unitcell
         */

        /* 1) get box center */
        rvec boxcenter;
        calc_box_center(ecenterTRIC, box, boxcenter);

        auto xreadRef = gmx::arrayRefFromArray(reinterpret_cast<gmx::RVec*>(xread), nAtomsTotal);

        /* 2) move protein to box center */
        mv_cog_to_rvec(nAtomsTotal, xreadRef, index_solute, nsolute, boxcenter, nullptr);

        /* 3) put all atoms into the compact unit cell */
        put_atoms_in_compact_unitcell(pbcType, ecenterTRIC, box, xreadRef);

        /* 4) Check if the protein has a reasonable distance from the box surface */
        real boxdist;
        check_prot_box_distance(xreadRef, index_solute, nsolute, box, nullptr, FALSE, &boxdist);

        if (boxdist < WAXS_WARN_BOX_DIST and nWarn < nWarnMax) {
            printf("\nWARNING, solute getting close to the box surface (%g nm). Your solute is probably not whole. Fix this with trjconv,\n"
                   "e.g. with options -pbc mol or -pbc nojump or -pbc cluster before calling g_genenv.\n", boxdist);

            nWarn++;
        }

        if (bFit) {
            rvec xbefore;
            copy_rvec(xread[0], xbefore);

            /* shift to zero (required for do_fit) */
            reset_x(nfit, ifit, nAtomsTotal, nullptr, xread, w_rls);

            rvec xdiff;
            rvec_sub(xbefore, xread[0], xdiff);

            do_fit(nAtomsTotal, w_rls, xref, xread);

            for (int j = 0; j < nAtomsTotal; j++) {
                /* shift back */
                rvec_inc(xread[j], xdiff);
            }
        }

        if (bCentBoundingSphere) {
            /* This option is deprecated - we now always shift the COG to the origin */
            mv_bounding_sphere_to_origin(xreadRef, nAtomsTotal, nsolute, index_solute);
        } else {
            mv_cog_to_rvec(nAtomsTotal, xreadRef, index_solute, nsolute, origin, nullptr);
        }

        #pragma omp parallel shared(xread, isolvent, envelopes, nElec_loc, nElectronsPerAtom, nd)
        {
            int threadID = gmx_omp_get_thread_num();
            int iEnvMax, iEnvMin, iEnvTest;

            /* Clear nElec_loc elements used in this thread */
            for (int iEnv = 0; iEnv < (nd - 1); iEnv++) {
                nElec_loc[threadID * nlayers + iEnv] = 0;
            }

            #pragma omp for
            for (int j = 0; j < nsolvent; j++) {
                /* First check if atom is really between the largest and the smallest envelope */
                if (!gmx_envelope_isInside(envelopes[nd - 1], xread[isolvent[j]])
                        or gmx_envelope_isInside(envelopes[0], xread[isolvent[j]])) {
                    continue;
                }

                iEnvMax = nd - 1;
                iEnvMin = 0;

                while (iEnvMax - iEnvMin > 1) {
                    /* Test if atom is inside the envelpe in the middle of iEnvMax and iEnvMin */
                    iEnvTest = (iEnvMax + iEnvMin) / 2;

                    if (gmx_envelope_isInside(envelopes[iEnvTest], xread[isolvent[j]])) {
                        iEnvMax = iEnvTest;
                    } else {
                        iEnvMin = iEnvTest;
                    }
                }

                /* Make sure that every thread writes into different elements */
                nElec_loc[threadID * nlayers + iEnvMin] += nElectronsPerAtom[isolvent[j]];
            }
        }

        for (int iEnv = 0; iEnv < (nd - 1); iEnv++) {
            /* Sum over the threads (reduce array) */
            double nElecReduce = 0;

            for (int iThread = 0; iThread < nThreads; iThread++) {
                nElecReduce += nElec_loc[iThread * nlayers + iEnv];
            }

            /* Keep sum and sum^2 to compute errors below */
            nElecInLayer[iEnv] += nElecReduce;
            nElecInLayer_sqr[iEnv] += gmx::square(nElecReduce);
        }

        nframes++;
    } while (read_next_x(oenv, status, &t, xread, box));

    FILE* fp = xvgropen(solvdensfile, "Solvent density", "R (nm)", "density (e/nm\\S3\\N)", oenv);

    fprintf(fp, "@TYPE xydy\n");

    for (int iEnv = 0; iEnv < (nd - 1); iEnv++) {
        nElecInLayer[iEnv] /= nframes;
        nElecInLayer_sqr[iEnv] /= nframes;

        double volInnerEnv = swaxs::envelope_volume(envelopes[iEnv]);
        double volOuterEnv = swaxs::envelope_volume(envelopes[iEnv + 1]);
        double volLayer = volOuterEnv - volInnerEnv;

        double densLayer = nElecInLayer[iEnv] / volLayer;
        double densLayerErr = sqrt(nElecInLayer_sqr[iEnv] - gmx::square(nElecInLayer[iEnv])) / sqrt(nframes) / volLayer;
        fprintf(fp, "%10g %10g %10g\n", dmin + dd * (iEnv + 0.5), densLayer, densLayerErr);
    }

    gmx_ffclose(fp);

    printf("\nComputed solvent density from %d frames\n", nframes);
    printf("\nWrote solvent density to %s\n", solvdensfile);
}

static void write_viewable_envelope(gmx_envelope* e, const char* name, rvec rgb, rvec rgb_inside, real alpha, gmx_bool bVMD) {
    char *outfn, *fn_root;
    int filelen = strlen(name);

    snew(outfn, filelen + 10);
    snew(fn_root, filelen + 10);

    strncpy(fn_root, name, filelen - 4);
    fn_root[filelen - 4] = '\0';

    if (bVMD) {
        sprintf(outfn, "%s.tcl", fn_root);
        swaxs::envelope_write_vmd_cgo(e, outfn, rgb, alpha);
    } else {
        sprintf(outfn, "%s.py", fn_root);
        swaxs::envelope_write_pymol_cgo(e, outfn, name, rgb, rgb_inside, alpha);
    }

    sfree(outfn);
    sfree(fn_root);
}

static void get_good_pbc_atom(gmx::ArrayRef<gmx::RVec> x, int nsolute, int* index, int* iPBCsolute, real* rpbc, real* Rbsphere) {
    rvec cent = { 0, 0, 0 };
    real R;
    get_bounding_sphere(x, index, nsolute, cent, &R, FALSE);

    real rmin2 = 1e20;
    int imin = -1;

    /* Get a good PBC atom (solute-internal numbering)
       Note that this solute-internal numbering may differ from the global atom number if, e.g.,
       virtual sites are present */
    for (int i = 0; i < nsolute; i++) {
        rvec diff;
        rvec_sub(x[index[i]], cent, diff);

        real r2 = norm2(diff);

        if (r2 < rmin2) {
            rmin2 = r2;
            imin = i;
        }
    }

    *iPBCsolute = imin;
    *rpbc = sqrt(rmin2);
    *Rbsphere = R;
}

static void env_writeFourier(t_spherical_map* qvecs, real* ft_re, real* ft_im, const char* ftfile,
        const char* ftabsfile, const char* intfile, gmx_output_env_t* oenv) {
    FILE* out;

    if (ftfile) {
        out = gmx_ffopen(ftfile, "w");

        fprintf(out, "#      %-10s %-12s %-12s   %-12s %-12s\n", "qx", "qy", "qz", "Re(FT)", "Im(FT)");

        for (int i = 0; i < qvecs->n; i++) {
            fprintf(out, "%12g %12g %12g   %12g %12g\n", qvecs->q[i][XX], qvecs->q[i][YY], qvecs->q[i][ZZ], ft_re[i],
                    ft_im[i]);
        }

        gmx_ffclose(out);

        printf("Wrote Fourier transform of envelope to %s\n", ftfile);
    }

    real av_re, av_im;

    if (ftabsfile) {
        out = xvgropen(ftabsfile, "FT of envelope (rotationally averaged)", "q (1/nm)", "FT ", oenv);

        for (int i = 0; i < qvecs->nabs; i++) {
            av_re = 0.;
            av_im = 0.;

            for (int j = qvecs->ind[i]; j < qvecs->ind[i + 1]; j++) {
                av_re += ft_re[j];
                av_im += ft_im[j];
            }

            av_re /= qvecs->ind[i + 1] - qvecs->ind[i];
            av_im /= qvecs->ind[i + 1] - qvecs->ind[i];

            fprintf(out, "%12g %12g %12g\n", qvecs->abs[i], av_re, av_im);
        }

        printf("Wrotes spherically averaged Fourier transform of envelope to %s\n", ftabsfile);
    }

    if (intfile) {
        out = xvgropen(intfile, "|FT|\\S2\\N (rotaionally averaged)", "q (1/nm)", "Intensity", oenv);

        for (int i = 0; i < qvecs->nabs; i++) {
            av_re = 0.;

            for (int j = qvecs->ind[i]; j < qvecs->ind[i + 1]; j++) {
                av_re += gmx::square(ft_re[j]) + gmx::square(ft_im[j]);
            }

            av_re /= qvecs->ind[i + 1] - qvecs->ind[i];
            fprintf(out, "%12g %12g\n", qvecs->abs[i], av_re);
        }

        printf("Wrotes spherically averaged scattering intensity of envelope to %s\n", intfile);
    }
}

int gmx_genenv(int argc, char* argv[]) {
    const char* desc[] = {
        "[TT]gmx genenv[tt] writes an envelope that encloses the solutes at a pre-selected",
        "distance. In the SWAXS/SANS calculations with gmx mdrun, all solvent atoms",
        "within the envelope are included when computing the SWAXS/SANS curve, thereby",
        "taking scattering contributions from the hydration layer into accout.[PAR]"
        "The envelope is constructed from an icosphere.",
        "The icosphere is constructed from an icosahedron (with 20 triangular faces)",
        "by subdividing the triangles [TT]-nrec[tt] times. Hence, the icosphere has",
        "20 x 4^nrec triangular faces. Then, each vertex is moved radially outwards",
        "until the vertex has a pre-selected distance from all atoms in all frames.[PAR]",
        "The distance of the envelope from the atoms is specified with [TT]-d[tt]. With",
        "the default value of 0.7 nm, the solvent density modulations owing to the ",
        "hydration layers are taken into account. Only for highly charged solutes with",
        "a large counter ion cloud, you should consider using a much larger envelope.[PAR]"
        "For solutes with a larger hole at the center (such as a virus capsid), the",
        "envelope can have a inner and and outer radius. For normal solutes, the",
        "inner radii are all zero, hence there is only an outer surface.",
        "",
        "Default output",
        "^^^^^^^^^^^^^^",
        "* envelope.dat - this is the main enevelope file written by gmx mdrun.",
        "  The format is: ",
        "  First line: nrec (number of recursions used to prepare the",
        "                    icosphere).",
        "  All other lines: inner radius, outer radius (inner radius ",
        "                   is typically zero, except for solutes with a hole).",
        "* envelope-ref.gro - reference coordiante file used to superimpose simulation frames onto",
        "  the envelope. The atoms in envelope-ref.gro must correspond to the group",
        "  defined with the mdp option waxs-rotfit.",
        "* envelope.py - Visualization of the envelope for PyMol. Use [TT]-vmdout[tt] to"
        "  visualize with VMD instead. The color and alpha values of the visualization",
        "  are controlled with [TT]-rgb[tt] and [TT]-alpha[tt].[PAR]",
        "Note: The files prot+solvlayer.pdb and excludedvolume.pdb written by gmx mdrun",
        "      are superimposed with envelope.py, so they can be visualized together with",
        "      PyMol.[PAR]",
        "",
        "Van der Walls radii",
        "^^^^^^^^^^^^^^^^^^^",
        "By default, the VdW radii of the atoms are ignored. With [TT]-vdw[tt], the VdW",
        "radii are added to the distance defined with [TT]-d[tt]. With [TT]-vdw ff[tt], VdW",
        "radii are taken from the force field and defined as sigma/2, where sigma",
        "is one of the Lennard-Jones parameters. With [TT]-sig56[tt], the VdW radii",
        "are instead defined as rmin/2. With [TT]-vdw db[tt], the VdW radii are",
        "taken from the database file share/gromacs/top/vdwradii.dat. With [TT]-ovdw[tt]",
        "the used VdW radii are written to the file vdwradii-used.dat.",
        "",
        "To build an envelope just on the surface of the protein, use e.g.",
        "",
        "  gmx genenv -s topol.tpr -f traj_comp.xtc -d 0 -vdw ff (or -vdw db)",
        "",
        "Solvent density around the solute",
        "^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^",
        "With [TT]-od[tt], the solvent density around the solute is computed between",
        "the distances defined with [TT]-dmin[tt] and [TT]-dmax[tt], using [TT]-nd[tt]",
        "distance bins. This will show the hydration layers around the solute.[PAR]",
        "",
        "Fitting and centering of the frames",
        "^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^",
        "By default, trajectory frames are fitted onto the first frame,",
        "and the resulting envelope is located around the origin. Option [TT]-fittpr[tt]",
        "can be used fit to the structure in the tpr file instead. This requires that ",
        "WAXSMD runs and reruns also use those coordinates as fit reference.",
        "[PAR]",
        "Spherical envelope",
        "^^^^^^^^^^^^^^^^^^",
        "A spherical enelope is created around the solute with [TT]-sphere[tt]. By "
        "adding [TT]-d_sphere[tt] the structure and trajectory are ignored and a spherical envelope with",
        "this pre-selected radius is written.[PAR]",
        "Two envelopes are merged into one envelope by reading them with [TT]-e1[tt]",
        "and [TT]-e2[tt].[PAR]",
        "",
        "Fourier transform of envelope",
        "^^^^^^^^^^^^^^^^^^^^^^^^^^^^^",
        "The Fourier transform (FT) of a unit density in the shape of the envelope can be written with [TT]-ft[tt].",
        "Then, the spherically averaged FT is written with [TT]-fta[tt]. The corresponding intensity",
        "(= absolute value squared of the FT, then spherically averaged) is written to [TT]-int[tt].",
        "For the FT, the number of q-points ([TT]-nq[tt]), maximum q ([TT]-qmax[tt]), and the number",
        "of q-directions per absolute value of q used for the spherical quadrature ([TT]-J[tt]) can",
        "be specified. For most users, these output files are not relevant.[PAR]"
        "",
        "Solvent RDF inside the envelope",
        "^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^",
        "The RDF of solvent atoms inside the hydation layer is computed with [TT]-ordf[tt].",
        "This RDF may provide information on the solvent structure inside the hydration layer. The RDF is ",
        "computed from solvent atoms within a distance of DMIN and DMAX from the solute defined with",
        "[TT]-nd 2 -dmin DMIN -dmax DMAX[tt]. Options "
        "[TT]-rdf-max[tt] and [TT]-rdf-bins[tt] define the maximum atom-atom distance and number of bins.",
        "Note that the RDF is not normalized. To compare the RDF with the RDF of bulk water, you",
        "must overlay a pure-solvent simulation with the solute, such that gmx genenv can cut out identical",
        "volumes from the hydration layer and from the bulk-solvent simulations. "
        "You typically must provide an index file with, e.g., the OW atoms of water.[PAR]",
    };

    real d = 0.7, d_sphere = -1., dmin = 0.3, dmax = 2.5;
    gmx_bool bFit = TRUE, bFitTPR = FALSE, bOrig = TRUE, bPBC = TRUE, bCentBoundingSphere = FALSE;
    gmx_bool bCGO = TRUE, bVMDout = FALSE, bSphere = FALSE, bCheckEnvInBox = FALSE;
    int nrec = 4, J = 200, nq = 50, nthreads = 1;
    real sigma_smooth = GMX_ENVELOPE_SMOOTH_SIGMA, qmax = 30., alpha = 0.5, nTry_vol_mc = 0;
    rvec rgb = { 0.0, 0.644531, 1.0 };  /* marine blue */
    rvec rgb_inside = { 1.0, 0.746094, 0.0, };  /* orange (hardcoded) */
    rvec r_elipsoid = { 0, 0, 0 };
    int seed = -1, nd_scan = 40, nfrEnvMax = -1;
    static const char *vdwtype[] = { nullptr, "none", "ff", "db", nullptr };
    static gmx_bool bSig56 = FALSE;
    real rdf_max = 1.5;
    int rdf_nbins = 750;

    t_pargs shellArguments[] = {
        { "-nt", FALSE, etINT, { &nthreads }, "Number of threads used by g_genenv" },
        { "-d", FALSE, etREAL, { &d }, "Distance to surface" },
        { "-vdw", FALSE, etENUM, { vdwtype }, "Add VdW radius of each atom to distance: none, from forcefield (ff), from database (db)" },
        { "-sig56", FALSE, etBOOL, { &bSig56 }, "Use rmin/2 (minimum in the Van der Waals potential) rather than [GRK]sigma[grk]/2 " },
        { "-nrec", FALSE, etINT, { &nrec }, "# of rerusions for icosphere (20 * 4^N faces). <=0 means automatic determination." },
        { "-sig", FALSE, etREAL, { &sigma_smooth }, "Angular sigma to smooth the envelope (rad)" },
        { "-fit", FALSE, etBOOL, { &bFit }, "Fit frames, by default to the first frame read (not to tpr/pdb)." },
        { "-fittpr", FALSE, etBOOL, { &bFitTPR }, "Fit frames to the TPR file instead." },
        { "-orig", FALSE, etBOOL, { &bOrig }, "Move COG of solute atoms to origin before building envelope" },
        { "-pbc", FALSE, etBOOL, { &bPBC }, "PBC check" },
        { "-nq", FALSE, etINT, { &nq }, "for Fourier tranform: # of abolute |q|" },
        { "-qmax", FALSE, etREAL, { &qmax }, "for Fourier tranform: maximum |q| (1/nm)" },
        { "-J", FALSE, etINT, { &J }, "for Fourier tranform: # of q vectors per |q|" },
        { "-centbs", FALSE, etBOOL, { &bCentBoundingSphere }, "Move bounding sphere to origin (instead of center of geometry)" },
        { "-sphere", FALSE, etBOOL, { &bSphere }, "Make spherical envelope" },
        { "-d_sphere", FALSE, etREAL, { &d_sphere }, "Write a sphere of this size (nm), and skip all calculations." },
        { "-elipsoid", FALSE, etRVEC, { &r_elipsoid }, "HIDDENBuild elipsoid with those half-axes." },
        { "-cgo", FALSE, etBOOL, { &bCGO }, "Also write a CGO file for visualisation." },
        { "-vmdout", FALSE, etBOOL, { &bVMDout }, "write visualisation as a VMD-tcl file instead of python" },
        { "-rgb", FALSE, etRVEC, { &rgb }, "RGB color of envelope" },
        { "-alpha", FALSE, etREAL, { &alpha }, "Transparency of envelope" },
        { "-inbox", FALSE, etBOOL, { &bCheckEnvInBox }, "Write warnings if the envelope does not fit into any simulation frame boxes" },
        { "-vol-mc", FALSE, etREAL, { &nTry_vol_mc }, "Write volume of envelope, computed by Monte Carlo." },
        { "-seed", FALSE, etINT, { &seed }, "Monte Carlo seed (-1 = generate seed)." },
        { "-dmin", FALSE, etREAL, { &dmin }, "Minimum distance for solvent density scan." },
        { "-dmax", FALSE, etREAL, { &dmax }, "Maximum distance for solvent density scan." },
        { "-nd", FALSE, etINT, { &nd_scan }, "Number of distance for solvent density scan." },
        { "-nfrEnvMax", FALSE, etINT, { &nfrEnvMax }, "Maximum number of frames used for envelope (useful with -od)." },
        { "-rdf-max", FALSE, etREAL, { &rdf_max }, "Maximum distance for solvent RDF (used with -ordf)" },
        { "-rdf-bins", FALSE, etINT, { &rdf_nbins }, "Number of bins of solvent RDF" },
    };

#define npargs asize(pa)
    t_filenm fnm[] = {
        { efTPS, "-s", nullptr, ffREAD },
        { efTRX, "-f", nullptr, ffREAD },
        { efNDX, "-n", "index", ffOPTRD },
        { efSTO, "-r", "fit", ffOPTRD },
        { efDAT, "-o", "envelope", ffWRITE },
        { efSTO, "-or", "envelope-ref", ffWRITE },
        { efPDB, "-of", "allframes", ffOPTWR },
        { efDAT, "-ovdw", "vdwradii-used", ffOPTWR },
        { efDAT, "-e1", "envelope1", ffOPTRD },
        { efDAT, "-e2", "envelope2", ffOPTRD },
        { efDAT, "-ft", "fourier", ffOPTWR },
        { efDAT, "-fta", "fourier_qabs", ffOPTWR },
        { efDAT, "-int", "intensity", ffOPTWR },
        { efXVG, "-od", "solvdens", ffOPTWR },
        { efXVG, "-ordf", "solvent-rdf", ffOPTWR },

    };

#define NFILE asize(fnm)

    gmx_output_env_t* oenv;

    if (!parse_common_args(&argc, argv, PCA_CAN_TIME, NFILE, fnm, asize(shellArguments), shellArguments, asize(desc), desc, 0, nullptr, &oenv)) {
        return 0;
    }

    const char* tps_file = ftp2fn(efTPS, NFILE, fnm);
    const char* index_file = ftp2fn_null(efNDX, NFILE, fnm);
    const char* traj_file = ftp2fn(efTRX, NFILE, fnm);
    const char* env_file = opt2fn("-o", NFILE, fnm);
    const char* out_ref_file = opt2fn("-or", NFILE, fnm);
    const char* all_fr_file = ftp2fn_null(efPDB, NFILE, fnm);
    const char* ft_file = opt2fn_null("-ft", NFILE, fnm);
    const char* ftabs_file = opt2fn_null("-fta", NFILE, fnm);
    const char* int_file = opt2fn_null("-int", NFILE, fnm);

    const char* in_env_file1 = opt2fn_null("-e1", NFILE, fnm);
    const char* in_env_file2 = opt2fn_null("-e2", NFILE, fnm);
    const char* in_ref_file = opt2fn_null("-r", NFILE, fnm);
    const char* solv_dens_file = opt2fn_null("-od", NFILE, fnm);
    const char* solvent_rdf_file = opt2fn_null("-ordf", NFILE, fnm);

    /* Check if we are just computing envelopes without using any coordinate information.
     * A premature return is needed to not use any TPR file information? */

    gmx_envelope* e1 = nullptr;
    gmx_envelope* e2 = nullptr;

    /* Combining two envelopes */

    if (in_env_file1) {
        e1 = swaxs::envelope_read_from_file(in_env_file1);
    }

    real* ft_re = nullptr;
    real* ft_im = nullptr;

    if (in_env_file1 and in_env_file2) {
        e2 = swaxs::envelope_read_from_file(in_env_file2);

        gmx_envelope_superimposeEnvelope(e1, e2);

        /* Write envelope */
        swaxs::envelope_write_to_file(e2, env_file);
        printf("Wrote envelope to %s\n", env_file);

        if (bCGO) {
            write_viewable_envelope(e2, env_file, rgb, rgb_inside, alpha, bVMDout);
        }

        if (ft_file or ftabs_file or int_file) {
            t_spherical_map* qvecs = gen_qvecs_map(0., qmax, nq, J, FALSE, nullptr, nullptr, 0, 0., FALSE);
            swaxs::envelope_unit_ft(e2, qvecs->q, qvecs->n, &ft_re, &ft_im);
            env_writeFourier(qvecs, ft_re, ft_im, ft_file, ftabs_file, int_file, oenv);
        }

        return 0;
    }

    gmx_envelope *envelope = nullptr;

    /* Hack a sphere of a specific dimension. */
    if (bSphere and d_sphere > 0) {
        if (nrec <= 0) {
            gmx_fatal(FARGS, "Automatic determination of number of recursions (-nrec) not available for spherical envelope\n");
        }

        envelope = gmx_envelope_init(nrec, TRUE);
        gmx_envelope_buildSphere(envelope, d_sphere);

        /* Write envelope */
        swaxs::envelope_write_to_file(envelope, env_file);
        printf("Wrote envelope to %s\n", env_file);

        if (bCGO) {
            write_viewable_envelope(envelope, env_file, rgb, rgb_inside, alpha, bVMDout);
        }

        if (ft_file or ftabs_file or int_file) {
            t_spherical_map* qvecs = gen_qvecs_map(0., qmax, nq, J, FALSE, nullptr, nullptr, 0, 0., FALSE);
            swaxs::envelope_unit_ft(envelope, qvecs->q, qvecs->n, &ft_re, &ft_im);
            env_writeFourier(qvecs, ft_re, ft_im, ft_file, ftabs_file, int_file, oenv);
        }

        return 0;
    }

    if (r_elipsoid[XX] > 0) {
        if (nrec < 0) {
            gmx_fatal(FARGS, "Automatic determination of number of recursions (-nrec) not available for spherical envelope\n");
        }

        envelope = gmx_envelope_init(nrec, TRUE);
        gmx_envelope_buildEllipsoid(envelope, r_elipsoid);
        swaxs::envelope_write_to_file(envelope, env_file);
        // gmx_fatal(FARGS, "gmx_envelope_buildElipsoid() mising\n");

        if (ft_file or ftabs_file or int_file) {
            t_spherical_map* qvecs = gen_qvecs_map(0., qmax, nq, J, FALSE, nullptr, nullptr, 0, 0., FALSE);
            swaxs::envelope_unit_ft(envelope, qvecs->q, qvecs->n, &ft_re, &ft_im);
            env_writeFourier(qvecs, ft_re, ft_im, ft_file, ftabs_file, int_file, oenv);
        }

        if (bCGO) {
            write_viewable_envelope(envelope, env_file, rgb, rgb_inside, alpha, bVMDout);
        }

        return 0;
    }

    PbcType pbcType;
    rvec* xtps = nullptr;
    t_atoms* atoms;
    t_topology top;
    matrix box;

    gmx_bool bTop = read_tps_conf(tps_file, &top, &pbcType, &xtps, nullptr, box, FALSE);
    atoms = &top.atoms;

    if ((bFitTPR or in_ref_file) and !bFit) {
        bFit = TRUE;
    }

    if (bFitTPR and in_ref_file) {
        gmx_fatal(FARGS, "Not possible to fit to the TPR and a given reference file at the same time!\n");
    }

    int nfit = 0;
    int* ifit = nullptr;
    rvec* xref = nullptr;
    real* w_rls = nullptr;

    if (bFit) {
        printf("\nChoose a group for the least squares fit\n");

        char* fitname;
        get_index(atoms, index_file, 1, &nfit, &ifit, &fitname);

        if (nfit < 3) {
            gmx_fatal(FARGS, "Need >= 3 points to fit!\n");
        }

        /* Read TPR coordinates instead of first-frame coordinates. */
        if (bFitTPR) {
            snew(xref, atoms->nr);

            for (int i = 0; i < atoms->nr; i++) {
                copy_rvec(xtps[i], xref[i]);
            }
        }

        /* Doing a non-weighted fit */
        snew(w_rls, atoms->nr);

        for (int i = 0; i < nfit; i++) {
            w_rls[ifit[i]] = 1.;
        }
    }

    int nsolute;
    int* index = nullptr;

    printf("\nChoose the solute group\n");
    char* solutename;
    get_index(atoms, index_file, 1, &nsolute, &index, &solutename);

    rvec* xread = nullptr;
    t_trxstatus* status;

    real t;
    int nat = read_first_x(oenv, &status, traj_file, &t, &xread, box);

    if (nat != top.atoms.nr) {
        gmx_fatal(FARGS, "\nTopology/PDB has %d atoms, whereas trajectory has %d\n", top.atoms.nr, nat);
    }

    gmx_rmpbc_t gpbc = nullptr;

    if (bPBC) {
        gpbc = gmx_rmpbc_init(&top.idef, pbcType, top.atoms.nr);
        gmx_rmpbc(gpbc, top.atoms.nr, box, xread);
    }

    if (bFit) {
        snew(xref, nat);

        for (int i = 0; i < nat; i++) {
            clear_rvec(xref[i]);
        }

        /* Read TPR coordinates instead of first-frame coordinates. */
        if (bFitTPR) {
            for (int i = 0; i < atoms->nr; i++) {
                copy_rvec(xtps[i], xref[i]);
            }
        }
        /* Take from the input reference file. */
        else if (in_ref_file) {
            read_fit_reference(in_ref_file, xref, nat, index, nsolute, ifit, nfit);
        }
        /* Take from the first frame. */
        else {
            for (int i = 0; i < nat; i++) {
                copy_rvec(xread[i], xref[i]);
            }
        }

        reset_x(nfit, ifit, nat, nullptr, xref, w_rls);
    }

    t_atoms useatoms;
    FILE* out = nullptr;

    if (all_fr_file) {
        init_t_atoms(&useatoms, atoms->nr, FALSE);
        sfree(useatoms.resinfo);
        useatoms.resinfo = atoms->resinfo;

        for (int i = 0; (i < nsolute); i++) {
            useatoms.atomname[i] = atoms->atomname[index[i]];
            useatoms.atom[i] = atoms->atom[index[i]];
            useatoms.nres = std::max(useatoms.nres, useatoms.atom[i].resind + 1);
        }

        useatoms.nr = nsolute;

        out = gmx_ffopen(all_fr_file, "w");
    }

    int frame_index = 0;

    rvec origin;
    clear_rvec(origin);

    int iPBCsolute;
    rvec* x = nullptr;
    real Rbsphere = 0;

    do {
        if (bPBC) {
            gmx_rmpbc(gpbc, top.atoms.nr, box, xread);
        }

        if (bFit) {
            rvec xbefore;
            copy_rvec(xread[0], xbefore);

            /* shift to zero (required for do_fit) */
            reset_x(nfit, ifit, nat, nullptr, xread, w_rls);

            rvec xdiff;
            rvec_sub(xbefore, xread[0], xdiff);

            do_fit(nat, w_rls, xref, xread);

            for (int i = 0; i < nat; i++) {
                /* shift back */
                rvec_inc(xread[i], xdiff);
            }
        }

        auto xreadRef = gmx::arrayRefFromArray(reinterpret_cast<gmx::RVec*>(xread), top.atoms.nr);

        if (frame_index == 0) {
            /* In the first frame, get a good PBC atom that is close to the center
               of the bounding sphere*/

            real rPBCatom;
            get_good_pbc_atom(xreadRef, nsolute, index, &iPBCsolute, &rPBCatom, &Rbsphere);

            printf("\n############ G O O D   P B C   A T O M ################################\n"
                   "N.B.: Solute atom number %d is near the center of the bounding sphere -\n"
                   "      it would make a good waxs-pbc atom (distance  = %g)\n"
                   "      Global atom number = %d (name %s, residue %s-%d)\n"
                   "#######################################################################\n\n",
                    iPBCsolute + 1, rPBCatom, index[iPBCsolute] + 1, *(atoms->atomname[index[iPBCsolute]]),
                    *(atoms->resinfo[atoms->atom[index[iPBCsolute]].resind].name),
                    atoms->resinfo[atoms->atom[index[iPBCsolute]].resind].nr);
        }

        if (bOrig) {
            if (bCentBoundingSphere) {
                /* This option is deprecated - we now always shift the COG to the origin */
                mv_bounding_sphere_to_origin(xreadRef, nat, nsolute, index);
            } else {
                mv_cog_to_rvec(nat, xreadRef, index, nsolute, origin, nullptr);
            }
        }

        /* Now store solute coordinates in a single long array */
        srenew(x, (frame_index + 1) * nsolute);

        for (int i = 0; i < nsolute; i++) {
            copy_rvec(xread[index[i]], x[frame_index * nsolute + i]);
        }

        if (all_fr_file) {
            fprintf(out, "REMARK    GENERATED BY G_GENENV\n");

            char title[256];
            sprintf(title, "frame %d", frame_index + 1);

            gmx_conect gc = nullptr;
            write_pdbfile(out, title, &useatoms, x + frame_index * nsolute, pbcType, box, ' ', frame_index + 1, gc);
        }

        frame_index++;
    }

    while (read_next_x(oenv, status, &t, xread, box) and (nfrEnvMax < 0 or frame_index < nfrEnvMax));

    int nframes = frame_index;

    if (out) {
        gmx_ffclose(out);
    }

    printf("Read %d frames from %s\n", frame_index, traj_file);

    int* index_all = nullptr;

    snew(index_all, nframes * nsolute);

    for (int i = 0; i < nframes * nsolute; i++) {
        index_all[i] = i;
    }

    /** Automatic determination of number of recursions */
    if (nrec <= 0) {
        nrec = WAXS_ENVELOPE_NREC_MIN;

        while (TRUE) {
            /* get length of side of face triangle */
            real area1 = 4 * M_PI * Rbsphere * Rbsphere / (20 * pow(4.0, nrec));
            real l1 = sqrt(4 * area1 / sqrt(3.0));

            if (l1 < d / 2) {
                printf("\nAutomatic determination of number of recursions of icosphere:\n"
                       "\tR bounding sphere = %g\n"
                       "\tnrec              = %d\n"
                       "\tnumber of faces   = %d\n"
                       "\tface side length  = %g\n\n",
                        Rbsphere, nrec, (int)(20 * pow(4, nrec)), l1);
                break;
            }

            nrec++;
        }
    }

    real* vdwRadii = nullptr;
    int nRadii = 0;

    if (vdwtype[0][0] != 'n') {
        snew(vdwRadii, nsolute);
        nRadii = nsolute;

        switch (vdwtype[0][0]) {
            case 'f': {
                /**
                 * vdw radii from force field:
                 *   with bSig56 == FALSE: vdwradius = 0.5 * sigma (default)
                 *   with bSig56 == TRUE:  vdwradius = 0.5 * rmin
                 */
                if (fn2ftp(tps_file) != efTPR) {
                    gmx_fatal(FARGS, "Option -vdw ff takes Van-der-Waals radii from the Lennard-Jones parameters, hence it requires a TPR file for the -s option.");
                }

                int ntype = top.idef.atnr;

                for (int i = 0; i < nsolute; i++) {
                    int itype = top.atoms.atom[index[i]].type;
                    real c12 = top.idef.iparams[itype * ntype + itype].lj.c12;
                    real c6 = top.idef.iparams[itype * ntype + itype].lj.c6;

                    if ((c6 != 0) and (c12 != 0)) {
                        real sig6;

                        if (bSig56) {
                            sig6 = 2 * c12 / c6;
                        } else {
                            sig6 = c12 / c6;
                        }

                        vdwRadii[i] = 0.5 * gmx::sixthroot(sig6);
                    } else {
                        vdwRadii[i] = 0;
                    }
                }

                break;
            }
            case 'd': {
                /** vdw radii from database in shared/top/vdwradii.dat */

                AtomProperties aps;

                if (fn2ftp(traj_file) == efPDB) {
                    get_pdb_atomnumber(&top.atoms, &aps);
                }

                for (int i = 0; i < nsolute; i++) {
                    real vdwradius;
                    aps.setAtomProperty(epropVDW, *top.atoms.resinfo[top.atoms.atom[index[i]].resind].name,
                            *top.atoms.atomname[index[i]], &vdwradius);
                    vdwRadii[i] = vdwradius;
                }

                break;
            }
            default: {
                gmx_incons("Invalid value found in vdwtype[0][0]");
                break;
            }
        }

        if (opt2bSet("-ovdw", NFILE, fnm)) {
            FILE* fp = gmx_ffopen(opt2fn("-ovdw", NFILE, fnm), "w");

            for (int i = 0; i < nsolute; i++) {
                fprintf(fp, "%8d  %-4s %8g\n", index[i] + 1, *top.atoms.atomname[index[i]], vdwRadii[i]);
            }

            gmx_ffclose(fp);
            printf("Wrote used Van der Waals radii to %s\n", opt2fn("-ovdw", NFILE, fnm));
        }
    }

    /* Set # of OpenMP threads */
    gmx_omp_set_num_threads(nthreads);
    printf("\nNote: Will use %d OpenMP threads.\n", gmx_omp_get_max_threads());

    /* Finally, build the envelope - make sure all the output is file, so we can grep it */
    fflush(stdout);
    fflush(stderr);

    envelope = gmx_envelope_init(nrec, TRUE);

    auto xRef = gmx::arrayRefFromArray(reinterpret_cast<gmx::RVec*>(x), nsolute);

    gmx_envelope_buildEnvelope_omp(envelope, xRef, index_all, nframes * nsolute, d, sigma_smooth, bSphere, vdwRadii, nRadii);

    fflush(stdout);
    fflush(stderr);

    /* Superimpose additional envelope if loaded */
    if (in_env_file1) {
        printf("\nSuper-imposing %s onto constructed envelope.\n", in_env_file1);
        gmx_envelope_superimposeEnvelope(e1, envelope);
    }

    /* Writing envelope to pymol cgo file */
    if (bCGO) {
        write_viewable_envelope(envelope, env_file, rgb, rgb_inside, alpha, bVMDout);
    }

    /* Write envelope */
    swaxs::envelope_write_to_file(envelope, env_file);
    printf("Wrote envelope to %s\n", env_file);

    /* Write reference coordinates */
    char reftitle[256];
    sprintf(reftitle, "Reference coordinates for envelope file %s", env_file);
    write_sto_conf_indexed(out_ref_file, reftitle, atoms, xref, nullptr, pbcType, box, nfit, ifit);

    /* Do Fourier transform */
    if (ft_file or ftabs_file or int_file) {
        t_spherical_map* qvecs = gen_qvecs_map(0., qmax, nq, J, FALSE, nullptr, nullptr, 0, 0., FALSE);
        swaxs::envelope_unit_ft(envelope, qvecs->q, qvecs->n, &ft_re, &ft_im);
        env_writeFourier(qvecs, ft_re, ft_im, ft_file, ftabs_file, int_file, oenv);
    }

    /* For each frame in xtc, check if the enelope fits into the compact box */
    if (bCheckEnvInBox) {
        matrix Rinv = { { 1, 0, 0 }, { 0, 1, 0 }, { 0, 0, 1 } };

        rewind_trj(status);
        real t;
        read_next_x(oenv, status, &t, xread, box);

        t_pbc pbc;

        int nFrOutside = 0;

        do {
            if (gpbc) {
                set_pbc(&pbc, pbcType, box);
            }

            /* Find the position of the envelope center in the box */
            rvec boxcenter, env_cent;
            calc_box_center(ecenterTRIC, box, boxcenter);

            if (TRICLINIC(box)) {
                /* Triclinic box: we put the center of the bounding sphere to the center of the box */
                real env_R2;
                gmx_envelope_bounding_sphere(envelope, env_cent, &env_R2);
            } else {
                /* Cuboid box: we put the center in x/y/z to the center of the box */
                gmx_envelope_center_xyz(envelope, Rinv, env_cent);
            }

            rvec protRefInBox;
            rvec_sub(boxcenter, env_cent, protRefInBox);

            /* Check if in box, and write pymol cgo of vertices outside for the first frame */
            if (!gmx_envelope_bInsideCompactBox(envelope, Rinv, box, protRefInBox, &pbc, nFrOutside == 0, 0.1)) {
                printf("WARNING, xtc time %10g : constructed envelope does not fit into the compact box (with tolerance 0.1nm)\n", t);
                nFrOutside++;
            }
        } while (read_next_x(oenv, status, &t, xread, box));

        printf("\nNumber of frames for which the envelope does not fit into the compact box: %d of %d\n", nFrOutside, nframes);
    }

    if (nTry_vol_mc > 0) {
        swaxs::envelope_calculate_volume(envelope, nTry_vol_mc, seed);
    }

    if (solv_dens_file) {
        if (!bTop) {
            gmx_fatal(FARGS, "Need a tpr file for computing the solvent density (option -od).\n");
        }

        rewind_trj(status);

        solvent_density_scan(nd_scan, dmin, dmax, xRef, index_all, nframes * nsolute, index, nsolute, vdwRadii, nRadii,
                xref, w_rls, nfit, ifit, bFit, bCentBoundingSphere, nat, sigma_smooth, status, oenv, &top, index_file,
                nrec, ftp2fn(efTPS, NFILE, fnm), solv_dens_file, pbcType);
    }

    if (solvent_rdf_file) {
        rewind_trj(status);
        solvent_rdf_inside_envelope(nd_scan, dmin, dmax, xRef, index_all, nframes * nsolute, index, nsolute, vdwRadii, nRadii,
                                    xref, w_rls, nfit, ifit,  bFit, bCentBoundingSphere,
                                    nat, sigma_smooth, status, oenv, &top,
                                    index_file, nrec, solvent_rdf_file, pbcType,
                                    rdf_max, rdf_nbins);
    }

    return 0;
}
