#include <cctype>  // toupper, tolower, isalpha
#include <cmath>  // fabs, round
#include <cstdlib>  // exit
#include <cstring>  // strcpy, strlen, strcpy
#include <ctime>  // time_t

#include <gromacs/gmxana/gmx_ana.h>  // Required to register this file as a CLI tool

#include <gromacs/commandline/filenm.h>  // ftp2fn, fn2ftp, opt2bSet, opt2fn
#include <gromacs/commandline/pargs.h>  // etBOOL, etINT, etREAL, parse_common_args, t_pargs, PCA_CAN_VIEW
#include <gromacs/fileio/confio.h>  // read_tps_conf, t_inputrec
#include <gromacs/fileio/oenv.h>  // gmx_output_env_t
#include <gromacs/fileio/tpxio.h>  // read_tps_conf, read_tpx_state
#include <gromacs/mdtypes/inputrec.h>  // t_inputrec
#include <gromacs/mdtypes/state.h>  // t_state
#include <gromacs/pbcutil/pbc.h>  // PbcType
#include <gromacs/topology/ifunc.h>  // interaction_function, IS_CHEMBOND
#include <gromacs/topology/index.h>  // get_index
#include <gromacs/topology/topology.h>  // t_atoms, gmx_mtop_global_atoms
#include <gromacs/utility/arraysize.h>  // asize
#include <gromacs/utility/cstringutil.h>  // gmx_strcasecmp, gmx_strcasecmp, STRLEN
#include <gromacs/utility/fatalerror.h>  // FARGS, gmx_fatal
#include <gromacs/utility/futil.h>  // gmx_ffopen, gmx_ffclose
#include <gromacs/utility/smalloc.h>  // snew, srenew, sfree
#include <gromacs/utility/sysinfo.h>  // gmx_ctime_r

#include <waxs/types/waxsrec.h>  // t_waxsrec

typedef char charElem[20];

typedef struct {
    real maxmassdiff;
    gmx_bool bGromos;
    gmx_bool bVsites;
    gmx_bool bChargeModifiedCM;
} t_genscatt_opt;

const struct {
    char el[3];
    real m;
} elemMass[] = { { "H", 1.0079 }, { "He", 4.0026 }, { "Li", 6.941 }, { "Be", 9.0122 }, { "B", 10.811 },
    { "C", 12.0107 }, { "N", 14.0067 }, { "O", 15.9994 }, { "F", 18.9984 }, { "Ne", 20.1797 }, { "Na", 22.9897 },
    { "Mg", 24.305 }, { "Al", 26.9815 }, { "Si", 28.0855 }, { "P", 30.9738 }, { "S", 32.065 }, { "Cl", 35.453 },
    { "Ar", 39.948 }, { "K", 39.0983 }, { "Ca", 40.078 }, { "Sc", 44.9559 }, { "Ti", 47.867 }, { "V", 50.9415 },
    { "Cr", 51.9961 }, { "Mn", 54.938 }, { "Fe", 55.845 }, { "Co", 58.9332 }, { "Ni", 58.6934 }, { "Cu", 63.546 },
    { "Zn", 65.39 }, { "Ga", 69.723 }, { "Ge", 72.64 }, { "As", 74.9216 }, { "Se", 78.96 }, { "Br", 79.904 },
    { "Kr", 83.8 }, { "Rb", 85.4678 }, { "Sr", 87.62 }, { "Y", 88.9059 }, { "Zr", 91.224 }, { "Nb", 92.9064 },
    { "Mo", 95.94 }, { "Tc", 98 }, { "Ru", 101.07 }, { "Rh", 102.906 }, { "Pd", 106.42 }, { "Ag", 107.868 },
    { "Cd", 112.411 }, { "In", 114.818 }, { "Sn", 118.71 }, { "Sb", 121.76 }, { "Te", 127.6 }, { "I", 126.904 },
    { "Xe", 131.293 }, { "Cs", 132.905 }, { "Ba", 137.327 }, { "La", 138.905 }, { "Ce", 140.116 }, { "Pr", 140.908 },
    { "Nd", 144.24 }, { "Pm", 145 }, { "Sm", 150.36 }, { "Eu", 151.964 }, { "Gd", 157.25 }, { "Tb", 158.925 },
    { "Dy", 162.5 }, { "Ho", 164.93 }, { "Er", 167.259 }, { "Tm", 168.934 }, { "Yb", 173.04 }, { "Lu", 174.967 },
    { "Hf", 178.49 }, { "Ta", 180.948 }, { "W", 183.84 }, { "Re", 186.207 }, { "Os", 190.23 }, { "Ir", 192.217 },
    { "Pt", 195.078 }, { "Au", 196.966 }, { "Hg", 200.59 }, { "Tl", 204.383 }, { "Pb", 207.2 }, { "Bi", 208.98 },
    { "Po", 209 }, { "At", 210 }, { "Rn", 222 }, { "Fr", 223 }, { "Ra", 226 }, { "Ac", 227 }, { "Th", 232.038 },
    { "Pa", 231.036 }, { "U", 238.029 }, { "Np", 237 }, { "Pu", 244 }, { "Am", 243 }, { "Cm", 247 }, { "Bk", 247 },
    { "Cf", 251 }, { "Es", 252 }, { "Fm", 257 }, { "Md", 258 }, { "No", 259 }, { "Lr", 262 }, { "Rf", 261 },
    { "Db", 262 }, { "Sg", 266 }, { "Bh", 264 }, { "Hs", 277 }, { "Mt", 268 } };

const char* protResNames[] = { "ABU", "ACE", "AIB", "ALA", "ARG", "ARGN", "ASN", "ASN1", "ASP", "ASP1", "ASPH", "ASH",
    "CT3", "CYS", "CYS1", "CYS2", "CYSH", "DALA", "GLN", "GLU", "GLUH", "GLH", "GLY", "HIS", "HIS1", "HISA", "HISB",
    "HISH", "HISD", "HISE", "HISP", "HSD", "HSE", "HSP", "HYP", "ILE", "LEU", "LSN", "LYS", "LYSH", "MELEU", "MET",
    "MEVAL", "NAC", "NME", "NHE", "NH2", "PHE", "PHEH", "PHEU", "PHL", "PRO", "SER", "THR", "TRP", "TRPH", "TRPU",
    "TYR", "TYRH", "TYRU", "VAL", "PGLU", "HID", "HIE", "HIP", "LYP", "LYN", "CYN", "CYM", "CYX", "DAB", "ORN", "HYP",
    "NALA", "NGLY", "NSER", "NTHR", "NLEU", "NILE", "NVAL", "NASN", "NGLN", "NARG", "NHID", "NHIE", "NHIP", "NHISD",
    "NHISE", "NHISH", "NTRP", "NPHE", "NTYR", "NGLU", "NASP", "NLYS", "NORN", "NDAB", "NLYSN", "NPRO", "NHYP", "NCYS",
    "NCYS2", "NMET", "NASPH", "NGLUH", "CALA", "CGLY", "CSER", "CTHR", "CLEU", "CILE", "CVAL", "CASN", "CGLN", "CARG",
    "CHID", "CHIE", "CHIP", "CHISD", "CHISE", "CHISH", "CTRP", "CPHE", "CTYR", "CGLU", "CASP", "CLYS", "CORN", "CDAB",
    "CLYSN", "CPRO", "CHYP", "CCYS", "CCYS2", "CMET", "CASPH", "CGLUH" };

const char* dnaResnames[] = { "DA5", "DA", "DA3", "DAN", "DT5", "DT", "DT3", "DTN", "DG5", "DG", "DG3", "DGN", "DC5", "DC", "DC3", "DCN" };
const char* rnaResnames[] = { "RA5", "RA", "RA3", "RAN", "RU5", "RU", "RU3", "RUN", "RG5", "RG", "RG3", "RGN", "RC5", "RC", "RC3", "RCN", "A", "G", "U", "C" };

/* Check whether a string is in a list of strings */
static gmx_bool isInList(const char* string, const char* list[], int size) {
    for (int i = 0; i < size; i++) {
        if (!gmx_strcasecmp(list[i], string)) {
            return TRUE;
        }
    }

    return FALSE;
}

static gmx_bool isBackboneHydrogen(int iHyd, int iHeavy, t_atoms* atoms) {
    gmx_bool bProtein = FALSE;

    for (int i = 0; i < asize(protResNames); i++) {
        if (!gmx_strcasecmp(protResNames[i], *(atoms->resinfo[atoms->atom[iHyd].resind].name))) {
            bProtein = TRUE;
            break;
        }
    }

    gmx_bool bN = (!gmx_strcasecmp(*(atoms->atomname[iHeavy]), "N"));
    gmx_bool bH = (!gmx_strcasecmp(*(atoms->atomname[iHyd]), "H") or !gmx_strcasecmp(*(atoms->atomname[iHyd]), "HN"));

    return (bProtein and bN and bH);
}


static gmx_bool isPolarizingHydrogen(const char* boundToElem) {
    const char* elemIsPolarizingHydrogens[] = { "O", "N", "S", "P" };

    for (int i = 0; i < asize(elemIsPolarizingHydrogens); i++) {
        if (!gmx_strcasecmp(elemIsPolarizingHydrogens[i], boundToElem)) {
            return TRUE;
        }
    }

    return FALSE;
}

/* Check if non-protonated Glu or Asp */
static gmx_bool isCarboxylateOminus(const char* atomName, const char* resName) {
    const char* gluResnames[] = { "GLU", "NGLU", "CGLU" };
    const char* aspResnames[] = { "ASP", "NASP", "CASP" };

    if (isInList(resName, gluResnames, asize(gluResnames))) {
        if (!strcmp(atomName, "OE1") or !strcmp(atomName, "OE2")) {
            return TRUE;
        }
    }

    if (isInList(resName, aspResnames, asize(aspResnames))) {
        if (!strcmp(atomName, "OD1") or !strcmp(atomName, "OD2")) {
            return TRUE;
        }
    }

    return FALSE;
}

/* Check if protonated Lys */
static gmx_bool isLysineNplus(const char* atomName, const char* resName) {
    const char* lysResnames[] = { "LYS", "NLYS", "CLYS" };

    if (isInList(resName, lysResnames, asize(lysResnames))) {
        if (!strcmp(atomName, "NZ")) {
            return TRUE;
        }
    }

    return FALSE;
}

/* Check if protonated Arg */
static gmx_bool isArginineCplus(const char* atomName, const char* resName) {
    const char* argResnames[] = { "ARG", "NARG", "CARG" };

    if (isInList(resName, argResnames, asize(argResnames))) {
        if (!strcmp(atomName, "CZ")) {
            return TRUE;
        }
    }

    return FALSE;
}

/* Check phophorous of DNA or RNA backbone */
static gmx_bool isNucleicAcidPhosphate(const char* atomName, const char* resName) {
    const char* dnaResnames[] = { "DA5", "DA", "DA3", "DAN", "DT5", "DT", "DT3", "DTN", "DG5", "DG", "DG3", "DGN", "DC5", "DC", "DC3", "DCN" };
    const char* rnaResnames[] = { "RA5", "RA", "RA3", "RAN", "RU5", "RU", "RU3", "RUN", "RG5", "RG", "RG3", "RGN", "RC5", "RC", "RC3", "RCN", "A", "G", "U", "C" };

    if ( (!strcmp(atomName, "P")) && (isInList(resName, dnaResnames, asize(dnaResnames))
                                   || isInList(resName, rnaResnames, asize(rnaResnames))) ) {
        return TRUE;
    }

    return FALSE;
}

static gmx_bool isCTerminusCarboxylateOxygen(const char* atomName) {
    const char* CTermiinusOxygenNames[] = { "OC1", "OC2", "OT1", "OT2" "O1", "O2" };

    return isInList(atomName, CTermiinusOxygenNames, asize(CTermiinusOxygenNames));
}

/**
 * Given a one- or two-character string, such as O, HE, or so,
 * return the element such as O, He, or so, and the mass.
 */
static int search_elements(const char* atom, real* mReturn, char* elem) {
    char el[3];
    strcpy(el, atom);

    el[0] = toupper(el[0]);

    if (strlen(el) == 2) {
        el[1] = tolower(el[1]);
    }

    int size = asize(elemMass);

    for (int i = 0; i < size; i++) {
        // printf("Compare .%s. with .%s.\n",el, elemMass[i].el);

        if (!strcmp(el, elemMass[i].el)) {
            strcpy(elem, elemMass[i].el);
            *mReturn = elemMass[i].m;

            return 0;
        }
    }

    return 1;
}


#define ELEMENT_ALLOW_REL_MASS_DIFF 0.05

static int get_elem(const char* atomName, const char* resName, const real m, char* elemStr, int* maxWarn,
        t_genscatt_opt* opt, char* comment) {
    /* For now, avoid compiler warning */
    *maxWarn += 0;

    if (comment) {
        /* empty comment */
        comment[0] = '\0';
    }

    if (m < 0.0) {
        gmx_fatal(FARGS, "Atom %s has mass < zero.\nThis is not implemented yet\n", atomName);
    }

    if (strlen(atomName) > 5) {
        gmx_fatal(FARGS, "Atom name %s too long\n", atomName);
    }

    /** Remove numbers and make fist and second char upper and lower-case, respectively */

    char nm[6];
    char* ptr;
    strcpy(nm, atomName);
    ptr = &nm[0];

    while (!isalpha(ptr[0])) {
        ptr++;
    }

    if (strlen(ptr) < 1) {
        gmx_fatal(FARGS, "Atom name %s is invalid. Maybe only numbers\n", atomName);
    }

    ptr[0] = toupper(ptr[0]);

    if (isalpha(ptr[1])) {
        ptr[1] = tolower(ptr[1]);
        ptr[2] = '\0';
    } else {
        ptr[1] = '\0';
    }

    if (m == 0.0) {
        /* If no mass is given:
           if (atom name == residue name) -> assume two letter element, otherwise use the first letter of the atom name */
        if (resName == nullptr) {
            gmx_fatal(FARGS, "Inconsistency in get_elem()\n");
        }

        /* Check for water */
        if (!strcmp(resName, "HOH") or !strcmp(resName, "SOL")) {
            if (ptr[0] == 'O') {
                strcpy(elemStr, "Owat");

                return 0;
            } else if (ptr[0] == 'H') {
                strcpy(elemStr, "Hwat");

                return 0;
            }
        }

        char capitalizedResName[6];
        strcpy(capitalizedResName, resName);

        capitalizedResName[0] = toupper(capitalizedResName[0]);

        for (int i = 1; isalpha(resName[i]); i++) {
            capitalizedResName[i] = tolower(capitalizedResName[i]);
        }

        // printf ("comparing '%s' to '%s'\n", ptr, capitalizedResName);

        if (!strcmp(ptr, capitalizedResName)) {
            strcpy(elemStr, ptr);
        } else {
            elemStr[0] = ptr[0];
            // strncpy(elemStr, ptr, 1); Causes a warning.
            elemStr[1] = '\0';
        }

        // printf("Returning %s for atom %s (resName %s)\n", elemStr, atomName, resName);

        return 0;
    }


    /** Check one-character elements */

    char tmp[2];
    strncpy(tmp, ptr, 1);
    tmp[1] = '\0';

    gmx_bool bOne = FALSE;
    gmx_bool bTwo = FALSE;

    real mass = 0.0;
    char elem[3];

    if (search_elements(tmp, &mass, &elem[0]) == 0) {
        // fprintf(stderr,"Debug: mass comparisons. elem %s mass: %f, tmp %s m: %f.\n", elem, mass, tmp, m);
        real relMassDiff = fabs((mass - m) / m);
        real massdiff = fabs(mass - m);

        /** Strict test first */

        if (relMassDiff < ELEMENT_ALLOW_REL_MASS_DIFF) {
            // printf("Atom %s, mass %g: Found matching element: %s, mass %g\n",atomName, m, elem, mass);
            bOne = TRUE;
            strcpy(elemStr, elem);
        } else {
            /**
             * Catch for virtual sites C and N. Takes advantage of the fact that all 2-char elements have much larger masses.
             * Split into integral part and percentage deviation.
             */
            int whole = round((m - mass) / 1.008);  /* Needs to be 1, 2, or 3 for vsite */

            // gmx_bool bMaybeVsite = (0 < whole and whole < 4);
            gmx_bool bCarbon = (toupper(elem[0]) == 'C');
            gmx_bool bNitrogen = (toupper(elem[0]) == 'N');

            // gmx_bool bHydrogen = (toupper(elem[0]) == 'H');
            // gmx_bool bOxygen = (toupper(elem[0]) == 'O');
            // gmx_bool bSulfur = (toupper(elem[0]) == 'S');

            // real frac = fabs(m - mass - 1.008 * whole) / 1.008;
            // fprintf(stderr, "frac: %f. whole: %i.\n", frac, whole);

            if (opt->bGromos and bCarbon and (massdiff < 3.5)) {
                sprintf(elemStr, "CH%d", whole);
                bOne = TRUE;

                if (comment) {
                    sprintf(comment, "Gromos united atom");
                }
            } else if (opt->bVsites and (massdiff < 3.5) and (bCarbon or bNitrogen)) {
                bOne = TRUE;
                strcpy(elemStr, elem);

                if (comment) {
                    sprintf(comment, "mass difference of %g au due to v-sites", massdiff);
                }

                // if (*maxWarn > 0) {
                //     fprintf(stderr, "NB: Atom %-3s (mass %g) could be element %s - allowed for v-sites only, so please"
                //             " check the assignments!\n", atomName, m, elem);
                //     *maxWarn = *maxWarn - 1;
                // }
            } else if (massdiff < opt->maxmassdiff) {
                bOne = TRUE;
                strcpy(elemStr, elem);

                if (comment) {
                    sprintf(comment, "large mass difference of %g au - correct?", massdiff);
                }

                fprintf(stderr, "nAtom %-3s (mass %7g) could be element %s - "
                        "but better check the assignment (option -el)\n",
                        atomName, m, elem);
            }
        }
    }
    // else {
    //    fprintf(stderr, "Debug: search_element has failed on single chars. Proceeding...\n");
    // }

    // fprintf(stderr,"Debug: single-character comparison results: ptr: %s, tmp: %s, elemStr: %s.\n", ptr, tmp, elemStr);

    /** Check two-character elements */

    if (strlen(ptr) == 2) {
        if (search_elements(ptr, &mass, &elem[0]) == 0) {
            real relMassDiff = fabs((mass - m) / m);

            if (relMassDiff < ELEMENT_ALLOW_REL_MASS_DIFF) {
                if (bOne) {
                    gmx_fatal(FARGS,"For atom %s (mass %g), both a one-letter and two-letter "
                              "chemical element seems to match\n", atomName, m);
                }

                // printf("Atom %s: Found matching element: %s, mass %g\n", atomName, elem, mass);

                bTwo = TRUE;
                strcpy(elemStr, elem);
            }
        }
    }

    if (bOne or bTwo) {
        return 0;
    } else {
        // gmx_fatal(FARGS, "Could not identify chemical element of atom %s (mass %g)\n", atomName, m);
        strcpy(elemStr, "UNKNOWN");

        return 1;
    }
}

/**
 * Check for oxygen of COO(-) group in GLU or ASP or C-Terminus, NH3 of Lys or N-Terminus,
 * C of guanidinium moiety of Arg, or phosphorus of nucleic acids.
 * These are defined in $GMXDATA/top/cromer-mann-defs.itp
 */
static void ionicResiduesOverwriteElemstr(t_atoms* atoms, int iatom, char* elemStr, char* comment) {

    int natoms_mol = atoms->nr;
    const char* atomName = *(atoms->atomname[iatom]);

    int resid       = atoms->resinfo[atoms->atom[iatom       ].resind].nr;
    int resid_first = atoms->resinfo[atoms->atom[0           ].resind].nr;
    int resid_last  = atoms->resinfo[atoms->atom[natoms_mol-1].resind].nr;

    char* resName = *(atoms->resinfo[atoms->atom[iatom].resind].name);

    if (isCarboxylateOminus(atomName, resName)) {
        strcpy(elemStr, "COO_O");
        if (comment) {
            sprintf(comment, "Anionic carboxylate oxygen, # of electrons reduced by 0.5");
        }
    } else if (isLysineNplus(atomName, resName)) {
        strcpy(elemStr, "NHHH_N");
        if (comment) {
            sprintf(comment, "Amine nitrogen of Lysine, # of electrons increased by +1");
        }
    } else if (isArginineCplus(atomName, resName)) {
        strcpy(elemStr, "Arg_C");
        if (comment) {
           sprintf(comment, "Carbon of Arginine guanidinium moiety, # of electrons increased by +1");
        }
    } else if (isNucleicAcidPhosphate(atomName, resName)) {
        strcpy(elemStr, "Nucleic_P");
        if (comment) {
            sprintf(comment, "Phosphorus of DNA/RNA backbone, # of electrons reduced by 1");
        }
    } else if (resid == resid_first && !strcmp(atomName, "N")) {
        strcpy(elemStr, "NHHH_N");
        if (comment) {
            sprintf(comment, "Amine nitrogen of N-terminus, # of electrons increased by +1");
        }
    } else if (resid == resid_last && isCTerminusCarboxylateOxygen(atomName)) {
        strcpy(elemStr, "COO_O");
        if (comment) {
            sprintf(comment, "Carboxylate O of C-terminus, # of electrons reduced by 0.5");
        }
    }
}

static void write_dummy_top(const char* outFileName, const char* inFileName, t_topology* top, int* maxWarn,
        t_genscatt_opt* opt, gmx_bool bNSL) {
    char defineName[STRLEN];

    /** Choose wheather we write neutron scattering lengths or Cromer-mann parameters */
    sprintf(defineName, "%s", bNSL ? "NEUTRON_SCATT_LEN_" : "CROMER_MANN_");

    // int ft = bNSL ? 2 : 1;

    FILE* file = gmx_ffopen(outFileName, "w");

    fprintf(file, "; This is a trivial topology generated by gmx genscatt from file %s\n;\n"
            "; This topology is intended to be used with mdrun -rerun to compute the scattering\n"
            "; intensity I(q) of a group of atoms (such as a CH3 group or similar)\n\n", inFileName);
    fprintf(file, "[ defaults ]\n1       2        yes      0.5     0.8333\n\n");
    fprintf(file, "#include \"cromer-mann-defs.itp\"\n\n");
    fprintf(file, "[ atomtypes ]\n");

    charElem elemStr;
    charElem *elemHave = nullptr;

    int ntypes = 0;

    for (int i = 0; i < top->atoms.nr; i++) {
        char* resName = *(top->atoms.resinfo[top->atoms.atom[i].resind].name);
        get_elem(*(top->atoms.atomname[i]), resName, 0.0, &elemStr[0], maxWarn, opt, nullptr);

        gmx_bool bNew = TRUE;

        for (int j = 0; j < ntypes; j++) {
            if (!strcmp(elemStr, elemHave[j])) {
                bNew = FALSE;
            }
        }

        if (bNew) {
            fprintf(file, "dummy_%-4s  %d   %5.2f  0.000  A  0.00000e+00  0.00000e+00\n", elemStr, 1, top->atoms.atom[i].m);
            ntypes++;
            srenew(elemHave, ntypes);
            strcpy(elemHave[ntypes - 1], elemStr);
        }
    }

    fprintf(file,"\n[ moleculetype ]\nMolecule 2\n\n[ atoms ]\n");
    fprintf(file, ";   nr     type         resnr residue  atom   cgnr     charge       mass\n");

    for (int i = 0; i < top->atoms.nr; i++) {
        char* resName = *(top->atoms.resinfo[top->atoms.atom[i].resind].name);
        get_elem(*(top->atoms.atomname[i]), resName, 0.0, &elemStr[0], maxWarn, opt, nullptr);

        fprintf(file,"   %3d     dummy_%-4s       1     %3s       %4s       %d  0.0  1.0\n",
                i + 1, elemStr, resName, *(top->atoms.atomname[i]), i);
    }

    fprintf(file, "\n[ scattering_params ]\n");

    if (!bNSL) {
        fprintf(file, "; atom ft a1     a2      a3      a4      b1      b2      b3      b4      c\n");
    } else {
        fprintf(file, "; atom ft NSL(Coh b)\n");
    }

    for (int i = 0; i < top->atoms.nr; i++) {
        char* resName = *(top->atoms.resinfo[top->atoms.atom[i].resind].name);
        get_elem(*(top->atoms.atomname[i]), resName, 0.0, &elemStr[0], maxWarn, opt, nullptr);

        fprintf(file, "%5d  1  %s%s\n", i + 1, defineName, elemStr);
    }

    fprintf(file, "\n[ system ]\nA trivial topology to compute scattering intensities with mdrun -rerun\n");
    fprintf(file, "\n[ molecules ]\nMolecule       1\n");

    gmx_ffclose(file);
    fprintf(stderr, "\nWrote %s\n\n", outFileName);

    sfree(elemHave);
}

int gmx_genscatt(int argc, char* argv[]) {
    const char* desc[] = { "[TT]gmx genscatt[tt] produces include files (itp) for a topology containing ",
        "a list of atom numbers and definitions for X-ray or neutron scattering, ",
        "that is, definitions for Cromer-Mann parameters and Neutron Scattering Lengths ([TT]-nsl[tt]). ",
        "The tool will write one itp file per molecule type found in the selected index group. "
        "For instance, if your protein contains two chains A and B, and these are defined in "
        "separate [TT][ moleculetype ][tt] blocks, then gmx genscatt would write two itp files.[PAR]",
        "Like position restraint definitions, scattering-types are defined within molecules, ",
        "and therefore should be #included within the correct [TT][ moleculetype ][tt] ",
        "block in the topology. For instance, you can place it near the #include \"posre.itp\" statement ",
        "of the moleculetype definition.[PAR]",
        "Make sure to select a group that contains all physcial atoms of your solute, such as the protein, "
        "DNA/RNA, ligands, coordinated ions, heme groups etc. Maybe you will have to generate an index "
        "file first. Alternatively, you could also run gmx genscatt once for each scattering molecule.[PAR]"
        "In case you use virtual sites, make sure to select a group ",
        "that contains only physical atoms, such as \"Prot-Masses\".[PAR]",
        "Because the chemical element is not stored in a tpr file, the tool guesses ",
        "the element using a combination of the atom name and the atom mass. Consequently, "
        "if the atomic masses deviate from the physical masses, either due to a united-atom force field, ",
        "or because you model hydrogen atoms as virtual sites, you must help gmx genscatt with the options ",
        "[TT]-gromos[tt] or [TT]-vsites[tt], respectively.[PAR]",
        "Option [TT]-el[tt] writes files with more details on guessed chemical elements.[PAR]",
        "By default, charges on ionic residues are ignored, that is, the tabulated Cromer-Mann parameteres of the",
        "neutral elements are used. With [TT]-ionic[tt], you can correct the Cromer-Mann parameter by the charge for oxygen atoms of",
        "carboxyl group (Glu, Asp), carbon of the Arg moiety, nitrogen of Lys, and phosphate of DNA/RNA backbone.",
        "See comments in $GMXDATA/top/cromer-mann-defs.itp for more explanations.[PAR]"
        "To merge the Cromer-Mann parameters of hydrogen atoms into the heavy atoms, use [TT]-ua[tt]. This can ",
        "be used to test the effect of atomic details in the far WAXS regime.[PAR]" };

    static gmx_bool bDefault = FALSE, bUA = FALSE, bNSL = FALSE;
    static int maxWarn = 20;

    t_genscatt_opt opt;
    opt.bGromos = FALSE;
    opt.bVsites = FALSE;
    opt.bChargeModifiedCM = FALSE;
    opt.maxmassdiff = 0.3;

    t_pargs shellArguments[] = {
        { "-def", FALSE, etBOOL, { &bDefault },
                "Write no scattering factors and allow grompp to read from the forcefield definitions later" },
        { "-maxwarn", FALSE, etINT, { &maxWarn },
                "Limit of v-site related warnings gmx genscatt will show before they are suppressed." },
        { "-ionic", FALSE, etBOOL, { &opt.bChargeModifiedCM },
                "Correct number of electrons for ionic residues and nucleic acid backbone based on residue and atom names" },
        { "-ua", FALSE, etBOOL, { &bUA },
                "Merge atomic scattering factors of hydrogens into bonded heavy atom" },
        { "-gromos", FALSE, etBOOL, { &opt.bGromos },
                "Expect united-atom carbon atoms" },
        { "-vsites", FALSE, etBOOL, { &opt.bVsites },
                "Expect vsites (H with mass zero & heavier C and N)" },
        { "-mdiff", FALSE, etREAL, { &opt.maxmassdiff },
                "Largest allowed mass difference for element assignments" },
        { "-nsl", FALSE, etBOOL, { &bNSL },
                "Write Neutron Scattering Lengths in addition to Cromer-Mann definitions. Required for SANS calculations" },
    };

    t_filenm fileNames[] = {
        { efSTX, "-s", nullptr, ffREAD },
        { efNDX, "-n", nullptr, ffOPTRD },
        { efITP, "-o", "scatter", ffWRITE },
        { efDAT, "-el", "elemassign", ffOPTWR },
        { efTOP, "-p", "dummy", ffOPTWR },
    };

    gmx_output_env_t* outputEnv = nullptr;
    if (!parse_common_args(&argc, argv, PCA_CAN_VIEW, asize(fileNames), fileNames, asize(shellArguments), shellArguments,
        asize(desc), desc, 0, nullptr, &outputEnv))
    {
        return 0;
    }

    if (opt.bChargeModifiedCM and (opt.bGromos or bUA)) {
        gmx_fatal(FARGS, "Option -ionic is not supported together with -ua or -gromos.");
    }

    gmx_bool bDummyTop = opt2bSet("-p", asize(fileNames), fileNames);

    const char* in_file = ftp2fn(efSTX, asize(fileNames), fileNames);
    gmx_bool bHaveTpr = (fn2ftp(in_file) == efTPR);

    t_topology* top = nullptr;
    snew(top, 1);

    PbcType pbcType = PbcType::Unset; rvec* xtop = nullptr; matrix box;  // unused
    read_tps_conf(in_file, top, &pbcType, &xtop, nullptr, box, FALSE);

    int maxwarnStart = maxWarn;

    if (bDummyTop) {
        write_dummy_top(opt2fn("-p", asize(fileNames), fileNames), in_file, top, &maxWarn, &opt, bNSL);

        exit(0);
    }

    fprintf(stderr, "\nSelect atoms that scatter (e.g., Prot-Masses, Protein):\n");

    int isize = 0;
    int* index = nullptr;
    char* groupNames = nullptr;
    get_index(&top->atoms, ftp2fn_null(efNDX, asize(fileNames), fileNames), 1, &isize, &index, &groupNames);

    if (!bHaveTpr) {
        // printf("NOTE: Having only a PDB file, I don't know about masses or bonds. So I am assuming\n"
        //         "      that we have only one molecule, and I will guess the element based on the atom\n"
        //         "      name and residue number\n"
        //         "      IMPORTANT: This will definitely fail if you have united atoms (GROMOS) or virtual sites.\n"
        //         "      so carefully check the itp file with the scattering info.\n\n");

        // guessFromPdbFile(&top, index, isize);

        gmx_fatal(FARGS,"Need a tpr file to write itp file with scattering information\n");
    }

    if (bNSL and bUA) {
        gmx_fatal(FARGS, "United atom form factors are not supported for neutron scattering.\n");
    }

    /**
     * Read the molecule types from tpr, then check which atoms in the molecule types appear.
     * This is done because we write one scatter.itp file for each molecule type.
     */
    t_inputrec ir; t_state state;  // unused
    gmx_mtop_t mtop;
    read_tpx_state(in_file, &ir, &state, &mtop);

    gmx_bool** bInIndex_perMoltype = nullptr;
    int* nInIndex_perMoltype = nullptr;
    int** moltype_globalIndex = nullptr;

    snew(bInIndex_perMoltype, mtop.molblock.size());
    snew(nInIndex_perMoltype, mtop.molblock.size());
    snew(moltype_globalIndex, mtop.molblock.size());

    int moltype_firstAtomIndex = 0;
    int nMoltypeInSelection = 0;

    printf("\nThe following molecule types are found in the tpr file:\n"
            "------------------------------------------------------\n");

    /* Loop over molecule types */
    for (size_t mb = 0; mb < mtop.molblock.size(); mb++) {
        int nmols = mtop.molblock[mb].nmol;

        const gmx_molblock_t& molblock = mtop.molblock[mb];
        const gmx_moltype_t& moltype = mtop.moltype[molblock.type];
        int natoms_mol = moltype.atoms.nr;

        char* molname = *(mtop.moltype[mtop.molblock[mb].type].name);
        int moltype_lastAtomIndex = moltype_firstAtomIndex + nmols * natoms_mol - 1;

        snew(bInIndex_perMoltype[mb], natoms_mol);
        snew(moltype_globalIndex[mb], natoms_mol);

        for (int i = 0; i < natoms_mol; i++) {
            /* Make sure we get a Segfault in case we use below an atom that is not found in the index file */
            moltype_globalIndex[mb][i] = -999999999;
        }

        /* For this molecule type, make a boolean list that is TRUE if this atom appears in the index group */
        for (int i = 0; i < isize; i++) {
            if (moltype_firstAtomIndex <= index[i] and index[i] <= moltype_lastAtomIndex) {
                int molAtomIndex = (index[i] - moltype_firstAtomIndex) % natoms_mol;
                int moltype_instance = (index[i] - moltype_firstAtomIndex) / natoms_mol;

                if (moltype_instance > 0 and bInIndex_perMoltype[mb][molAtomIndex] == FALSE) {
                    gmx_fatal(FARGS, "Your selected index group \"%s\" contains multiple molecules of molecule type %s,\n"
                            "which is fine. However, your index groups seems to contain different atoms of the same\n"
                            "molecule type, which is not allowed. For instance, if you have two identical protein\n"
                            "chains, then your index group must contain exactly the same atoms of the two chains.\n"
                            "The problematic atom is atom with global index %d, or molecule no %d of type %s, \n"
                            "molecule-internal index %d\n",
                            groupNames, molname, index[i] + 1, moltype_instance + 1, molname, molAtomIndex + 1);
                }

                bInIndex_perMoltype[mb][molAtomIndex] = TRUE;

                /* Make an array that translates the atom number within a molecule type
                   into the global index */
                if (moltype_instance == 0) {
                    moltype_globalIndex[mb][molAtomIndex] = index[i];
                }
            }
        }

        /* Count the number of atoms of this moleucle type that appear somewhere in the index group. */
        for (int i = 0; i < natoms_mol; i++) {
            if (bInIndex_perMoltype[mb][i]) {
                nInIndex_perMoltype[mb]++;
            }
        }

        if (nInIndex_perMoltype[mb] > 0) {
            /* Count how many molecule types are represented in the index group */
            nMoltypeInSelection++;
        }

        printf("  %2d) %-25s  #molecules = %5d   #atoms = %4d   #atoms in user selection (\"%s\") = %d\n",
                static_cast<int>(mb + 1), molname, nmols, natoms_mol, groupNames, nInIndex_perMoltype[mb]);

        /* Set first index of the next molecule type */
        moltype_firstAtomIndex += nmols * natoms_mol;
    }

    printf("\nFound %d molecule types in the selection \"%s\".\n\n", nMoltypeInSelection, groupNames);

    FILE* fpAssign = nullptr;
    char comment[STRLEN];
    char buf[15];

    /* Loop over molecule types for writing itp files */
    for (size_t mb = 0; mb < mtop.molblock.size(); mb++) {
        /* Check if this moltype is in the index group */
        if (nInIndex_perMoltype[mb] == 0) {
            continue;
        }

        char* molname = *(mtop.moltype[mtop.molblock[mb].type].name);
        t_atoms* atoms = &mtop.moltype[mtop.molblock[mb].type].atoms;
        int natoms_mol = atoms->nr;

        const char* fnUser = opt2fn("-o", asize(fileNames), fileNames);
        char fnout[STRLEN];

        sprintf(fnout, "%.*s_%s.itp", (int)(strlen(fnUser) - 4), fnUser, molname);
        printf("Writing scatter parameters of molecule \"%s\" into %s\n", molname, fnout);

        FILE* file = gmx_ffopen(fnout, "w");
        fprintf(file, "; Written by gmx genscatt");

        time_t timeNow = 0;
        time(&timeNow);
        fprintf(file, "; This file was created %s\n", gmx_ctime_r(&timeNow).c_str());

        if (opt2bSet("-el", asize(fileNames), fileNames)) {
            fnUser = opt2fn("-el", asize(fileNames), fileNames);
            sprintf(fnout, "%.*s_%s.dat", (int)(strlen(fnUser) - 4), fnUser, molname);
            printf("\nWriting atom assignments of molecule %s into %s\n", molname, fnout);
            fpAssign = gmx_ffopen(fnout, "w");
        }

        charElem *elems = nullptr;
        int* bHydrogen = nullptr;
        int* bondedTo = nullptr;

        /* array to store the element, whether the atom is a hydrogen atom, and to which heavy atom this
           H-atom is bound to (if it is an H-atom) */
        snew(elems, natoms_mol);
        snew(bHydrogen, natoms_mol);
        snew(bondedTo, natoms_mol);

        for (int i = 0; i < natoms_mol; i++) {
            bondedTo[i] = -1;
        }

        /* Next block: writing Cromer-Mann */
        fprintf(file, "[ scattering_params ]\n");
        fprintf(file, "; atom ft a1     a2      a3      a4      b1      b2      b3      b4      c\n");

        char defineName[STRLEN];
        sprintf(defineName, "%s", "CROMER_MANN_");

        int ft = 1;

        int nHeavy = 0;
        gmx_bool bAlert = FALSE;

        for (int i = 0; i < natoms_mol; i++) {
            if (!bInIndex_perMoltype[mb][i]) {
                continue;
            }

            int global_index = moltype_globalIndex[mb][i];

            if (bDefault) {
                fprintf(file, "%5d  1  \n", i + 1);

                continue;
            }

            charElem elemStr;

            char* resname = *(atoms->resinfo[atoms->atom[i].resind].name);
            int resid = atoms->resinfo[atoms->atom[i].resind].nr;

            int ret = get_elem(*(atoms->atomname[i]), resname, atoms->atom[i].m, &elemStr[0], &maxWarn, &opt, comment);

            /* Keep element of this atom for NSL */
            strcpy(elems[i], elemStr);

            if (ret != 0 and maxWarn > 0) {
                fprintf(stderr,"WARNING -Atom nr %d, name %s, mass %g could not be assigned"
                        " to an element (maybe need option -vsites?)\n", global_index, *(atoms->atomname[i]),
                        atoms->atom[i].m);

                maxWarn--;
            }

            /* Overwrite elemement string for some charged groups, such as Lys-NZ, Arg-CZ, termini, DNA/RNA-P, Glu/Asp */
            if (opt.bChargeModifiedCM)
            {
                ionicResiduesOverwriteElemstr(atoms, i, &elemStr[0], comment);
            }

            /* If we don't write united-atom form factors, we can write the CM parameters now. Otherwise,
               the CM parametes are written below */
            if (!bUA) {
                /* Write the Cromer-Mann or NSL definition */
                fprintf(file, "%5d  %d  %s%-10s   ; %4s - %4s-%d\n", i + 1, ft, defineName, elemStr,
                        *(atoms->atomname[i]), resname, resid);

                if (fpAssign) {
                    fprintf(fpAssign, "%4s-%-4d  %4s, mass %7.3f -> %-10s   %s\n", resname, resid,
                            *(atoms->atomname[i]), atoms->atom[i].m, elemStr, comment);
                }
            }


            if (!strcmp(elemStr, "H")) {
                bHydrogen[i] = 1;
            } else {
                nHeavy++;
            }

            if (!bAlert and !maxWarn) {
                fprintf(stderr, "\nMaximum number of mass and vsite-related warnings reached. Further notes suppressed.\n\n");
                fprintf(stderr, "NOTE: Warnings may occur because your system contains virtual sites. If so, make sure to select only the physical\n"
                        "atoms (e.g. group \"Prot-Masses\") but not the non-physical dummy atoms.\n\n");

                bAlert = TRUE;
            }
        }

        int* nHydOnHeavy = nullptr;

        /* For each H-atom, find the heavy atom to which it is bound. This is needed for:
         *  1) United atom form factors.
         *  2) For Neutron Scattering Lengths to tell whether an H-atom is polar or not, and whether it is bound
         *     to a backbone nitrogen.
         */
        if (bUA or bNSL) {
            /* To do: The following routine should better pick the bonds from the mtop, not from the top. This way,
               we would not have to translate the molecule-internal atom numbers into the global atom numbers. Well,
               but it works for now...
            */

            /* Find out to which heavy atom the hydrogen is bound */
            printf("%s: Found %d hydrogen atoms and %d heavy atoms\n", molname, nInIndex_perMoltype[mb] - nHeavy, nHeavy);
            snew(nHydOnHeavy, nInIndex_perMoltype[mb]);

            for (int interaction_type = 0; interaction_type < F_NRE; interaction_type++) {
                if (IS_CHEMBOND(interaction_type)) {
                    /* Now loop over all chemical bonds */

                    // t_iatom* iatom = top->idef.il[interaction_type].iatoms;
                    // int ncons = top->idef.il[interaction_type].nr / 3;

                    for (int j = 0; (j < top->idef.il[interaction_type].nr); j += interaction_function[interaction_type].nratoms + 1) {
                        /**
                         * Caution: atom1 and atom2 are global atom indices, whereas the counter i, atom1ind, and atom2ind
                         * are indices within the molecule type (0 <= atom1ind < natoms_mol)
                         */
                        int atom1 = top->idef.il[interaction_type].iatoms[j + 1];
                        int atom2 = top->idef.il[interaction_type].iatoms[j + 2];

                        /* find nr for index[] */
                        int atom1ind = -1;
                        int atom2ind = -1;

                        for (int i = 0; i < natoms_mol; i++) {
                            if (bInIndex_perMoltype[mb][i]) {
                                int global_index = moltype_globalIndex[mb][i];

                                if (global_index == atom1) {
                                    atom1ind = i;
                                }

                                if (global_index == atom2) {
                                    atom2ind = i;
                                }
                            }

                            if (atom1ind >= 0 and atom2ind >= 0) {
                                break;
                            }
                        }

                        if (atom1ind == -1 or atom2ind == -1) {
                            /* Not both atoms are in index[] -> continue */
                            continue;
                        }

                        if ((bHydrogen[atom1ind] == 0 and bHydrogen[atom2ind] == 1)
                                or (bHydrogen[atom2ind] == 0 and bHydrogen[atom1ind] == 1)) {
                            /* increase nHydOnHeavy[] of the heavy atom */
                            if (bHydrogen[atom1ind] == 1) {
                                nHydOnHeavy[atom2ind]++;
                                bondedTo[atom1ind] = atom2ind;
                            } else {
                                nHydOnHeavy[atom1ind]++;
                                bondedTo[atom2ind] = atom1ind;
                            }
                        }
                    }
                }
            }
        }

        if (bNSL) {
            /* Next block: writing NSL */
            fprintf(file, "\n; atom ft NSL(Coh b)\n");
            sprintf(defineName, "%s", "NEUTRON_SCATT_LEN_");
            ft = 2;

            for (int i = 0; i < natoms_mol; i++) {
                if (!bInIndex_perMoltype[mb][i]) {
                    continue;
                }

                char* resname = *(atoms->resinfo[atoms->atom[i].resind].name);
                int resid = atoms->resinfo[atoms->atom[i].resind].nr;

                if (!bHydrogen[i]) {
                    /* empty comment */
                    comment[0] = '\0';

                    /* Write the NSL definition */
                    fprintf(file, "%5d  %d  %s%-10s   ; %4s - %s-%d\n", i + 1, ft, defineName,
                            elems[i], *(atoms->atomname[i]), resname, resid);
                } else {
                    /* Check if the heavy atom to which this is bound is a backbone nitrogen */
                    gmx_bool bBackboneH = isBackboneHydrogen(i, bondedTo[i], atoms);

                    /* Check if this is a polar hydrogen since it is bound to O, N, S, or P */
                    gmx_bool bPolarH = isPolarizingHydrogen(elems[bondedTo[i]]);

                    if (bBackboneH) {
                        sprintf(comment, " -- Protein backbone hydrogen");
                        fprintf(file, "%5d  %d  %-28s   ; %4s - %s-%d\n", i + 1, ft,
                                "NSL_H_DEUTERATABLE_BACKBONE", *(atoms->atomname[i]), resname, resid);
                    } else if (bPolarH) {
                        sprintf(comment, " -- deuteratable non-backbone hydrogen");
                        fprintf(file, "%5d  %d  %-28s   ; %4s - %s-%d\n", i + 1, ft,
                                "NSL_H_DEUTERATABLE", *(atoms->atomname[i]), resname, resid);
                    } else {
                        sprintf(comment, " -- non-deuteratable non-polar hydrogen");
                        fprintf(file, "%5d  %d  %-28s   ; %4s - %s-%d\n", i + 1, ft,
                                "NEUTRON_SCATT_LEN_1H", *(atoms->atomname[i]), resname, resid);
                    }
                }

                if (fpAssign) {
                    fprintf(fpAssign, "%4s-%-4d  %4s, mass %7.3f -> %-10s   %s\n", resname, resid,
                            *(atoms->atomname[i]), atoms->atom[i].m, elems[i], comment);
                }
            }
        }

        if (bUA) {
            /** Next block: writing unite-atom Cromer-Mann */
            sprintf(defineName, "%s", "CROMER_MANN_");
            ft = 1;

            /** For united atom scattering factors, write CH, CH2, NH3, etc. */
            for (int i = 0; i < natoms_mol; i++) {
                if (!bInIndex_perMoltype[mb][i]) {
                    continue;
                }

                if (!bHydrogen[i]) {
                    // printf("Found %d hydrogen bonded to heavy atom %d (%s)\n", nHydOnHeavy[i], index[i] + 1,
                    //        *(atoms->atomname[i]));

                    if (nHydOnHeavy[i] > 1) {
                        sprintf(buf, "%sH%d", elems[i], nHydOnHeavy[i]);
                    } else if (nHydOnHeavy[i] == 1) {
                        sprintf(buf, "%sH", elems[i]);
                    } else {
                        sprintf(buf, "%s", elems[i]);
                    }

                    /* Write the Cromer-Mann or NSL definition */
                    fprintf(file, "%5d  %d  %s%s\n", i + 1, ft, defineName, buf);

                    if (fpAssign) {
                        fprintf(fpAssign, "%4s, mass %7.3f -> %s, %s bound to %d H\n", *(atoms->atomname[i]),
                                atoms->atom[i].m, buf, elems[i], nHydOnHeavy[i]);
                    }
                } else {
                    if (bondedTo[i] == -1) {
                        /* Catch the case that this hydrogen is not bonded to any heavy atom */
                        fprintf(stderr,"WARNING -\n Atom %d (%s) of molecule type %s is not bonded to any heavy atom in the group.\n",
                                i + 1, *(atoms->atomname[i]), molname);

                        fprintf(file, "%5d  %d  %s%s\n", i + 1, ft, defineName, elems[i]);

                        if (fpAssign) {
                            fprintf(fpAssign, "%4s, mass %7.3f -> %s\n", *(atoms->atomname[i]),
                                    atoms->atom[i].m, elems[i]);
                        }
                    } else {
                        if (fpAssign) {
                            fprintf(fpAssign, "%4s, mass %7.3f -> merged into atom %d (%s)\n", *(atoms->atomname[i]),
                                    atoms->atom[i].m, bondedTo[i], *(atoms->atomname[bondedTo[i]]));
                        }
                    }
                }
            }
        }

        gmx_ffclose(file);

        if (fpAssign) {
            gmx_ffclose(fpAssign);
        }

        if (nHydOnHeavy) {
            sfree(nHydOnHeavy);
        }

        sfree(elems);
        sfree(bHydrogen);
        sfree(bondedTo);
    }

    sfree(top);

    if (maxwarnStart == maxWarn) {
        return 0;
    } else {
        return 1;
    }
}
