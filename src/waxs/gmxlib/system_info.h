#pragma once

namespace swaxs {

class SystemInfo {
public:
    /** Returns total amount of memory in bytes. 0 means unknown. */
    static unsigned long getTotalMemorySize();

    /** Returns number of logical cores. */
    static unsigned int getCpuCoreCount();
};

};
