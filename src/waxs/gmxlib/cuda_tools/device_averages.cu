#include "./device_averages.cuh"

#include <cuda.h>
#include <cuda_runtime.h>

/* Update cumulative averages: new = fac1*sum + fac2*new */

__device__ t_complex_d d_cadd_d(t_complex_d a, t_complex_d b)
{
    t_complex_d c;

    c.re = a.re+b.re;
    c.im = a.im+b.im;

    return c;
}

__device__ t_complex d_cadd(t_complex a, t_complex b)
{
    t_complex c;

    c.re = a.re+b.re;
    c.im = a.im+b.im;

    return c;
}

__device__ t_complex d_cdiff(t_complex a , t_complex b)
{
    t_complex c;

    c.re = a.re - b.re;
    c.im = a.im - b.im;

    return c;
}

__device__ t_complex_d d_cmul_rd(t_complex a, t_complex b)
{
    t_complex_d c;

    c.re = a.re*b.re - a.im*b.im;
    c.im = a.re*b.im + a.im*b.re;

    return c;
}
__device__ t_complex_d d_cmul_d(t_complex_d a, t_complex_d b)
{
    t_complex_d c;

    c.re = a.re*b.re - a.im*b.im;
    c.im = a.re*b.im + a.im*b.re;

    return c;
}

__device__ t_complex d_rcmul(real r, t_complex c)
{
    t_complex d;
    d.re = r*c.re;
    d.im = r*c.im;

    return d;
}

__device__ t_complex_d d_rcmul_d(double r, t_complex_d c)
{
    t_complex_d d;
    d.re = r*c.re;
    d.im = r*c.im;

    return d;
}

__device__ double d_accum_avg( double sum, double new_value, double fac1, double fac2 )
{
    double tmp = fac1*sum + fac2*new_value;
    return tmp;
}

__device__ t_complex d_c_accum_avg( t_complex sum, t_complex new_value, real fac1, real fac2 )
{
    t_complex tmp = d_cadd( d_rcmul(fac1,sum), d_rcmul(fac2,new_value) );
    return tmp;
}

__device__ t_complex_d d_cd_accum_avg( t_complex_d sum, t_complex_d new_value, double fac1, double fac2 )
{
    t_complex_d tmp = d_cadd_d( d_rcmul_d(fac1,sum), d_rcmul_d(fac2,new_value) );
    return tmp;
}
