#pragma once

#include <gromacs/math/vec.h>  // rvec, gmx::RVec
#include <gromacs/utility/arrayref.h>  // gmx::ArrayRef
#include <gromacs/utility/real.h>  // real

/**
 * Return an estimate for the center and the radius of the bounding sphere
 * around atoms x[]. It is a simplified version of Ritter's algorithm.
 */
void get_bounding_sphere_Ritter_COM(gmx::ArrayRef<gmx::RVec> x, int* index, int n, rvec cent, real* R, gmx_bool bVerbose);

/**
 * Return an estimate for the center and the radius of the bounding sphere
 * around atoms x[]. Uses Ritter's bounding sphere (see Wikipedia)
 */
void get_bounding_sphere(gmx::ArrayRef<gmx::RVec> x, int* index, int n, rvec cent, real* R, gmx_bool bVerbose);
