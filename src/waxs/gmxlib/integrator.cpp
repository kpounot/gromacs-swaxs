#include "./integrator.h"

#include <gromacs/fileio/oenv.h>  // gmx_output_env_t
#include <gromacs/mdlib/forcerec.h>  // t_forcerec
#include <gromacs/mdtypes/commrec.h>  // t_commrec

#include <waxs/gmxlib/sftypeio.h>  // done_t_waxsrec
#include <waxs/gmxlib/waxsmd.h>  // waxsEstimateNumberIndepPoints, waxs_alloc_globalavs, waxsDoMPIComm_qavgs, waxs_free_globalavs, done_waxs_md

void done_swaxs(t_forcerec* fr, t_commrec* cr, const gmx_output_env_t* oenv) {
    if (!fr->waxsrec->bHaveNindep) {
        /** If we don't have Nindep yet: Get em now and scale var(I) accordingly */
        if (MASTER(cr)) {
            printf("Reached end of simulation, number of independent points were not not yet computed. Doing now.\n");
        }

        waxsEstimateNumberIndepPoints(fr->waxsrec, cr, FALSE, TRUE);
    }

    if (MASTER(cr)) {
        waxs_alloc_globalavs(fr->waxsrec);
    }

    if (PAR(cr)) {
        waxsDoMPIComm_qavgs(fr->waxsrec, cr, TRUE);
    }

    if (MASTER(cr)) {
        fr->waxsrec->swaxsOutput->writeOut(fr->waxsrec, oenv);

        waxs_free_globalavs(fr->waxsrec);
    }

    done_waxs_md(fr->waxsrec);
    done_t_waxsrec(fr->waxsrec);
}
