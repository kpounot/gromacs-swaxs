#pragma once

#include <gromacs/mdtypes/md_enums.h>  // enum_name

#include <waxs/types/enums.h>  // escatter*, ewaxs*

extern const char* escatter_names[escatterNR + 1];
extern const char* waxs_bScaleI0_names[ewaxsscalei0NR + 1];
extern const char* waxscorrectbuff_names[waxscorrectbuffNR + 1];
extern const char* waxspotential_names[ewaxsPotentialNR + 1];
extern const char* waxsweights_names[ewaxsWeightsNR + 1];
extern const char* waxsensemble_names[ewaxsEnsembleNR + 1];
extern const char* waxs_Iexp_fit_names[ewaxsIexpFitNR + 1];
extern const char* waxssolvdensunsertbayesian_names[ewaxsSolvdensUncertBayesianNR + 1];

#define EWAXSBSCALEI0(e) enum_name(e, ewaxsscalei0NR, waxs_bScaleI0_names)
#define EWAXSCORRECTBUFF(e) enum_name(e, waxscorrectbuffNR, waxscorrectbuff_names)
#define EWAXSWEIGHTS(e) enum_name(e, ewaxsWeightsNR, waxsweights_names)
#define EWAXSENSEMBLES(e) enum_name(e, ewaxsEnsembleNR, waxsensemble_names)
#define EWAXSSOLVDENSUNCERTBAYESIAN(e) enum_name(e, ewaxsSolvdensUncertBayesianNR, waxssolvdensunsertbayesian_names)
#define EWAXSPOTENTIAL(e) enum_name(e, ewaxsPotentialNR, waxspotential_names)
#define EWAXSIEXPFIT(e) enum_name(e, ewaxsIexpFitNR, waxs_Iexp_fit_names)
