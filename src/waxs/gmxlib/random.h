/**
 * This is a subset of random.cpp present in Gromacs 5.1.
 * Copyright belongs to authors of Gromacs.
 *
 * This partial copy is a stop-gap solution. New API should be used in gmx_envelope.cpp.
 */

#pragma once

#include "gromacs/utility/basedefinitions.h"
#include "gromacs/utility/real.h"


/*! \brief Abstract datatype for a random number generator
 *
 * This is a handle to the full state of a random number generator.
 * You can not access anything inside the gmx_rng structure outside this
 * file.
 */
typedef struct gmx_rng* gmx_rng_t;


/*! \brief Create a new RNG, seeded from a single integer.
 *
 * If you dont want to pick a seed, just call it as
 * rng=gmx_rng_init(gmx_rng_make_seed()) to seed it from
 * the system time or a random device.
 *
 * \param seed Random seed, unsigned 32-bit integer.
 *
 * \return Reference to a random number generator, or NULL if there was an
 *         error.
 *
 * \threadsafe Yes.
 */
gmx_rng_t gmx_rng_init(unsigned int seed);


/*! \brief Generate a 'random' RNG seed.
 *
 * This routine tries to get a seed from /dev/random if present,
 * and if not it uses time-of-day and process id to generate one.
 *
 * \return 32-bit unsigned integer random seed.
 *
 * Tip: If you use this in your code, it is a good idea to write the
 * returned random seed to a logfile, so you can recreate the exact sequence
 * of random number if you need to reproduce your run later for one reason
 * or another.
 *
 * \threadsafe Yes.
 */
unsigned int gmx_rng_make_seed(void);


/*! \brief Initialize a RNG with 624 integers (>32 bits of entropy).
 *
 *  The Mersenne twister RNG used in Gromacs has an extremely long period,
 *  but when you only initialize it with a 32-bit integer there are only
 *  2^32 different possible sequences of number - much less than the generator
 *  is capable of.
 *
 *  If you really need the full entropy, this routine makes it possible to
 *  initialize the RNG with up to 624 32-bit integers, which will give you
 *  up to 2^19968 bits of entropy.
 *
 *  \param seed Array of unsigned integers to form a seed
 *  \param seed_length Number of integers in the array, up to 624 are used.
 *
 * \return Reference to a random number generator, or NULL if there was an
 *         error.
 *
 * \threadsafe Yes.
 */
gmx_rng_t gmx_rng_init_array(unsigned int seed[], int seed_length);


/*! \brief Random 32-bit integer from a uniform distribution
 *
 *  This routine returns a random integer from the random number generator
 *  provided, and updates the state of that RNG.
 *
 *  \param rng Handle to random number generator previously returned by
 *		       gmx_rng_init() or gmx_rng_init_array().
 *
 *  \return 32-bit unsigned integer from a uniform distribution.
 *
 *  \threadsafe Function yes, input data no. You should not call this function
 *	        from two different threads using the same RNG handle at the
 *              same time. For performance reasons we cannot lock the handle
 *              with a mutex every time we need a random number - that would
 *              slow the routine down a factor 2-5. There are two simple
 *		solutions: either use a mutex and lock it before calling
 *              the function, or use a separate RNG handle for each thread.
 */
unsigned int gmx_rng_uniform_uint32(gmx_rng_t rng);


/*! \brief Random gmx_real_t 0<=x<1 from a uniform distribution
 *
 *  This routine returns a random floating-point number from the
 *  random number generator provided, and updates the state of that RNG.
 *
 *  \param rng Handle to random number generator previously returned by
 *		       gmx_rng_init() or gmx_rng_init_array().
 *
 *  \return floating-point number 0<=x<1 from a uniform distribution.
 *
 *  \threadsafe Function yes, input data no. You should not call this function
 *		from two different threads using the same RNG handle at the
 *              same time. For performance reasons we cannot lock the handle
 *              with a mutex every time we need a random number - that would
 *              slow the routine down a factor 2-5. There are two simple
 *		solutions: either use a mutex and lock it before calling
 *              the function, or use a separate RNG handle for each thread.
 */
real gmx_rng_uniform_real(gmx_rng_t rng);
