#pragma once

#include <cstdio>  // FILE
#include <vector>  // vector

#include <gromacs/mdtypes/commrec.h>  // t_commrec
#include <gromacs/utility/basedefinitions.h>  // gmx_bool

/** Starts timing after these steps */
#define SWAXS_STEPS_RESET_TIME_AVERAGES 30

namespace swaxs {

/** Measures time spent on different stages of the WAXS-related logic. */
class Timing {
public:
    enum class Stage {
        Step,
        PrepareSolute,
        PrepareSolvent,
        ScattAmplitude,
        OneScattAmplitude,
        dkI,
        Fourier,
        Gibbs,
        PotForces,
        ScattUpdates,
        ComputeIdkI,
        SolvDensCorr,
        Count
    };

    Timing();

    /** Starts a measurement */
    void start(int waxsStep, Stage stage, const t_commrec& cr);

    /** Finishes a measurement */
    void end(int waxsStep, Stage stage, const t_commrec& cr);

    /** Adds time measured somewhere else */
    void add(int waxsStep, Stage stage, double seconds, const t_commrec& cr);

    /** Writes timing summary */
    void write(FILE* fp);

    void writeLast(FILE* fp);

private:
    int size;

    std::vector<int> nMeasurements;
    std::vector<double> sumTimes, tStartThisMeasurement, lastTime;
    std::vector<gmx_bool> currentlyMeasuring;
};

}
