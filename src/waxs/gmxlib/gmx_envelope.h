#pragma once

#include <gromacs/mdtypes/commrec.h>  // t_commrec
#include <gromacs/pbcutil/pbc.h>  // t_pbc

#include <waxs/envelope/envelope.h>  // gmx_envelope

/* Width of Gaussian used to smooth the evelope over the space angle (in rad) */

/* Our current smoothing sometimes leads to strange peaks in the envelope at present - use not
   smoothing as default until we fixed this */
#define GMX_ENVELOPE_SMOOTH_SIGMA (0. / 180. * M_PI)

/* With automatic determination of the number of recursions for envelope, use at least: */
#define WAXS_ENVELOPE_NREC_MIN 4

/* Setting up and destroying envelope *************************************** */
void gmx_envelope_superimposeEnvelope(gmx_envelope* envelope_add, gmx_envelope* envelope_base);

void gmx_envelope_buildSphere(gmx_envelope* envelope, real rad);

void gmx_envelope_buildEllipsoid(gmx_envelope* envelope, rvec r);

void gmx_envelope_buildEnvelope_omp(gmx_envelope* envelope, gmx::ArrayRef<gmx::RVec> x, int* index, int isize, real d,
        real phiSmooth, gmx_bool bSphere, real* vdwradii, int nRadii);

gmx_envelope* gmx_envelope_init_md(int nreq, const t_commrec* cr, gmx_bool bVerbose);

void gmx_envelope_bcast(gmx_envelope* e, t_commrec* cr);
/* ************************************************************************ */


/* Using the envelope ****************************************************** */
gmx_bool gmx_envelope_isInside(gmx_envelope* envelope, const rvec x);

void gmx_envelope_minimumDistance(gmx_envelope* envelope, gmx::ArrayRef<gmx::RVec> x, int* index, int isize,
        real* mindist, int* imin, gmx_bool* bMinDistToOuter);

gmx_bool gmx_envelope_bInsideCompactBox(gmx_envelope* envelope, matrix Rinv, const matrix box, rvec boxToXRef,
        t_pbc* pbc, gmx_bool bVerbose, real tolerance);

void gmx_envelope_bounding_sphere(gmx_envelope* envelope, rvec center, real* R2);

double gmx_envelope_diameter(gmx_envelope* envelope);

void gmx_envelope_center_xyz(gmx_envelope* envelope, matrix Rinv, rvec cent);
/* ********************************************************************************** */


/* ************************************************************************ */
/* Envelope currently constructed? */
gmx_bool gmx_envelope_bHaveSurf(gmx_envelope* envelope);

/* Maximum radius of envelope */
double gmx_envelope_maxR(gmx_envelope* envelope);

void gmx_envelope_setStats(gmx_envelope* envelope);
