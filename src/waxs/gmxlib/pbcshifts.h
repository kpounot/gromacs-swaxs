#pragma once

#include <gromacs/math/vec.h>  // rvec
#include <gromacs/math/vectypes.h>  // matrix
#include <gromacs/topology/topology.h>  // gmx_mtop_t
#include <gromacs/pbcutil/pbc.h>  // PbcType
#include <gromacs/utility/arrayref.h>  // ArrayRef
#include <gromacs/utility/basedefinitions.h>  // gmx_bool

#include <waxs/gmxlib/sftypeio.h>  // t_waxsrec

/**
 * Get solvent atoms inside the envelope of solute/solvent and pure-solvent system.
 * Include solvent atoms along one shift along all PBC vectors.
 */
void get_solvation_shell_periodic(gmx::ArrayRef<gmx::RVec> x, gmx::ArrayRef<gmx::RVec> xPureSolvent, t_waxsrec* wr,
        const gmx_mtop_t* mtop, gmx_mtop_t* mtopPureSolvent, PbcType pbcType, const matrix box, matrix boxPureSolvent,
        gmx_bool bPrintPdbFile);

/**
 * Shift atom by shift index s, where 0 <= s < N_SWAXS_BOX_SHIFTS(27)
 * The shifts are defined in waxsBoxShift[] in waxsrec.h
 */
void shiftRvecByShiftIndex(const rvec x, int s, const matrix box, rvec xShifted);

/**
 * Fill the envelope, using also PBC images of solvent atoms:
 * Set bits in shiftsInsideEnvelope[], where
 *     bit 0 indicates shift s = 0 (the central box)
 *     bit 1 indicates shift s = 1 etc.
 * The shifts are defined in waxsBoxShift[] in waxsrec.h
 */
void setAtomShiftsInsideEnvelope(gmx_envelope* envelope, gmx::ArrayRef<gmx::RVec> x, const matrix box, int nind,
        int* index, uint32_t* shiftsInsideEnvelope);

/**
 * Count number of atoms in the envelope, as well as the number of periodic images (outside the compact box)
 * inside the envelope (if nShiftedAtomsInside != nullptr.
 */
void nAtomsInsideEnvelope(int nSolute, int* shiftsInsideEnvelope_solute, int nSolvent,
        int* shiftsInsideEnvelope_solvent, int* nAtomsInside, int* nShiftedAtomsInside);

/**
 * Write PDB file of protei+solvation layer or excluded solvent, including shifted atoms (from periodic images)
 */
void writePdbfileIncludingShifts(const char* file_name, const char* title, t_atoms* atoms, rvec* x, PbcType pbcType,
        const matrix box, int nSolute, int* indexSolute, uint32_t* shiftsInsideEnvelope_solute, int nSolvent,
        int* indexSolvent, uint32_t* shiftsInsideEnvelope_solvent);

/**
 * Print coord of an atom, including all 26 shifts.
 * If shiftsInsideEnvelope != nullptr, also print whether the shifted atom is inside the envelope
 */
void fprintShiftsOfX(FILE* file, rvec x, const matrix box, gmx_bool bPrintIsInside, uint32_t shiftsInsideEnvelope);

/** Print String of bits of atom shifts, one 0/1 for each N_SWAXS_BOX_SHIFTS */
void printAtomShiftBits(FILE* file, uint32_t shiftsInsideEnvelope);

/** Print statistics of atom shifts inside the envelope to file pointer */
void summarizeAtomShifts(FILE* fp, int nind, int* index, uint32_t* shiftsInsideEnvelope);

/**
 * Check wether the maximum and minimum coordiantes of solvation layer and excluded solvent are apprximately
 * equal. This is a sanity check to make sure that the solvent is not "cut" somewhere due to a box surface.
 */
void check_selected_solvent_periodic(
    gmx::ArrayRef<gmx::RVec> xA,
    gmx::ArrayRef<gmx::RVec> xB,
    int nSolventA,
    int nSolventB,
    int *indexSolventA,
    int *indexSolventB,
    matrix boxA,
    matrix boxB,
    int *shiftsInsideEnvelope_solvent_A,
    int *shiftsInsideEnvelope_solvent_B,
    gmx_bool bFatal);


/**
 * Shifts along box vectors to get periodic images of solvent atoms. Solvent atoms
 * along these shifts are taken into account to fill up the envelope
 */
#define N_SWAXS_BOX_SHIFTS 27

const ivec waxsBoxShift[N_SWAXS_BOX_SHIFTS] = {
    { 0, 0, 0 },                                        // shift 0 is the central box
    { -1, -1, -1 }, { -1, -1,  0 }, { -1, -1, +1 },
    { -1,  0, -1 }, { -1,  0,  0 }, { -1,  0, +1 },
    { -1, +1, -1 }, { -1, +1,  0 }, { -1, +1, +1 },

    {  0, -1, -1 }, {  0, -1,  0 }, {  0, -1, +1 },
    {  0,  0, -1 },                 {  0,  0, +1 },
    {  0, +1, -1 }, {  0, +1,  0 }, {  0, +1, +1 },

    { +1, -1, -1 }, { +1, -1,  0 }, { +1, -1, +1 },
    { +1,  0, -1 }, { +1,  0,  0 }, { +1,  0, +1 },
    { +1, +1, -1 }, { +1, +1,  0 }, { +1, +1, +1 },
};

/**
 * one/unity in uint32_t (32-bit unsigned int). This is used to set and read individual bits of
 * uint32_t shiftsInsideEnvelope_A_prot[] etc.
 */
#define ONE_UI32 ((uint32_t)1)
