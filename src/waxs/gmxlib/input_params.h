#pragma once

#include <gromacs/utility/basedefinitions.h>  // gmx_bool

#include <waxs/types/enums.h>  // ewaxsEnsembleNone

/**
 * According to the Cryson paper, the fraction of deuterated backbone H
 * is typically 10% below the deuteration of the other polar H
 */
// #define NOT_DEUTERATED_BACKBONE_H_FRACTION 0.1
#define BACKBONE_DEUTERATED_PROB_DEFAULT 0.9

/** Default settint for tolerance on state weights */
#define GMX_WAXS_WEIGHT_TOLERANCE_DEFAULT 0.1

/**
 * Default settings for J, the number of points for numerical spherical average
 * In automatic mode,
 *    J(q) = MAX( Jmin , alpha * (D*q)^2 )
 * where D is the maximum diameter of the envelope.
 */
#define GMX_WAXS_JMIN    100
#define GMX_WAXS_J_ALPHA 0.1

namespace swaxs {

struct InputParams {
    InputParams() = default;
    ~InputParams() = default;

    int nTypes = 0;  /**< Number of scattering groups: xray or neutron types */

    gmx_bool bUseGPU = FALSE;  /**< To use GPU for scattering amplitude calculation */

    int nstlog = 0;  /**< Log output frequency for sWAXS stuff */

    /** Stuff for neutron scattering */
    gmx_bool bDoingNeutron = FALSE;  /**< Doing (also) Neutron scattering */
    gmx_bool bStochasticDeuteration = FALSE;  /**< Assigning 1H and 2H randomly to deuteratable hydrogen atoms */
    real backboneDeuterProb = BACKBONE_DEUTERATED_PROB_DEFAULT;  /**< Probability that the backbone H is deuterated at 100% D2O. Default = 0.9 */

    real solv_warn_lay;  /**< Warn if layer gets thinner that this (with sphere) */

    /** Short-hand lists of Cromer-Mann scattering types and NSL scattering types */
    int* cmtypeList = nullptr;  /**< array (size mtop->natoms) of atoms.atom[].cmtype */
    int* nsltypeList = nullptr;  /**< array (size mtop->natoms) of atoms.atom[].nsltype */

    real tau = 0.;  /**< Coupling time constant in ps */
    real tausteps = 0.;  /**< Coupling time constant in WAXS steps */
    int nstcalc = 0;  /**< Calculating I(q) every these simulation steps   */
    int nfrsolvent;  /**< # of pure solvent structure factors to compute (-1 = take from xtc) */
    gmx_bool bVacuum = FALSE;  /**< No solvent scattering */
    double scale = 0.;  /**, factor used for cumulative averages - allows non-weighted, instantaneous, exponential average */
    int potentialType = 0;  /**< Enum for type of potential (log or linear scale) */
    int weightsType = 0;  /**< Enum for coupling weights (uniform, experiment, experiment+calculated uncertainties) */
    int bBayesianSolvDensUncert = FALSE;  /**< Bayesian sampling of the uncertainty of the solvent density */

    real t_target; /**< Switch the target curve linearly from current to the experimental curve within this time (e.g. 2ns)
               This reduces very large forces just after waxs_tau and allows for more gradual transitions.
               Implemented simply such that the force constant is scaled by (t/t_target)^2 if t < t_target. */
    int ewaxs_Iexp_fit = ewaxsIexpFit_NO;  /**< Fitting I(exp) to I(calc) on the fly, or marginalizing the fitting parameters no / scale-and-offset / scale */

    real kT;  /**< Boltzmann const times temperature (in units (kJ/mol K)). To get a natural energy scale for WAXS-potential */

    int J = 0;  /**< Nr of pointes on unit sphere in q-space for spherical quadrature  */
    int Jmin = GMX_WAXS_JMIN;  /**< If J==0, determine J automatically by J = max(Jmin, alpha*(D*q)^2 */
    real Jalpha = GMX_WAXS_J_ALPHA;  /**< where alpha ~ 0.05 and D is the maximum diameter of the envelope. Jmin and Jalpha can be modified by environment variables.         */

    int nAverI;  /**< Nr of previous I(q) used to compute uncertainty of I(q). Will use 2 * tau / nstcalc. */
    int stepCalcNindep = 0;  /**< calc number of independent D(q) very # steps */

    double calcWAXS_begin = -1, calcWAXS_end = -1;  /**< Time to begin and end WAXS calculations - useful to avoid trjconv before rerun */

    gmx_bool bDoingMD;  /**< Within mdrun or, if not, within g_waxs */
    gmx_bool bCalcForces = FALSE;  /**< Calculate forces form WAXS coupling (normal MD, not in rerun) */
    gmx_bool bPrintForces = FALSE;  /**< Controls for printing out WAXS-forces. */
    gmx_bool bPrintNindep = FALSE;  /**< Control for prining the number of independent q-vectors per absolute value of q into waxs_nindep.xvg, which is used to compute the errors of I[calc] */
    gmx_bool bSwitchOnForce;  /**< Smooth switching-on of force until t=tau (so we have some averaging already) */
    gmx_bool bCalcPot = FALSE;  /**< Calculate potential from WAXS-coupling (in normal MD, or in rerun if Iexp provided) */
    gmx_bool bScaleI0 = FALSE;  /**< Scaling I(q = 0) to target pattern (assumes that the density of the protein is constant upon conformational transitions - useful at low contrast) */
    gmx_bool bDoNotCorrectForceByContrast = FALSE;  /**< Do not scale the forces with factor = contrast / solute-density */
    gmx_bool bDoNotCorrectForceByNindep = FALSE;  /**< Do not scale the forces by number of independet data points */

    gmx_bool bGridDensity = FALSE;  /**< Average density on a grid around the envelope */
    int gridDensityMode = 0;  /**< Specifies which densit is averaged: 0=solute+hydration layer, 1=solute, 2=hydration layer */

    gmx_bool bFixSolventDensity = FALSE;  /**< Fixing the solvent density to a given (experimental) value in waxs-solvdens */
    real givenElecSolventDensity = 0.;  /**< Given electron density of solvent (= 0 means use xtc) (mdp option waxs-solvdens) */
    real solventDensRelErr = 0.;  /**< Relative uncertainty of the solvent density - this can be translated into an uncertainty of I(q), which may enter the E[waxs] */

    /** If the buffer subtraction was reduced by the volume of the solute, we need: */
    gmx_bool bCorrectBuffer = FALSE;  /**< Add back oversubtracted buffer */

    /** Stuff for ensemble refinement */
    int ewaxs_ensemble_type = ewaxsEnsembleNone;  /**< SWAXS ensemble type (none, bayesian_fixed, bayesian_allopt) */
    int ensemble_nstates = 0;  /**< Number of states (in addition to the simulated state) */
    double* ensemble_weights_init = nullptr;  /**< Initial weights */
    double ensemble_weights_fc = 0;  /**< Force constant to restrain weights to initial value */
    double stateWeightTolerance = GMX_WAXS_WEIGHT_TOLERANCE_DEFAULT;  /**< Tolerance for weights, allowing state weights to be <0 or >1, such that weights are [-tol, 1+N*tol -tol]. Default is 0.1. */

    /** More WAXS-MD controls */
    gmx_bool bHaveFittedTraj = FALSE;
    gmx_bool bRotFit = FALSE;
    gmx_bool bMassWeightedFit = TRUE;  /**< Do a mass-weighted fit */
    int verbosityLevel = 1;  /**< Control verbosity */

    /** Variables used for fitting the protein into the envelope */
    gmx_bool bHaveRefFile;

    int npbcatom = 0;  /**< Edited to include several candidates to faciliate a COG calculation thereof. */
    gmx_bool bHaveWholeSolute = FALSE;  /**< Solute is already whole (e.g., due to posres simulation). No need to make select SWAXS PBC atoms */
};

}
