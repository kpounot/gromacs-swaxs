#include <gromacs/fileio/pdbio.h>  // get_coordnum
#include <gromacs/pbcutil/pbc.h>  // PbcType
#include <gromacs/topology/topology.h>  // gmx_mtop_t
#include <gromacs/utility/arrayref.h>  // gmx::ArrayRef
#include <gromacs/utility/smalloc.h>  // snew, sfree
#include <gromacs/topology/mtop_util.h>  // t_atoms, gmx_mtop_global_atoms
#include <gromacs/utility/fatalerror.h>  // FARGS, gmx_fatal
#include <gromacs/utility/futil.h>  // gmx_ffopen, gmx_ffclose

#include <waxs/debug.h> // swaxs_debug
#include <waxs/gmxlib/gmx_envelope.h>  // gmx_envelope_minimumDistance
#include <waxs/types/waxsrec.h>  // t_waxsrec

#include "./pbcshifts.h"

/**
 * Get solvent atoms inside the envelope of solute/solvent and pure-solvent system.
 * Include solvent atoms along one shift along all PBC vectors.
 */
void get_solvation_shell_periodic(gmx::ArrayRef<gmx::RVec> x, gmx::ArrayRef<gmx::RVec> xPureSolvent, t_waxsrec* wr,
        const gmx_mtop_t* mtop, gmx_mtop_t* mtopPureSolvent, PbcType pbcType, const matrix box, matrix boxPureSolvent,
        gmx_bool bWritePdbFile) {
    const int nWarnOutsideMax = 10;

    swaxs_debug("Begin of get_solvation_shell_periodic()\n");

    wr->nAtomsInEnvelopeA = 0;
    wr->nAtomsInEnvelopeB = 0;
    wr->nAtomsShiftedInEnvelopeA = 0;
    wr->nAtomsShiftedInEnvelopeB = 0;

    /* Find solute/protein atoms inside the envelope. Warn if some atoms are outside. */
    setAtomShiftsInsideEnvelope(wr->wt[0].envelope, x, box, wr->nindA_prot, wr->indA_prot, wr->shiftsInsideEnvelope_A_prot);

    int nOutside = 0;

    /* Warn if some solute atoms are outside of the envelope */
    for (int j = 0; j < wr->nindA_prot; j++) {
        /* Periodic images of solute atoms must not be inside the enelope */
        if (wr->shiftsInsideEnvelope_A_prot[j] != 0 and wr->shiftsInsideEnvelope_A_prot[j] != ONE_UI32) {
            fprintf(stderr, "\n\nERROR, a periodic image of solute atom %d is inside the envelope:", wr->indA_prot[j]);
            fprintShiftsOfX(stderr, x[wr->indA_prot[j]], box, TRUE, wr->shiftsInsideEnvelope_A_prot[j]);

            gmx_fatal(FARGS, "A periodic image of solute atom %d is inside the envelope.\n"
                "Maybe your solute has massively expanded, or your envelope is far too large (compared to the box size).\n"
                "See error message above.", wr->indA_prot[j]);
        }

        if (wr->shiftsInsideEnvelope_A_prot[j] == 0) {
            nOutside++;

            if (nOutside <= nWarnOutsideMax) {
                fprintf(stderr, "WARNING, solute atom %d x = (%6.2f %6.2f %6.2f) is outside of the envelope. It will not contribute to I(q).\n",
                        wr->indA_prot[j]+1, x[wr->indA_prot[j]][XX], x[wr->indA_prot[j]][YY],x[wr->indA_prot[j]][ZZ]);
            }

            if (nOutside == nWarnOutsideMax) {
                fprintf(stderr, "       NOTE: Will not report more warnings of solute atoms outside of envelope\n");
            }
        }
    }

    if (nOutside) {
        fprintf(stderr, "WARNING, %d solute atoms outside of envelope\n", nOutside);
    }

    /* Check if solute atoms are close to the envelope, so the solvation layer too thin */
    if (nOutside == 0) {
        gmx_bool bMinDistToOuter;
        int iMindist;
        real mindist;

        gmx_envelope_minimumDistance(wr->wt[0].envelope, x, wr->indA_prot, wr->nindA_prot,
            &mindist, &iMindist, &bMinDistToOuter);

        if (mindist < wr->inputParams->solv_warn_lay) {
            wr->nSolvWarn++;

            fprintf(stderr, "\n*** WARNING ***\n"
                    "Solute atom %d has a distance of only %.3f nm from the %s envelope surface (below the warning threashold of %.3f nm).\n"
                    "You may partly neglect the effect of the hydration layer on the SAXS/SANS curve. Probably, you should\n"
                    "use a larger envelope. If only few atoms are close the enevlope surface (e.g. from a flexible tail), the error\n"
                    "is probably small.",
                    wr->indA_prot[iMindist] + 1, mindist, bMinDistToOuter ? "outer" : "inner", wr->inputParams->solv_warn_lay);
        }
    }

    /* Find solute/protein atoms inside the envelope. Warn if some atoms are outside. */
    setAtomShiftsInsideEnvelope(wr->wt[0].envelope, x, box, wr->nindA_sol, wr->indA_sol, wr->shiftsInsideEnvelope_A_sol);

    /* Get atoms of pure-solvent system inside the envelope. */
    if (wr->bDoingSolvent) {
        setAtomShiftsInsideEnvelope(wr->wt[0].envelope, xPureSolvent, boxPureSolvent,
                wr->nindB_sol, wr->indB_sol, wr->shiftsInsideEnvelope_B);
    }

    if (bWritePdbFile) {
        char file_name[256];
        t_atoms* atoms = nullptr;

        sprintf(file_name, "prot+solvlayer_%d.pdb", wr->waxsStep);
        snew(atoms, 1);

        *atoms = gmx_mtop_global_atoms(mtop);

        writePdbfileIncludingShifts(file_name, "Solute and solvent within the envelope", atoms, as_rvec_array(x.data()),
                pbcType, box, wr->nindA_prot, wr->indA_prot, wr->shiftsInsideEnvelope_A_prot, wr->nindA_sol,
                wr->indA_sol, wr->shiftsInsideEnvelope_A_sol);

        sfree(atoms);

        printf("Wrote %s\n", file_name);

        if (wr->bDoingSolvent) {
            sprintf(file_name, "excludedvolume_%d.pdb", wr->waxsStep);
            snew(atoms, 1);

            *atoms = gmx_mtop_global_atoms(mtopPureSolvent);

            writePdbfileIncludingShifts(file_name, "Excluded solvent within the envelope", atoms,
                    as_rvec_array(xPureSolvent.data()), pbcType, boxPureSolvent, 0, nullptr, nullptr, wr->nindB_sol,
                    wr->indB_sol, wr->shiftsInsideEnvelope_B);

            sfree(atoms);
            printf("Wrote %s\n", file_name);
        }
    }

    swaxs_debug("End of get_solvation_shell_periodic()\n");
}


void shiftRvecByShiftIndex(const rvec x, int s, const matrix box, rvec xShifted) {
    rvec temp;
    copy_rvec(x, xShifted);

    /* s==0 is the central box without shift, so no need to add many zeros */
    if (s > 0) {
        for (int d = 0; d < DIM; d++) {
            svmul(1.0 * waxsBoxShift[s][d], box[d], temp);
            rvec_inc(xShifted, temp);
        }
    }
}

void printAtomShiftBits(FILE* file, uint32_t shiftsInsideEnvelope) {
    char c[N_SWAXS_BOX_SHIFTS + 1];

    for (int s = 0; s < N_SWAXS_BOX_SHIFTS; s++) {
        c[s] = shiftsInsideEnvelope & (ONE_UI32 << s) ? '1' : '0';
    }

    c[N_SWAXS_BOX_SHIFTS] = '\0';

    fprintf(file, "%s", c);
}

void summarizeAtomShifts(FILE* file, int nind, int* index, uint32_t* shiftsInsideEnvelope) {
    int nInside              = 0;  // Atoms inside the envelope
    int nCentralInside       = 0;  //       inside without a shift
    int nShiftedInside       = 0;  //       inside due to a shift
    int multipleShiftsInside = 0;  // At least two periodic images inside the envelope
    int nOutside             = 0;  // not in the envelope

    uint32_t tmp;

    for (int i = 0; i < nind; i++) {
        /** Global atom index */

        #pragma GCC diagnostic ignored "-Wunused-variable"
        int k = index[i];
        #pragma GCC diagnostic pop

        gmx_bool bInEnv         = (shiftsInsideEnvelope[i] != 0);
        gmx_bool bInCentralEnv  = (shiftsInsideEnvelope[i] & (ONE_UI32 << 0));

        // Set bit 0 to zero, to test if other bits are nonzero
        tmp = shiftsInsideEnvelope[i];
        tmp &= ~(ONE_UI32 << 0);
        gmx_bool bShiftedInside = (tmp != 0);

        if (bInEnv) {
            nInside++;
        } else {
            nOutside++;
        }

        if (bInCentralEnv) {
            nCentralInside++;
        }

        if (bShiftedInside) {
            nShiftedInside++;
        }

        if (bInCentralEnv and bShiftedInside) {
            multipleShiftsInside++;
        }
    }

    fprintf(file, "\nStatistics of atom shifts inside the envelope:\n");
    fprintf(file, "\tInside  the envelope        = %d\n", nInside);
    fprintf(file, "\tOutside the envelope        = %d\n", nOutside);
    fprintf(file, "\tShifted atoms in envelope   = %d\n", nShiftedInside);
    fprintf(file, "\tMultiple shifts in enelope  = %d\n\n", multipleShiftsInside);
}

/**
 * Print coordinates of all shifts of given atom coord
 * Also print whether inside of the envelope, if bPrintIsInside
 */
void fprintShiftsOfX(FILE* file, rvec x, const matrix box, gmx_bool bPrintIsInside, uint32_t shiftsInsideEnvelope) {
    for (int s = 0; s < N_SWAXS_BOX_SHIFTS; s++) {
        rvec xShifted;

        shiftRvecByShiftIndex(x, s, box, xShifted);
        fprintf(file, "Shift %2d (%2d %2d %2d) = %7.3f %7.3f %7.3f",
            s, waxsBoxShift[s][XX], waxsBoxShift[s][YY], waxsBoxShift[s][ZZ],
            xShifted[XX], xShifted[YY], xShifted[ZZ]);

        if (bPrintIsInside) {
            int inside = shiftsInsideEnvelope & (ONE_UI32 << s) ? 1 : 0;
            fprintf(file, "  inside = %d\n", inside);
        } else {
            fprintf(file, "\n");
        }
    }
}

/**
 * Fill the bits in shiftsInsideEnvelope, indicating which PBC shifts of solvent atoms
 * are inside the envelope
 */
void setAtomShiftsInsideEnvelope(gmx_envelope* envelope, gmx::ArrayRef<gmx::RVec> x, const matrix box, int nind,
        int* index, uint32_t* shiftsInsideEnvelope) {
    real envMaxR2 = gmx::square(gmx_envelope_maxR(envelope));

    for (int i = 0; i < nind; i++) {
        /* Global atom index */
        int k = index[i];

        /* Clear all bits */
        shiftsInsideEnvelope[i] = 0;

        /* Loop over 27 (3x3x3) possible shift */
        for (int s = 0; s < N_SWAXS_BOX_SHIFTS; s++) {
            rvec xShifted;
            gmx_bool bInside;

            /* Shift along box vectors */
            shiftRvecByShiftIndex(x[k], s, box, xShifted);

            bInside = gmx_envelope_isInside(envelope, xShifted);

            if (norm2(xShifted) > envMaxR2 and bInside) {
                gmx_fatal(FARGS, "Envelope says inside, but diff = %g (maxR = %g). Shift s = %d. This is a bug.\n",
                          sqrt(norm2(xShifted)), sqrt(envMaxR2), s);
            }

            /* Set the s'th bit if shifted atom is inside the envelope */
            if (bInside) {
                /* Set the s'th bit */
                shiftsInsideEnvelope[i] |= (ONE_UI32 << s);
            }
        }
    }
}


/**
 * Write a single PDB line of a given atom number.
 * Put shift id (0..27) into B-factor line.
 */
static void gmx_fprintf_pdb_atomline_i(FILE* fp, t_atoms* atoms, int i, rvec x, int s) {
    /** Pick atomname, residue id and name, chain id */

    int res_index = atoms->atom[i].resind;

    char res_name[6];
    strncpy(res_name, *atoms->resinfo[res_index].name, sizeof(res_name) - 1);
    res_name[sizeof(res_name) - 1] = '\0';

    char atom_name[6];
    strncpy(atom_name, *atoms->atomname[i], sizeof(atom_name) - 1);
    atom_name[sizeof(atom_name) - 1] = '\0';

    int resnr = atoms->resinfo[res_index].nr;

    unsigned char chain_id = atoms->resinfo[res_index].chainid;

    if (chain_id == 0) {
        chain_id = ' ';
    }

    gmx_fprintf_pdb_atomline(fp, epdbATOM, i + 1, atom_name, ' ', res_name, chain_id, resnr, ' ',
            10 * x[XX], 10 * x[YY], 10 * x[ZZ], 1.0, 1.0 * s, atoms->atom[i].elem);
}

/**
 * Write protein + solvation layer or excluded solvent into PDB file
 * When writing excluded solvent, we simply have nSolute == 0 and shiftsInsideEnvelope_solute == nullptr
*/
void writePdbfileIncludingShifts(const char* file_name, const char* title, t_atoms* atoms, rvec* x, PbcType pbcType,
        const matrix box, int nSolute, int* indexSolute, uint32_t* shiftsInsideEnvelope_solute, int nSolvent,
        int* indexSolvent, uint32_t* shiftsInsideEnvelope_solvent) {
    FILE* fp = gmx_ffopen(file_name, "w");
    fprintf(fp, "TITLE     %s\n", title);
    gmx_write_pdb_box(fp, pbcType, box);
    fprintf(fp, "MODEL %8d\n", 1);

    /* Write solute atoms, only for shift 0 (central box) */
    for (int i = 0; i < nSolute; i++) {
        /* Global atom id */
        int k = indexSolute[i];

        /* Solute atoms in envelope (if nSolute>0). No need for shifts. */
        if (shiftsInsideEnvelope_solute[i]) {
            gmx_fprintf_pdb_atomline_i(fp, atoms, k, x[k], 0);
        }
    }

    /* Write solvent, including all shifts inside the envelope */
    for (int s = 0; s < N_SWAXS_BOX_SHIFTS; s++) {
        for (int i = 0; i < nSolvent; i++) {
            rvec xShifted;

            int k = indexSolvent[i];

            /* Check if atom with shift s is inside the envelope */
            if (shiftsInsideEnvelope_solvent[i] & (ONE_UI32 << s)) {
                shiftRvecByShiftIndex(x[k], s, box, xShifted);

                /* Write shifted atom into PDB, put shift ID (0-27) into B-factor column */
                gmx_fprintf_pdb_atomline_i(fp, atoms, k, xShifted, s);
            }
        }
    }

    fprintf(fp, "ENDMDL\n");
    gmx_ffclose(fp);
}

/**
 * Count total number of atoms inside envelope as well as (if nShiftedAtomsInside != nullptr) the number of
 * shifted atoms inside the envelope, that is the number of periodic images inside.
 * Works for protein+hydrationlayer system (nSolute>0), and fore excluded solvent (nSolute==0)
 */
void nAtomsInsideEnvelope(int nSolute, int* shiftsInsideEnvelope_solute, int nSolvent,
        int* shiftsInsideEnvelope_solvent, int* nAtomsInside, int* nShiftedAtomsInside) {
    *nAtomsInside = 0;

    if (nShiftedAtomsInside) {
        *nShiftedAtomsInside = 0;
    }

    for (int i = 0; i < nSolute; i++) {
        /* Solute atoms: No need to consider shifts. */
        if (shiftsInsideEnvelope_solute[i]) {
            (*nAtomsInside)++;
        }
    }

    for (int i = 0; i < nSolvent; i++) {
        for (int s = 0; s < N_SWAXS_BOX_SHIFTS; s++) {
            if (shiftsInsideEnvelope_solvent[i] & (ONE_UI32 << s)) {
                (*nAtomsInside)++;
                if (s > 0 and nShiftedAtomsInside) {
                    (*nShiftedAtomsInside)++;
                }
            }
        }
    }
}

static void getMinMaxOfCoordinates(rvec* x, int nAtoms, int* index, int* shiftsInsideEnvelope, matrix box, rvec min, rvec max) {
    for (int d = 0; d < DIM; d++) {
        min[d] = -1e20;
        max[d] = 1e20;
    }

    for (int s = 0; s < N_SWAXS_BOX_SHIFTS; s++) {
        for (int i = 0; i < nAtoms; i++) {
            rvec xShifted;

            /* Global atom index */
            int k = index[i];

            /* Check if atom with shift s is inside the envelope */
            if (shiftsInsideEnvelope[i] & (ONE_UI32 << s)) {
                shiftRvecByShiftIndex(x[k], s, box, xShifted);

                for (int d = 0; d < DIM; d++) {
                    if (xShifted[d] < min[d]) {
                        min[d] = xShifted[d];
                    }

                    if (xShifted[d] > max[d]) {
                        max[d] = xShifted[d];
                    }
                }
            }
        }
    }
}

#define GMX_WAXS_SOLVENT_ROUGHNESS 0.4

void check_selected_solvent_periodic(
        gmx::ArrayRef<gmx::RVec> xA,
        gmx::ArrayRef<gmx::RVec> xB,
        int nSolventA,
        int nSolventB,
        int *indexSolventA,
        int *indexSolventB,
        matrix boxA,
        matrix boxB,
        int *shiftsInsideEnvelope_solvent_A,
        int *shiftsInsideEnvelope_solvent_B,
        gmx_bool bFatal) {
    rvec maxA, minA, maxB, minB;

    auto xARaw = as_rvec_array(xA.data());
    auto xBRaw = as_rvec_array(xB.data());

    /* find smallest and largest coordinate of solvation layer */
    getMinMaxOfCoordinates(xARaw, nSolventA, indexSolventA, shiftsInsideEnvelope_solvent_A,
                           boxA, minA, maxA);


    /* find smallest and largest coordinate of excluded solvent */
    getMinMaxOfCoordinates(xBRaw, nSolventB, indexSolventB, shiftsInsideEnvelope_solvent_B,
                           boxB, minB, maxB);


    for (int d = 0; d < DIM; d++) {
        if (fabs(maxA[d] - maxB[d]) > GMX_WAXS_SOLVENT_ROUGHNESS or fabs(minA[d] - minB[d]) > GMX_WAXS_SOLVENT_ROUGHNESS) {
            char buf[1024];
            const char dimletter[3][2] = { "x", "y", "z" };

            sprintf(buf, "Solvation layer and excluded solvent are different in direction %s\n"
                    "Found minA / minB = %g / %g -- maxA / maxB = %g / %g\n",
                    dimletter[d], minA[d], minB[d], maxA[d], maxB[d]);

            if (bFatal) {
                gmx_fatal(FARGS, "%s", buf);
            } else {
                fprintf(stderr, "\n\nWARNING - WARNING - WARNING \n\n%s\n\nWARNING - WARNING - WARNING\n\n", buf);
            }
        }
    }
}
