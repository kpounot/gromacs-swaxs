#include "./sftypeio.h"

#include <cstring>  // strcpy, strncmp

#include <gromacs/gmxpreprocess/notset.h>  // NOTSET
#include <gromacs/math/vec.h>  // clear_rvec
#include <gromacs/utility/fatalerror.h>  // FARGS, gmx_fatal
#include <gromacs/utility/smalloc.h>  // snew, srenew, sfree

#include <waxs/types/waxstop.h>  // t_scatt_types
#include <waxs/types/waxsrec.h>  // t_waxsrec

/* This section contains all base structure factor operations. */

void print_cmsf(FILE* out, t_cromer_mann* cm, int nTab) {
    for (int i = 0; i < nTab; i++) {
        fprintf(out, "\t");
    }

    for (int i = 0; i < 4; i++) {
        fprintf(out, " %8g", cm->a[i]);
    }

    for (int i = 0; i < 4; i++) {
        fprintf(out, " %8g", cm->b[i]);
    }

    fprintf(out, " %8g\n", cm->c);
}

void init_scattering_types(t_scatt_types* t) {
    t->nCromerMannParameters = 0;
    t->nNeutronScatteringLengths = 0;

    t->cromerMannParameters = nullptr;
    t->neutronScatteringLengths = nullptr;
}

void done_scattering_types(t_scatt_types* t) {
    sfree(t->cromerMannParameters);
    sfree(t->neutronScatteringLengths);

    t->nCromerMannParameters = 0;
    t->nNeutronScatteringLengths = 0;
}

#if 0
/* A shorthand way of setting individual factors */
static t_cromer_mann cromer_mann(real a[], real b[], real c) {
    t_cromer_mann cm;

    for (int i = 0; i < 4; i++) {
        cm.a[i] = a[i];
        cm.b[i] = b[i];
    }

    cm.c = c;

    return cm;
}
#endif

static int is_eq_cmsf(t_cromer_mann* x, t_cromer_mann* y) {
    const double eps = 1e-5;

    for (int i = 0; i < 4; i++) {
        if (fabs(x->a[i] - y->a[i]) > eps or fabs(x->b[i] - y->b[i]) > eps) {
            return FALSE;
        }
    }

    if (fabs(x->c - y->c) > eps) {
        return FALSE;
    }

    return TRUE;
}

static int is_eq_nsl(t_neutron_sl* x, t_neutron_sl* y) {
    const double eps = 1e-6;

    if (fabs(x->cohb - y->cohb) > eps) {
        return FALSE;
    }

    return TRUE;
}

/* Look for the Cromer-Mann types cm_new in the list scattTypes */
int search_scattTypes_by_cm(const t_scatt_types* scattTypes, t_cromer_mann* cm_new) {
    if (cm_new == nullptr) {
        gmx_fatal(FARGS, "Trying to search structure-factor table using a null-CMSF!\n");
    }

    for (int i = 0; i < scattTypes->nCromerMannParameters; i++) {
        if (is_eq_cmsf(&scattTypes->cromerMannParameters[i], cm_new)) {
            return i;
        }
    }

    return NOTSET;
}

/* Look for the NSL types nsl_new in the list scattTypes */
int search_scattTypes_by_nsl(const t_scatt_types* scattTypes, t_neutron_sl* nsl_new) {
    if (nsl_new == nullptr) {
        gmx_fatal(FARGS, "Trying to search structure-factor table, but nsl_new = nullptr!\n");
    }

    for (int i = 0; i < scattTypes->nNeutronScatteringLengths; i++) {
        if (is_eq_nsl(&scattTypes->neutronScatteringLengths[i], nsl_new)) {
            return i;
        }
    }

    return NOTSET;
}


int add_scatteringType(t_scatt_types* scattTypes, t_cromer_mann* cm, t_neutron_sl* nsl) {
    if (cm and nsl) {
        gmx_fatal(FARGS, "Can only add a Cromer-Mann or a NSL in add_scatteringType\n");
    }

    if (cm) {
        int icm = scattTypes->nCromerMannParameters;

        scattTypes->nCromerMannParameters++;
        srenew(scattTypes->cromerMannParameters, scattTypes->nCromerMannParameters);

        scattTypes->cromerMannParameters[icm] = *cm;

        return icm;
    } else if (nsl) {
        int insl = scattTypes->nNeutronScatteringLengths;

        scattTypes->nNeutronScatteringLengths++;
        srenew(scattTypes->neutronScatteringLengths, scattTypes->nNeutronScatteringLengths);

        scattTypes->neutronScatteringLengths[insl] = *nsl;

        return insl;
    }

    return -1;
}

t_waxsrecType init_t_waxsrecType() {
    t_waxsrecType t;

    t.type = -1;
    t.fc = 0;
    t.nq = 0;
    t.minq = t.maxq = 0;
    t.nShannon = 0;
    t.qvecs = nullptr;
    t.Ipuresolv = nullptr;
    t.Iexp = nullptr;
    t.Iexp_sigma = nullptr;
    t.f_ml = t.c_ml = 0;
    t.targetI0 = 0;
    t.ensemble_I = nullptr;
    t.ensemble_Ivar = nullptr;
    t.ensembleSum_I = nullptr;
    t.ensembleSum_Ivar = nullptr;
    t.wd = nullptr;
    strcpy(t.saxssansStr, "");

    t.atomEnvelope_scatType_A = nullptr ;
    t.atomEnvelope_scatType_B = nullptr ;

    t.naff_table = 0;
    t.aff_table = nullptr;

    t.vLast = 0;
    t.fLast = nullptr;

    t.nnsl_table = 0;
    t.nsl_table = nullptr;
    t.deuter_conc = -1;

    t.envelope = nullptr;
    t.givenSolventDensity = 0;
    t.soluteSumOfScattLengths = 0;
    t.soluteContrast = 0;
    t.contrastFactor = 0;

    t.n2HAv_A  = 0;
    t.n1HAv_A  = 0;
    t.nHydAv_A = 0;
    t.n2HAv_B  = 0;
    t.n1HAv_B  = 0;
    t.nHydAv_B = 0;

    return t;
}

t_waxsrec* init_t_waxsrec(void)
{
    t_waxsrec* t = new t_waxsrec;

    t->x_ref = nullptr;

    /* Make origin 0,0,0 - this is where the protein is eventually shifted
       to fit into the envelope */
    clear_rvec(t->origin);
    t->w_fit = nullptr;

    t->wt = nullptr;
    t->bHaveNindep = FALSE;

    t->nSolvWarn = 0;
    t->nWarnCloseBox = 0;
    t->soluteVolAv = 0;

    t->solElecDensAv = 0;
    t->nElectrons = nullptr;
    t->nElecAddedA = t->nElecAddedB = 0.;
    t->nElecTotA = 0.;
    t->nElecProtA = 0.;

    t->epsilon_buff = 1;
    t->rng = nullptr;

    t->waxsStep = 0;
    t->nAtomsExwaterAver = t->nAtomsLayerAver = 0;
    t->nElecAvA = t->nElecAv2A = t->nElecAv4A = 0.;
    t->nElecAvB = t->nElecAv2B = t->nElecAv4B = 0.;
    t->solElecDensAv = t->solElecDensAv2 = t->solElecDensAv4 = 0.;
    t->RgAv = 0;

    t->isizeA = t->isizeB = 0;
    t->pbcatoms = nullptr;

    t->atomEnvelope_coord_A = t->atomEnvelope_coord_B = nullptr;

    t->indexA = t->indexB = nullptr;
    t->indexA_nalloc = t->indexB_nalloc = 0;

    t->ensemble_weights = nullptr;

    t->indA_prot = nullptr;
    t->indA_sol = nullptr;
    t->indB_sol = nullptr;
    t->nindA_prot = 0;
    t->nindA_sol = 0;
    t->nindB_sol = 0;
    t->nSoluteMols = 0;

    t->shiftsInsideEnvelope_A_prot = nullptr;
    t->shiftsInsideEnvelope_A_sol = nullptr;
    t->shiftsInsideEnvelope_B = nullptr;

    t->ind_RotFit = nullptr;
    t->nind_RotFit = 0;

    t->vLast = 0;
    t->fLast = nullptr;

    t->solvent = nullptr;
    t->local_state = nullptr;

    return t;
}

void done_t_waxsrec(t_waxsrec* t) {
    /* This function is totally incomplete, but we don't really need it anyway */

    if (t->x_ref) sfree(t->x_ref);
    if (t->w_fit) sfree(t->w_fit);
    if (t->pbcatoms) sfree(t->pbcatoms);

    sfree(t->indexA);
    if (t->indexB) sfree(t->indexB);
    if (t->indA_prot) sfree(t->indA_prot);
    if (t->indA_sol) sfree(t->indA_sol);
    if (t->indB_sol) sfree(t->indB_sol);
    if (t->ind_RotFit) sfree(t->ind_RotFit);

    delete t;
    t = nullptr;
}

int countGroups(const SimulationGroups& groups, SimulationAtomGroupType groupType) {
    int n = groups.groups[groupType].size();

    if (n == 1) {
        /* Can be either all or nothing. */
        if (strncmp("rest", *groups.groupNames[groups.groups[groupType][0]], 4) != 0) {
            /* Found real group. */
            n++;
        } else {
            /* Is FAKE. */
        }
    }

    if (n <= 0) {
        gmx_fatal(FARGS, "Got impossible (negative) number of groups. Terminating.\n");
    }

    return n - 1;  /** Subtracts the fake group. */
}
