#include <cstdio>  // FILE, fprintf
#include <ctime>  // clock_gettime, CLOCK_MONOTONIC_RAW, timespec
#include <vector>  // std::vector

#include <gromacs/mdtypes/commrec.h>  // t_commrec
#include <gromacs/utility/basedefinitions.h>  // gmx_bool, TRUE, FALSE
#include <gromacs/utility/cstringutil.h>  // STRLEN
#include <gromacs/utility/fatalerror.h>  // gmx_fatal, FARGS

#include "./timing.h"  // swaxs::Timing

static const char* timingNames[static_cast<int>(swaxs::Timing::Stage::Count)] = {
    "SWAXS step",
    "Solute preparation",
    "Solvent preparation",
    "Scattering amplitudes",
    "One scattering amplitude",
    "grad I(q)",
    "Solvent Fourier Tr",
    "Gibbs sampling",
    "Potential/forces",
    "Updates",
    "Compute I / grad I(q)",
    "Solvent density corr",
};

swaxs::Timing::Timing() {
    size = static_cast<int>(swaxs::Timing::Stage::Count);

    nMeasurements = std::vector<int>(size);

    sumTimes              = std::vector<double>(size);
    tStartThisMeasurement = std::vector<double>(size);
    lastTime              = std::vector<double>(size);

    currentlyMeasuring = std::vector<gmx_bool>(size);
}

void swaxs::Timing::start(int waxsStep, Stage stage, const t_commrec& cr) {
    /** Timing only done on master, and after a few steps to reduce the bias from
        initial Fourier transforms */

    if ((waxsStep < SWAXS_STEPS_RESET_TIME_AVERAGES) or !MASTER(&cr)) {
        return;
    }

    int stageIndex = static_cast<int>(stage);

    if (currentlyMeasuring[stageIndex]) {
        gmx_fatal(FARGS, "Trying to start a time measurement of %s, but I am measuring already\n", timingNames[stageIndex]);
    }

    struct timespec now;

    clock_gettime(CLOCK_MONOTONIC_RAW, &now);

    double now_sec = 1.0 * now.tv_sec + now.tv_nsec * 1.0e-9;

    tStartThisMeasurement[stageIndex] = now_sec;
    currentlyMeasuring[stageIndex] = TRUE;
}

void swaxs::Timing::end(int waxsStep, Stage stage, const t_commrec& cr) {
    /** Timing only done on master, and after a few steps to reduce the bias from
        initial Fourier transforms */

    if ((waxsStep < SWAXS_STEPS_RESET_TIME_AVERAGES) or !MASTER(&cr)) {
        return;
    }

    int stageIndex = static_cast<int>(stage);

    if (!currentlyMeasuring[stageIndex]) {
        gmx_fatal(FARGS, "Trying to finish a time measurement of %s, but I am currently not measuring it.\n",
                timingNames[stageIndex]);
    }

    struct timespec now;
    clock_gettime(CLOCK_MONOTONIC_RAW, &now);

    double nowSec = 1.0 * now.tv_sec + now.tv_nsec * 1.0e-9;
    double timeDeltaSec = nowSec - tStartThisMeasurement[stageIndex];

    sumTimes[stageIndex] += timeDeltaSec;
    lastTime[stageIndex] = timeDeltaSec;
    nMeasurements[stageIndex]++;
    currentlyMeasuring[stageIndex] = FALSE;
}

void swaxs::Timing::add(int waxsStep, Stage stage, double seconds, const t_commrec& cr) {
    /** Timing only done on master, and after a few steps to reduce the bias from
        initial Fourier transforms */

    if ((waxsStep < SWAXS_STEPS_RESET_TIME_AVERAGES) or !MASTER(&cr)) {
        return;
    }

    if (seconds == 0) {
        gmx_fatal(FARGS, "Error while storing computing time, found time = %g\n", seconds);
    }

    int stageIndex = static_cast<int>(stage);

    if (currentlyMeasuring[stageIndex]) {
        gmx_fatal(FARGS, "Trying to add a time for measurement of %s, but I am measuring already.\n", timingNames[stageIndex]);
    }

    sumTimes[stageIndex] += seconds;
    nMeasurements[stageIndex]++;
    lastTime[stageIndex] = seconds;
}

void swaxs::Timing::write(FILE* fp) {
    char buf[STRLEN];

    fprintf(fp, "\n=============== Computing time Statistics ===============\n");

    for (int stageIndex = 0; stageIndex < size; ++stageIndex) {
        /** Write average compute time in milliseconds */
        sprintf(buf, "%-30s (n = %4d) [ms]", timingNames[stageIndex], nMeasurements[stageIndex]);

        // waxs::print2log(fp, buf, "%g", (sumTimes[stageIndex] / nMeasurements[stageIndex]) * 1000.0);
    }

    fprintf(fp, "=========================================================\n");
}

void swaxs::Timing::writeLast(FILE* fp) {
    fprintf(fp, "\nLast compute times:\n");

    for (int stageIndex = 0; stageIndex < size; ++stageIndex) {
        /** Write average computing time in milliseconds */
        fprintf(fp, "%-30s (last of %5d) [ms] = %10.3g\n", timingNames[stageIndex], nMeasurements[stageIndex],
                lastTime[stageIndex] * 1000.0);
    }

    fprintf(fp, "\n");
}
