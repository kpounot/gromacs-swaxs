#include "./swaxs_output.h"

#include <cstring>  // strlen, strncpy

#include <gromacs/fileio/trrio.h>  // gmx_trr_open
#include <gromacs/fileio/xvgr.h>  // xvgropen
#include <gromacs/math/functions.h>  // gmx::square
#include <gromacs/statistics/statistics.h>  // lsq_y_ax_b_error
#include <gromacs/utility/cstringutil.h>  // STRLEN
#include <gromacs/utility/futil.h>  // gmx_ffopen, gmx_ffclose
#include <gromacs/utility/smalloc.h>  // snew, sfree

#include <waxs/envelope/grid_density.h>  // envelope_grid_density_write
#include <waxs/envelope/volume.h>  // swaxs::envelope_volume
#include <waxs/gmxlib/swaxs_datablock.h>  // waxs_datablock
#include <waxs/gmxlib/waxsmd_utils.h>  // print2log
#include <waxs/types/enums.h>  // ewaxsEnsemble_BayesianOneRefined, ewaxsWeightsUNIFORM, escatterNEUTRON, ewaxsIexpFit_NO
#include <waxs/types/waxsrec.h>  // t_waxsrec

#define WAXS_WARN_BOX_DIST 0.5

#define WAXS_ENSEMBLE(x) (x->inputParams->ensemble_nstates > 1)

swaxs::SwaxsOutput::SwaxsOutput(t_waxsrec* wr, const char* fnOut, const gmx_output_env_t* oenv) {
    swaxs::InputParams& inputParams = *(wr->inputParams);

    /** Get base name of output (without .dat/.xvg extension) */
    int len = strlen(fnOut);
    char* base;
    snew(base, len);
    strncpy(base, fnOut, len - 4);
    base[len - 4] = '\0';
    fprintf(stderr, "WAXS-MD: Base file name for output: %s\n", base);

    /**
     * Output that needs only one output file for all scattering types
     */
    const int lenNameExt = 50;

    snew(fnLog, len + lenNameExt);
    snew(fnDensity, len + lenNameExt);

    sprintf(fnLog, "%s.log", base);
    sprintf(fnDensity, "%s_density.dat", base);

    fpLog = gmx_ffopen(fnLog, "w");

    if (inputParams.bRotFit) {
        fprintf(fpLog, "NB: WAXS-rotational fit has been turned on.\n");
    }

    /** Output that needs one output file for each scattering type */
    snew(fnSpectra,              inputParams.nTypes);
    snew(fnStddevs,              inputParams.nTypes);
    snew(fnFinal,                inputParams.nTypes);
    snew(fnContrib,              inputParams.nTypes);
    snew(fnNindep,               inputParams.nTypes);
    snew(fnD,                    inputParams.nTypes);
    snew(fnForces,               inputParams.nTypes);
    snew(fnSpecEns,              inputParams.nTypes);
    snew(fnAvgA,                 inputParams.nTypes);
    snew(fnAvgB,                 inputParams.nTypes);
    snew(fnPot,                  inputParams.nTypes);
    snew(fnGibbsEnsW,            inputParams.nTypes);
    snew(fnGibbsSolvDensRelErr,  inputParams.nTypes);

    snew(fpSpectra,              inputParams.nTypes);
    snew(fpStddevs,              inputParams.nTypes);
    snew(fpNindep,               inputParams.nTypes);
    snew(fpD,                    inputParams.nTypes);
    snew(fpForces,               inputParams.nTypes);
    snew(fpSpecEns,              inputParams.nTypes);
    snew(fpAvgA,                 inputParams.nTypes);
    snew(fpAvgB,                 inputParams.nTypes);
    snew(fpPot,                  inputParams.nTypes);
    snew(xfout,                  inputParams.nTypes);
    snew(fpGibbsEnsW,            inputParams.nTypes);
    snew(fpGibbsSolvDensRelErr,  inputParams.nTypes);

    for (int t = 0; t < inputParams.nTypes; t++) {
        char typeStr[STRLEN];

        if (inputParams.nTypes > 1) {
            sprintf(typeStr, "_%d", t);
        } else {
            strcpy(typeStr, "");
        }

        snew(fnSpectra[t], len + lenNameExt);
        snew(fnStddevs[t], len + lenNameExt);
        snew(fnFinal  [t], len + lenNameExt);
        snew(fnContrib[t], len + lenNameExt);
        snew(fnNindep [t], len + lenNameExt);
        snew(fnD      [t], len + lenNameExt);
        snew(fnForces [t], len + lenNameExt);
        snew(fnSpecEns[t], len + lenNameExt);
        snew(fnAvgA   [t], len + lenNameExt);
        snew(fnAvgB   [t], len + lenNameExt);
        snew(fnPot    [t], len + lenNameExt);
        snew(fnGibbsEnsW[t], len + lenNameExt);
        snew(fnGibbsSolvDensRelErr[t], len + lenNameExt);

        sprintf(fnSpectra            [t], "%s_spectra%s.xvg",          base, typeStr);
        sprintf(fnSpecEns            [t], "%s_spectra_ensemlbe%s.xvg", base, typeStr);
        sprintf(fnStddevs            [t], "%s_stddevs%s.xvg",          base, typeStr);
        sprintf(fnFinal              [t], "%s_final%s.xvg",            base, typeStr);
        sprintf(fnContrib            [t], "%s_contrib%s.xvg",          base, typeStr);
        sprintf(fnNindep             [t], "%s_nindep%s.xvg",           base, typeStr);
        sprintf(fnD                  [t], "%s_averageD%s.xvg",         base, typeStr);
        sprintf(fnForces             [t], "%s_forces%s.trr",           base, typeStr);
        sprintf(fnAvgA               [t], "%s_averageA%s.xvg",         base, typeStr);
        sprintf(fnAvgB               [t], "%s_averageB%s.xvg",         base, typeStr);
        sprintf(fnPot                [t], "%s_pot%s.xvg",              base, typeStr);
        sprintf(fnGibbsEnsW          [t], "%s_ensembleWeights%s.xvg",  base, typeStr);
        sprintf(fnGibbsSolvDensRelErr[t], "%s_solvDensUncert%s.xvg",   base, typeStr);

        char title[STRLEN];

        sprintf(title, "%s curves", wr->wt[t].saxssansStr);
        fpSpectra[t] = xvgropen(fnSpectra[t], title, "q [nm\\S-1\\N]", "I [e\\S2\\N]", oenv);

        if (inputParams.bPrintForces) {
            xfout[t] = gmx_trr_open(fnForces[t], "w");
            fprintf(fpLog, "NB: Printing coordinates and forces at each waxs-step (scattering type %d, %s).\n",
                    t, wr->wt[t].saxssansStr);
        }

        if (WAXS_ENSEMBLE(wr)) {
            sprintf(title, "%s curves of ensemble", wr->wt[t].saxssansStr);
            fpSpecEns[t] = xvgropen(fnSpecEns[t], title, "q [nm\\S-1\\N]", "I [e\\S2\\N]", oenv);
        }

        if (inputParams.ewaxs_ensemble_type == ewaxsEnsemble_BayesianOneRefined) {
            fpGibbsEnsW[t] = xvgropen(fnGibbsEnsW[t], "Ensemble weights", "t [ps]", "weights", oenv);
        }

        if (inputParams.bBayesianSolvDensUncert) {
            sprintf(title, "Relative solvent density uncertainty");
            fpGibbsSolvDensRelErr[t] = xvgropen(fnGibbsSolvDensRelErr[t],
                    "Relative solvent density uncertainty", "t [ps]", "\\xdr\\f{}\\sbuf\\N / \\xr\\f{}\\sbuf", oenv);
        }

        if (inputParams.bCalcPot) {
            fprintf(stderr, "Writing %s potentials for each q into %s (scattering type %d) \n",
                    wr->wt[t].saxssansStr, fnPot[t], t);
            sprintf(title, "%s potentials at individual q's", wr->wt[t].saxssansStr);
            fpPot[t] = xvgropen(fnPot[t], title, "t [ps]", "E [kJ mol\\S-1\\N]", oenv);

            real dq = (wr->wt[t].nq > 1) ? (wr->wt[t].maxq - wr->wt[t].minq) / (wr->wt[t].nq - 1) : 0.0;

            for (int i = 0; i < wr->wt[t].nq; i++) {
                fprintf(fpPot[t], "@ s%d legend \"q = %.2f\"\n", i, wr->wt[t].minq + dq * i);
            }
        }

        if (inputParams.weightsType != ewaxsWeightsUNIFORM) {
            /* Open file with standard deviations of intensities */
            sprintf(title, "Stddevs entering %s potential", wr->wt[t].saxssansStr);
            fpStddevs[t] = xvgropen(fnStddevs[t], title, "q [nm\\S-1\\N]", "\\xs\\f{} [e\\S2\\N]", oenv);

            int i = 0;

            fprintf(fpStddevs[t], "@ s%d legend \"\\xs\\f{}(I\\scalc\\N)\"\n", i++);

            if (wr->wt[t].Iexp_sigma) {
                fprintf(fpStddevs[t], "@ s%d legend \"\\xs\\f{}(I\\sexp\\N)\"\n", i++);
            }

            if (inputParams.solventDensRelErr) {
                fprintf(fpStddevs[t], "@ s%d legend \"\\xs\\f{}(I\\sbuf\\N)\"\n", i++);
            }
        }
    }

    sfree(base);
}

/**
 * Write I vs. q to grace file, and optionally the error.
 * With bVariance == TRUE, take sqrt of error before writing the error column.
 * fact(= 1/sqrt(N)) allows to switch from stddev to stddev / sqrt(N)
 */
static void print_Ivsq(FILE* fp, t_waxsrecType* wt, double* I, double* Ierror, gmx_bool bVariance, double fact, const char* type) {
    if (type) {
        fprintf(fp, "%s\n", type);
    }

    real dq = (wt->nq > 1) ? (wt->maxq - wt->minq) / (wt->nq - 1) : 0.0;

    for (int i = 0; i < wt->nq; i++) {
        fprintf(fp, "%12g  %12g", wt->minq + i * dq, fact * I[i]);

        if (Ierror) {
            real err = bVariance ? sqrt(Ierror[i]) : Ierror[i];
            fprintf(fp, "  %12g\n", err);
        } else {
            fprintf(fp, "\n");
        }
    }

    fprintf(fp, "&\n");
}

static void write_averages(t_waxsrecType* wt, const char* fnAvgA, const char* fnAvgB) {
    /** Write Average Scattering amplitude A(vec{q}) */
    FILE* fp = gmx_ffopen(fnAvgA, "w");

    fprintf(fp, "# %10s = %d\n", "nabs", wt->qvecs->nabs);
    fprintf(fp, "# %10s %12s %12s  %12s %12s %12s %12s %12s %12s\n", "qx", "qy", "qz", "< Re A >", "< Im A >",
            "< |A|^2 >", "< |A|^4 >", "< (Re A)^2 >", "< (Im A)^2 >");

    for (int i = 0; i < wt->qvecs->nabs; i++) {
        fprintf(fp, "# %12s = %d\n", "indexofqabs", i);
        fprintf(fp, "# %12s = %10g\n", "qabs", wt->qvecs->abs[i]);
        fprintf(fp, "# %12s = %d\n", "nofqvec", wt->qvecs->ind[i + 1] - wt->qvecs->ind[i]);

        for (int j = wt->qvecs->ind[i]; j < wt->qvecs->ind[i + 1]; j++) {
            fprintf(fp, "%12g %12g %12g  %12g %12g %12g %12g %12g %12g\n", wt->qvecs->q[j][XX], wt->qvecs->q[j][YY],
                    wt->qvecs->q[j][ZZ], wt->wd->avAglobal[j].re, wt->wd->avAglobal[j].im, wt->wd->avAsqglobal[j],
                    wt->wd->avA4global[j], wt->wd->av_ReA_2global[j], wt->wd->av_ImA_2global[j]);
        }

        fprintf(fp, "\n\n");
    }

    printf("Wrote all averages of scattering amplitudes of System A to %s\n", fnAvgA);
    gmx_ffclose(fp);

    if (fnAvgB) {
        fp = gmx_ffopen(fnAvgB, "w");
        fprintf(fp, "# %10s = %d\n", "nabs", wt->qvecs->nabs);
        fprintf(fp, "# %8s %10s %10s  %10s %10s %10s %10s %10s %10s\n", "qx", "qy", "qz", "< Re B >", "< Im B >",
                "< |B|^2 >", "< |B|^4 >", "< (Re B)^2 >", "< (Im B)^2 >");

        for (int i = 0; i < wt->qvecs->nabs; i++) {
            fprintf(fp, "# %10s = %d\n", "indexofqabs", i);
            fprintf(fp, "# %10s = %10g\n", "qabs", wt->qvecs->abs[i]);
            fprintf(fp, "# %10s = %d\n", "nofqvec", wt->qvecs->ind[i + 1] - wt->qvecs->ind[i]);

            for (int j = wt->qvecs->ind[i]; j < wt->qvecs->ind[i + 1]; j++) {
                fprintf(fp, "%12g %12g %12g  %10g %10g %10g %10g %10g %10g\n", wt->qvecs->q[j][XX], wt->qvecs->q[j][YY],
                        wt->qvecs->q[j][ZZ], wt->wd->avBglobal[j].re, wt->wd->avBglobal[j].im, wt->wd->avBsqglobal[j],
                        wt->wd->avB4global[j], wt->wd->av_ReB_2global[j], wt->wd->av_ImB_2global[j]);
            }

            fprintf(fp, "\n\n");
        }

        printf("Wrote all averages of scattering amplitudes of System B to %s\n", fnAvgB);
        gmx_ffclose(fp);
    }
}

static real droplet_volume(t_waxsrec* wr) {
    return swaxs::envelope_volume(wr->wt[0].envelope);
}

static double guinierFit(t_waxsrecType* wt, double* I, double* varI, double RgSolute) {
    const double qRgMinDefault = 1.2;  /**< Fitting up to q = 1.2/Rg */

    if (RgSolute <= 0) {
        printf("WARNING, cannot do Guinier fit, because the aporoximate radius of gyration is unknown\n");
        return -1;
    }

    real dq = (wt->nq > 1) ? (wt->maxq - wt->minq) / (wt->nq - 1) : 0.0;

    if (dq == 0.0) {
        printf("\n\nNOTE: cannot do Guinier fit, because only %d intensity points available in total\n", wt->nq);
        return -1;
    }

    double qRgMin = qRgMinDefault;

    char* buf;

    if ((buf = getenv("GMX_WAXS_GUINIER_FIT_MAX")) != nullptr) {
        qRgMin = atof(buf);
        printf("Found GMX_WAXS_GUINIER_FIT_MAX: Doing Guinier fit up to q*Rg = %g\n", qRgMin);
    }

    int n = 0;

    while (wt->minq + n * dq <= qRgMin / RgSolute and n < wt->nq) {
        n++;
    }

    if (n > wt->nq) {
        n = wt->nq;
    }

    if (n < 2) {
        printf("\n\nNOTE: cannot do Guinier fit, because only %d intensity points available up to q = %g\n", n,
                qRgMin / RgSolute);
        return -1;
    } else if (n == 2) {
        printf("\n\nNOTE: only %d intensity points available for Guinier fit up to q = %g\n", n, qRgMin / RgSolute);
    }

    real* x;
    real* y;
    real* yerr;
    snew(x, n);
    snew(y, n);
    snew(yerr, n);

    gmx_bool bHaveErrors = TRUE;

    for (int i = 0; i < n; i++) {
        if (varI[i] <= 0.) {
            bHaveErrors = FALSE;
            break;
        }
    }

    for (int i = 0; i < n; i++) {
        x[i] = gmx::square(wt->minq + i * dq);
        y[i] = log(I[i]);
        yerr[i] = bHaveErrors ? sqrt(varI[i]) / I[i] : 1.0;
    }

    /** Linear least-square fit y = ax + b */
    real a; real b; real da; real db; real r;
    real chi2;  /**< Unused */
    if (lsq_y_ax_b_error(n, x, y, yerr, &a, &b, &da, &db, &r, &chi2) != 0) {
        printf("WARNING, least-square fit for Guinier fit failed\n");

        return -1;
    }

    real Rg = sqrt(-3 * a);
    real dRg = -3 * da / sqrt(-3 * a);
    real I0 = exp(b);

    if (bHaveErrors) {
        printf("\nGuinier fit: Rg [nm]   = %g +- %g", Rg, dRg);
        printf("\n             I(0) [e2] = %g +- %g", I0, exp(b) * db);
    } else {
        printf("\nGuinier fit: Rg [nm]   = %g", Rg);
        printf("\n             I(0) [e2] = %g", I0);
    }

    printf("\n             (from %d intensity points up to q = %g)\n\n", n, qRgMin / RgSolute);

    if (r < 0.95) {
        printf("\nWARNING, the Guinier fit was not successful, found a correlation on only %g\n", r);
    }

    sfree(x);
    sfree(y);
    sfree(yerr);

    return Rg;
}

void swaxs::SwaxsOutput::writeOut(t_waxsrec* wr, const gmx_output_env_t* oenv) {
    swaxs::InputParams& inputParams = *(wr->inputParams);

    fprintf(stderr, "\nClosing WAXS-MD output\n\n");

    for (int t = 0; t < inputParams.nTypes; t++) {
        t_waxsrecType* wt = &wr->wt[t];
        waxs_datablock* wd = wt->wd;

        real dq = (wt->nq > 1) ? (wt->maxq - wt->minq) / (wt->nq - 1) : 0.0;

        gmx_ffclose(fpSpectra[t]);

        if (fpStddevs[t]) {
            gmx_ffclose(fpStddevs[t]);
        }
        if (fpSpecEns[t]) {
            gmx_ffclose(fpSpecEns[t]);
        }

        if (inputParams.bCalcPot) {
            fprintf(stderr,    "\nWriting average %s potentials into %s, group %d\n", wt->saxssansStr, fnLog, t);
            fprintf(fpLog, "\n\n### Average %s-%d Potentials\n", wt->saxssansStr, t);
            fprintf(fpLog, "@    title \"%s potentials: average and stddev\"\n", wt->saxssansStr);
            fprintf(fpLog, "@    xaxis  label \"q [nm\\S-1\\N]\"\n");
            fprintf(fpLog, "@    yaxis  label \"E [kJ mol\\S-1\\N]\"\n");
            fprintf(fpLog, "@type xydy\n");

            for (int i = 0; i < wt->nq; i++) {
                /** Get standard deviation */
                double stddev = wd->vAver2[i] - gmx::square(wd->vAver[i]);
                stddev = ((stddev > 0.) ? sqrt(stddev) : 0.);
                fprintf(fpLog, "%8g  %10g  %10g\n", wt->minq + i * dq, wd->vAver[i], stddev);
            }

            fprintf(fpLog, "### Use: sed -n '/Average %s-%d Pot/,/End average %s-%d/p' < %s\n", wt->saxssansStr, t,
                    wt->saxssansStr, t, fnLog);
            fprintf(fpLog, "### End average %s-%d Potentials\n\n", wt->saxssansStr, t);
        }

        /** Write final spectrum and target spectrum to xxx_final.xvg */
        fprintf(stderr, "Writing final spectrum to %s (group %s %d)\n", fnFinal[t], wt->saxssansStr, t);

        char title[STRLEN];
        FILE* fp;

        sprintf(title, "Final %s intensity", wt->saxssansStr);
        fp = xvgropen(fnFinal[t], title, "q [nm\\S-1\\N]", "Intensity [e\\S2\\N]", oenv);

        if (wt->type == escatterNEUTRON) {
            fprintf(fp, "@    subtitle \"Deuterium concentration %g %%\"\n", wt->deuter_conc * 100);
        }

        fprintf(fp, "@    s0 legend  \"Final intensity\"\n");
        print_Ivsq(fp, wt, wd->I, wd->varI, TRUE, 1., "@type xydy");

        if (wt->Iexp) {
            /** Also write the target curve */
            fprintf(fp, "@    s1 legend  \"Target intensity\"\n@type xydy\n");
            print_Ivsq(fp, wt, wt->Iexp, wt->Iexp_sigma, FALSE, 1, "@type xydy");
            fprintf(stderr, "Wrote target intensity to %s\n", fnFinal[t]);

            if (inputParams.ewaxs_Iexp_fit != ewaxsIexpFit_NO) {
                /** Write target curve corrected by maximum likelihood estimates scale f and offset c */
                fprintf(fp, "@    s2 legend  \"ML Target\"\n@type xydy\n");
                fprintf(fp, "@type xydy\n");

                for (int i = 0; i < wt->nq; i++) {
                    fprintf(fp, "%12g  %12g  %12g\n", wt->minq + i * dq, wt->f_ml * wt->Iexp[i] + wt->c_ml,
                            wt->f_ml * wt->Iexp_sigma[i]);
                }

                fprintf(fp, "&\n");
                fprintf(stderr, "Wrote maximum-likelihood-scaled target intensity to %s\n", fnFinal[t]);
            }
        }

        gmx_ffclose(fp);

        /** Write D(vec{q}), see eq. 10 of Chen/Hub, Biophys J, 2014 */
        fp = gmx_ffopen(fnD[t], "w");

        fprintf(fp, "# Average scattering amplitudes D(vec{q}), that is the buffer-subtracted\n"
                "# scattering intensity before doing the orientational average\n#\n");
        fprintf(fp, "# %8s %10s %10s  %10s\n", "qx", "qy", "qz", "Intensity(vec{q})");

        for (int i = 0; i < wt->qvecs->nabs; i++) {
            fprintf(fp, "# iq %2d  --  |q| = %g\n", i, wt->qvecs->abs[i]);

            for (int j = wt->qvecs->ind[i]; j < wt->qvecs->ind[i + 1]; j++) {
                fprintf(fp, "%12g %12g %12g  %10g\n", wt->qvecs->q[j][XX], wt->qvecs->q[j][YY], wt->qvecs->q[j][ZZ],
                        wt->wd->Dglobal[j]);
            }

            fprintf(fp, "\n\n");
        }

        printf("Wrote all scattering intensities D(q) to %s\n", fnD[t]);
        gmx_ffclose(fp);

        /** Write averages of A and B into files */
        write_averages(&wr->wt[t], fnAvgA[t], inputParams.bVacuum ? nullptr : fnAvgB[t]);

        /** Write file with contributions to I(q) */
        fprintf(stderr, "Writing contributions to I(q) to %s (group %s %d)\n", fnContrib[t], wt->saxssansStr, t);

        sprintf(title, "Contributions to %s pattern", wt->saxssansStr);
        fp = xvgropen(fnContrib[t], title, "q [nm\\S-1\\N]", "Intensity [e\\S2\\N]", oenv);

        if (wt->type == escatterNEUTRON) {
            fprintf(fp, "@    subtitle \"Deuterium concentration %g %%\"\n", wt->deuter_conc * 100);
        }

        if (!inputParams.bVacuum) {
            fprintf(fp,
                    "@    s0 legend  \"<|A(q)|\\S2\\N>\"\n"
                    "@    s1 legend  \"<|B(q)|\\S2\\N>\"\n"
                    "@    s2 legend  \"2Re[ -<B\\S*\\N(q)> <A(q) - B(q)> ]\"\n"
                    "@    s3 legend  \"Re <A(|q|)>\"\n"
                    "@    s4 legend  \"Im <A(|q|)>\"\n"
                    "@    s5 legend  \"Re <B(|q|)>\"\n"
                    "@    s6 legend  \"Im <B(|q|)>\"\n"
                    "@    s7 legend  \"|<A-B>|\\S2\"\n"
                    "@    s8 legend  \"var(A)\"\n"
                    "@    s9 legend  \"var(B)\"\n");

            int s = 9;

            if (inputParams.bScaleI0) {
                fprintf(fp, "@    s%d legend  \"2Re[ \\xd\\f{}A\\S*\\N<A-B>] + |\\xd\\f{}A|\\S2\\N\"\n", ++s);
            }

            if (inputParams.bCorrectBuffer) {
                fprintf(fp, "@    s%d legend  \"I\\sbuffcorr\\N\"\n", ++s);
            }

            if (wd->I_errSolvDens) {
                fprintf(fp, "@    s%d legend  \"\\xD\\f{}I\\ssolvdens\\N\"\n", ++s);
            }

            for (int i = 0; i <= s; i++) {
                fprintf(fp, "@    s%d errorbar size 0.250000\n", i);
            }

            /** For these, we have devided by the # of frames already in calculate_I_dkI()
            Therefore, use factor of 1.0 instead of f. */
            print_Ivsq(fp, wt, wd->IA,         wd->varIA,       TRUE, 1., "@type xydy");
            print_Ivsq(fp, wt, wd->IB,         wd->varIB,       TRUE, 1., "@type xydy");
            print_Ivsq(fp, wt, wd->Ibulk,      wd->varIbulk,    TRUE, 1., "@type xydy");
            print_Ivsq(fp, wt, wd->avAqabs_re, nullptr,         TRUE, 1., "@type xy");
            print_Ivsq(fp, wt, wd->avAqabs_im, nullptr,         TRUE, 1., "@type xy");
            print_Ivsq(fp, wt, wd->avBqabs_re, nullptr,         TRUE, 1., "@type xy");
            print_Ivsq(fp, wt, wd->avBqabs_im, nullptr,         TRUE, 1., "@type xy");
            print_Ivsq(fp, wt, wd->I_avAmB2,   wd->varI_avAmB2, TRUE, 1., "@type xydy");
            print_Ivsq(fp, wt, wd->I_varA,     wd->varI_varA,   TRUE, 1., "@type xydy");
            print_Ivsq(fp, wt, wd->I_varB,     wd->varI_varB,   TRUE, 1., "@type xydy");

            if (inputParams.bScaleI0) {
                print_Ivsq(fp, wt, wd->I_scaleI0, nullptr, TRUE, 1., "@type xy");
            }

            if (inputParams.bCorrectBuffer and t == 0) {
                /** Ipuresolv presently only available with one scattering group */
                print_Ivsq(fp, wt, wt->Ipuresolv, nullptr, TRUE, wr->soluteVolAv, "@type xy");
            }

            if (wd->I_errSolvDens) {
                print_Ivsq(fp, wt, wd->I_errSolvDens, nullptr, TRUE, 1., "@type xy");
            }
        } else {
            fprintf(fp, "@    s0 legend  \"<|A(q)|\\S2\\N>\"\n"
                    "@    s1 legend  \"Re <A(|q|)>\"\n"
                    "@    s2 legend  \"Im <A(|q|)>\"\n");
            print_Ivsq(fp, wt, wd->IA, wd->varIA, TRUE, 1, "@type xydy");
            print_Ivsq(fp, wt, wd->avAqabs_re, nullptr, TRUE, 1, "@type xy");
            print_Ivsq(fp, wt, wd->avAqabs_im, nullptr, TRUE, 1, "@type xy");
        }

        gmx_ffclose(fp);
        fp = nullptr;

        if (inputParams.bPrintNindep and wd->Nindep) {
            fp = xvgropen(fnNindep[t], "# of independent I(q)", "q [nm\\S-1\\N]", "# independent", oenv);
            print_Ivsq(fp, wt, wd->Nindep, nullptr, TRUE, 1., "@type xy");
            gmx_ffclose(fp);
        }

        if (fpPot[t]) {
            gmx_ffclose(fpPot[t]);
        }

        if (inputParams.bPrintForces) {
            gmx_trr_close(xfout[t]);
        }
    }

    /** Write computation time statistics to log */
    wr->timing->write(fpLog);

    fprintf(fpLog, "\n\n======== WAXS MD STATISTICS ======\n");
    swaxs::print2log(fpLog, "Nr of waxs steps", "%d", wr->waxsStep);
    swaxs::print2log(fpLog, "Average nr of solvation shell atoms", "%g", wr->nAtomsLayerAver);
    swaxs::print2log(fpLog, "Average nr of excluded volume atoms", "%g", wr->nAtomsExwaterAver);

    swaxs::print2log(fpLog, "\nNr of electrons in solute", "%g", wr->nElecProtA);
    swaxs::print2log(fpLog, "Average nr of electrons in A", "%g", wr->nElecAvA);
    swaxs::print2log(fpLog, "Stddev  nr of electrons in A", "%g", sqrt(wr->nElecAv2A - gmx::square(wr->nElecAvA)));
    swaxs::print2log(fpLog, "Stddev/sqrt(N) nr of electrons in A", "%g",
            sqrt(wr->nElecAv2A - gmx::square(wr->nElecAvA)) / sqrt(wr->waxsStep));
    swaxs::print2log(fpLog, "Average nr^2  of electrons in A", "%g", wr->nElecAv2A);
    swaxs::print2log(fpLog, "Stddev (nr^2) of electrons in A", "%g", sqrt(wr->nElecAv4A - gmx::square(wr->nElecAv2A)));
    swaxs::print2log(fpLog, "Stddev/sqrt(N) (nr^2) of electrons in A", "%g",
            sqrt(wr->nElecAv4A - gmx::square(wr->nElecAv2A)) / sqrt(wr->waxsStep));

    if (!inputParams.bVacuum) {
        swaxs::print2log(fpLog, "Approximate av. volume of solute (nm3)", "%g", wr->soluteVolAv);
        swaxs::print2log(fpLog, "Approximate density of solute (e/nm3)", "%g", wr->nElecProtA / wr->soluteVolAv);
        swaxs::print2log(fpLog, "Volume of envelope (nm3)", "%g", droplet_volume(wr));
        swaxs::print2log(fpLog, "\nAverage nr of electrons in B", "%g", wr->nElecAvB);
        swaxs::print2log(fpLog, "Stddev  nr of electrons in B", "%g", sqrt(wr->nElecAv2B - gmx::square(wr->nElecAvB)));
        swaxs::print2log(fpLog, "Stddev/sqrt(N) nr of electrons in B", "%g",
                sqrt(wr->nElecAv2B - gmx::square(wr->nElecAvB)) / sqrt(wr->waxsStep));
        swaxs::print2log(fpLog, "Average  nr^2  of electrons in B", "%g", wr->nElecAv2B);
        swaxs::print2log(fpLog, "Stddev  (nr^2) of electrons in B", "%g", sqrt(wr->nElecAv4B - gmx::square(wr->nElecAv2B)));
        swaxs::print2log(fpLog, "Stddev/sqrt(N) (nr^2) of electrons in B", "%g",
                sqrt(wr->nElecAv4B - gmx::square(wr->nElecAv2B)) / sqrt(wr->waxsStep));
    }

    swaxs::print2log(fpLog, "\nAverage electron density in A NOT protein+solvation layer (e/nm^3)", "%g", wr->solElecDensAv);
    swaxs::print2log(fpLog, "Stddev  electron density in A NOT protein+solvation layer", "%g",
            sqrt(wr->solElecDensAv2 - gmx::square(wr->solElecDensAv)));
    swaxs::print2log(fpLog, "Stddev/sqrt(N) electron density in A NOT protein+solvation layer", "%g",
            sqrt(wr->solElecDensAv2 - gmx::square(wr->solElecDensAv)) / sqrt(wr->waxsStep));

    real compwater = 18.01528 / 6.002214129;

    swaxs::print2log(fpLog, "This corresponds to a pure H2O density of (kg/m^3)", "%g", compwater * wr->solElecDensAv);
    swaxs::print2log(fpLog, "Stddev     pure H2O density", "%g",
            sqrt(wr->solElecDensAv2 * compwater * compwater - gmx::square(wr->solElecDensAv * compwater)));
    swaxs::print2log(fpLog, "Stddev/sqrt(N)  pure H2O density", "%g",
            sqrt(wr->solElecDensAv2 * compwater * compwater - gmx::square(wr->solElecDensAv * compwater)) / sqrt(wr->waxsStep));

    swaxs::print2log(fpLog, "\nAverage (electron density)^2 in A NOT protein+solvation layer", "%g", wr->solElecDensAv2);
    swaxs::print2log(fpLog, "Stddev  (electron density)^2 in A NOT protein+solvation layer", "%g",
            sqrt(wr->solElecDensAv4 - gmx::square(wr->solElecDensAv2)));
    swaxs::print2log(fpLog, "Stddev/sqrt(N) (electron density)^2 in A NOT protein+solvation layer", "%g",
            sqrt(wr->solElecDensAv4 - gmx::square(wr->solElecDensAv2)) / sqrt(wr->waxsStep));

    swaxs::print2log(fpLog, "\nElectron density in the bulk of A [e/nm3]", "%g", wr->solElecDensAv);

    if (!inputParams.bVacuum) {
        swaxs::print2log(fpLog, "Electron density in the bulk of B [e/nm3]", "%g", wr->solElecDensAv_SysB);
    }

    if (inputParams.bFixSolventDensity) {
        swaxs::print2log(fpLog, "\nSolvent density was fixed to [e/nm3]:", "%g", inputParams.givenElecSolventDensity);
        fprintf(fpLog, "Average # of electrons added to solvent in A: ");
        fprintf(fpLog, "%10g (added density [e/nm3] = %g)\n", wr->nElecAddedA,
                inputParams.givenElecSolventDensity - wr->solElecDensAv);
        fprintf(fpLog, "Average # of electrons added to solvent in B: %10g (added density [e/nm3] = %g)\n\n",
                wr->nElecAddedB, inputParams.givenElecSolventDensity - wr->solElecDensAv_SysB);
    } else {
        fprintf(fpLog, "\nSovlent density was simply from the the xtc file and not fixed.\n");
    }

    for (int t = 0; t < inputParams.nTypes; t++) {
        fprintf(fpLog, "\nScattering group %d (%s)", t, wr->wt[t].saxssansStr);

        if (wr->wt[t].type == escatterNEUTRON) {
            fprintf(fpLog, ", %g %% D2O\n", 100 * wr->wt[t].deuter_conc);
        } else {
            fprintf(fpLog, "\n");
        }

        swaxs::print2log(fpLog, "Average number of electrons or NSL in A", "%g", wr->wt[t].wd->avAsum);
        swaxs::print2log(fpLog, "Average number of electrons or NSL in B", "%g", wr->wt[t].wd->avBsum);
        swaxs::print2log(fpLog, "Solvent density (electrons or NSL per nm3)", "%g", wr->solvent->averageScatteringLengthDensity[t]);
        swaxs::print2log(fpLog, "Approx. contrast between solute and buffer (electrons or NSL per nm3)", "%g",
                wr->wt[t].soluteContrast);
        swaxs::print2log(fpLog, "Contrast", "%s", (wr->wt[t].wd->avAsum > wr->wt[t].wd->avBsum) ? "positive" : "negative");

        if (wr->wt[t].wd->Nindep) {
            swaxs::print2log(fpLog, "Max. nr of independent q per |q|", "%g", wr->wt[t].wd->Nindep[wr->wt[t].nq - 1]);
        }
    }

    for (int t = 0; t < inputParams.nTypes; t++) {
        if (wr->wt[t].type == escatterNEUTRON) {
            fprintf(fpLog, "\nScattering group %d (Neutron), statistics on deuteratable hydrogens:\n", t);
            swaxs::print2log(fpLog, "Average number of hydrogen atoms in A", "%g", wr->wt[t].nHydAv_A);
            swaxs::print2log(fpLog, "Average number of     deuterated in A", "%g", wr->wt[t].n2HAv_A);
            swaxs::print2log(fpLog, "Average number of not deuterated in A", "%g", wr->wt[t].n1HAv_A);
            swaxs::print2log(fpLog, "Average fraction deuterated      in A", "%g", wr->wt[t].n2HAv_A / wr->wt[t].nHydAv_A);
            swaxs::print2log(fpLog, "Average number of hydrogen atoms in B", "%g", wr->wt[t].nHydAv_B);
            swaxs::print2log(fpLog, "Average number of     deuterated in B", "%g", wr->wt[t].n2HAv_B);
            swaxs::print2log(fpLog, "Average number of not deuterated in B", "%g", wr->wt[t].n1HAv_B);
            swaxs::print2log(fpLog, "Average fraction deuterated      in B", "%g", wr->wt[t].n2HAv_B / wr->wt[t].nHydAv_B);
        }
    }

    rvec cent;
    real r2;
    gmx_envelope_bounding_sphere(wr->wt[0].envelope, cent, &r2);
    swaxs::print2log(fpLog, "\nDiameter of the bounding envelope [nm]", "%g", 2 * sqrt(r2));
    swaxs::print2log(fpLog, "Average radius of gyration (solute only, electron-weighted) [nm]", "%g", wr->RgAv);

    /** Guinier fit of final intensity */
    for (int t = 0; t < inputParams.nTypes; t++) {
        double RgGuinier = guinierFit(&wr->wt[t], wr->wt[t].wd->I, wr->wt[t].wd->varI, wr->RgAv);

        if (inputParams.nTypes == 1) {
            swaxs::print2log(fpLog, "Final radius of gyration (Guiner fit) [nm]", "%g", RgGuinier);
        } else {
            char title[STRLEN];
            sprintf(title, "Final radius of gyration (Guiner fit) [nm] (%s group %d)", wr->wt[t].saxssansStr, t);
            swaxs::print2log(fpLog, title, "%g", RgGuinier);
        }
    }

    for (int t = 0; t < inputParams.nTypes; t++) {
        char title[STRLEN];
        sprintf(title, "Number of Shannon channels (%s group %d)", wr->wt[t].saxssansStr, t);
        swaxs::print2log(fpLog, title, "%g", wr->wt[t].nShannon);
    }

    fprintf(fpLog, "\nNr of frames with a solvation layer thinner than %g: %d of %d\n",
            inputParams.solv_warn_lay, wr->nSolvWarn, wr->waxsStep);
    fprintf(fpLog, "Nr of frames with a distance to the box boundary smaller than %g: %d of %d\n",
            WAXS_WARN_BOX_DIST, wr->nWarnCloseBox, wr->waxsStep);
    fprintf(fpLog, "\n");

    if (inputParams.bGridDensity) {
        swaxs::envelope_grid_density_write(wr->wt[0].envelope, fnDensity);
    }
}
