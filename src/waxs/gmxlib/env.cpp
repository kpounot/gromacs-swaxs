#include <cstdio>  // fprintf, stderr
#include <cstdlib>  // atof, getenv
#include <cstring>  // strcasecmp

#include <gromacs/gmxlib/network.h>  // gmx_bcast
#include <gromacs/mdtypes/commrec.h>  // t_commrec, MASTER, PAR
#include <gromacs/utility/basedefinitions.h>  // FALSE, gmx_bool, real, TRUE
#include <gromacs/utility/fatalerror.h>  // FARGS, gmx_fatal

#include <waxs/types/waxsrec.h>  // t_waxsrec

#include "./env.h"

static gmx_bool readBool(const char* name) {
    char* rawValue = nullptr;

    if ((rawValue = getenv(name)) == nullptr) {
        return FALSE;
    }

    if (!strcasecmp("FALSE", rawValue) or !strcasecmp("NO", rawValue) or !strcasecmp("0", rawValue)) {
        printf("WAXS-MD: Found environment variable %s set to FALSE\n", name);

        return FALSE;
    } else if (!strcasecmp("TRUE", rawValue) or !strcasecmp("YES", rawValue) or !strcasecmp("1", rawValue)) {
        printf("WAXS-MD: Found environment variable %s set to TRUE\n", name);

        return TRUE;
    } else {
        gmx_fatal(FARGS, "Found boolean environment variable %s, rawValue should be false, \n"
                "true, yes, no, 1, or 0 (case-insensitive), but found rawValue: \"%s\"\n", name, rawValue);
    }
}

static gmx_bool readBoolWithDefault(const char* name, gmx_bool defaultValue, const char* description) {
    if (getenv(name) == nullptr) {
        return defaultValue;
    }

    gmx_bool value = readBool(name);

    printf("WAXS-MD: Found boolean environment variable %s = %s", name, value ? "true" : "false");

    if (description) {
        printf(" - %s%s\n", value ? "" : "NOT ", description);
    } else {
        printf("\n");
    }

    return value;
}

void swaxs::readEnvironmentVariables(t_waxsrec* swaxs, t_commrec* comm) {
    if (MASTER(comm)) {
        swaxs->inputParams->bDoNotCorrectForceByContrast = readBoolWithDefault("GMX_WAXS_DONT_CORRECT_FORCE_BY_CONTRAST", FALSE,
                "Do not correct SAXS/SANS-derived forces by the contrast.");
        swaxs->inputParams->bDoNotCorrectForceByNindep = readBoolWithDefault("GMX_WAXS_DONT_CORRECT_FORCE_BY_NINDEP", FALSE,
                "Do not multiply SAXS/SANS-derived forces/potential by the number of independent data points.");

        char* rawValue = nullptr;

        if ((rawValue = getenv("GMX_WAXS_JMIN")) != nullptr) {
            swaxs->inputParams->Jmin = atoi(rawValue);
            fprintf(stderr, "\nWAXS-MD: Found environment variable GMX_WAXS_JMIN = %d\n", swaxs->inputParams->Jmin);
        }

        if ((rawValue = getenv("GMX_WAXS_J_ALPHA")) != nullptr) {
            swaxs->inputParams->Jalpha = atof(rawValue);
            fprintf(stderr, "\nWAXS-MD: Found environment variable GMX_WAXS_J_ALAPHA = %g\n", swaxs->inputParams->Jalpha);
        }

        swaxs->inputParams->bHaveFittedTraj = readBool("GMX_WAXS_HAVE_FITTED_TRAJ");

        if (swaxs->inputParams->bHaveFittedTraj) {
            fprintf(stderr, "\nWAXS-MD:\nI am not fitting solute, and not shifting water into the box.\n"
                    " Probably, you are doing a rerun with a rotation-fitted trajectory\n\n");
        }

        swaxs->inputParams->bRotFit = (readBool("GMX_WAXS_NOROTFIT") == FALSE);

        if (swaxs->inputParams->bRotFit) {
            fprintf(stderr, "\nWAXS-MD:\nWill rotate the box at each waxs calculation."
                    "This is intended to test the action of fitted forces...\n\n");
        }

        swaxs->inputParams->bMassWeightedFit = (readBoolWithDefault("GMX_WAXS_NONWEIGHTED_FIT", FALSE, "doing non-mass-weighted fit") == FALSE);

        swaxs->x_ref_file = getenv("GMX_WAXS_FIT_REFFILE");
        swaxs->inputParams->bHaveRefFile = (swaxs->x_ref_file != nullptr);

        if (swaxs->inputParams->bHaveRefFile) {
            fprintf(stderr, "Using coordinates from %s as WAXS-MD fit coordinates.\n", swaxs->x_ref_file);
        }

        swaxs->inputParams->bPrintForces = readBool("GMX_WAXS_PRINTFORCES") and swaxs->inputParams->bCalcForces;

        if (swaxs->inputParams->bPrintForces) {
            fprintf(stderr, "\nWAXS-MD:\n Will print coordinates and forces on WAXS-solute atoms."
                    "This may take up a lot of space in your storage!\n\n");
        }

        swaxs->inputParams->bPrintNindep = readBoolWithDefault("GMX_WAXS_PRINT_NINDEP", FALSE,
                "Writing number of independent data points to file.");

        if ((rawValue = getenv("GMX_WAXS_VERBOSE")) != nullptr) {
            swaxs->inputParams->verbosityLevel = atoi(rawValue);
        }

        fprintf(stderr, "\nWAXS-MD: WAXS verbosity level = %d\n\n", swaxs->inputParams->verbosityLevel);

        if ((rawValue = getenv("GMX_WAXS_BEGIN")) != nullptr) {
            swaxs->inputParams->calcWAXS_begin = atof(rawValue);
            fprintf(stderr, "\nWAXS-MD: Found environment variable GMX_WAXS_BEGIN = %f\n", swaxs->inputParams->calcWAXS_begin);
        }

        if ((rawValue = getenv("GMX_WAXS_END")) != nullptr) {
            swaxs->inputParams->calcWAXS_end = atof(rawValue);
            fprintf(stderr, "\nWAXS-MD: Found environment variable GMX_WAXS_END = %f\n", swaxs->inputParams->calcWAXS_end);
        }

        if ((rawValue = getenv("GMX_WAXS_WEIGHT_TOLERANCE")) != nullptr) {
            swaxs->inputParams->stateWeightTolerance = atof(rawValue);
            fprintf(stderr, "\nWAXS-MD: Found environment variable GMX_WAXS_WEIGHT_TOLERANCE = %g\n",
                    swaxs->inputParams->stateWeightTolerance);
        }

        swaxs->inputParams->bGridDensity = readBoolWithDefault("GMX_WAXS_GRID_DENSITY", FALSE, "computing electron density on a grid.");

        if ((rawValue = getenv("GMX_WAXS_GRID_DENSITY_MODE")) != nullptr) {
            swaxs->inputParams->gridDensityMode = atoi(rawValue);
            fprintf(stderr, "\nWAXS-MD: Found environment variable GMX_WAXS_GRID_DENSITY_MODE = %d\n", swaxs->inputParams->gridDensityMode);
        }

        if ((rawValue = getenv("BACKBONE_DEUTERATED_PROB")) != nullptr) {
            swaxs->inputParams->backboneDeuterProb = atof(rawValue);
            fprintf(stderr, "\nWAXS-MD: Found environment variable BACKBONE_DEUTERATED_PROB, backbone deuterated with probability %g\n",
                    swaxs->inputParams->backboneDeuterProb);

            if (swaxs->inputParams->backboneDeuterProb < 0 or swaxs->inputParams->backboneDeuterProb > 1) {
                gmx_fatal(FARGS, "Environment variable BACKBONE_DEUTERATED_PROB must be between 0 and 1 (found %g)\n",
                        swaxs->inputParams->backboneDeuterProb);
            }
        }

        swaxs->inputParams->bHaveWholeSolute = readBoolWithDefault("GMX_WAXS_SOLUTE_IS_WHOLE", FALSE, "Will not make solute whole.");
        swaxs->inputParams->bStochasticDeuteration = readBoolWithDefault("GMX_WAXS_STOCHASTIC_DEUTERATION", FALSE, "");

        fprintf(stderr, "\n");
    }

    if (PAR(comm)) {
        gmx_bcast(sizeof(int),      &swaxs->inputParams->Jmin, comm->mpi_comm_mygroup);
        gmx_bcast(sizeof(real),     &swaxs->inputParams->Jalpha, comm->mpi_comm_mygroup);
        gmx_bcast(sizeof(gmx_bool), &swaxs->inputParams->bHaveFittedTraj, comm->mpi_comm_mygroup);
        gmx_bcast(sizeof(gmx_bool), &swaxs->inputParams->bDoNotCorrectForceByContrast, comm->mpi_comm_mygroup);
        gmx_bcast(sizeof(gmx_bool), &swaxs->inputParams->bDoNotCorrectForceByNindep, comm->mpi_comm_mygroup);
        gmx_bcast(sizeof(gmx_bool), &swaxs->inputParams->bRotFit, comm->mpi_comm_mygroup);
        gmx_bcast(sizeof(gmx_bool), &swaxs->inputParams->bPrintForces, comm->mpi_comm_mygroup);
        gmx_bcast(sizeof(gmx_bool), &swaxs->inputParams->bScaleI0, comm->mpi_comm_mygroup);
        gmx_bcast(sizeof(int),      &swaxs->inputParams->verbosityLevel, comm->mpi_comm_mygroup);
        gmx_bcast(sizeof(double),   &swaxs->inputParams->calcWAXS_begin, comm->mpi_comm_mygroup);
        gmx_bcast(sizeof(double),   &swaxs->inputParams->calcWAXS_end, comm->mpi_comm_mygroup);
        gmx_bcast(sizeof(gmx_bool), &swaxs->inputParams->bGridDensity, comm->mpi_comm_mygroup);
        gmx_bcast(sizeof(gmx_bool), &swaxs->inputParams->bHaveWholeSolute, comm->mpi_comm_mygroup);
        gmx_bcast(sizeof(real),     &swaxs->inputParams->backboneDeuterProb, comm->mpi_comm_mygroup);
        gmx_bcast(sizeof(gmx_bool), &swaxs->inputParams->bStochasticDeuteration, comm->mpi_comm_mygroup);
        gmx_bcast(sizeof(double),   &swaxs->inputParams->stateWeightTolerance, comm->mpi_comm_mygroup);
    }
}
