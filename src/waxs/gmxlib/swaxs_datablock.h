#pragma once

#include <gromacs/math/gmxcomplex.h>  // t_complex
#include <gromacs/math/vec.h>  // dvec

#include <waxs/gmxcomplex.h>  // t_complex_d

/**
 * Large structure that keeps all the averages specific to one scattering type (such
 * as X-ray scattering, or Neutron scattering with a specific D2O concentration
 */
struct waxs_datablock {
    /** Scattering amplitude of the present step, size qhomenr */
    t_complex_d* A;
    t_complex_d* B;

    /** The gradients
     *  grad[k] A(q) = i . q . f[k](q) . exp(iq.r[k])
     *     require lots of memory. Therefore, we only store [ i . f[k](q) . exp(iq.r[k]) ]* and
     *     multipy by q later.
     */

    t_complex* dkAbar;  /** size qhomenr * nind_prot */

    /**
     * Note on precision:
     *  - dkAbar is done in real
     *  - All averages are done in double
     */

    double normA, normB;  /** Norm for exponential or equally-weighted averaging.
                              N(n) = 1 + f + f^2 + ... f^n, where f=exp(-deltat / tau)
                              Use <A(n)> = N(n)^[-1] [ A(n) + f.<A(n-1)> ] */

    t_complex_d* avA;      /** <A>, size qhomenr     */
    t_complex_d* avAcorr;  /** Correction added to <A> to scale I(q=0) to a given value
                               if waxsrec->inputParams->bScaleI0, see mdp option waxs-scalei0 */
    t_complex_d* avB;      /** <B>, size qhomenr     */

    double* avAsq;          /** <|A|^2>, size qhomenr */
    double* avBsq;          /** <|B|^2>, size qhomenr */
    double* re_avBbar_AmB;  /** Re < B* . (A-B) > */
    double* D;              /** size qhomenr */
    double* DerrSolvDens;   /** Uncertainty of D(q) due to uncertainty in solvent density */

    double* Dglobal;         /** D        of size qvecs->n - for Master for output only */
    t_complex_d* avAglobal;  /** avA      of size qvecs->n - for Master for output only */
    double* avAsqglobal;     /** avAsq    of size qvecs->n - for Master for output only */
    double* avA4global;      /** avA4     of size qvecs->n - for Master for output only */
    double* av_ReA_2global;  /** av_ReA_2 of size qvecs->n - for Master for output only */
    double* av_ImA_2global;  /** av_ImA_2 of size qvecs->n - for Master for output only */
    t_complex_d* avBglobal;  /** avB      of size qvecs->n - for Master for output only */
    double* avBsqglobal;     /** avBsq    of size qvecs->n - for Master for output only */
    double* avB4global;      /** avB4     of size qvecs->n - for Master for output only */
    double* av_ReB_2global;  /** av_ReB_2 of size qvecs->n - for Master for output only */
    double* av_ImB_2global;  /** av_ImB_2 of size qvecs->n - for Master for output only */

    double* avA4;      /** < |A|^4 >    - to compute errors */
    double* avB4;      /** < |B|^4 >    - to compute errors */
    double* av_ReA_2;  /** < (Re A)^2 > - to compute errors */
    double* av_ImA_2;  /** < (Im A)^2 > - to compute errors */
    double* av_ReB_2;  /** < (Re B)^2 > - to compute errors */
    double* av_ImB_2;  /** < (Im B)^2 > - to compute errors */

    /**
     * Orientationa average of Re[ grad[k](A*) (A-<B>) ].
     * Explanation: In eq. 13, of Chen and Hub, Biophys J 2015 (http://dx.doi.org/10.1016/j.bpj.2015.03.062), we
     * use that <B(q)> does not change after a while any more. Hence, we replace everything within the square brackets by
     * < grad[k](A*) (A-<B>) >_t
     * -> All operations (Real part, time average, and orientational average) can be done in an arbitrary order.
     * -> Do the orientational average immediately, before doing the time average.
     * -> No need to save the huge array grad[k](A*)(vec[q])
     * -> Same memory, so we can do the calculation on the GPU.
     */
    dvec* Orientational_Av;

    /** Byte blocks and offsets for communications. */
    int64_t nByteAlloc;  /** Memory allocated to store averages (on this node) */

    int* masterlist_qhomenr;  /** qstart and qhomenr kept on the Master node. Required for checkpointing */
    int* masterlist_qstart;

    /* The SAXS/SANS intensities, size nIvalues */
    int nIvalues;  /** # intensity values stored. nabs with isotropic patterns, otherwise more */
    double* I;     /** SAXS/SANS inentities  - size nIvalues */
    dvec* dkI;     /** grad[k] I(q)          - size nIvalues * nind_prot */

    /**
     * All of the following intensities, variances, etc. are only for computing errors and for
     * reporting the contributions to the SAXS/SANS curves, as provided by waxs_contrib.xvg
     */
    double* IA, * IB;       /** Intensity from only A or B atoms, respectively */
    double* Ibulk;          /** Intensity from coupling between solute/excluded solvent and bulk water Ibulk = 2Re [ -<B*>.<A-B> ] */
    double* I_avAmB2;       /** Intensities from the three terms in eq. 26 of Park et al. */
    double* I_varA;         /** D = |<A-B>|^2 + var(A) - var(B), size nIvalues */
    double* I_varB;
    double* I_errSolvDens;  /** Uncertainty in I(q) due to uncertainty of the solvent density */
    double* I_scaleI0;      /** Additional intensity due to scaling of I(q = 0) */
    double* Nindep;         /** For each |q|: number of statistically independent D(q), or "effective sample size */

    double* varIA, * varIB;  /** There respective variances - to compute errors */
    double* varIbulk, * varI;
    double* varI_avAmB2, * varI_varA, * varI_varB;
    double* avAqabs_re, * avAqabs_im;  /** < A(|q|) >, size nIvalues */
    double* avBqabs_re, * avBqabs_im;  /** < B(|q|) >, size nIvalues */

    double* vAver, * vAver2;  /** Average WAXS potential vs q:  < V[waxs] (q) > */

    /** Used to compute the total contrast between A and B system, used to test if we have overall positive or negative contrast */
    double avAsum;  /** average total number of electrons/NSLs of A system */
    double avBsum;  /** average total number of electrons/NSLs of B system */
};
