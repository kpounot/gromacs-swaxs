#pragma once

#include <gromacs/math/vec.h>  // rvec
#include <gromacs/math/vectypes.h>  // matrix
#include <gromacs/mdtypes/commrec.h>  // t_commrec
#include <gromacs/pbcutil/pbc.h>  // PbcType
#include <gromacs/topology/topology.h>  // gmx_mtop_t
#include <gromacs/utility/basedefinitions.h>  // gmx_bool
#include <gromacs/utility/real.h>  // real

#include <waxs/envelope/envelope.h>  // gmx_envelope
#include <waxs/gmxlib/gmx_envelope.h>
#include <waxs/gmxlib/sftypeio.h>  // t_waxsrec*
#include <waxs/types/waxsrec.h>  // swaxs::Solvent

void get_cog(gmx::ArrayRef<gmx::RVec> x, int* index, int n, rvec cent);

void mv_boxcenter_to_rvec(int natoms, gmx::ArrayRef<gmx::RVec> x, rvec cent, matrix box);

void mv_cog_to_rvec(int natoms, gmx::ArrayRef<gmx::RVec> x, int* index, int nsolute, rvec target, rvec cog);

/* Get minimum distance of the solute to the box boundary.
   If mindistReturn != nullptr, put the minimum distance into *mindistReturn
   Works with wr == nullptr */
void check_prot_box_distance(gmx::ArrayRef<gmx::RVec> x, int* index, int isize, const matrix box, t_waxsrec* wr,
        gmx_bool bPrintWarn, real* mindistReturn);

/* Do all the steps required on the solute frame before getting the solvation shell */
void waxs_prepareSoluteFrame(t_waxsrec* wr, const gmx_mtop_t* mtop, gmx::ArrayRef<gmx::RVec> x, const matrix box,
        PbcType pbcType, FILE* fpLog, matrix Rinv);

/* Read an STX file name, and then copy only the relevant sections to xref.*/
void read_fit_reference(const char* fn, rvec xref[], int nsys, int* isol, int nsol,
        int* ifit, int nfit);

double CMSF_q(t_cromer_mann cmsf, real q);

real soluteRadiusOfGyration(t_waxsrec* wr, gmx::ArrayRef<gmx::RVec> x);

/* Weighted average and stddev of x. Weights may be nullptr */
void average_stddev_d(double* x, int n, double* av, double* sigma, double* weights);

void average_x2_d(double* x, int n, double* av, double* wptr);

void average_xy_d(double* x, double* y, int n, double* av, double* wptr);

void sum_squared_residual_d(double* x, double* y, int n, double* av, double* wptr);

/* Pearson correlation coeff. Note: avx, avy, sigx and sigy must be computed before. Weights may be nullptr. */
double pearson_d(int n, double* x, double* y, double avx, double avy, double sigx, double sigy,
        double* weights);

double sum_array_d(int n, double* x);

void nIndep_Shannon_Nyquist(t_waxsrec* wr, gmx::ArrayRef<gmx::RVec> x, const t_commrec* cr, gmx_bool bVerbose);

namespace swaxs {

/** Gives continuous array with number of electrons per atom */
double* countElectronsPerAtom(const gmx_mtop_t* topology);

/** Formatted writing to log file */
void print2log(FILE* fp, const char* s, const char* fmt, ...);

}
