#pragma once

#include <cstdio>  // FILE

#include <gromacs/fileio/oenv.h>  // gmx_output_env_t

#include <waxs/gmxlib/input_params.h>  // swaxs::InputParams
#include <waxs/gmxlib/swaxs_datablock.h>  // waxs_datablock
#include <waxs/types/waxsrec.h>  // t_waxsrec

struct t_waxsrec;

namespace swaxs {

class SwaxsOutput {
public:
    SwaxsOutput() = default;

    SwaxsOutput(t_waxsrec* wr, const char* fnOut, const gmx_output_env_t* oenv);

    /** Write final WAXS output */
    void writeOut(t_waxsrec* wr, const gmx_output_env_t* oenv);

    ~SwaxsOutput() = default;

    /** Output that needs only one file, even with multiple scattering types */
    FILE* fpLog;
    char* fnLog, * fnDensity;

    /** Output that need an extra file for each scattering type (xray, neutron, etc) */
    FILE** fpSpectra, ** fpStddevs,                           ** fpForces, ** fpSpecEns, ** fpPot, ** fpNindep, ** fpD;
    char** fnSpectra, ** fnStddevs, ** fnFinal, ** fnContrib, ** fnForces, ** fnSpecEns, ** fnPot;
    FILE** fpAvgA, ** fpAvgB, ** fpGibbsEnsW, ** fpGibbsSolvDensRelErr;
    char** fnAvgA, ** fnAvgB, ** fnGibbsEnsW, ** fnGibbsSolvDensRelErr, ** fnNindep, ** fnD;

    t_fileio **xfout;
};

}
