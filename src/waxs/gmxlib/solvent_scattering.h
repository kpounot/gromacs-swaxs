#pragma once

#include <gromacs/mdtypes/commrec.h>  // t_commrec
#include <gromacs/topology/topology.h>  // gmx_mtop_t

#include <waxs/types/waxsrec.h>  // t_waxsrec

namespace swaxs {

void solventScatteringFromRdfs(t_waxsrec* wr, t_commrec* cr, const char* fnxtcSolv, const gmx_mtop_t* mtop);

}
