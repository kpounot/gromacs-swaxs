#include "./names.h"

#include <waxs/types/enums.h>  // escatterNR, ewaxs*, waxs*

/**
 * .mdp names for SWAXS
 */

const char* escatter_names[escatterNR + 1] = {
    "xray", "neutron", nullptr
};

const char* waxspotential_names[ewaxsPotentialNR + 1] = {
    "linear", "log", nullptr
};

const char* waxsweights_names[ewaxsWeightsNR + 1] = {
    "uniform", "exp", "exp+calc", "exp+solvdens", "exp+calc+solvdens", nullptr
};

const char* waxsensemble_names[ewaxsEnsembleNR + 1] = {
    "none", "bayesian-one-refined", "maxent-ensemble", nullptr
};

const char* waxs_Iexp_fit_names[ewaxsIexpFitNR + 1] = {
    "no", "scale-and-offset", "scale", nullptr
};

const char* waxssolvdensunsertbayesian_names[ewaxsSolvdensUncertBayesianNR + 1] = {
    "no", "yes", nullptr
};

const char* waxscorrectbuff_names[waxscorrectbuffNR + 1] = {
    "no", "yes", nullptr
};

const char* waxs_bScaleI0_names[ewaxsscalei0NR + 1] = {
    "no", "yes", nullptr
};
