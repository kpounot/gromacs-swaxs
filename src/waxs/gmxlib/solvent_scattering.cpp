#include "./solvent_scattering.h"

#include <cstring>  // strcpy, strncpy

#include <gromacs/fileio/gmxfio.h>  // gmx_ffopen, gmx_ffclose, gmx_fexist
#include <gromacs/fileio/xvgr.h>  // read_xvg
#include <gromacs/gmxlib/network.h>  // gmx_bcast, gmx_sumd, gmx_barrier
#include <gromacs/math/functions.h>  // gmx::square
#include <gromacs/mdtypes/commrec.h>  // MASTER, PAR, t_commrec
#include <gromacs/pbcutil/pbc.h>  // PbcType
#include <gromacs/topology/topology.h>  // gmx_mtop_t
#include <gromacs/utility/fatalerror.h>  // FARGS, gmx_fatal
#include <gromacs/utility/smalloc.h>  // snew, srenew, sfree

#include <waxs/gmxlib/solvent.h>  // swaxs::Solvent
#include <waxs/gmxlib/waxsmd_utils.h>  // CMSF_q
#include <waxs/types/enums.h>  // escatterXRAY
#include <waxs/types/waxsrec.h>  // t_waxsrec, t_waxsrecType

static void interpolateSolventIntensity(double* Igiven, real qmax, int nq, t_waxsrecType* wt) {
    if (wt->maxq > qmax) {
        gmx_fatal(FARGS, "Maximum q passed in mdp file (%g) is larger than the maximum q at which\n"
                "the pure-solvent intensity is available *%g)\n", wt->maxq, qmax);
    }

    double dqgiven = qmax / (nq - 1);
    double dq = (wt->nq > 1) ? (wt->maxq - wt->minq) / (wt->nq - 1) : 0.;

    for (int i = 0; i < wt->nq; i++) {
        double q = wt->minq + i * dq;

        /** Simple linear interpolation. Maybe we can improve this later. */
        int j = floor(q / dqgiven);

        if (j == nq - 1) {
            /** q is exactly == qmax (it cannot be larger, since we checke above) */
            wt->Ipuresolv[i] = Igiven[j];
        } else if (j < nq - 1) {
            double left = j * dqgiven;
            double right = (j + 1) * dqgiven;
            double Ileft = Igiven[j];
            double Iright = Igiven[j + 1];
            double slopeI = (Iright - Ileft) / (right - left);

            wt->Ipuresolv[i] = Ileft + (q - left) * slopeI;
        } else {
            gmx_fatal(FARGS, "Error in interpolateSolventIntensity(), this should not happen\n");
        }
    }

    /** Debug output */
    FILE* fp = fopen("Ibuffer_interp.dat", "w");

    for (int i = 0; i < wt->nq; i++) {
        fprintf(fp, "%g %g\n", wt->minq + i * dq, wt->Ipuresolv[i]);
    }

    fclose(fp);
}

static void readPureSolventIntensityFile(const char* fn, t_waxsrecType* wt) {
    int ncol;
    double** y;
    int nlines = read_xvg(fn, &y, &ncol);

    if (ncol < 2) {
        gmx_fatal(FARGS, "Found only %d columns in %s\n", ncol, fn);
    }

    if (nlines < 3) {
        gmx_fatal(FARGS, "Found only %d lines in %s\n", nlines, fn);
    }

    if (y[0][0] > wt->minq) {
        gmx_fatal(FARGS, "Smallest q-value in %s is %g, but the smallest q requested is %g. Provide a different\n "
                "scattering intensity file or increase waxs-startq in the mdp file\n", fn, y[0][0], wt->minq);
    }

    if (y[0][nlines - 1] < wt->maxq) {
        gmx_fatal(FARGS, "Largest q-value in %s is %g, but the largest q requested is %g. Provide a different\n "
                "scattering intensity file or decrease waxs-endq in the mdp file\n", fn, y[0][nlines - 1], wt->maxq);
    }

    /** Check if we have increasing equally-spaced q values */
    for (int i = 1; i < nlines - 1; i++) {
        if (fabs((y[0][i] - y[0][i - 1]) - (y[0][i + 1] - y[0][i])) > 1e-4) {
            // printf("i = %d, y = %g diffs %g %g\n", i, y[1][i], (y[1][i] - y[1][i - 1]), (y[1][i + 1] - y[1][i]));
            gmx_fatal(FARGS, "The q values in file\n%s\nare not in equally-spaced increasing order\n", fn);
        }
    }

    if (fabs(y[0][0]) > 1e-5) {
        gmx_fatal(FARGS, "The q values in %s don't start at zero\n", "?");
    }

    printf("Read %d pure-solvent intensities between q = %g and %g\n", nlines, y[0][0], y[0][nlines - 1]);
    interpolateSolventIntensity(y[1], y[0][nlines - 1], nlines, wt);

    sfree(y[0]);
    sfree(y[1]);
    sfree(y);
}

#if 0
#include <gromacs/math/invertmatrix.h>  // gmx::invertBoxMatrix
#include <gromacs/math/vec.h>  // rvec
#include <gromacs/math/vectypes.h>  // matrix

/** Return x,y and z from the cell index */
static void waxs_ci2xyz(ivec gridn, int ci, ivec ind) {
    ind[XX]  = ci / (gridn[YY] * gridn[ZZ]);
    ci      -= (ind[XX]) * gridn[YY] * gridn[ZZ];
    ind[YY]  = ci / gridn[ZZ];
    ci      -= (ind[YY]) * gridn[ZZ];
    ind[ZZ]  = ci;
}

/**
 * return array containg the cell indices of cell ci *and* ci's unique neighbors.
 *
 * For a large simulation boxes, this will always be the central cell + 26 neighbors = 27 cells (3^3)
 * For small system, however, there may be less neighbors (the cell to the left may be the same
 * as the cell to the right. Therefore, return the number of unique neighbors
 */
static int waxs_ciNeighbors(int ci, ivec gridn, int* cinb) {
    ivec k, kk, ind;

    waxs_ci2xyz(gridn, ci, ind);
    // fprintf(stderr, " waxs_ciNeighbors: ci %d  ind = %d %d %d\n", ci, ind[XX], ind[YY], ind[ZZ]);

    int ncells = 0;

    for (k[XX] = ind[XX] - 1; k[XX] <= ind[XX] + 1; k[XX]++) {
        for (k[YY] = ind[YY] - 1; k[YY] <= ind[YY] + 1; k[YY]++) {
            for (k[ZZ] = ind[ZZ] - 1; k[ZZ] <= ind[ZZ] + 1; k[ZZ]++) {
                /** Check boundaries */
                copy_ivec(k, kk);

                for (int d = 0; d < DIM; d++) {
                    /** Make sure 0 <= kk < ngrid */
                    kk[d] = (kk[d] + gridn[d]) % gridn[d];
                }

                /** Get the new cell index, and make sure we don't have the cell yet */
                int cinew = gridn[YY] * gridn[ZZ] * kk[XX] + gridn[ZZ] * kk[YY] + kk[ZZ];

                for (int j = 0; j < ncells; j++) {
                    if (cinew == cinb[j]) {
                        continue;
                    }
                }

                cinb[ncells] = cinew;
                ncells++;
            }
        }
    }

    return ncells;
}

/**
 * Compute RDFs between the nTypes different atom types (nRDFs RDFs in total)
 *
 * It should be much faster for large water boxes compared to atomTypesRdfsSlow`, since it (by putting atoms on a grid)
 * avoids the double-loop over all atoms.
 *
 * This code haven't been tested thoroughly for different box shapes and sizes, hence it was not used so far.
 * It should be tested at some point.
 */
static void atomTypesRdfs(int nFramesLocal, rvec* x[], matrix* box, int nTypes, int* isize, int* index[], double** rdf,
        int** irdf, int nR, real rmax, gmx_bool bVerbose) {
    int ifr, j, k, ibin, tt, t1, t2, ***indexgrid = nullptr, **nAlloc, **nOnGrid;
    int iNeighbors[27], ci, cj, nb, atomci, atomcj, d, ngridTotal, nNeighbors;
    t_pbc pbc;
    rvec dx, veclen, xb, griddx;
    real dr, rmax2, r2;
    double r;
    ivec ngrid, igrid;
    matrix transf, boxvecnorm;
    const int allocBlock = 1000;

    PbcType pbcType;

    if (nFramesLocal > 0) {
        pbcType = guessPbcType(box[0]);
    }

    dr = rmax / nR;
    rmax2 = gmx::square(rmax);

    if (bVerbose) {
        printf("Computing RDFs of pure-solvent system, doing ~ %d frames per node\n", nFramesLocal);
    }

    snew(indexgrid, nTypes);
    snew(nOnGrid, nTypes);
    snew(nAlloc, nTypes);

    for (ifr = 0; ifr < nFramesLocal; ifr++) {
        if (bVerbose) {
            printf("\rComputing RDFs, %5.1f%% done\n", 100. * ifr / nFramesLocal);
        }

        set_pbc(&pbc, pbcType, box[ifr]);

        /* Put atoms on a grid */
        for (d = 0; d < DIM; d++) {
            veclen[d] = sqrt(norm2(box[ifr][d]));
            ngrid[d] = floor(veclen[d] / rmax);
            griddx[d] = veclen[d] / ngrid[d];
        }

        ngridTotal = ngrid[XX] * ngrid[YY] * ngrid[ZZ];

        if (bVerbose and ifr == 0) {
            printf("Using %d cells (%d x %d x %d)\n", ngridTotal, ngrid[XX], ngrid[YY], ngrid[ZZ]);
        }

        /**
         * Transformation matrix from cartesian to box vector coordinates.
         *
         * First get normalized box vectors:
         */
        for (d = 0; d < DIM; d++) {
            r = sqrt(norm2(box[ifr][d]));
            svmul(1. / r, box[ifr][d], boxvecnorm[d]);
        }

        gmx::invertBoxMatrix(boxvecnorm, transf);

        for (t1 = 0; t1 < nTypes; t1++) {
            snew(indexgrid[t1], ngridTotal);
            snew(nOnGrid[t1], ngridTotal);
            snew(nAlloc[t1], ngridTotal);

            for (j = 0; j < isize[t1]; j++) {
                /* Transform x to box coordinates of the box vectors */
                mvmul(transf, x[ifr][index[t1][j]], xb);

                /* get the grid points for x */
                for (d = 0; d < DIM; d++) {
                    igrid[d] = round(xb[d] / griddx[d]);

                    while (igrid[d] < 0) {
                        igrid[d] += ngrid[d];
                    }

                    while (igrid[d] >= ngrid[d]) {
                        igrid[d] -= ngrid[d];
                    }
                }

                /** Get continuous grid number */
                ci = igrid[XX] * ngrid[YY] * ngrid[ZZ] + igrid[YY] * ngrid[ZZ] + igrid[ZZ];

                if (ci >= ngridTotal) {
                    gmx_fatal(FARGS, "Invalid igridcont %d (should be < %d)\n", ci, ngridTotal);
                }

                if (nAlloc[t1][ci] == nOnGrid[t1][ci]) {
                    nAlloc[t1][ci] += allocBlock;
                    srenew(indexgrid[t1][ci], nAlloc[t1][ci]);
                }

                indexgrid[t1][ci][nOnGrid[t1][ci]] = index[t1][j];
                nOnGrid[t1][ci]++;
            }
        }

        /* Now compute the RDFs */
        /* Loop over atom types */
        for (t1 = 0; t1 < nTypes; t1++) {
            /* Loop over atom types */
            for (t2 = t1; t2 < nTypes; t2++) {
                /* Index of the RDF */
                tt = irdf[t1][t2];

                /* Loop over all grid points (or cells) */
                for (ci = 0; ci < ngridTotal; ci++) {
                    /* Get neighbors of cell ci */
                    nNeighbors = waxs_ciNeighbors(ci, ngrid, iNeighbors);

                    /* Loop over the cell ci *and* its unique neighbors (=27 only if cells << simulation box)  */
                    for (nb = 0; nb < nNeighbors; nb++) {
                        /* cell index of the neighbor */
                        cj = iNeighbors[nb];
                        // printf("cell %d (%d atoms), doing its neighbor %d (%d atoms)\n", ci, nOnGrid[t1][ci], cj, nOnGrid[t2][cj]);

                        /* Loop over atoms of type t1 in cell ci */
                        for (k = 0; k < nOnGrid[t1][ci]; k++) {
                            /* Atom index */
                            atomci = indexgrid[t1][ci][k];

                            /* Loop over the atoms of type t2 in cell cj */
                            for (j = 0; j < nOnGrid[t2][cj]; j++) {
                                /* Atom index */
                                atomcj = indexgrid[t2][cj][j];

                                /* increment the RDF */
                                pbc_dx(&pbc, x[ifr][atomci], x[ifr][atomcj], dx);
                                r2 = norm2(dx);

                                if (r2 < rmax2) {
                                    r = sqrt(r2);
                                    ibin = floor(r / dr);

                                    if (ibin < nR and atomci != atomcj) {
                                        rdf[tt][ibin] += 1.;
                                        // n[tt]         += 1.;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        for (t1 = 0; t1 < nTypes; t1++) {
            for (j = 0; j < ngridTotal; j++) {
                sfree(indexgrid[t1][j]);
            }

            sfree(indexgrid[t1]);
            sfree(nAlloc[t1]);
            sfree(nOnGrid[t1]);
        }
    }

    sfree(indexgrid);
    sfree(nAlloc);
    sfree(nOnGrid);

    if (bVerbose) {
        printf("\n");
    }
}
#endif

/* Compute RDFs between the nTypes different atom types (nRDFs RDFs in total) */
static void atomTypesRdfsSlow(int nFramesLocal, rvec* x[], matrix* box, int nTypes, int* isize, int* index[],
        double** rdf, int** irdf, int nR, real rmax, gmx_bool bVerbose) {
    t_pbc pbc;

    real dr = rmax / nR;
    real rmax2 = gmx::square(rmax);

    PbcType pbcType;

    if (nFramesLocal > 0) {
        pbcType = guessPbcType(box[0]);
    }

    for (int ifr = 0; ifr < nFramesLocal; ifr++) {
        if (bVerbose) {
            printf("\rComputing RDFs, %5.1f%% done\n", 100. * ifr / nFramesLocal);
        }

        set_pbc(&pbc, pbcType, box[ifr]);

        /* Loop over atom types */
        for (int t1 = 0; t1 < nTypes; t1++) {
            /* Loop over atom types */
            for (int t2 = t1; t2 < nTypes; t2++) {
                /* Index of the RDF */
                int tt = irdf[t1][t2];

                for (int j = 0; j < isize[t1]; j++) {
                    int k0;
                    double incr;

                    if (t1 == t2) {
                        /* Pairs of the same atom type (t1 == t2), do the sum only once */
                        k0 = j + 1;
                        incr = 2.;
                    } else {
                        k0 = 0;
                        incr = 1.;
                    }

                    for (int k = k0; k < isize[t2]; k++) {
                        rvec dx;
                        pbc_dx(&pbc, x[ifr][index[t1][j]], x[ifr][index[t2][k]], dx);
                        real r2 = norm2(dx);

                        if (r2 < rmax2) {
                            real r = sqrt(r2);
                            int ibin = floor(r / dr);

                            if (ibin < nR and index[t1][j]) {
                                rdf[tt][ibin] += incr;
                                // n[tt] += 1.;
                            }
                        }
                    }
                }
            }
        }
    }

    if (bVerbose) {
        printf("\n");
    }
}

/** Compute the sine transform of the RDF */
static void rdfSineTransform(double* rdf, int nR, real rmax, real qmax, int nq, double* sinetransf) {
    double dr = rmax / nR;
    double dq = qmax / (nq - 1);

    double sumg = 0.;
    double sumgamma = 0.;

    for (int i = 0; i < nR; i++) {
        double r = (i + 0.5) * dr;
        sumg += r * r * rdf[i] * dr;
        sumgamma += r * r * dr;
    }

    // double gamma = sumg / sumgamma;
    // double f = sumgamma / sumg;

    for (int j = 0; j < nq; j++) {
        double sum = 0.;
        double q = dq * j;

        for (int i = 0; i < nR; i++) {
            double r = (i + 0.5) * dr;
            double qr = q * r;

            double fact;

            if (qr > 0) {
                fact = sin(qr) / qr;
            } else {
                fact = 1.;
            }

            sum += r * r * (rdf[i] - 1.) * fact * dr;
        }

        sinetransf[j] = 4 * M_PI * sum;
    }
}

static void solventIntensity(t_waxsrec* wr, t_commrec* cr, const gmx_mtop_t* mtop, swaxs::Solvent* solvent,
        double qmax, int nq, double* intensitySum, char* fnIntensity, int t) {
    rvec** x = nullptr;
    matrix* box = nullptr;

    const int allocBlockSize = 100;
    const double nAtomsTimesFrames = 20e5;
    real rMax = 1.6;
    real fade = 0.9;
    int nR = 1001;

    int nFramesUsed = -1;

    rvec** xSolv     = MASTER(cr) ? solvent->x : nullptr;
    matrix* boxSolv  = MASTER(cr) ? solvent->box : nullptr;

    int nAtomsTotal  = MASTER(cr) ? solvent->topology->natoms : 0;
    int nFramesRead  = MASTER(cr) ? solvent->nFrames : 0;

    int* cmtypesList = MASTER(cr) ? solvent->cromerMannTypes : nullptr;
    int nTypes       = MASTER(cr) ? solvent->topology->scattTypes.nCromerMannParameters : 0;

    if (MASTER(cr)) {
        /* Specify the number of frames we want to use to compute the RDFs.
           Note: nAtomsTotal include all atoms including Tip4p/Tip5p dummy atoms, therefore nAtomsTotal >= wr->nindB_sol */
        nFramesUsed = nAtomsTimesFrames / wr->nindB_sol;

        if (nFramesUsed == 0) {
            nFramesUsed = 1;
        }

        printf("%d atoms in the solvent box - averaging over %d frames should give converged RDFs.\n",
                wr->nindB_sol, nFramesUsed);

        if (nFramesUsed > nFramesRead) {
            nFramesUsed = nFramesRead;
            printf("Warning, found only %d frames in solvent xtc. The RDFs may not fully converge\n", nFramesRead);
        }

        char* buf;

        if ((buf = getenv("GMX_WAXS_RDF_RMAX")) != nullptr) {
            rMax = atof(buf);
            printf("Found envrionment variable GMX_WAXS_RDF_RMAX, computing RDFs up to %g nm\n", rMax);
        }

        if ((buf = getenv("GMX_WAXS_RDF_NPOINTS")) != nullptr) {
            nR = atof(buf);
            printf("Found envrionment variable GMX_WAXS_RDF_NPOINTS, using %d bins for RDFs.\n", nR);
        }

        if ((buf = getenv("GMX_WAXS_RDF_FADE")) != nullptr) {
            fade = atof(buf);
            printf("Found environment variable GMX_WAXS_RDF_FADE, turning RDFs to 1 at %g.\n", fade);
        }
    }

    if (PAR(cr)) {
        gmx_bcast(sizeof(real), &rMax, cr->mpi_comm_mygroup);
        gmx_bcast(sizeof(int), &nR, cr->mpi_comm_mygroup);
        gmx_bcast(sizeof(int), &nAtomsTotal, cr->mpi_comm_mygroup);
        gmx_bcast(sizeof(int), &nFramesUsed, cr->mpi_comm_mygroup);
        gmx_bcast(sizeof(int), &nTypes, cr->mpi_comm_mygroup);
    }

    int** index = nullptr;
    int* isize = nullptr;
    int* nAlloc = nullptr;

    int* cmtypesList_B = nullptr;
    int ncmtypesList_B = 0;

    if (MASTER(cr)) {
        snew(index, nTypes);
        snew(isize, nTypes);
        snew(nAlloc, nTypes);

        gmx_bool bFound;

        /** First, make a list of the SF types in the pure solvent */
        for (int i = 0; i < wr->nindB_sol; i++) {
            int t1 = cmtypesList[wr->indB_sol[i]];

            /** Check if type t1 is already in the list cmtypesList_B[] */
            bFound = FALSE;

            for (int j = 0; j < ncmtypesList_B; j++) {
                if (cmtypesList_B[j] == t1) {
                    bFound = TRUE;
                    break;
                }
            }

            /** If t1 is not yet in the list, add it */
            if (!bFound) {
                srenew(cmtypesList_B, ncmtypesList_B + 1);
                cmtypesList_B[ncmtypesList_B] = t1;
                ncmtypesList_B++;
            }
        }

        if (ncmtypesList_B != nTypes) {
            gmx_fatal(FARGS, "Found %d SF types in the topology of the pure-water system, but only %d in the solvent group\n",
                    nTypes, ncmtypesList_B);
        }

        /**
         * Make index groups of atoms of the same scattering factor type
         * (for pure water this would be a list of O (j==0) and a list of H (j==1) atoms)
         */
        for (int i = 0; i < wr->nindB_sol; i++) {
            int t1 = cmtypesList[wr->indB_sol[i]];

            int j;

            for (j = 0; j < ncmtypesList_B; j++) {
                if (cmtypesList_B[j] == t1) {
                    bFound = TRUE;
                    break;
                }
            }

            if (j >= nTypes) {
                gmx_fatal(FARGS, "Invalid SF type for pure solvent: %d, should be < %d\n", t1, nTypes);
            }

            if (nAlloc[j] == isize[j]) {
                nAlloc[j] += allocBlockSize;
                srenew(index[j], nAlloc[j]);
            }

            index[j][isize[j]] = wr->indB_sol[i];

            if (wr->indB_sol[i] >= nAtomsTotal) {
                gmx_fatal(FARGS, "Invalid atom number %d, max is %d (i = %d)\n", wr->indB_sol[i], nAtomsTotal, i);
            }

            isize[j]++;
        }
    }

    if (PAR(cr)) {
        gmx_barrier(cr->mpi_comm_mygroup);
    }

    /** Send the index groups to the nodes */
    if (PAR(cr)) {
        if (!MASTER(cr)) {
            snew(index, nTypes);
            snew(isize, nTypes);
        }

        gmx_bcast(sizeof(int) * nTypes, isize, cr->mpi_comm_mygroup);

        if (!MASTER(cr)) {
            for (int j = 0; j < nTypes; j++) {
                snew(index[j], isize[j]);
            }
        }

        for (int j = 0; j < nTypes; j++) {
            gmx_barrier(cr->mpi_comm_mygroup);
            gmx_bcast(sizeof(int) * isize[j], index[j], cr->mpi_comm_mygroup);
        }

        gmx_barrier(cr->mpi_comm_mygroup);
    }

    /** Distribute solvent coordinates and box size over the nodes */
    int nFramesLocal = 0;
    int* nodeIdFrame = nullptr;

    if (!PAR(cr)) {
        x = xSolv;
        box = boxSolv;
        nFramesLocal = nFramesUsed;
    } else {
        if (cr->nodeid >= cr->nnodes - cr->npmenodes) {
            gmx_fatal(FARGS, "Inconsistency, found too large nodeid = %d\n", cr->nodeid);
        }

        /** Send frames to the nodes */
        snew(nodeIdFrame, nFramesUsed);

        for (int i = 0; i < nFramesUsed; i++) {
            nodeIdFrame[i] = i % (cr->nnodes - cr->npmenodes);

            if (nodeIdFrame[i] == cr->nodeid) {
                nFramesLocal++;
            }
        }

        snew(x, nFramesLocal);
        snew(box, nFramesLocal);

        if (PAR(cr)) {
            gmx_barrier(cr->mpi_comm_mygroup);
        }

        if (MASTER(cr) and PAR(cr)) {
            printf("\nSending solvent frames to the nodes ...");
            fflush(stdout);
        }

        int nFramesReceived = 0;

        for (int i = 0; i < nFramesUsed; i++) {
            if (nodeIdFrame[i] == 0) {
                if (MASTER(cr)) {
                    /* If the master does this frame, there is no need to copy x and box */
                    x[nFramesReceived] = xSolv[i];
                    copy_mat(boxSolv[i], box[nFramesReceived]);
                    nFramesReceived++;
                }
            } else {
                if (MASTER(cr)) {
                    MPI_Send((void*)xSolv[i], nAtomsTotal * sizeof(rvec), MPI_BYTE, nodeIdFrame[i], 0, cr->mpi_comm_mysim);
                    MPI_Send((void*)(&boxSolv[i]), sizeof(matrix), MPI_BYTE, nodeIdFrame[i], 0, cr->mpi_comm_mysim);
                }

                if (!MASTER(cr) and nodeIdFrame[i] == cr->nodeid) {
                    snew(x[nFramesReceived], nAtomsTotal);

                    MPI_Status status;
                    MPI_Recv((void*)x[nFramesReceived], nAtomsTotal * sizeof(rvec), MPI_BYTE, 0, 0, cr->mpi_comm_mysim, &status);
                    MPI_Recv((void*)(&box[nFramesReceived]), sizeof(matrix), MPI_BYTE, 0, 0, cr->mpi_comm_mysim, &status);

                    nFramesReceived++;
                }
            }
        }

        if (MASTER(cr) and PAR(cr)) {
            printf(" done\n");
        }

        if (PAR(cr)) {
            gmx_barrier(cr->mpi_comm_mygroup);
        }
    }

    /** Number of RDFs to compute */
    int nRDFs = nTypes + nTypes * (nTypes - 1) / 2;

    if (MASTER(cr)) {
        printf("Found %d atom types in the pure-solvent system; this makes %d RDFs.\n", nTypes, nRDFs);
    }

    double** rdf = nullptr;
    snew(rdf, nRDFs);

    for (int i = 0; i < nRDFs; i++) {
        snew(rdf[i], nR);
    }

    /** Numbering of the rdfs */
    int** irdf = nullptr;
    snew(irdf, nTypes);

    for (int t1 = 0; t1 < nTypes; t1++) {
        snew(irdf[t1], nTypes);
    }

    int irdfDiff = 0;

    for (int t1 = 0; t1 < nTypes; t1++) {
        for (int t2 = t1; t2 < nTypes; t2++) {
            if (t1 == t2) {
                /* t1 == t2: We get the RDF between identical atom types (e.g., O-O or H-H):
                   Store those in the first nTypes entries of rdf[][] */
                irdf[t1][t2] = t1;
            } else {
                /* RDF between different atom types, e.g. O-H.
                   Store these *after* the first nTypes entries in rdf[][] */
                irdf[t1][t2] = nTypes + irdfDiff;
                irdfDiff++;
            }
        }
    }

    /** Compute the RDFs */
    atomTypesRdfsSlow(nFramesLocal, x, box, nTypes, isize, index, rdf, irdf, nR, rMax, MASTER(cr));

    /** Collect RDFs from the nodes */
    for (int t1 = 0; t1 < nRDFs; t1++) {
        if (PAR(cr)) {
            gmx_sumd(nR, rdf[t1], cr);
        }
    }

    double** sinetransf = nullptr;
    double** intensity = nullptr;

    /** Get average inverse volume and density */
    if (MASTER(cr)) {
        real inv_vol = 0.;

        for (int i = 0; i < nFramesUsed; i++) {
            inv_vol += 1. / det(boxSolv[i]);
        }

        inv_vol /= nFramesUsed;

        for (int t1 = 0; t1 < nTypes; t1++) {
            printf("Density of atom type %d = %g 1/nm3\n", t1, inv_vol * isize[t1]);
        }

        /** Densities from RDFs */
        double* dens;
        snew(dens, nTypes);
        real dr = rMax / nR;

        for (int t1 = 0; t1 < nTypes; t1++) {
            double sum = 0.;
            int tt = irdf[t1][t1];

            for (int i = 0; i < nR; i++) {
                // real r = (i + 0.5) * dr;
                /** Before normalizing the rdfs (see below), this gives simply the cumulative RDF: */
                sum += rdf[tt][i] / (nFramesUsed * isize[t1]);
            }

            dens[t1] = sum / (4 * M_PI / 3. * rMax * rMax * rMax);
            printf("Type %d, density from RDF = %g\n", t1, dens[t1]);
        }

        /* Normalize RDFs */
        dr = rMax / nR;

        for (int i = 0; i < nR; i++) {
            real ri = dr * i;
            real ro = dr * (i + 1);
            real r = (ri + ro) / 2;
            /** Volume of spherical shell, equivalent to 4 / 3 * pi (ro^3 - ri^3), but numerically more stable */
            real dv = 4. / 3. * M_PI * dr * (ro * ro + ro * ri + ri * ri);

            for (int t1 = 0; t1 < nTypes; t1++) {
                for (int t2 = t1; t2 < nTypes; t2++) {
                    int tt = irdf[t1][t2];
                    rdf[tt][i] = rdf[tt][i] / dv / (inv_vol * isize[t1] * isize[t2]) / nFramesUsed;

                    if (fade > 0 and r >= fade) {
                        /* Smoothly switch off RDF behind r = fade */
                        rdf[tt][i] = 1 + (rdf[tt][i] - 1) * exp(-16 * gmx::square(r / fade - 1));
                    }
                }
            }
        }

        /** Do sine transform of RDFs: int_0^infty { r^2 [RDF-1] sin(qr)/(qr) } */
        snew(sinetransf, nRDFs);

        for (int t1 = 0; t1 < nRDFs; t1++) {
            snew(sinetransf[t1], nq);
        }

        for (int t1 = 0; t1 < nTypes; t1++) {
            for (int t2 = t1; t2 < nTypes; t2++) {
                int tt = irdf[t1][t2];
                rdfSineTransform(rdf[tt], nR, rMax, qmax, nq, sinetransf[tt]);
            }
        }

        /** Compute intensity */
        snew(intensity, nRDFs);

        for (int t1 = 0; t1 < nRDFs; t1++) {
            snew(intensity[t1], nq);
        }

        for (int i = 0; i < nq; i++) {
            real q = 1.0 * i * qmax / (nq - 1);

            for (int t1 = 0; t1 < nTypes; t1++) {
                for (int t2 = t1; t2 < nTypes; t2++) {
                    /* atomic scattering factors */
                    real f1 = CMSF_q(mtop->scattTypes.cromerMannParameters[cmtypesList_B[t1]], q);
                    real f2 = CMSF_q(mtop->scattTypes.cromerMannParameters[cmtypesList_B[t2]], q);

                    real dens1 = isize[t1] * inv_vol;
                    real dens2 = isize[t2] * inv_vol;
                    // real dens1 = dens[t1];
                    // real dens2 = dens[t2];

                    int tt = irdf[t1][t2];

                    /** eq. 2 in Koefinger & Hummer, Phys Rev E 87 052712 (2013) */
                    real tmp = dens1 * dens2 * sinetransf[tt][i];

                    if (t1 == t2) {
                        tmp += dens1;
                    }

                    if (t1 != t2) {
                        /** We sum only once in case of different atom types, so we need to multiply by 2 */
                        tmp *= 2;
                    }

                    // printf("i %d t = %d / %d (tt %d); f = %g / %g; dens = %g / %g\n", i, t1, t2, tt, f1, f2, dens1, dens2);

                    intensity[tt][i] = f1 * f2 * tmp;

                    if (fabs(f1 * f2 * tmp) < 0.01) {
                        printf("Strange int = %g, f1 %g f2 %g, tmp %g dens %g %g\n", intensity[tt][i], f1, f2, tmp,
                                dens1, dens2);
                    }

                    intensitySum[i] += f1 * f2 * tmp;
                }
            }
        }

        /** Interpolate intensity to the q values in the mdp file */
        interpolateSolventIntensity(intensitySum, qmax, nq, &wr->wt[t]);
    }

    if (MASTER(cr)) {
        FILE* fp = gmx_ffopen(fnIntensity, "w");

        for (int i = 0; i < nq; i++) {
            fprintf(fp, "%g %g\n", 1.0 * i * qmax / (nq - 1), intensitySum[i]);
        }

        gmx_ffclose(fp);
        printf("Wrote solvent intensity to %s\n", fnIntensity);
    }

    if (MASTER(cr)) {
        FILE* fp = fopen("rdf.dat", "w");
        FILE* fpr = fopen("rdf_m1_r2.dat", "w");

        FILE* fp1 = fopen("sinetransf.dat", "w");
        FILE* fp2 = fopen("intensities.dat", "w");

        for (int tt = 0; tt < nRDFs; tt++) {
            for (int i = 0; i < nR; i++) {
                fprintf(fp, "%g %g\n", (i + 0.5) * rMax / nR, rdf[tt][i]);
                fprintf(fpr, "%g %g\n", (i + 0.5) * rMax / nR, (rdf[tt][i] - 1) * gmx::square((i + 0.5) * rMax / nR));
            }

            fprintf(fp, "&\n");
            fprintf(fpr, "&\n");

            for (int i = 0; i < nq; i++) {
                fprintf(fp1, "%g %g\n", 1.0 * i * qmax / (nq - 1), sinetransf[tt][i]);
                fprintf(fp2, "%g %g\n", 1.0 * i * qmax / (nq - 1), intensity[tt][i]);
            }

            fprintf(fp1, "&\n");
            fprintf(fp2, "&\n");
        }

        fclose(fp);
        fclose(fpr);
        fclose(fp1);
        fclose(fp2);
    }

    if (PAR(cr)) {
        if (!MASTER(cr)) {
            for (int i = 0; i < nFramesLocal; i++) {
                sfree(x[i]);
            }
        }

        if (x) {
            sfree(x);
            sfree(box);
        }

        sfree(nodeIdFrame);
    }
}

/**
 * Compute scattering of the pure solvent from RDFs.
 * Required if the buffer subtraction was reduced by the partial volume of the solute.
 */
void swaxs::solventScatteringFromRdfs(t_waxsrec* wr, t_commrec* cr, const char* fnxtcSolv, const gmx_mtop_t* mtop) {
    /** Pure solvent works only if we have exactly one scattering type, and that is XRAY */
    if (wr->inputParams->nTypes != 1) {
        gmx_fatal(FARGS, "Pure solvent scattering works only with one scattering type XRAY (found %d types)\n", wr->inputParams->nTypes);
    }

    if (wr->wt[0].type != escatterXRAY) {
        gmx_fatal(FARGS, "Pure solvent scattering for correct for over-subtracted buffer works only with scattering type XRAY\n");
    }

    int t = 0;
    t_waxsrecType* wt = &wr->wt[t];
    snew(wt->Ipuresolv, wt->nq);

    gmx_bool bHaveSolventIntesityFile = FALSE;

    char path[10000];

    if (MASTER(cr)) {
        char* fn;

        /** Check if there is a solvent intensity file in the solvent xtc directory, or passed by an environment variable */
        if ((fn = getenv("GMX_WAXS_BUFFER_INTENSITY_FILE")) != nullptr) {
            printf("Found envrionment variable GMX_WAXS_BUFFER_INTENSITY_FILE, reading buffer intensity from %s.\n", fn);
            bHaveSolventIntesityFile = TRUE;
            strcpy(path, fn);
        } else {
            const char* base = strrchr(fnxtcSolv, '/');

            if (!base) {
                strcpy(path, "./");
            } else {
                strncpy(path, fnxtcSolv, base - fnxtcSolv + 1);
                path[base - fnxtcSolv + 1] = '\0';
            }

            const char fnSolventIntensity[] = "Isolvent.dat";

            strcat(path, fnSolventIntensity);
            printf("\nLooking for pure solvent intensity file at: %s ... ", path);
            bHaveSolventIntesityFile = gmx_fexist(path);

            if (bHaveSolventIntesityFile) {
                printf("found\n");
            } else {
                printf("not found.\n\t -> Will compute the RDFs, which may take a while, but it needs to be done only once.\n");
            }
        }

        if (bHaveSolventIntesityFile) {
            /** Read pure-solvent intensity and store in wr->Ipuresolv[] */
            readPureSolventIntensityFile(path, wt);
        }
    }

    if (PAR(cr)) {
        gmx_bcast(sizeof(gmx_bool), &bHaveSolventIntesityFile, cr->mpi_comm_mygroup);
    }

    if (bHaveSolventIntesityFile) {
        /** If we found a pure-solvent intensity file, nothing more to be done. */
        if (PAR(cr)) {
            gmx_bcast(wt->nq * sizeof(double), wt->Ipuresolv, cr->mpi_comm_mygroup);
        }

        return;
    }

    /** Compute RDFs between all atom types, do the sine transforms, and put the intensity into intensitySum[] */
    int nq = 501;
    int qmax = 50;

    double* intensitySum = nullptr;
    snew(intensitySum, nq);

    solventIntensity(wr, cr, mtop, wr->solvent, qmax, nq, intensitySum, path, t);

    /** Interpolate intensity to the q values in the mdp file, and write the result to wr->Ipuresolv */
    if (MASTER(cr)) {
        interpolateSolventIntensity(intensitySum, qmax, nq, wt);
    }

    /** Send wr->Ipuresolv to the nodes */
    if (PAR(cr)) {
        gmx_bcast(wt->nq * sizeof(double), wt->Ipuresolv, cr->mpi_comm_mygroup);
    }

    /** Clean up */
    sfree(intensitySum);

    if (PAR(cr)) {
        gmx_barrier(cr->mpi_comm_mygroup);
    }
}
