#pragma once

#include <gromacs/fileio/gmxfio.h>  // t_fileio
#include <gromacs/mdtypes/inputrec.h>  // t_inputrec
#include <gromacs/utility/basedefinitions.h>  // gmx_bool
#include <gromacs/utility/iserializer.h>  // gmx::ISerializer

#include <waxs/types/waxstop.h>  // t_scatt_types

void do_waxs_inputrec(gmx::ISerializer* serializer, t_inputrec* ir);

void do_scattering_types(gmx::ISerializer* serializer, t_scatt_types* scattTypes);
