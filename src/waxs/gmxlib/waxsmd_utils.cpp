/*
 *  This source file was written by Jochen Hub.
 */

#include "./waxsmd_utils.h"  // print2log

#include <cctype>  // isalpha
#include <cstring>  // memcpy
#include <cstdarg>  // va_*
#include <cstdlib>  // atof, getenv

#include <gromacs/fileio/confio.h>  // read_tps_conf, write_sto_conf_mtop
#include <gromacs/fileio/groio.h>  // get_coordnum
#include <gromacs/fileio/xvgr.h>  // read_xvg
#include <gromacs/gmxlib/network.h>  // gmx_bcast, gmx_barrier
#include <gromacs/gmxpreprocess/notset.h>  // NOTSET
#include <gromacs/math/do_fit.h>  // reset_x, calc_fit_R calc_fit_R, reset_x
#include <gromacs/math/functions.h>  // gmx::square
#include <gromacs/math/vec.h>  // *mul, *sqr, copy_*, exp, log, M_PI, norm2, rvec_*
#include <gromacs/mdtypes/commrec.h>  // t_commrec
#include <gromacs/pbcutil/pbc.h>  // c_pbcTypeNames, calc_box_center, ecenterTRIC, pbc_*, PbcType, set_pbc, TRICLINIC, put_atoms_in_compact_unitcell, t_pbc
#include <gromacs/utility/cstringutil.h>  // isalpha, memcpy, strlen
#include <gromacs/utility/fatalerror.h>  // gmx_fatal, gmx_incons, FARGS
#include <gromacs/utility/smalloc.h>  // snew, sfree

#include <waxs/debug.h>  // swaxs_debug
#include <waxs/gmxlib/bounding_sphere.h>  // get_bounding_sphere_Ritter_COM
#include <waxs/gmxlib/sftypeio.h>  // t_cromer_mann, countGroups
#include <waxs/gmxlib/waxsmd.h>  // CROMERMANN_2_NELEC
#include <waxs/types/waxsrec.h>  // t_waxsrec

double CMSF_q(t_cromer_mann cmsf, real q) {
    real f = cmsf.c;

    /**
     * Note: Cromer-Mann parameters use inverse Angstroem, but our q is in inv. nm.
     * Also note: q / 4pi = sin(theta) / lambda
     */
    real k2 = gmx::square(q / (4 * M_PI * 10));

    for (int i = 0; (i < 4); i++) {
        f += cmsf.a[i] * exp(-cmsf.b[i] * k2);
    }

    return f;
}

/** Non-weighted center of mass */
void get_cog(gmx::ArrayRef<gmx::RVec> x, int* index, int n, rvec cent) {
    rvec temp = { 0., 0., 0. };

    for (int i = 0; i < n; i++) {
        rvec_inc(temp, x[index[i]]);
    }

    svmul(1.0 / n, temp, cent);
}

#define WAXS_WARN_BOX_DIST 0.5

void check_prot_box_distance(gmx::ArrayRef<gmx::RVec> x, int* index, int isize, const matrix box, t_waxsrec* wr,
        gmx_bool bPrintWarn, real* mindistReturn) {
    gmx_bool bWarn = FALSE, bTric;

    bTric = TRICLINIC(box);

    rvec boxcenter;
    calc_box_center(ecenterTRIC, box, boxcenter);

    auto xRaw = as_rvec_array(x.data());

    rvec min = { 1e20, 1e20, 1e20 };
    rvec max = { -1e20, -1e20, -1e20 };
    // int imin = -1;
    int imax = -1;

    real max2 = -1;

    for (int i = 0; i < isize; i++) {
        rvec* ptr_x = &(xRaw[index[i]]);

        /** Take note of the maximum radius and dimensions. */
        if (!bTric) {
            /** Rectangular box */
            for (int d = 0; d < DIM; d++) {
                if ((*ptr_x)[d] < min[d]) {
                    min[d] = (*ptr_x)[d];
                    // imin = i;
                }

                if ((*ptr_x)[d] > max[d]) {
                    max[d] = (*ptr_x)[d];
                    imax = i;
                }
            }
        } else {
            rvec rad;
            rvec_sub(*ptr_x, boxcenter, rad);
            real rad2 = norm2(rad);

            if (rad2 > max2) {
                max2 = rad2;
                imax = i;
            }
        }
    }

    real mindist = 1e20;

    /** Pick minimum distance to box from all atoms */
    if (!bTric) {
        for (int d = 0; d < DIM; d++) {
            real thisdist = (box[d][d] - (max[d] - min[d])) / 2;

            if (thisdist < mindist) {
                mindist = thisdist;
            }
        }
    } else {
        /** GROMACS convention stores x as (d,0,0), the maximum dimension of rhombic dodecahedron and trunc. octahedron cells. */
        mindist = box[0][0] / 2 - sqrt(max2);
    }

    /** Now check if the protein is still not whole... */
    if (!bTric) {
        for (int d = 0; d < DIM; d++) {
            if (mindist < WAXS_WARN_BOX_DIST) {
                if (bPrintWarn) {
                    fprintf(stderr, "\n\n** WARNING **\n\n"
                            "Solute spans between %g and %g in %c direction (box length = %g).\n"
                            "Seems like the solute is either not whole, or very big.\n\n"
                            " ** Hint: maybe you have not chosen a good waxs-pbc atom ? **\n",
                            min[d], max[d], 'X'+d, box[d][d]);
                }

                bWarn = TRUE;
            }
        }
    } else {
        /** GROMACS convention stores x as (d,0,0), the maximum dimension of rhombic dodecahedron and trunc. octahedron cells. */
        if (mindist < WAXS_WARN_BOX_DIST) {
            if (bPrintWarn) {
                fprintf(stderr, "\n\n** WARNING **\n\n"
                        "Solute is reaching dangerously close the edge of the compact PBC-box! (max. distance from "
                        "box center: %g, box radius = %g).\n"
                        "\tbox center   at %8g %8g %8g\n"
                        "\tatom %4d    at %8g %8g %8g\n"
                        "\tSeems like the solute is either not whole, or too big.\n\n",
                        sqrt(max2), box[0][0] / 2.,
                        boxcenter[0], boxcenter[1], boxcenter[2],
                        index[imax], x[index[imax]][0], x[index[imax]][1], x[index[imax]][2]);
            }

            bWarn = TRUE;
        }
    }

    if (bWarn and wr) {
        wr->nWarnCloseBox++;
    }

    if (mindistReturn) {
        *mindistReturn = mindist;
    }
}

/** Check if all atoms are inside the comapact unit cell */
static gmx_bool assert_all_atoms_inside_compact_box(gmx::ArrayRef<gmx::RVec> x, int* index, int isize, const matrix box,
        t_pbc* pbc) {
    real small = box[XX][XX] / 10;

    rvec boxcenter;
    calc_box_center(ecenterTRIC, box, boxcenter);

    for (int i = 0; i < isize; i++) {
        /** Use that shortest distance (retured by pbc_dx()) from the box center is (by definition) inside the compact box */
        rvec dxpbc;
        pbc_dx(pbc, x[index[i]], boxcenter, dxpbc);

        rvec dxdirect;
        rvec_sub(x[index[i]], boxcenter, dxdirect);

        for (int d = 0; d < DIM; d++) {
            if (fabs(dxpbc[d] - dxdirect[d]) > small) {
                printf("\n\nAtom %d (solute atom nr %d) is outside the compact box (x/y/z = %g %g %g) after shifting "
                       "the bounding sphere to the box center.\n"
                       "This means that your solute is not whole.\n\n"
                       "Probably you (a) need to choose a better waxs-pbc atom, or (b) atoms of your waxs-solute group"
                       "are freely diffusing around (such as ions)\n",
                        index[i], i, x[index[i]][XX], x[index[i]][YY], x[index[i]][ZZ]);

                return FALSE;
            }
        }
    }

    return TRUE;
}

/** mv center of geometry to origin */
void mv_cog_to_rvec(int natoms, gmx::ArrayRef<gmx::RVec> x, int* index, int isize, rvec target, rvec cog) {
    rvec cent;

    /** Center of geometry */
    get_cog(x, index, isize, cent);

    rvec shift;

    /** Shift geomtric center to orgin */
    rvec_sub(target, cent, shift);

    for (int i = 0; i < natoms; i++) {
        rvec_inc(x[i], shift);
    }

    if (cog) {
        copy_rvec(cent, cog);
    }
}

void mv_boxcenter_to_rvec(int natoms, gmx::ArrayRef<gmx::RVec> x, rvec cent, matrix box) {
    rvec box_center, diff;

    calc_box_center(ecenterTRIC, box, box_center);
    rvec_sub(cent, box_center, diff);

    for (int i = 0; i < natoms; i++) {
        rvec_inc(x[i], diff);
    }
}

/**
 * This is adopted from rm_gropbc() in src/gmxlib/rmpbc.c
 * A simple routine to make the protein whole based on the disnace between number-wise
 * atomic neighbors.
 */
static void waxs_rm_gropbc(gmx::ArrayRef<gmx::RVec> x, int* index, int isize, const matrix box) {
    const real maxAllowedDist = 2.0;

    /** Check periodic boundary */
    for (int n = 1; (n < isize); n++) {
        for (int m = DIM - 1; m >= 0; m--) {
            real dist = x[index[n]][m] - x[index[n - 1]][m];

            if (fabs(dist) > 0.5 * box[m][m]) {
                if (dist > 0) {
                    for (int d = 0; d <= m; d++) {
                        x[index[n]][d] -= box[m][d];
                    }
                } else {
                    for (int d = 0; d <= m; d++) {
                        x[index[n]][d] += box[m][d];
                    }
                }
            }
            /* Check if the two atoms are now close to each other */
            real absdist = fabs(x[index[n]][m] - x[index[n - 1]][m]);

            if (absdist > maxAllowedDist or absdist > 0.4 * box[m][m]) {
                const char* dimLett[] = { "X", "Y", "Z" };

                gmx_fatal(FARGS, "Trying to make the solute whole based on distances between number-wise atom\n"
                        "neighbors. However, found a distance of of %g between atoms %d and %d in direction %s\n",
                        fabs(dist), index[n], index[n - 1], dimLett[m]);
            }
        }
    }
}

static void waxs_mk_prot_whole(gmx::ArrayRef<gmx::RVec> x, int* index, int isize, int* ipbcatom, int npbcatom,
        t_pbc* pbc, gmx_bool bVerbose) {
    swaxs_debug("Begin of waxs_mk_prot_whole()\n");

    rvec x_pbc;
    clear_rvec(x_pbc);

    /* pbcatoms are a set of atoms that are close together to ensure a correct and consistent box centering from the solute frame */

    /** The first atom is used here to define a center. */
    for (int i = 0; i < npbcatom; i++) {
        /** Note: Out of bounds checks should have been done in tpr generation. */
        int ipbc_global = ipbcatom[i];  /** ipbcatom is a global index, independent of *index. */

        /** Locate shortest pbc-distance of this sub-collection. */
        if (bVerbose) {
            fprintf(stderr, "pbcatom[%d] = %d, x[pbcatom[%d]] before-shift: %g %g %g .\n",
                    i, ipbc_global, i, x[ipbc_global][0], x[ipbc_global][1], x[ipbc_global][2]);
        }

        if (i > 0) {
            int ipbc_global0 = ipbcatom[0];

            rvec dx;
            pbc_dx(pbc, x[ipbc_global], x[ipbc_global0], dx);
            rvec_add(x[ipbc_global0], dx, x[ipbc_global]);

            if (bVerbose) {
                fprintf(stderr, "pbcatom[%d] = %d, x[pbcatom[%d]] after-shift: %g %g %g .\n",
                        i, ipbc_global, i, x[ipbc_global][0], x[ipbc_global][1], x[ipbc_global][2]);
            }
        }

        /** Add the correct PBC image to the COM. */
        rvec_inc(x_pbc, x[ipbc_global]);
    }

    svmul(1.0 / npbcatom, x_pbc, x_pbc);

    if (bVerbose) {
        fprintf(stderr, "x[pbc_center]: %g %g %g .\n", x_pbc[0], x_pbc[1], x_pbc[2]);
    }

    auto* xRaw = as_rvec_array(x.data());

    /** Shift the protein by dx */
    for (int i = 0; i < isize; i++) {
        rvec* ptr_x = &xRaw[index[i]];

        rvec dx;
        pbc_dx(pbc, *ptr_x, x_pbc, dx);

        rvec_add(x_pbc, dx, *ptr_x);
    }

    swaxs_debug("End of waxs_mk_prot_whole()\n");
}

#define WAXS_WRITE_FRAME_PDB_DEBUG(req, fname) {                       \
        if (req){                                                      \
            sprintf(fn, fname"_%d.pdb", wr->waxsStep);                 \
            write_sto_conf_mtop(fn, *mtop->name, mtop, as_rvec_array(x.data()), nullptr, pbcType, box); \
            printf("Wrote %s\n", fn); }                                \
    }

/**
 * Do the required steps with the solute before getting the solvation shell:
 *   1. Make solute/protein whole, using pbcatom if given.
 *   2. If requested, shift fit-group COG to origin, calculate rotation matrix of the solute
 *      (using the fitgroup atoms such as Backbone), and store the inverse rotation matrix.
 *   2.5. Build envelope, if needed. First step: Calculate envelope bounding sphere. Allways: Check if envelope fits into box.
 *   3. Move the center of the envelope in the reference frame of the protein to the center of the box.
 *   4. Check distance between solute and box walls.
 *   5. put all solvent atoms into the compact box.
 *   6. If requested, move fit-group cog to center and do a rotation of the solute.
 *   7. Move the solute COG to the origin (origin).
 */
void waxs_prepareSoluteFrame(t_waxsrec* wr, const gmx_mtop_t* mtop, gmx::ArrayRef<gmx::RVec> x, const matrix box,
        PbcType pbcType, FILE* fpLog, matrix Rinv) {
    if (pbcType != PbcType::Unset and pbcType != PbcType::Xyz) {
        gmx_fatal(FARGS, "The PBC of your pure-solvent system should be %s, but I found %s\n",
                c_pbcTypeNames[PbcType::Xyz].c_str(), c_pbcTypeNames[pbcType].c_str());
    }

    t_pbc pbc;
    set_pbc(&pbc, pbcType, box);

    char fn[1024];  /** Used implicitly by the debug macro */

    /**** 0 ****/
    WAXS_WRITE_FRAME_PDB_DEBUG(wr->inputParams->verbosityLevel > 2 or (wr->inputParams->verbosityLevel == 2 and wr->waxsStep == 0), "before_shift");

    /**** 1 ****/
    if (wr->inputParams->bHaveWholeSolute == FALSE) {
        if (wr->nSoluteMols == 1 and wr->pbcatoms[0] < -1) {
            /** Make whole using distances number-wise atomic neighbors */
            if (wr->waxsStep == 0) {
                printf("\nWAXS-MD: Will make solute whole using distances between number-wise atomic neighbors\n");
            }

            waxs_rm_gropbc(x, wr->indA_prot, wr->nindA_prot, box);
        } else {
            /** Make whole using waxs-pbc atom(s) */
            waxs_mk_prot_whole(x, wr->indA_prot, wr->nindA_prot, wr->pbcatoms, wr->inputParams->npbcatom, &pbc,
                    wr->inputParams->verbosityLevel > 1);
        }
    } else {
        if (wr->waxsStep == 0) {
            printf("\nNote: Assuming that the solute is already whole.\n");
        }

        if (!wr->inputParams->bHaveFittedTraj) {
            /**
             * With wr->inputParams->bHaveWholeSolute == TRUE, we assume that the solute is whole in the COMPACT unit cell.
             * So in case that the solute is boken in the triclinic/rectangular unit cell, first put atoms in the compact box
             */
            put_atoms_in_compact_unitcell(pbcType, ecenterTRIC, box, x);
        }

        if (wr->waxsStep == 0) {
            WAXS_WRITE_FRAME_PDB_DEBUG(wr->inputParams->verbosityLevel > 2 or (wr->inputParams->verbosityLevel == 1 and wr->waxsStep == 0),
                    "withWholeSolute_checkIfWhole");
        }
    }

    WAXS_WRITE_FRAME_PDB_DEBUG((wr->inputParams->verbosityLevel > 2 or (wr->inputParams->verbosityLevel == 2 and wr->waxsStep == 0)), "protein_whole");

    /**** 2 ****/

    auto* xRaw = as_rvec_array(x.data());

    matrix Rfit = { { 1, 0, 0 }, { 0, 1, 0 }, { 0, 0, 1 } };

    /** Find the rotation matrix based on the fitgroup atoms */
    if (wr->inputParams->bRotFit and !wr->inputParams->bHaveFittedTraj) {
        reset_x(wr->nind_RotFit, wr->ind_RotFit, mtop->natoms, nullptr, xRaw, wr->w_fit);

        if (wr->waxsStep == 0 and !wr->inputParams->bHaveRefFile) {
            fprintf(stderr, "Writing first frame into x_ref...\n");

            /* Store the reference coordinates */
            for (int j = 0; j < mtop->natoms; j++) {
                copy_rvec(x[j], wr->x_ref[j]);
            }

            /* Inverse matrix is the idendity matrix */
            copy_mat(Rfit, Rinv);
        } else {
            calc_fit_R(DIM, mtop->natoms, wr->w_fit, wr->x_ref, xRaw, Rfit);

            /* Store the inverse-fit to rotate forces. R^T = R^-1 for rotation matrices */
            transpose(Rfit, Rinv);
        }

        if (wr->inputParams->verbosityLevel > 0) {
            fprintf(fpLog, "                       [ %7.4f %7.4f %7.4f ]\n"
                           "Rotation matrix Rfit = [ %7.4f %7.4f %7.4f ]\n"
                           "                       [ %7.4f %7.4f %7.4f ]\n",
                    Rfit[0][0], Rfit[0][1], Rfit[0][2],
                    Rfit[1][0], Rfit[1][1], Rfit[1][2],
                    Rfit[2][0], Rfit[2][1], Rfit[2][2]);
        }

        WAXS_WRITE_FRAME_PDB_DEBUG(wr->inputParams->verbosityLevel > 2 or (wr->inputParams->verbosityLevel > 1 and wr->waxsStep == 0), "after_to_rotmat");
    } else {
        /** Inverse matrix is the idendity matrix */
        copy_mat(Rfit, Rinv);
    }

    /**** 2.5 ****/

    /** Envelope must now be built before starting mdrun */
    if (!gmx_envelope_bHaveSurf(wr->wt[0].envelope)) {
        gmx_fatal(FARGS, "No envelope was defined. Provide the envelope and the envelope reference coordinate file with\n"
                  "environment variables GMX_ENVELOPE_FILE and GMX_WAXS_FIT_REFFILE\n");
    }

    real min = 1e20;

    rvec env_cent;

    if (TRICLINIC(box)) {
        /** If the box vectors are skewed (dodecahedron or so), then center the bounding sphere
            of the envelope at the center of the box */
        real env_R2;
        gmx_envelope_bounding_sphere(wr->wt[0].envelope, env_cent, &env_R2);

        if (wr->waxsStep == 0) {
            fprintf(stderr, "Radius of the envelopes bounding sphere = %g \n", sqrt(env_R2));
        }

        /** Check if envelope fits into box. */
        for (int d = 0; d < DIM; d++) {
            real bvec = 0.5 * sqrt(norm2(wr->local_state->box[d]));

            if (bvec < min) {
                min = bvec;
            }
        }

        if (env_R2 >= min * min) {
            if (!wr->inputParams->bHaveFittedTraj) {
                fprintf(stderr, "\n"
                        "*******************************************************************************\n"
                        "WARNING, the Bounding sphere of envelope is larger than the smallest box vector\n"
                        "If your protein rotates, the envelope might not fit into the unit cell any more\n"
                        "So better choose a larger simulation box.\n"
                        "*******************************************************************************\n");
            }

            fprintf(stderr, "   Radius of the bounding sphere = %g \n", sqrt(env_R2));
            fprintf(stderr, "   Max. radius of sphere fitting box = %g \n", sqrt(min));
        }
    } else {
        /** For a cubic or cuboid box (alpha = beta = gamma = 90 degree), center the maximum extension of the envelope
            IN EACH DIRECTION (x/y/z) separately */
        gmx_envelope_center_xyz(wr->wt[0].envelope, Rinv, env_cent);
    }

    /**** 3 ****/

    rvec env_cent_solv;
    clear_rvec(env_cent_solv);

    rvec protRefInBox;
    clear_rvec(protRefInBox);

    mvmul(Rinv, env_cent, env_cent_solv);

    rvec box_center;
    calc_box_center(ecenterTRIC, box, box_center);
    rvec_sub(box_center, env_cent_solv, protRefInBox);

    if (wr->inputParams->verbosityLevel > 1 and wr->waxsStep == 0) {
        fprintf(stderr, "\nBox center: %g %g %g", box_center[0], box_center[1], box_center[2]);
        fprintf(stderr, "\nCog center: %g %g %g", protRefInBox[0], protRefInBox[1], protRefInBox[2]);
    }

    mv_cog_to_rvec(mtop->natoms, x, wr->indA_prot, wr->nindA_prot, protRefInBox, nullptr);

    WAXS_WRITE_FRAME_PDB_DEBUG(wr->inputParams->verbosityLevel > 2 or (wr->inputParams->verbosityLevel > 1 and wr->waxsStep == 0), "envcenter_to_boxcenter");

    /**** 4 ****/

    /** Safety check: are solute atoms are far enough from the box walls? */
    int nWarnCloseBoxLast = wr->nWarnCloseBox;
    check_prot_box_distance(x, wr->indA_prot, wr->nindA_prot, box, wr, TRUE, nullptr);
    WAXS_WRITE_FRAME_PDB_DEBUG(wr->nWarnCloseBox > nWarnCloseBoxLast and wr->inputParams->verbosityLevel > 1, "box_dist_warning");

    /** Assert that all solute atoms are now inside the compact unit cell */
    if (!assert_all_atoms_inside_compact_box(x, wr->indA_prot, wr->nindA_prot, box, &pbc)) {
        WAXS_WRITE_FRAME_PDB_DEBUG(TRUE, "atoms_outside_box_error");

        gmx_fatal(FARGS, "Some atoms are outside of the compact box - check error message above for more details.\n"
                  "Wrote problematic frame into atoms_outside_box_error_%d.pdb\n", wr->waxsStep);
    }

    /** Check if envelope will fit into Compact Box */
    if (!gmx_envelope_bInsideCompactBox(wr->wt[0].envelope, Rinv, box, protRefInBox, &pbc, TRUE, 0.)) {
        // This fatal error will be removed when the filling the envelope with PBC shifts is fully implemented
        gmx_fatal(FARGS, "The envelope does not fit into the compact unitcell\n"
                  "Choose larger simulation box\n");
        printf("NOTE: The envelope does not fit into the compact box, using PBC shift of solvent to fill the envelope.\n");
    }

    /**** 5 ****/

    if (!wr->inputParams->bHaveFittedTraj) {
        put_atoms_in_compact_unitcell(pbcType, ecenterTRIC, box, x);
    }

    WAXS_WRITE_FRAME_PDB_DEBUG(wr->inputParams->verbosityLevel > 2 or (wr->inputParams->verbosityLevel > 1 and wr->waxsStep == 0), "all_in_box");

    /**** 6 ****/
    /** Rotate system */

    if (wr->inputParams->bRotFit and !wr->inputParams->bHaveFittedTraj) {
        reset_x(wr->nind_RotFit, wr->ind_RotFit, mtop->natoms, nullptr, xRaw, wr->w_fit);

        WAXS_WRITE_FRAME_PDB_DEBUG(wr->inputParams->verbosityLevel > 2 or (wr->inputParams->verbosityLevel > 1 and wr->waxsStep == 0), "before_rotate");

        if (wr->waxsStep == 0 or wr->inputParams->bHaveRefFile) {
            for (int j = 0; j < mtop->natoms; j++) {
                /** v' = R(v0-c0)' ; Rotate. Centered around COM (0,0,0) */
                rvec tmpvec2;
                mvmul(Rfit, x[j], tmpvec2);
                copy_rvec(tmpvec2, x[j]);
            }
        }

        WAXS_WRITE_FRAME_PDB_DEBUG(wr->inputParams->verbosityLevel > 2 or (wr->inputParams->verbosityLevel > 1 and wr->waxsStep == 0), "after_rotate");
    }

    /**** 7 ****/

    /** Move all atoms such that the protein is at the origin, so the protein is inside the envelope */
    mv_cog_to_rvec(mtop->natoms, x, wr->indA_prot, wr->nindA_prot, wr->origin, nullptr);

    WAXS_WRITE_FRAME_PDB_DEBUG(wr->inputParams->verbosityLevel > 2 or (wr->inputParams->verbosityLevel > 1 and wr->waxsStep == 0), "in_center");
    swaxs_debug("Centering of solute box done.\n");
}

void read_fit_reference(const char* fn, rvec x_ref[], int nsys, int* isol, int nsol, int* ifit, int nfit) {
    rvec *xtemp = nullptr;

    if (!x_ref) {
        snew(x_ref, nsys);
    }

    for (int i = 0; i < nsys; i++) {
        clear_rvec(x_ref[i]);
    }

    t_topology top;
    PbcType pbcType = PbcType::Unset;

    int nread = 0;
    get_coordnum(fn, &nread);

    if (nread == nsys) {
        /** Whole system */
        fprintf(stderr, "\nReading fit-reference coords of: system\n");
        matrix box;
        read_tps_conf(fn, &top, &pbcType, &x_ref, nullptr, box, FALSE);
    } else if (nread == nsol) {
        /** Solute */
        snew(xtemp, nread);

        for (int i = 0; i < nsys; i++) {
            clear_rvec(x_ref[i]);
        }

        fprintf(stderr,"\nReading fit-reference coords of: solute\n");
        matrix box;
        read_tps_conf(fn, &top, &pbcType, &xtemp, nullptr, box, FALSE);

        for (int i = 0; i < nread; i++) {
            copy_rvec(xtemp[i], x_ref[isol[i]]);
        }
    } else if (nread == nfit) {
        /** Fitgroup */
        snew(xtemp, nread);

        for (int i = 0; i < nsys; i++) {
            clear_rvec(x_ref[i]);
        }

        fprintf(stderr, "\nReading fit-reference coords of: fit-group\n");
        matrix box;
        read_tps_conf(fn, &top, &pbcType, &xtemp, nullptr, box, FALSE);

        for (int i = 0; i < nread; i++) {
            copy_rvec(xtemp[i], x_ref[ifit[i]]);
        }
    } else {
        fprintf(stderr, "nread = %i. nfit = %i, nsol = %i, nSystem = %i.\n", nread, nfit, nsol, nsys);
        gmx_fatal(FARGS, "Incorrect number of atoms in the reference coordinreades used to fit to the WAXS envelope! Please provide either the whole system, the solute, or the fit group only.\n");
    }

    if (xtemp) {
        sfree(xtemp);
    }
}

/**
 * Return the radius of gyration of the solute only (without taking the solvation layer into account)
 * Note: solute must be whole.
 */
real soluteRadiusOfGyration(t_waxsrec* wr, gmx::ArrayRef<gmx::RVec> x) {
    rvec tmp, diff;

    /** Get electron-weighted center-of-mass */
    rvec com;
    clear_rvec(com);

    real w = 0;

    for (int i = 0; i < wr->nindA_prot; i++) {
        real nelec = wr->nElectrons[wr->indA_prot[i]];
        w += nelec;
        svmul(nelec, x[wr->indA_prot[i]], tmp);
        rvec_inc(com, tmp);
    }

    svmul(1. / w, com, com);

    /**
     * Rg^2 = 1 / N sum[i] w[i] * (r[i] - com)^2,
     * where w[i] is the number of electrons, and N = sum[i] w[i] is the norm
     */
    real sum2 = 0;

    for (int i = 0; i < wr->nindA_prot; i++) {
        real nelec = wr->nElectrons[wr->indA_prot[i]];
        rvec_sub(x[wr->indA_prot[i]], com, diff);
        sum2 += nelec * norm2(diff);
    }

    return sqrt(sum2 / w);
}

/** Return average and variance in double prec. */
void average_stddev_d(double* x, int n, double* av, double* sigma, double* wptr) {
    *av = 0;

    double wsum = 0;

    for (int i = 0; i < n; i++) {
        double w = wptr ? wptr[i] : 1.;

        (*av) += w * x[i];
        wsum += w;
    }

    (*av) /= wsum;

    double var = 0;

    for (int i = 0; i < n; i++) {
        double w = wptr ? wptr[i] : 1.;
        double tmp = x[i] - (*av);

        var += w * gmx::square(tmp);
    }

    var /= wsum;

    *sigma = sqrt(var);
}

/** Return the average of x^2 in double prec. */
void average_x2_d(double* x, int n, double* av, double* wptr) {
    *av = 0;

    double wsum = 0;

    for (int i = 0; i < n; i++) {
        double w = wptr ? wptr[i] : 1.;

        (*av) += w * (x[i] * x[i]);
        wsum += w;
    }

    (*av) /= wsum;
}

/** Return the average of x*y in double prec. */
void average_xy_d(double* x, double* y, int n, double* av, double* wptr) {
    *av = 0;

    double wsum = 0;

    for (int i = 0; i < n; i++) {
        double w = wptr ? wptr[i] : 1.;

        (*av) += w * (x[i] * y[i]);
        wsum += w;
    }

    (*av) /= wsum;
}

/** Return the average of x*y in double prec. */
void sum_squared_residual_d(double* x, double* y, int n, double* chi2, double* wptr) {
    *chi2 = 0;

    for (int i = 0; i < n; i++) {
        double w = wptr ? wptr[i] : 1.;
        (*chi2) += w * gmx::square((x[i] - y[i]));
    }
}

double pearson_d(int n, double* x, double* y, double avx, double avy, double sigx, double sigy, double* wptr) {
    double cov = 0, wsum = 0;

    for (int i = 0; i < n; i++) {
        double w = wptr ? wptr[i] : 1.;

        cov += w * (x[i] - avx) * (y[i] - avy);
        wsum += w;
    }

    cov /= wsum;

    return cov / (sigx * sigy);
}

double sum_array_d(int n, double* x) {
    double sum = 0;

    for (int i = 0; i < n; i++) {
        sum += x[i];
    }

    return sum;
}

void nIndep_Shannon_Nyquist(t_waxsrec* wr, gmx::ArrayRef<gmx::RVec> x, const t_commrec* cr, gmx_bool bVerbose) {
    if (MASTER(cr)) {
        if (bVerbose) {
            printf("Number of independent points (Nyquist-Shannon) =");
        }

        for (int t = 0; t < wr->inputParams->nTypes; t++) {
            t_waxsrecType* wt = &wr->wt[t];

            /** First get maximum diameter from the simplified bounding sphere */
            rvec cent;
            real R;
            get_bounding_sphere_Ritter_COM(x, wr->indA_prot, wr->nindA_prot, cent, &R, FALSE);

            /** Nyquist-Shannon: N[indep] = (qmax -qmin) D / pi, where D is the maximum diameter of the solute */
            wt->nShannon = (wt->maxq - wt->minq) * 2 * R / M_PI;

            if (wt->nShannon > wt->nq) {
                wt->nShannon = wt->nq;
            }

            if (bVerbose) {
                printf(" %g", wt->nShannon);
            }
        }

        if (bVerbose) {
            printf("\n");
        }
    }

    if (PAR(cr)) {
        for (int t = 0; t < wr->inputParams->nTypes; t++) {
            t_waxsrecType* wt = &wr->wt[t];

            gmx_bcast(sizeof(real), &wt->nShannon, cr->mpi_comm_mygroup);
        }
    }
}

double* swaxs::countElectronsPerAtom(const gmx_mtop_t* topology) {
    int isum = 0;

    /* Bookkeeping: natoms = Sum _i ^ nmolblock ( molblock[i]->nmol * molblock[i]->natoms_mol ) */
    /* moltype.atoms->nr = molblock[i]->natoms_mol */

    int nSoluteGroups = countGroups(topology->groups, SimulationAtomGroupType::egcWAXSSolute);
    int nSolventGroups = countGroups(topology->groups, SimulationAtomGroupType::egcWAXSSolvent);

    double* nElectronsPerAtom;
    snew(nElectronsPerAtom, topology->natoms);

    for (int i = 0; i < topology->natoms; i++) {
        nElectronsPerAtom[i] = -1000;
    }

    /** Loop over molecule types */
    for (std::size_t mb = 0; mb < topology->molblock.size(); mb++) {
        const t_atoms* atoms = &topology->moltype[topology->molblock[mb].type].atoms;
        int nmols = topology->molblock[mb].nmol;
        int natoms_mol = atoms->nr;

        /* Loop over atoms in this molecule type */
        for (int iatom = 0; iatom < natoms_mol; iatom++) {
            int cmtype = atoms->atom[iatom].cmtype;

            if (cmtype >= topology->scattTypes.nCromerMannParameters or (cmtype < 0 and cmtype != NOTSET)) {
                gmx_fatal(FARGS, "Encountered invalid Cromer-Mann parameter (%d). Terminating.\n", cmtype);
            }

            /** Loop over molecules of this molecule type */
            for (int imol = 0; imol < nmols; imol++) {
                int itot = isum + imol * natoms_mol + iatom;

                gmx_bool bSolvent = (getGroupType(topology->groups, SimulationAtomGroupType::egcWAXSSolvent, itot) < nSolventGroups);
                gmx_bool bSolute = (getGroupType(topology->groups, SimulationAtomGroupType::egcWAXSSolute, itot) < nSoluteGroups);

                if ((bSolute or bSolvent) and cmtype < 0) {
                    gmx_fatal(FARGS, "Atom %d is in solute or solvent group, but the Cromer Mann type is %d\n", itot, cmtype);
                }

                /** cmtype < 0 means that we did not read Cromer-Mann parameters for this atom (because it is not in solute or solvent) */
                double nElectrons;

                if (cmtype >= 0) {
                    nElectrons = CROMERMANN_2_NELEC(topology->scattTypes.cromerMannParameters[cmtype]);
                } else {
                    nElectrons = -1;
                }

                nElectronsPerAtom[itot] = nElectrons;
            }
        }

        isum += nmols * natoms_mol;
    }

    /** For debugging */
    for (int i = 0; i < topology->natoms; i++) {
        if (nElectronsPerAtom[i] == -1000) {
            gmx_fatal(FARGS, "nelec[%d] was not set in countElectronsPerAtom()\n", i);
        }
    }

    return nElectronsPerAtom;
}

void swaxs::print2log(FILE* fp, const char* s, const char* fmt, ...) {
    char buf[STRLEN];
    char* p;
    const char* p1 = fmt;
    int length = 75;

    /* write Text and dots */
    if (*s == '\n') {
        length++;
    }

    p = buf;
    sprintf(p, "%s ", s);
    p += strlen(s) + 1;

    for (; (p - buf) < length - 1; p++) {
        sprintf(p, ".");
    }

    sprintf(p++, " ");

    va_list ap;

    /** Write number */
    va_start(ap, fmt);
    while (!isalpha(*(++p1)))
        ;

    switch (*p1) {
        case 'd':
            sprintf(p, fmt, va_arg(ap, int));
            break;
        case 'g':
            sprintf(p, fmt, va_arg(ap, double));
            break;
        case 's':
            sprintf(p, fmt, va_arg(ap, char*));
            break;
        default:
            gmx_fatal(FARGS, "Unsupported format character %c in print2log()\n", *p1);
    }

    va_end(ap);

    fprintf(fp, "%s\n", buf);
}
