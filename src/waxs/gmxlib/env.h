#pragma once

#include <gromacs/mdtypes/commrec.h>  // t_commrec
#include <waxs/types/waxsrec.h>  // t_waxsrec

namespace swaxs {

/**
 * Reads and parses environmental variables. Shows possible warnings and errors. Broadcasts values across nodes.
 *
 * Note: Reading and parsing happens only on the master node.
 *
 * @param[out] swaxs Values from environment variables are stored on the main SWAXS structure.
 * @param[in] comm Used to check if running on the master or a node, but also needed for broadcasting.
 */
void readEnvironmentVariables(t_waxsrec* swaxs, t_commrec* comm);

}
