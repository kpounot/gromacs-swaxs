#pragma once

#include <gromacs/fileio/oenv.h>  // gmx_output_env_t
#include <gromacs/mdtypes/commrec.h>  // t_commrec
#include <gromacs/mdtypes/forcerec.h>  // t_forcerec
#include <gromacs/mdtypes/state.h>  // t_state
#include <gromacs/pbcutil/pbc.h>  // PbcType
#include <gromacs/utility/basedefinitions.h>  // gmx_boo

#include <waxs/envelope/envelope.h>  // gmx_envelope
#include <waxs/gmxlib/gmx_envelope.h>
#include <waxs/types/waxsrec.h>  // t_waxsrec

/* suggested by Daniel Bowron in his EPSR tutorial (March 29,2010)
   Also see Sorensen et al, J. Chem. Phys. 113,9149 (2000)
   Note: We use NOT the Sorensen et al, formula, but instead
         f'(Q) = [ 1 + alpha*exp(-Q^2/2delta^2) ] * f(Q)
*/
/*#define WAXS_OPT_WATER_ALPHAO  0.12
#define WAXS_OPT_WATER_ALPHAH -0.48
#define WAXS_OPT_WATER_DELTA  22. */

/* Placeholders for deuteratable hydrogen atoms. These MUST correspond to
 * definitions in share/top/neutron-scatt-len-defs.itp
 */
#define NSL_H_DEUTERATABLE            -1000
#define NSL_H_DEUTERATABLE_BACKBONE   -2000

#define NEUTRON_SCATT_LEN_1H        (-3.7406)
#define NEUTRON_SCATT_LEN_2H          6.671
#define NEUTRON_SCATT_LEN_O           5.803

#define CONSTANT_HC 1239.842

#define CROMERMANN_2_NELEC(x) (x.c + x.a[0] + x.a[1] + x.a[2] + x.a[3])

/* This is called near the beginning of MD runs to setup all the variables
 * and allocate all necessary blocks given the conditions of the run.
 * Later functions should not have to allocate anything that lasts
 * for the duration of the simulation once this is invoked. */
void init_waxs_md(t_forcerec* fr, t_commrec* cr, t_inputrec* ir, const gmx_mtop_t* top_global,
        const gmx_output_env_t* oenv, double t0, const char* runFilePath, const char* trajectoryFilePath,
        const char* fnOut, const char* fnScatt, t_state* state_local, gmx_bool bRerunMD, gmx_bool bWaterOptSet,
        gmx_bool bReadI, gmx_bool started_from_checkpoint, gmx_bool use_gpu);

/* Iterates through various sub-structures of t_waxsrec to ensure safe removal */
void done_waxs_md(t_waxsrec* wr);

/* Checks if global are present, and if not, allocate them. */
void waxs_alloc_globalavs(t_waxsrec* wr);

/* Checks if global are present, and if so, free them. */
void waxs_free_globalavs(t_waxsrec* wr);

t_spherical_map* gen_qvecs_map(real minq, real maxq, int nqabs, int J, gmx_bool bDebug, t_commrec* cr,
        gmx_envelope* envelope, int Jmin, real Jalpha, gmx_bool bVerbose);

void do_waxs_md_low(const t_commrec* cr, gmx::ArrayRef<gmx::RVec> x, double t, int64_t step, t_waxsrec* wr,
        const gmx_mtop_t* mtop, const matrix box, PbcType pbcType, gmx_bool bDoLog);

/* Does MPI communication between global averages on master, and its local copies,
 * using a prepared nq_masterlist, qoff_masterlist on master to determine
 * size and offsets of arrays based on q-points handled by the node.
 * Combines dissemination and collection below into one function.
 * Without MPI, switches to an old inefficient method.
 * bCollect TRUE is inwards communication, FALSE is outwards communication */
void waxsDoMPIComm_qavgs(t_waxsrec* wr, const t_commrec* cr, gmx_bool bCollect);

void waxsEstimateNumberIndepPoints(t_waxsrec* wr, const t_commrec* cr, gmx_bool bWriteACF2File,
        gmx_bool bScale_varI_now);
