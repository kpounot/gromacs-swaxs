#pragma once

/* Authors' Notes:
 *  We would like this file to contain the main I/O and basic utilities of
 *  the types defined in waxsrec.h. One would ideally include just this file to gain
 *  the basic reading, writing, searching, and editing of the types.
 *
 *  Whereas, sfactors.c contains more complex routines more directly related to
 *  scattering factor calculations.
 *
 *  This makes it easier to fork general mathematical constructs in the future:
 *  - t_spherical_maps
 *  - vector_maps
 *  - t_reduced_atoms
 * */

#include <waxs/types/waxsrec.h>  // t_waxsrec
#include <waxs/types/waxstop.h>  // t_scatt_types, t_neutron_sl

void init_scattering_types(t_scatt_types* t);
void done_scattering_types(t_scatt_types* t);

void print_cmsf(FILE* out, t_cromer_mann* cm, int nTab);

int search_scattTypes_by_cm(const t_scatt_types* scattTypes, t_cromer_mann* cm_new);
int search_scattTypes_by_nsl(const t_scatt_types* scattTypes, t_neutron_sl* nsl_new);
/* Returns the index of the existing table if found, and NOTSET if not found. */

int add_scatteringType(t_scatt_types* sim_sf, t_cromer_mann* cmsf, t_neutron_sl* nusf);
/* Generic adder for sorted-unque list for simulations. */

t_waxsrec* init_t_waxsrec();

void done_t_waxsrec(t_waxsrec* t);

t_waxsrecType init_t_waxsrecType();

/** Gives the number of groups, minus the 'rest' group. */
int countGroups(const SimulationGroups& groups, SimulationAtomGroupType groupType);
