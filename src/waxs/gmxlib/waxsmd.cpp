/*
 *  This source file was written by Jochen Hub, with contributions from Po-chia Chen.
 */

#ifdef HAVE_CONFIG_H
#  include <config.h>  // GMX_GPU, GMX_MPI, GMX_DOUBLE
#endif

#include "./waxsmd.h"

#include <cstring>  // strlen, strncpy
#include <ctime>  // time, time_t
#include <memory>  // std::make_unique

#include <gromacs/fileio/confio.h>  // write_sto_conf_indexed
#include <gromacs/fileio/gmxfio.h>  // t_fileio, gmx_ffopen, gmx_ffclose, gmx_fexist
#include <gromacs/fileio/trrio.h>  // gmx_trr_write_frame
#include <gromacs/fileio/xvgr.h>  // xvgropen, read_xvg
#include <gromacs/gmxlib/network.h>  // gmx_bcast, gmx_sumd
#include <gromacs/gmxpreprocess/notset.h>  // NOTSET
#include <gromacs/math/do_fit.h>  // reset_x
#include <gromacs/math/functions.h>  // gmx::square
#include <gromacs/math/gmxcomplex.h>  // t_complex
#include <gromacs/math/units.h>  // BOLTZ
#include <gromacs/math/vec.h>  // sqr, dsqr
#include <gromacs/mdlib/gmx_omp_nthreads.h>  // gmx_omp_nthreads_get, emntDefault
#include <gromacs/mdtypes/commrec.h>  // t_commrec
#include <gromacs/mdtypes/forcerec.h>  // t_forcerec
#include <gromacs/mdtypes/inputrec.h>  // t_inputrec
#include <gromacs/mdtypes/md_enums.h>  // ENUM_*
#include <gromacs/mdtypes/state.h>  // t_state
#include <gromacs/pbcutil/pbc.h>  // PbcType
#include <gromacs/random/seed.h>
#include <gromacs/random/uniformrealdistribution.h>  // UniformRealDistribution
#include <gromacs/simd/simd.h>  // gmx::SimdReal, gmx_simd_mul_r
#include <gromacs/simd/simd_math.h>  // gmx::sincos
#include <gromacs/topology/mtop_util.h>  // t_atoms, gmx_mtop_global_atoms
#include <gromacs/utility/fatalerror.h>  // gmx_fatal
#include <gromacs/utility/smalloc.h>  // snew, sfree

#include <waxs/envelope/density.h>  // swaxs::envelope_solvent_density_next_frame, swaxs::envelope_solvent_density_add_atom, swaxs::envelope_solvent_density_bcast, swaxs::envelope_solvent_ft, swaxs::envelope_have_solvent_ft, swaxs::envelope_get_solvent_nelec
#include <waxs/envelope/envelope.h>  // gmx_envelope
#include <waxs/envelope/grid_density.h>  // envelope_grid_density_next_frame, envelope_grid_density_add_atom, envelope_grid_density_close_frame
#include <waxs/envelope/volume.h>  // swaxs::envelope_volume
#include <waxs/gmxcomplex.h>  // t_complex_d
#include <waxs/gmxlib/env.h>  // swaxs::read_env_variables
#include <waxs/gmxlib/gmx_envelope.h>
#include <waxs/gmxlib/names.h>  // EWAXS*
#include <waxs/gmxlib/pbcshifts.h>  // get_solvation_shell_periodic, setAtomShiftsInsideEnvelope, summarizeAtomShifts
#include <waxs/gmxlib/sftypeio.h>  // init_t_waxsrecType
#include <waxs/gmxlib/solvent.h>  // swaxs::Solvent
#include <waxs/gmxlib/solvent_scattering.h>  // swaxs::solventScatteringFromRdfs
#include <waxs/gmxlib/swaxs_datablock.h>  // waxs_datablock
#include <waxs/gmxlib/swaxs_output.h>  // swaxs::SwaxsOutput
#include <waxs/gmxlib/timing.h>  // swaxs::Timing, SWAXS_STEPS_RESET_TIME_AVERAGES
#include <waxs/gmxlib/waxsmd_utils.h>  // average_stddev_d, average_x2_d, average_xy_d, CMSF_q, swaxs::countElectronsPerAtom, nIndep_Shannon_Nyquist, pearson_d, read_fit_reference, soluteRadiusOfGyration, sum_array_d, sum_squared_residual_d, waxs_prepareSoluteFrame
#include <waxs/types/enums.h>  // escatter
#include <waxs/types/waxsrec.h>  // t_waxsrec
#include <waxs/types/waxstop.h>  // t_scatt_types
#include <waxs/gmxlib/cuda_tools/scattering_amplitude_cuda.cuh>  // calculate_dkI_GPU, init_gpudata_type, push_unitFT_to_GPU, update_envelope_solvent_density_GPU, compute_scattering_amplitude_cuda

#define UPDATE_CUMUL_AVERAGE(f, w, fadd) (f = 1.0 / (w) * ((w - 1.0) * (f) + 1.0 * (fadd)))

#define WAXS_WARN_BOX_DIST 0.5

#define WAXS_ENSEMBLE(x) (x->inputParams->ensemble_nstates > 1)

#include <waxs/debug.h>  // swaxs_debug

/** Uncomment some of the following definitions if you want more debugging output */
// #define RERUN_CalcForces
// #define PRINT_A_dkI

enum { mpi_typeINT, mpi_typeFLOAT, mpi_typeDOUBLE, mpi_typeREAL };

static const char* waxs_simd_string() {
#if defined(GMX_WAXS_NO_SIMD)
    return "None";
#elif defined(GMX_X86_AVX_256)
    return "AVX-256";
#elif defined(GMX_X86_AVX_128_FMA)
    return "AVX-128-FMA";
#elif defined(GMX_X86_SSE4_1)
    return "SSE-4.1";
#elif defined(GMX_X86_SSE2)
    return "SSE-2";
#else
    return "Unknown";
#endif
}

static void assert_scattering_type_is_set(int stype, int type, int atomno) {
    if (stype == NOTSET) {
        gmx_fatal(FARGS, "Found a scattering type (%s) that is NOTSET, atom no %d. This should not happen.\n",
                (type == escatterXRAY) ? "Cromer-Mann type" : "Neutron scatterling length type", atomno + 1);
    }
}

/**
 * Lower function that handles an individual communication step.
 * nPerq is the number of eTypes that will be sent per vector q.
 * e.g. avAglobal is of size nq * dcomplex, which reduces to 1 complex(2 doubles) per q.
 */
static void waxsDoMPIComm_qavgs_low(t_waxsrecType* wt, const t_commrec* cr, void* loc_buf, void* glb_buf, int datatype,
        int nPerq, gmx_bool bCollect) {
    int nq_loc = wt->qvecs->qhomenr;  // Number of q's this node is responsible for.
    int loc_cnts;  // Size of each package on outer nodes.
    int* glb_cnts = nullptr;  // Master array: expected number of incoming bytes from each node.
    int* glb_offs = nullptr;  // Master array: offsets to put into destination.
    // gmx_bool bLocBufEmpty = FALSE;

    int64_t test;
    int nnodes = cr->nnodes - cr->npmenodes;
    int limit = 2147483647;  // 2 Gigs, largest possible int?
    const gmx_bool bVerbose = FALSE;

    waxs_datablock* wd = wt->wd;

    /** Test for size */
    test = wt->qvecs->n * nPerq;

    if (test > limit) {
        gmx_fatal(FARGS, "Integer overflow detected in waxsDoMPIComm!\n"
                "Perhaps your system is too big for WAXS checkpointing.");
    }

    loc_cnts = nq_loc * nPerq;

    if (loc_buf == nullptr) {
        if (loc_cnts > 0) {
            gmx_fatal(FARGS, "Error in waxsDoMPIComm_qavgs_low(): loc_cnts = %d, but loc_buf = nullptr (rank %d)\n",
                    loc_cnts, cr->nodeid);
        }

        /**
         * If no q-vectors are stored on a node, loc_buf is zero, leadig to an MPI error below.
         * Therefore, in this case, allocate a small arrray and clear it below
         */
        // bLocBufEmpty = TRUE;
        loc_buf = new double[8];
    }

    /**
     * Construct count arrays on master.
     * This is counting the number of eType that each will send.
     */
    if (MASTER(cr)) {
        snew(glb_cnts, nnodes);
        snew(glb_offs, nnodes);

        for (int i = 0; i < nnodes; i++) {
            glb_cnts[i] = wd->masterlist_qhomenr[i] * nPerq;
            test = wd->masterlist_qstart[i] * nPerq;

            if (test > limit) {
                gmx_fatal(FARGS, "Integer overflow detected in waxsDoMPIComm!\n"
                        "Perhaps your system is too big for WAXS checkpointing, or"
                        "convince the devs to implement multi-step communications.");
            }

            glb_offs[i] = test;
        }
    }

    if (PAR(cr)) {
        gmx_barrier(cr->mpi_comm_mygroup);
    }

    if (bVerbose) {
        if (MASTER(cr)) {
            for (int i = 0; i < nnodes; i++) {
                fprintf(stderr, "Collecting to master: %2d) glb_cnts = %4d glb_offs = %4d to %p\n", i, glb_cnts[i],
                        glb_offs[i], glb_buf);
            }
        }

        if (PAR(cr)) {
            gmx_barrier(cr->mpi_comm_mygroup);
        }

        for (int i = 0; i < nnodes; i++) {
            if (cr->nodeid == i) {
                fprintf(stderr, "Sending from master: %2d) loc_cnts = %4d from %p\n", i, loc_cnts, loc_buf);
            }

            if (PAR(cr)) {
                gmx_barrier(cr->mpi_comm_mygroup);
            }
        }
    }

    if (datatype == mpi_typeREAL) {
#ifdef GMX_DOUBLE
        datatype = mpi_typeDOUBLE;
#else
        datatype = mpi_typeFLOAT;
#endif
    }

    switch (datatype) {
        case mpi_typeINT:
            if (bCollect) {
                MPI_Gatherv(loc_buf, loc_cnts, MPI_INT,
                        glb_buf, glb_cnts, glb_offs, MPI_INT, MASTERRANK(cr), cr->mpi_comm_mygroup);
            } else {
                MPI_Scatterv(glb_buf, glb_cnts, glb_offs, MPI_INT,
                        loc_buf, loc_cnts, MPI_INT, MASTERRANK(cr), cr->mpi_comm_mygroup);
            }

            break;
        case mpi_typeFLOAT:
            if (bCollect) {
                MPI_Gatherv(loc_buf, loc_cnts, MPI_FLOAT,
                        glb_buf, glb_cnts, glb_offs, MPI_FLOAT, MASTERRANK(cr), cr->mpi_comm_mygroup);
            } else {
                MPI_Scatterv(glb_buf, glb_cnts, glb_offs, MPI_FLOAT,
                        loc_buf, loc_cnts, MPI_FLOAT, MASTERRANK(cr), cr->mpi_comm_mygroup);
            }

            break;
        case mpi_typeDOUBLE:
            if (bCollect) {
                MPI_Gatherv(loc_buf, loc_cnts, MPI_DOUBLE,
                        glb_buf, glb_cnts, glb_offs, MPI_DOUBLE, MASTERRANK(cr), cr->mpi_comm_mygroup);
            } else {
                MPI_Scatterv(glb_buf, glb_cnts, glb_offs, MPI_DOUBLE,
                        loc_buf, loc_cnts, MPI_DOUBLE, MASTERRANK(cr), cr->mpi_comm_mygroup);
            }

            break;
        default: gmx_fatal(FARGS, "Undefined datatype given to waxsDoMPIComm_qavgs_low!\n");
    }

    if (MASTER(cr)) {
        sfree(glb_cnts);
        sfree(glb_offs);
    }

    // if (bLocBufEmpty) {
    //     delete loc_buf;
    // }
}

void waxsDoMPIComm_qavgs(t_waxsrec* wr, const t_commrec* cr, gmx_bool bCollect) {
#ifdef GMX_MPI
    time_t begt;

    if (MASTER(cr)) {
        time(&begt);

        if (bCollect) {
            fprintf(stderr, "\nDoing MPI collection   of q-averages...");
        } else {
            fprintf(stderr, "\nDoing MPI distribution of q-averages...");
        }

        fflush(stdout);
    }

    if (!bCollect) {
        gmx_bcast(sizeof(double), &wr->waxsStep, cr->mpi_comm_mygroup);
    }

    if (!wr->inputParams->bVacuum) {
        if (!bCollect) {
            gmx_bcast(sizeof(double), &wr->solElecDensAv, cr->mpi_comm_mygroup);
            gmx_bcast(sizeof(double), &wr->nElecAvB, cr->mpi_comm_mygroup);
        }
    }

    /** Distibute/collect data in waxs_datablock, specifit to each scatterin type */
    for (int t = 0; t < wr->inputParams->nTypes; t++) {
        t_waxsrecType* wt = &wr->wt[t];
        waxs_datablock* wd = wt->wd;

        if (!bCollect) {
            gmx_bcast(sizeof(double), &wd->normA, cr->mpi_comm_mygroup);
        }

        waxsDoMPIComm_qavgs_low(wt, cr, wd->D,        wd->Dglobal,        mpi_typeDOUBLE, 1, bCollect);
        waxsDoMPIComm_qavgs_low(wt, cr, wd->avA,      wd->avAglobal,      mpi_typeDOUBLE, 2, bCollect);
        waxsDoMPIComm_qavgs_low(wt, cr, wd->avAsq,    wd->avAsqglobal,    mpi_typeDOUBLE, 1, bCollect);
        waxsDoMPIComm_qavgs_low(wt, cr, wd->avA4,     wd->avA4global,     mpi_typeDOUBLE, 1, bCollect);
        waxsDoMPIComm_qavgs_low(wt, cr, wd->av_ReA_2, wd->av_ReA_2global, mpi_typeDOUBLE, 1, bCollect);
        waxsDoMPIComm_qavgs_low(wt, cr, wd->av_ImA_2, wd->av_ImA_2global, mpi_typeDOUBLE, 1, bCollect);

        if (!wr->inputParams->bVacuum) {
            if (!bCollect) {
                gmx_bcast(sizeof(double), &wd->normB, cr->mpi_comm_mygroup);
            }

            waxsDoMPIComm_qavgs_low(wt, cr, wd->avB,      wd->avBglobal,      mpi_typeDOUBLE, 2, bCollect);
            waxsDoMPIComm_qavgs_low(wt, cr, wd->avBsq,    wd->avBsqglobal,    mpi_typeDOUBLE, 1, bCollect);
            waxsDoMPIComm_qavgs_low(wt, cr, wd->avB4,     wd->avB4global,     mpi_typeDOUBLE, 1, bCollect);
            waxsDoMPIComm_qavgs_low(wt, cr, wd->av_ReB_2, wd->av_ReB_2global, mpi_typeDOUBLE, 1, bCollect);
            waxsDoMPIComm_qavgs_low(wt, cr, wd->av_ImB_2, wd->av_ImB_2global, mpi_typeDOUBLE, 1, bCollect);
        }
    }

    if (MASTER(cr)) {
        time_t endt;
        time(&endt);
        printf(" MPI communication took %d seconds.\n", (int)(endt - begt));
    }
#else
    gmx_fatal(FARGS, "MPI Communications required for checkpointing. Don't use the old functions.\n");
#endif
}

static real droplet_volume(t_waxsrec* wr) {
    return swaxs::envelope_volume(wr->wt[0].envelope);
}

void waxsEstimateNumberIndepPoints(t_waxsrec* wr, const t_commrec* cr, gmx_bool bWriteACF2File, gmx_bool bScale_varI_now) {
    for (int t = 0; t < wr->inputParams->nTypes; t++) {
        t_waxsrecType* wt = &wr->wt[t];

        waxs_datablock* wd = wt->wd;
        double* D = wd->D;
        t_spherical_map* qvecs = wt->qvecs;

        int* ind = qvecs->ind;
        int qstart = qvecs->qstart;
        int qhomenr = qvecs->qhomenr;
        int nabs = qvecs->nabs;
        real invexp = 1. / exp(1.);

        if (MASTER(cr)) {
            printf("\nEstimating the number of independent I per |q|, scattering type %d.\n", t);
            printf("    Set environment variable GMX_WAXS_PRINT_NINDEP to print to file.\n");
            fprintf(wr->swaxsOutput->fpLog, "\nEstimating the number of independent I per |q|, scattering type %d:\n", t);
        }

        if (wd->Nindep == nullptr) {
            snew(wd->Nindep, wt->nq);
        }

        FILE* fp = nullptr;
        char buf[1024];

        if (bWriteACF2File and MASTER(cr)) {
            sprintf(buf, "acf_step_%d.dat", wr->waxsStep);

            /** avoid use of oenv here - so we don't have to pass it across the functions */
            fp = gmx_ffopen(buf, "w");

            fprintf(fp, "@    title \"ACF of D(q) vs. angle\"\n"
                    "@    xaxis  label \"\xf\f{} [Rad]\"\n"
                    "@    yaxis  label \"ACF\"\n@type xy\n");
        }

        int Jmax = 0;
        int nbinsmax = 0;

        for (int i = 0; i < nabs; i++) {
            int thisJ = ind[i + 1] - ind[i];
            int nbins = (int)floor(M_PI / (2 * sqrt(M_PI / thisJ)));

            Jmax = (thisJ > Jmax) ? thisJ : Jmax;
            nbinsmax = (nbins > nbinsmax) ? nbins : nbinsmax;
        }

        double* Dglob = nullptr;
        real* acf = nullptr;
        int* n = nullptr;

        snew(Dglob, Jmax);
        snew(acf, nbinsmax);
        snew(n, nbinsmax);

        int iset = 0;

        /** Loop over absolute q values */
        for (int i = 0; i < nabs; i++) {
            int thisJ = ind[i + 1] - ind[i];

            for (int j = 0; j < Jmax; j++) {
                Dglob[j] = 0.;
            }

            /** Loop over q-vectors with fixed |q|
                Only over overlap of [qstart,qend) and [ind[i],ind[i+1]) */
            for (int j = ind[i]; j < ind[i + 1]; j++) {
                if (j >= qstart and j < (qstart + qhomenr)) {
                    int jj = j - qstart;
                    int k = j - ind[i];

                    if (k < 0 or k >= thisJ) {
                        gmx_fatal(FARGS, "Error in waxsEstimateNumberIndepPoints(). Found k = %d (thisJ = %d)", k, thisJ);
                    }

                    Dglob[k] = D[jj];
                }
            }

            if (PAR(cr)) {
                gmx_sumd(thisJ, Dglob, cr);
            }

            if (MASTER(cr)) {
                if (thisJ == 1) {
                    wd->Nindep[i] = 1.;
                } else {
                    if (thisJ < 20) {
                        fprintf(stderr, "\n\nWARNING while estimating number of independent points.\n"
                                "have only J = %d points. This may get inaccurate.\n\n", thisJ);
                    }

                    /** 2 * sqrt(pi / J) is roughly the angle betwen adjacent q-vectors */
                    int nbins = (int)floor(M_PI / (2 * sqrt(M_PI / thisJ)));

                    if (nbins > nbinsmax or thisJ > Jmax) {
                        gmx_fatal(FARGS, "Error - this should not happen\n");
                    }

                    real dphi = M_PI / nbins;

                    double Dav = 0.;
                    double Dvar = 0.;

                    for (int k = 0; k < thisJ; k++) {
                        Dav += Dglob[k];
                    }

                    Dav /= thisJ;

                    for (int k = 0; k < thisJ; k++) {
                        Dvar += gmx::square(Dglob[k] - Dav);
                    }

                    Dvar /= thisJ;

                    for (int iphi = 0; iphi < nbins; iphi++) {
                        acf[iphi] = 0.;
                        n[iphi] = 0;
                    }

                    real invqabs = 1. / qvecs->abs[i];

                    for (int k = 0; k < thisJ; k++) {
                        for (int kk = k; kk < thisJ; kk++) {
                            /* Get angle between vector k and kk */
                            double tmpd = iprod(qvecs->q[ind[i] + k], qvecs->q[ind[i] + kk]) * invqabs * invqabs;

                            double phi;

                            /* Catch rounding errors */
                            if (tmpd > 1) {
                                phi = 0.;
                            } else if (tmpd < -1) {
                                phi = M_PI;
                            } else {
                                phi = acos(tmpd);
                            }

                            int iphi = (int)(round(phi / dphi));

                            if (iphi < nbins) {
                                acf[iphi] += (Dglob[k] - Dav) * (Dglob[kk] - Dav);
                                n[iphi]++;
                            }
                        }
                    }

                    for (int iphi = 0; iphi < nbins; iphi++) {
                        /* Normalize to one at phi == 0 */
                        acf[iphi] /= (n[iphi] * Dvar);
                    }

                    /* Check where acf drops below 1/e, and interpolate with prev. point */
                    int iphi = 0;

                    while (acf[++iphi] > invexp)
                        ;

                    real phi0 = (iphi - 1) * dphi;
                    real phi_exp1 = phi0 + dphi * (invexp - acf[iphi - 1]) / (acf[iphi] - acf[iphi - 1]);

                    /* Assume that acf = exp(-phi / tau) * 0.5 * (3 cos^2(x) - 1) */
                    real rtmp = 2 * invexp / (3 * gmx::square(cos(phi_exp1)) - 1);
                    real tau_phi = (rtmp > 1e-20) ? -phi_exp1 / log(rtmp) : 50.;

                    /* - autocorrelation area =
                       2pi * int_0^Pi/2 dtheta sin(theta) * exp(-theta/tau)
                       = 2pi tau * (tau-exp(-pi/2tau))/(1+tau^2)
                       - muliply by 2 because there is additional correlation on the opposite
                       side of the sphere
                     */
                    real corr_area = 2 * 2 * M_PI * tau_phi * (tau_phi - exp(-M_PI / (2 * tau_phi))) / (1 + gmx::square(tau_phi));

                    if (corr_area > 4 * M_PI) {
                        corr_area = 4 * M_PI;
                    } else if (corr_area < 4 * M_PI / thisJ) {
                        corr_area = 4 * M_PI / thisJ;
                    }

                    wd->Nindep[i] = 4 * M_PI / corr_area;

                    if (bWriteACF2File) {
                        fprintf(fp, "@type xy\n@   s%d legend  \"i = %d, q = %4g, \\xt\\f{}=%g, N\\sind\\N=%.1f\"\n",
                                iset++, i, qvecs->abs[i], tau_phi, wd->Nindep[i]);

                        for (iphi = 0; iphi < nbins; iphi++) {
                            fprintf(fp, "%g  %g   %d\n", iphi * dphi, acf[iphi], n[iphi]);
                        }
                    }
                }

                fprintf(wr->swaxsOutput->fpLog, "\tACF %2d of %d (q = %8g): %6.1f of %d points independent\n", i, nabs, qvecs->abs[i],
                        wd->Nindep[i], thisJ);
            }
        }

        if (MASTER(cr)) {
            printf("\n");
        }

        sfree(Dglob);
        sfree(acf);
        sfree(n);

        if (fp and MASTER(cr)) {
            gmx_ffclose(fp);
            printf("Wrote autocorrelation functions of D(q) to %s for each |q|\n", buf);
        }

        if (PAR(cr)) {
            gmx_bcast(wt->nq * sizeof(double), wd->Nindep, cr->mpi_comm_mygroup);
        }

        if (bScale_varI_now) {
            if (MASTER(cr)) {
                printf("Scaling the variance of I(q) by 1/# independent data points\n");
            }

            for (int i = 0; i < nabs; i++) {
                /** devide variance by # independent q-vectors */
                wd->varI[i] /= wd->Nindep[i];
            }
        }
    }

    wr->bHaveNindep = TRUE;
}

static void write_stddevs(t_waxsrec* wr, int64_t step, int t) {
    t_waxsrecType* wt = &wr->wt[t];
    FILE* fp = wr->swaxsOutput->fpStddevs[t];

    fprintf(fp, "\n# Std deviations %d simulation step ", wr->waxsStep);
    fprintf(fp, "%" PRId64, step);
    fprintf(fp, "\n");

    for (int i = 0; i < wt->nq; i++) {
        fprintf(fp, "%8g  %12g", wt->qvecs->abs[i], sqrt(wt->wd->varI[i]));

        if (wt->Iexp_sigma) {
            fprintf(fp, "  %12g", wt->f_ml * wt->Iexp_sigma[i]);
        }

        if (wt->wd->I_errSolvDens) {
            fprintf(fp, "  %12g", fabs(wr->epsilon_buff * wt->wd->I_errSolvDens[i]));
        }

        fprintf(fp, "\n");
    }

    fprintf(fp, "\n");
    fflush(fp);
}

static void write_intensity(FILE* fp, t_waxsrec* wr, int type) {
    t_waxsrecType* wt = &wr->wt[type];

    fprintf(fp, "\n@type xydy\n");

    for (int i = 0; i < wt->nq; i++) {
        fprintf(fp, "%8g  %12g %12g %12g %12g %12g\n", wt->qvecs->abs[i], wt->wd->I[i], sqrt(wt->wd->varI[i]),
                !wr->inputParams->bVacuum ? wt->wd->I_avAmB2[i] : 0.0, !wr->inputParams->bVacuum ? wt->wd->I_varA[i] : 0.0,
                !wr->inputParams->bVacuum ? wt->wd->I_varB[i] : 0.0);
    }
}

/** Get total number of electrons or NSLs inside the enelope */
static real number_of_electrons_or_NSLs(t_waxsrecType* wt, int* scatTypeIndex, int isize, const gmx_mtop_t* mtop) {
    real sum = 0;

    for (int i = 0; i < isize; i++) {
        if (wt->type == escatterXRAY) {
            sum += CROMERMANN_2_NELEC(mtop->scattTypes.cromerMannParameters[scatTypeIndex[i]]);
        } else {
            sum += wt->nsl_table[scatTypeIndex[i]];
        }
    }

    return sum;
}

/* Estimate the contrast of the solute. This is used to scale down the SAXS-derived forces.

   Rationale:

   SAXS: When we dispalce a solute groups by some SAXS-derived forces, any gaps will be
   filled by solvent, hence the change in the intensity is smaller than expected from the
   solute atoms alone. A good estimate for this reduced effect of conformational transitions
   on the SAXS/SANS curves is given by the contrast
     (rho_solute-rho_solvent) / rho_solvent.
   In other words: when computing the intensity gradients on solute atoms along, we would
   overestimate the true gradients.

   SANS: This effect is most apparent when doing SANS, here, if we have negative contrast
   in case high D2O concentration, the correcting factor is even negative, flipping the direction
   of SANS-derived forces. In other words: In this case, when the solute moves left, the
   contrast moves to the right.

   KNOWN ISSUES: If different domains have a different perdeuteration level, this
   correction does no work. In this case, we would have to work with buffer-subtracted
   form factors ("reduced form factors"). This may be the best solution for computing
   intensity gradients anyway, but we would need atomic forces.
*/
static void updateSoluteContrast(t_waxsrec* wr, const t_commrec* cr) {
    if (MASTER(cr) and !wr->solvent) {
        gmx_incons("waxs_solv not available in updateSoluteContrast()\n");
    }

    for (int t = 0; t < wr->inputParams->nTypes; t++) {
        t_waxsrecType* wt = &wr->wt[t];

        if (MASTER(cr)) {
            double soluteDensity = wt->soluteSumOfScattLengths / wr->soluteVolAv;
            double solventDensity = wr->solvent->averageScatteringLengthDensity[t];
            wt->soluteContrast = soluteDensity - solventDensity;
            wt->contrastFactor = wt->soluteContrast / soluteDensity;
        }

        if (PAR(cr)) {
            gmx_bcast(sizeof(double), &wt->soluteContrast, cr->mpi_comm_mygroup);
            gmx_bcast(sizeof(double), &wt->contrastFactor, cr->mpi_comm_mygroup);
        }
    }
}

/** Return average electron density inside the envelope (for A or B system) */
static real droplet_density(t_waxsrec* wr, int* index, int isize, double* nElectrons, real* nelecRet) {
    double nelec = 0.;

    for (int i = 0; i < isize; i++) {
        double nel = nElectrons[index[i]];

        if (nel < 0) {
            gmx_fatal(FARGS, "While computing the density of the droplet in droplet_density(), stepped over"
                    " atom %d does not have Cromer-Mann parameters (nElectrons = %g)\n", index[i] + 1, nel);
        }

        nelec += nel;
    }

    /** There is no volume with bVacuum */
    real dens = wr->inputParams->bVacuum ? -1. : nelec / droplet_volume(wr);

    *nelecRet = nelec;

    return dens;
}

static void read_intensity_and_interpolate(const char* fn, real minq, real maxq, int nq, gmx_bool bRequireErrors,
        gmx_bool bReturnVariance, double** I_ret, double** Ierr_ret) {
    int j;

    double** y;
    int ncol;
    int nlines = read_xvg(fn, &y, &ncol);

    /** Now: q = y[0], I = y[1], sigma = y[2] */
    if (ncol != 3 and ncol != 2) {
        gmx_fatal(FARGS, "Expected 2 or 3 rows in file %s (q, Intensity, and (optionally) sigma(Intensity). Found %d rows.\n",
                fn, ncol);
    }

    if (bRequireErrors and ncol < 3) {
        gmx_fatal(FARGS, "Require uncertainties in intensity file %s, but found only %d columns\n", fn, ncol);
    }

    if (ncol == 2) {
        printf("WAXS-MD: No experimental errors found in %s\n", fn);
    }

    if (y[0][0] > minq) {
        gmx_fatal(FARGS, "Smallest q-value in %s is %g, but the smallest q requested is %g. Provide a different\n "
                "scattering intensity file or increase waxs-startq in the mdp file\n", fn, y[0][0], minq);
    }

    if (y[0][nlines - 1] < maxq) {
        gmx_fatal(FARGS, "Largest q-value in %s is %g, but the largest q requested is %g. Provide a different\n "
                "scattering intensity file or decrease waxs-endq in the mdp file\n", fn, y[0][nlines - 1], maxq);
    }

    double* I;
    double* Ierr;

    snew(I, nq);

    if (ncol >= 3) {
        snew(Ierr, nq);
    } else {
        Ierr = nullptr;
    }

    real dq = nq > 1 ? (maxq - minq) / (nq - 1) : 0.0;

    real left = 0.;
    real right = 0.;

    for (int i = 0; i < nq; i++) {
        real q = minq + i * dq;
        gmx_bool bFound = FALSE;

        /** Simple linear interpolation. Maybe we can improve this later. */
        for (j = 0; j < nlines - 1; j++) {
            if ((left = y[0][j]) <= q and q <= (right = y[0][j + 1])) {
                bFound = TRUE;
                break;
            }
        }

        if (!bFound) {
            gmx_fatal(FARGS, "Error in read_intensity_and_interpolate(). This should not happen.\n");
        }

        real Ileft = y[1][j];
        real Iright = y[1][j + 1];
        real slopeI = (Iright - Ileft) / (right - left);
        I[i] = Ileft + (q - left) * slopeI;

        if (ncol >= 3) {
            real Isigleft = y[2][j];
            real Isigright = y[2][j + 1];
            real slopeIsig = (Isigright - Isigleft) / (right - left);
            Ierr[i] = Isigleft + (q - left) * slopeIsig;

            if (bReturnVariance) {
                Ierr[i] = gmx::square(Ierr[i]);
            }

            if (Ierr[i] < 1e-20) {
                gmx_fatal(FARGS, "sigma of exprimental I(q) is zero at q = %g. Provide a non-zero sigma.\n", q);
            }
        }
    }

    *I_ret = I;
    *Ierr_ret = Ierr;
}

static void read_waxs_curves(const char* fnScatt, t_waxsrec* wr, t_commrec* cr) {
    char* fnUsed = nullptr;
    char* base = nullptr;
    char* fnInterp = nullptr;

    if (MASTER(cr)) {
        int len = strlen(fnScatt);

        snew(base, len);

        strncpy(base, fnScatt, len - 4);
        base[len - 4] = '\0';
        fprintf(stderr, "Using base file name for WAXS curve reading: %s\n", base);

        snew(fnUsed, len + 10);
        snew(fnInterp, len + 10);
    }

    for (int t = 0; t < wr->inputParams->nTypes; t++) {
        t_waxsrecType* wt = &wr->wt[t];

        if (wt->nq <= 0 or wt->nq > 999999) {
            gmx_incons("Invalid waxs.nq in read_waxs_curves()\n");
        }

        if (MASTER(cr)) {
            /* With one type:       Reading file fnScatt, such as saxs-curve.xvg
             * With multiple types: Reading files such as saxs-curve_1.xvg, saxs-curve_2.xvg,
             */
            if (wr->inputParams->nTypes > 1) {
                sprintf(fnUsed, "%s_%d.xvg", base, t + 1);
                sprintf(fnInterp, "Iinterp_%d.dat", t + 1);
            } else {
                sprintf(fnUsed, "%s", fnScatt);
                sprintf(fnInterp, "Iinterp.dat");
            }

            printf("WAXS-MD: Reading intensities from %s\n", fnUsed);
            printf("WAXS-MD: Writing interpolated intensities to %s\n", fnInterp);
            read_intensity_and_interpolate(fnUsed, wt->minq, wt->maxq, wt->nq, FALSE, FALSE, &wt->Iexp, &wt->Iexp_sigma);

            if (wt->Iexp_sigma == nullptr and (
                    wr->inputParams->weightsType == ewaxsWeightsEXPERIMENT or
                    wr->inputParams->weightsType == ewaxsWeightsEXP_plus_CALC or
                    wr->inputParams->weightsType == ewaxsWeightsEXP_plus_SOLVDENS or
                    wr->inputParams->weightsType == ewaxsWeightsEXP_plus_CALC_plus_SOLVDENS
            )) {
                gmx_fatal(FARGS, "Requested to use exerimental SAXS errors, but no errors were read from "
                        "experimental intensity file %s\n", fnScatt);
            }

            FILE* fp = fopen(fnInterp, "w");
            fprintf(fp, "@type xydy\n");

            for (int i = 0; i < wt->nq; i++) {
                fprintf(fp, "%g %g %g\n", wt->minq + i * (wt->maxq - wt->minq) / (wt->nq - 1), wt->Iexp[i], wt->Iexp_sigma[i]);
            }

            fclose(fp);
        }

        /** Broadcast it */
        if (wt->Iexp_sigma == nullptr) {
            snew(wt->Iexp, wt->nq);
            snew(wt->Iexp_sigma, wt->nq);
        }

        if (PAR(cr)) {
            gmx_bcast(wt->nq * sizeof(double), wt->Iexp, cr->mpi_comm_mygroup);
            gmx_bcast(wt->nq * sizeof(double), wt->Iexp_sigma, cr->mpi_comm_mygroup);
        }

        /** Scale I(q=0) to input I(q)? Then store Iexp(q=0) to targetI0 */
        if (wr->inputParams->bScaleI0) {
            if (wt->minq != 0.) {
                gmx_fatal(FARGS, "When scaling I(q = 0) to target, you must compute I(q = 0). Use waxs-startq = 0 (found %g)\n",
                        wt->minq);
            }

            /*NEW, differend mode to get I0. Added by MH*/
            /* wt->targetI0 = wt->Iexp[0]; */
            char* buf;

            if ((buf = getenv("GMX_WAXS_I0")) != nullptr) {
                wt->targetI0 = atof(buf);

                fprintf(stderr, "\nWAXS-MD read saxs curve: Found environment variable GMX_WAXS_I0 = %.1f\n", wt->targetI0);
            } else {
                wt->targetI0 = wt->Iexp[0];

                if (MASTER(cr)) {
                    fprintf(stderr, "\nWAXS-MD read saxs curve: Did not  found GMX_WAXS_I0, taking the experimental value: %.1f\n",
                            wt->targetI0);
                }
            }

            if (MASTER(cr)) {
                printf("WAXS-MD read saxs curve: Will scale I(q=0) to predefined target from input I(q) curve: %g\n",
                        wt->targetI0);
            }

            if (PAR(cr)) {
                gmx_bcast(sizeof(real), &wt->targetI0, cr->mpi_comm_mygroup);
            }
        }
    }

    if (MASTER(cr)) {
        sfree(base);
        sfree(fnUsed);
        sfree(fnInterp);
    }
}

static void init_ensemble_stuff(t_waxsrec* wr, t_commrec* cr) {
    if (wr->inputParams->ensemble_nstates < 2 or wr->inputParams->ensemble_nstates > 50) {
        gmx_fatal(FARGS, "Error in init_ensemble_stuff(), wr->inputParams->ensemble_nstates = %d\n", wr->inputParams->ensemble_nstates);
    }

    for (int t = 0; t < wr->inputParams->nTypes; t++) {
        t_waxsrecType* wt = &wr->wt[t];

        /* Read and bcast intensities of fixed states from files intensity_stateXX.dat */
        snew(wt->ensemble_I, wr->inputParams->ensemble_nstates - 1);
        snew(wt->ensemble_Ivar, wr->inputParams->ensemble_nstates - 1);

        char typeStr[1024];

        if (wr->inputParams->nTypes > 1) {
            sprintf(typeStr, "_scatt%d", t);
        } else {
            strcpy(typeStr, "");
        }

        /* Refining only one state with the MD, while reading the SAXS curves of all other states from a file. */
        if (wr->inputParams->ewaxs_ensemble_type == ewaxsEnsemble_BayesianOneRefined) {
            for (int m = 0; m < wr->inputParams->ensemble_nstates - 1; m++) {
                if (MASTER(cr)) {
                    char fn[STRLEN];

                    sprintf(fn, "intensity%s_state%02d.dat", typeStr, m);

                    if (!gmx_fexist(fn)) {
                        gmx_fatal(FARGS, "For ensemble refinement with %d states, you need to place %d file(s) with name(s) "
                                "intensity_stateXX.dat\n"
                                "into the current directory, where XX are two digits (00, 01, 02, ...)\n"
                                "If you have multiple scattering groups (xray, neutron, etc.), then the file names "
                                "are\n"
                                "intensity_scattY_stateXX.dat, where Y = 0, 1, 2, etc. for each scatterig group "
                                "defined in the mdp file (option scatt-coupl)\n\n"
                                "However, %s was not found.\n",
                                wr->inputParams->ensemble_nstates, wr->inputParams->ensemble_nstates - 1, fn);
                    }

                    read_intensity_and_interpolate(fn, wt->minq, wt->maxq, wt->nq, TRUE, TRUE, &wt->ensemble_I[m],
                            &wt->ensemble_Ivar[m]);

                    printf("WAXS-MD: Read intensity of fixed state %d from file %s (scattering group %d)\n", m, fn, t);
                }

                /* Broadcast it */
                if (!MASTER(cr)) {
                    snew(wt->ensemble_I[m], wt->nq);
                    snew(wt->ensemble_Ivar[m], wt->nq);
                }

                if (PAR(cr)) {
                    gmx_bcast(wt->nq * sizeof(double), wt->ensemble_I[m], cr->mpi_comm_mygroup);
                    gmx_bcast(wt->nq * sizeof(double), wt->ensemble_Ivar[m], cr->mpi_comm_mygroup);
                }
            }
        } else {
            gmx_fatal(FARGS, "Unsupported ensemble type in  init_ensemble_stuff()\n");
        }

        /* init intensity sum arrays for ensemble refinement */
        snew(wt->ensembleSum_I, wt->nq);
        snew(wt->ensembleSum_Ivar, wt->nq);
    }
}

#define GMX_WAXS_SOLVENT_ROUGHNESS 0.4

static void check_selected_solvent(gmx::ArrayRef<gmx::RVec> xA, gmx::ArrayRef<gmx::RVec> xB, int* indexA, int isizeA,
        int* indexB, int isizeB, gmx_bool bFatal) {
    rvec maxA = { -1e20, -1e20, -1e20 };
    rvec minA = {  1e20,  1e20,  1e20 };
    rvec maxB = { -1e20, -1e20, -1e20 };
    rvec minB = {  1e20,  1e20,  1e20 };

    rvec* x;
    auto* xARaw = as_rvec_array(xA.data());
    auto* xBRaw = as_rvec_array(xB.data());

    /** find smallest and largest coordinate of solvation layer and excluded solvent */
    for (int i = 0; i < isizeA; i++) {
        x = &xARaw[indexA[i]];

        for (int d = 0; d < DIM; d++) {
            if ((*x)[d] < minA[d]) {
                minA[d] = (*x)[d];
            }

            if ((*x)[d] > maxA[d]) {
                maxA[d] = (*x)[d];
            }
        }
    }

    for (int i = 0; i < isizeB; i++) {
        x = &xBRaw[indexB[i]];

        for (int d = 0; d < DIM; d++) {
            if ((*x)[d] < minB[d]) {
                minB[d] = (*x)[d];
            }

            if ((*x)[d] > maxB[d]) {
                maxB[d] = (*x)[d];
            }
        }
    }

    for (int d = 0; d < DIM; d++) {
        if (fabs(maxA[d] - maxB[d]) > GMX_WAXS_SOLVENT_ROUGHNESS or fabs(minA[d] - minB[d]) > GMX_WAXS_SOLVENT_ROUGHNESS) {
            char buf[1024];
            const char dimLetter[3][2] = { "x", "y", "z" };

            sprintf(buf, "Solvation layer and excluded solvent are different in direction %s\n"
                    "Found minA / minB = %g / %g -- maxA / maxB = %g / %g\n", dimLetter[d], minA[d], minB[d], maxA[d], maxB[d]);

            if (bFatal) {
                gmx_fatal(FARGS, "%s", buf);
            } else {
                fprintf(stderr, "\n\nWARNING - WARNING - WARNING \n\n%s\n\nWARNING - WARNING - WARNING\n\n", buf);
            }
        }
    }
}

/* Get solvent atoms inside the envelope of solute/solvent and pure-solvent system */
static void get_solvation_shell(gmx::ArrayRef<gmx::RVec> x, gmx::ArrayRef<gmx::RVec> xex, t_waxsrec* wr,
        const gmx_mtop_t* mtop, gmx_mtop_t* mtopex, PbcType pbcType, const matrix box) {
    swaxs_debug("Begin of get_solvation_shell()\n");

    int nOutside = 0;
    const int nWarnOutsideMax = 10;

    /** Check if all atoms solute atoms are within envelope */
    for (int j = 0; j < wr->nindA_prot; j++) {
        if (!gmx_envelope_isInside(wr->wt[0].envelope, x[wr->indA_prot[j]])) {
            nOutside++;

            if (nOutside <= nWarnOutsideMax) {
                fprintf(stderr, "WARNING, atom %d of solute x = (%6.2f %6.2f %6.2f) is outside of the envelope\n",
                        wr->indA_prot[j] + 1, x[wr->indA_prot[j]][XX], x[wr->indA_prot[j]][YY], x[wr->indA_prot[j]][ZZ]);
            }

            if (nOutside == nWarnOutsideMax) {
                fprintf(stderr, "       NOTE: Will not report more warnings of atoms outside of envelope\n");
            }
        }
    }

    if (nOutside) {
        fprintf(stderr, "WARNING, %d atoms outside of envelope\n", nOutside);
    }

    /** Check if solvation layer too thin */
    real mindist;
    int iMindist;
    gmx_bool bMinDistToOuter;
    gmx_envelope_minimumDistance(wr->wt[0].envelope, x, wr->indA_prot, wr->nindA_prot, &mindist, &iMindist, &bMinDistToOuter);

    if (mindist < wr->inputParams->solv_warn_lay) {
        wr->nSolvWarn++;

        fprintf(stderr, "\n\n*** WARNING ***\n"
                "The current solvation layer is only %.3f nm thick.\n"
                "\tWarning at %g\n"
                "\tClosest atom #: %d, distance to %s envelope surface = %g\n\n",
                mindist, wr->inputParams->solv_warn_lay, wr->indA_prot[iMindist], bMinDistToOuter ? "outer" : "inner", mindist);
    }

    real envMaxR2 = gmx::square(gmx_envelope_maxR(wr->wt[0].envelope));

    /* Construct solvation layer around solute */
    wr->isizeA = wr->nindA_prot;

    const int allocBlocksize = 100;

    for (int j = 0; j < wr->nindA_sol; j++) {
        int k = wr->indA_sol[j];

        gmx_bool bInside = gmx_envelope_isInside(wr->wt[0].envelope, x[k]);

        if (norm2(x[k]) > envMaxR2 and bInside) {
            gmx_fatal(FARGS, "Envelope says inside, but diff = %g (maxR = %g)\n", sqrt(norm2(x[k])), sqrt(envMaxR2));
        }

        if (bInside) {
            wr->isizeA++;

            if (wr->isizeA >= wr->indexA_nalloc) {
                wr->indexA_nalloc += allocBlocksize;
                srenew(wr->indexA, wr->indexA_nalloc);
            }

            wr->indexA[wr->isizeA - 1] = k;
        }
    }

    /** Get atoms of pure-solvent system inside the envelope. */
    if (wr->bDoingSolvent) {
        wr->isizeB = 0;

        for (int j = 0; j < wr->nindB_sol; j++) {
            int k = wr->indB_sol[j];

            gmx_bool bInside = gmx_envelope_isInside(wr->wt[0].envelope, xex[k]);

            if (norm2(xex[k]) > envMaxR2 and bInside) {
                gmx_fatal(FARGS, "Envelope says inside, but diff = %g (maxR = %g)\n", sqrt(norm2(xex[k])), sqrt(envMaxR2));
            }

            if (bInside) {
                wr->isizeB++;

                if (wr->isizeB >= wr->indexB_nalloc) {
                    wr->indexB_nalloc += allocBlocksize;
                    srenew(wr->indexB, wr->indexB_nalloc);
                }

                wr->indexB[wr->isizeB - 1] = k;
            }
        }
    }

    if (wr->inputParams->verbosityLevel > 1 or wr->waxsStep == 0) {
        char fileName[256];

        sprintf(fileName, "prot+solvlayer_%d.pdb", wr->waxsStep);

        t_atoms* atoms = nullptr;

        snew(atoms, 1);
        *atoms = gmx_mtop_global_atoms(mtop);
        write_sto_conf_indexed(fileName, "Protein/Solute within the envelope",
                atoms, as_rvec_array(x.data()), nullptr, pbcType, box, wr->isizeA, wr->indexA);
        sfree(atoms);

        printf("Wrote %s\n", fileName);

        if (wr->bDoingSolvent) {
            sprintf(fileName, "excludedvolume_%d.pdb", wr->waxsStep);

            snew(atoms, 1);
            *atoms = gmx_mtop_global_atoms(mtopex);
            write_sto_conf_indexed(fileName, "Excluded solvent within the envelope",
                    atoms, as_rvec_array(xex.data()), nullptr, pbcType, box, wr->isizeB, wr->indexB);
            sfree(atoms);

            printf("Wrote %s\n", fileName);
        }
    }

    swaxs_debug("End of get_solvation_shell()\n");
}


static void calculate_node_qload(int nodeid, int nnodes, int nq, int* qhomenr, int* qstart, int* ind, int nabs) {
    /* Make sure that q-vectors of the same |q| are on the same node, needed when using multiple GPUs */
    int qAbsPerNode = nabs / nnodes;
    int rem = nabs - qAbsPerNode * nnodes;

    int iabsHomenr, iabsStart;

    if (nodeid >= rem) {
        iabsStart = nodeid * qAbsPerNode + rem;
        iabsHomenr = qAbsPerNode;
    } else {
        iabsStart = nodeid * (qAbsPerNode + 1);
        iabsHomenr = qAbsPerNode + 1;
    }

    *qstart = ind[iabsStart];
    *qhomenr = ind[iabsStart + iabsHomenr] - ind[iabsStart];
    fflush(stdout);

    if (*qhomenr > 1) {
        fprintf(stderr, "Node %2d) |q| indices range: %2d - %2d    q-vec indices range: %4d - %4d (of %d total)\n",
                nodeid, iabsStart, iabsStart + iabsHomenr - 1, *qstart, *qstart + *qhomenr - 1, nq);
    } else {
        fprintf(stderr, "Node %2d) No q-vectors on this node.\n", nodeid);
    }

    if ((*qstart + *qhomenr > nq) or (*qhomenr < 0) or (*qstart < 0)) {
        gmx_fatal(FARGS, "Inconsistent q vector distribution on node %d: qstart = %d  qhomenr = %d  nq = %d\n",
                nodeid, *qstart, *qhomenr, nq);
    }
}

static void gen_qvecs_accounting(t_waxsrec* wr, t_commrec* cr, int nnodes, int t) {
    waxs_datablock* wd = wr->wt[t].wd;
    int nq = wr->wt[t].qvecs->n;

    /** For the master, generate a comprehensive list */
    if (MASTER(cr)) {
        snew(wd->masterlist_qhomenr, nnodes);
        snew(wd->masterlist_qstart, nnodes);

        for (int i = 0; i < nnodes; i++) {
            int qstart, qhomenr;

            calculate_node_qload(i, nnodes, nq, &qhomenr, &qstart, wr->wt[t].qvecs->ind, wr->wt[t].qvecs->nabs);

            wd->masterlist_qhomenr[i] = qhomenr;
            wd->masterlist_qstart[i] = qstart;
        }

        fprintf(stderr, "Size and offsets of q-vecs constructed on master node for communications.\n");
    } else {
        wd->masterlist_qhomenr = nullptr;
        wd->masterlist_qstart = nullptr;
    }
}

static void free_qvecs_accounting(waxs_datablock* wd) {
    if (wd) {
        sfree(wd->masterlist_qhomenr);
        wd->masterlist_qhomenr = nullptr;
    }

    if (wd) {
        sfree(wd->masterlist_qstart);
        wd->masterlist_qstart = nullptr;
    }
}

/** Generate the list of q-vectors where we compute A(q), B(q), and D(q) */
t_spherical_map* gen_qvecs_map(real minq, real maxq, int nqabs, int J, gmx_bool bDebug, t_commrec* cr,
        gmx_envelope* envelope, int Jmin, real Jalpha, gmx_bool bVerbose) {

    int nWaxsNodes, nodeid;
    if (cr) {
        nWaxsNodes = cr->nnodes - cr->npmenodes;
        nodeid = cr->nodeid;
    } else {
        nWaxsNodes = 1;
        nodeid = 0;
    }

    if (nodeid >= nWaxsNodes) {
        gmx_fatal(FARGS, "Inconsistency in WAXS code: nodeid = %d, nWaxsNodes = %d\n", nodeid, nWaxsNodes);
    }

    t_spherical_map* qvecs;
    snew(qvecs, 1);

    real dq = (nqabs > 1) ? (maxq - minq) / (nqabs - 1) : 0.0;

    snew(qvecs->abs, nqabs);
    snew(qvecs->ind, nqabs + 1);

    qvecs->ind[0] = 0;
    qvecs->q = nullptr;
    qvecs->iTable = nullptr;
    qvecs->n = 0;
    qvecs->nabs = nqabs;

    real D = 0.;

    if (J <= 0) {
        /** Automatic detemination of J = alpha * (D*q)^2, but not smaller than wr->Jmin */
        if (!gmx_envelope_bHaveSurf(envelope)) {
            gmx_fatal(FARGS, "mdp option waxs-nsphere is zero, meaning that the number of q-vectors per |q|\n"
                    "is determined automatically. For that purpose, however, the envelope must be generated\n"
                    "before starting mdrun. Use g_genenv and provide the envelope file with the environment\n"
                    "variable GMX_ENVELOPE_FILE.\n");
        }

        /** Get maximum diameter of envelope */
        D = gmx_envelope_diameter(envelope);

        if (bVerbose) {
            printf("\nAutomatic selection of number of q-vectors per |q|: Using Jmin = %d, alpha = %g, D = %g nm:\n",
                    Jmin, Jalpha, D);
        }
    }

    int thisJ;

    for (int iabs = 0; iabs < nqabs; iabs++) {
        real qabs = minq + iabs * dq;

        if (qabs < 1e-5) {
            /** At q==0. we always use only J = 1 */
            thisJ = 1;
        } else {
            if (J > 0) {
                /** constant J over the entire q-range, given by waxs-nsphere */
                thisJ = J;
            } else {
                /** automatic selection of J = MAX(Jmin , alpha*(D*J)^2 */
                thisJ = round(Jalpha * gmx::square(qabs * D));

                if (thisJ < Jmin) {
                    thisJ = Jmin;
                }
            }
        }

        if (bVerbose and J <= 0) {
            printf("\tq = %8g   J = %d\n", qabs, thisJ);
        }

        /** Store absolute q, extend ind and q */
        qvecs->abs[iabs] = qabs;
        qvecs->ind[iabs + 1] = qvecs->ind[iabs] + thisJ;
        qvecs->n += thisJ;

        srenew(qvecs->q, qvecs->n);
        srenew(qvecs->iTable, qvecs->n);

        double fact = sqrt(M_PI * thisJ);

        for (int j = 0; j < thisJ; j++) {
            /** Spiral method */
            double tmp = (2.0 * (j + 1) - 1. - thisJ) / thisJ;

            double theta = acos(tmp);
            double phi = fact * asin(tmp);

            real qx = qabs * cos(phi) * sin(theta);
            real qy = qabs * sin(phi) * sin(theta);
            real qz = qabs * cos(theta);

            /** Fill qvecs->q vector by thisJ vectors */
            int jj = qvecs->ind[iabs] + j;

            qvecs->q[jj][XX] = qx;
            qvecs->q[jj][YY] = qy;
            qvecs->q[jj][ZZ] = qz;

            /**
             * Store the index of the absolute value. Used for table of atomic scattering factors as a function
             * of qabs: aff_table[sftype][iabs]
             */
            qvecs->iTable[jj] = iabs;
        }
    }

    if (cr == nullptr) {
        qvecs->qstart = 0;
        qvecs->qhomenr = qvecs->n;
    } else {
        /** Store for which q the scattering will be computed on this node */
        calculate_node_qload(nodeid, nWaxsNodes, qvecs->n, &(qvecs->qhomenr), &(qvecs->qstart), qvecs->ind, qvecs->nabs);

        if (PAR(cr)) {
            gmx_barrier(cr->mpi_comm_mygroup);
        }

        for (int j = 0; j < nWaxsNodes; j++) {
            if (nodeid == j) {
                fprintf(stderr, "Node %d uses %d q's between %d and %d\n",
                        nodeid, qvecs->qhomenr, qvecs->qstart, qvecs->qstart + qvecs->qhomenr - 1);
            }

            if (PAR(cr)) {
                gmx_barrier(cr->mpi_comm_mygroup);
            }
        }

        if (PAR(cr)) {
            gmx_barrier(cr->mpi_comm_mygroup);
        }
    }

    if (cr == nullptr or MASTER(cr)) {
        fprintf(stderr,"\n");
    }

    /** Output the spheremap vectors to assigned debug file. */
    if (bDebug and (cr == nullptr or MASTER(cr))) {
        FILE* fp = fopen("debug_spiral.xyz", "w");

        fprintf(fp, "# The spiral method of the spheremap\n");
        fprintf(fp, "# n = %i\n", qvecs->n);

        for (int j = 0; j < qvecs->n; j++) {
            fprintf(fp, "%g %g %g \n", qvecs->q[j][XX], qvecs->q[j][YY], qvecs->q[j][ZZ]);
        }

        fclose(fp);
        fp = fopen("debug_spiral.pdb","w");

        for (int j = qvecs->ind[nqabs - 1]; j < qvecs->ind[nqabs]; j++) {
            /** Make sure the coordinates are not getting too large for a PDB file */
            double fact = 50. / maxq;

            fprintf(fp, "ATOM  %5d %4s %3s %1s%4d    %8.3f%8.3f%8.3f%6.2f%6.2f\n",
                    j % 99999 + 1, "XX", "XXX", "", 1, fact * qvecs->q[j][XX], fact * qvecs->q[j][YY], fact * qvecs->q[j][ZZ], 1.0, 0.0);
        }

        fclose(fp);
        fprintf(stderr, "Wrote debug_spiral.xyz and debug_spiral.pdb for debugging spiral method.\n");
    }

    return qvecs;
}

/** Updating the electron density inside the envelope. This we do only with scattering type 0 */
static void update_envelope_griddensity(t_waxsrec* wr, gmx::ArrayRef<gmx::RVec> x) {
    swaxs::envelope_grid_density_next_frame(wr->wt[0].envelope, wr->inputParams->scale);

    int i0 = 0, ifinal = 0;

    switch (wr->inputParams->gridDensityMode) {
        case 0:
            /* Average density of solute and hydration layer */
            i0 = 0;
            ifinal = wr->isizeA;
            break;
        case 1:
            /* Average density of solute only */
            i0 = 0;
            ifinal = wr->nindA_prot;
            break;
        case 2:
            /* Average density of hydration layer only only */
            i0 = wr->nindA_prot;
            ifinal = wr->isizeA;
            break;
        default:
            gmx_fatal(FARGS, "Invalid mode for averaging grid density. Found %d, allowed: 0, 1, or 2\n", wr->inputParams->gridDensityMode);
    }

    for (int i = i0; i < ifinal; i++) {
        double nelec = wr->nElectrons[wr->indexA[i]];
        swaxs::envelope_grid_density_add_atom(wr->wt[0].envelope, x[wr->indexA[i]], nelec);
    }

    swaxs::envelope_grid_density_close_frame(wr->wt[0].envelope);
}

static void update_envelope_solvent_density(t_waxsrec* wr, const t_commrec* cr, gmx::ArrayRef<gmx::RVec> x, int t) {
    t_waxsrecType* wt = &wr->wt[t];

    /* Update solvent density around the protein inside envelope (cumulative average) */
    swaxs::envelope_solvent_density_next_frame(wt->envelope, wr->inputParams->scale);

    /** Master adds new electrons to the sphere */
    if (MASTER(cr)) {
        /** Loop over solvation shell atoms */
        for (int i = wr->nindA_prot; i < wr->isizeA; i++) {
            /** No matter if wt->type is XRAY or Neutron, we here collect the electron density. */
            double nelec = wr->nElectrons[wr->indexA[i]];
            swaxs::envelope_solvent_density_add_atom(wt->envelope, x[wr->indexA[i]], nelec);
        }
    }

    swaxs::envelope_solvent_density_bcast(wt->envelope, cr);

    /** Update FT of solvent density in the first steps a few times */
    bool recalculate_solvent_ft = (!swaxs::envelope_have_solvent_ft(wt->envelope) or wr->waxsStep <= 5
            or wr->waxsStep == 10 or wr->waxsStep == 15 or wr->waxsStep == 20 or wr->waxsStep == wr->inputParams->nfrsolvent - 1);

    if (wr->inputParams->tau > 0.) {
        /** with exponential averaging, update FT once every tau/4 (first time after tau/8) */
        recalculate_solvent_ft = recalculate_solvent_ft or ((wr->waxsStep + ((int)wr->inputParams->tausteps) / 8) % ((int)wr->inputParams->tausteps / 4)) == 0;
    } else if (wr->inputParams->tau < -1e-6) {
        /** With non-weighted average (typically in a rerun) update the FT a couple of times */
        recalculate_solvent_ft = recalculate_solvent_ft or wr->waxsStep == 30 or wr->waxsStep == 50
                or wr->waxsStep == 100 or wr->waxsStep == 300 or wr->waxsStep == 1000;
    } else if (fabs(wr->inputParams->tau) < 1e-6) {
        recalculate_solvent_ft = TRUE;
    }

    if (recalculate_solvent_ft and MASTER(cr)) {
        printf("Recalculating the FT of the solvent density around solute in waxs step %d (tau = %g, scattering type %d)\n",
                wr->waxsStep, wr->inputParams->tau, t);
    }

    /* Update the FT of the solvent in A if recalculate_solvent_ft */
    if (recalculate_solvent_ft) {
        wr->timing->start(wr->waxsStep, swaxs::Timing::Stage::Fourier, *cr);
    }

    int qstart = wt->qvecs->qstart;
    int qhomenr = wt->qvecs->qhomenr;
    rvec* q = wt->qvecs->q;
    real* ft_re;
    real* ft_im;
    swaxs::envelope_solvent_ft(wt->envelope, q + qstart, qhomenr, recalculate_solvent_ft, &ft_re, &ft_im);

    if (recalculate_solvent_ft) {
        wr->timing->end(wr->waxsStep, swaxs::Timing::Stage::Fourier, *cr);
    }

    wr->bRecalcSolventFT_GPU = recalculate_solvent_ft;
}


#define WAXS_SCALEI0_ADD_TO_SOLVENT_OF_A 0

static void scaleI0_getAddedDensity(t_waxsrec* wr, const t_commrec* cr) {
    for (int t = 0; t < wr->inputParams->nTypes; t++) {
        t_waxsrecType* wt = &wr->wt[t];
        waxs_datablock* wd = wt->wd;

        /* A few checks first */
        if (wt->targetI0 == -1.) {
            gmx_fatal(FARGS, "Trying to scale I(q=0), but no target I(q=0) found. Provide input intensity\n or "
                    "an environment varialbe GMX_WAXS_SCALE_I0, or set mdp option waxs-scale-i0 to no\n");
        }

        if (wt->qvecs->abs[0] != 0.) {
            gmx_fatal(FARGS, "Trying to scale I(q=0) to target I(q=0), but q = 0 is not computed\n");
        }

        if (wr->inputParams->bVacuum) {
            gmx_fatal(FARGS, "Cannot scale I(q=0) in vacuum since electron density is added to the solvent aound the protein\n");
        }

        /* Should this be taken from an input or from the experimental curve? It is here a 2nd time for rerun purposes. MH*/
        char* buf = nullptr;

        if ((buf = getenv("GMX_WAXS_I0")) != nullptr) {
            wt->targetI0 = atof(buf);
            fprintf(stderr, "\nWAXS-MD rerunpart: Found environment variable GMX_WAXS_I0 = %.1f\n", wt->targetI0);
        } else {
            wt->targetI0 = wt->Iexp[0];

            if (MASTER(cr)) {
                fprintf(stderr, "\nWAXS-MD rerunpart: Did not  found GMX_WAXS_I0, taking the experimental value: %.1f\n",
                        wt->targetI0);
            }
        }

        if (wt->targetI0 == 0) {
            fprintf(stderr, "\nTarget I0 is: %.1f. This should not happen.\n", wt->targetI0);
        }

        /* End of new part MH*/
        int qstart = wt->qvecs->qstart;
        int qhomenr = wt->qvecs->qhomenr;

        double c = 0.;

        if (qstart == 0) {
            /* This should be on the master with the smallest |q|, but double-check: */
            if (!MASTER(cr)) {
                gmx_fatal(FARGS, "scaleI0_getAddedDensity(): expected to be here on the Master\n");
            }

            if (norm2(wt->qvecs->q[0]) != 0.) {
                gmx_fatal(FARGS, "On this node, have qstart = 0, but norm2(q[0]) = %g\n", norm2(wt->qvecs->q[0]));
            }

            double Inow = gmx::square(wd->avA[0].re - wd->avB[0].re) + (wd->avAsq[0] - gmx::square(wd->avA[0].re))
                                                                        - (wd->avBsq[0] - gmx::square(wd->avB[0].re));
            double deltaI = wt->targetI0 - Inow;
            double deltaAq0 = -(wd->avA[0].re - wd->avB[0].re) + sqrt(gmx::square(wd->avA[0].re - wd->avB[0].re) + deltaI);

            if (WAXS_SCALEI0_ADD_TO_SOLVENT_OF_A) {
                double envelope_nelec = swaxs::envelope_get_solvent_nelec(wt->envelope);

                /* Add density of c * rho[solvent] to A system*/
                c = deltaAq0 / envelope_nelec;

                printf("Adding %g electrons to A (%.3f %% increase) in order to scale I(q=0) to %g\n",
                        deltaAq0, 100. * c, wt->targetI0);

                if (wt->type == escatterNEUTRON) {
                    gmx_fatal(FARGS, "WAXS_SCALEI0_ADD_TO_SOLVENT_OF_A is not implemented with Neutron scattering.");
                }
            } else {
                c = deltaAq0 / swaxs::envelope_volume(wt->envelope);
                printf("Removing %g electrons from B (%.3f %% decrease) in order to scale I(q=0) to %g\n",
                        deltaAq0, 100. * c / wr->solvent->averageDensity, wt->targetI0);
            }
        }

        if (PAR(cr)) {
            gmx_bcast(sizeof(double), &c, cr->mpi_comm_mygroup);
        }

        real* ft_re;
        real* ft_im;

        /** Pick up Fourier transform of solvent within envelope */
        if (WAXS_SCALEI0_ADD_TO_SOLVENT_OF_A) {
            swaxs::envelope_solvent_ft(wt->envelope, wt->qvecs->q + qstart, qhomenr, FALSE, &ft_re, &ft_im);
        } else {
            swaxs::envelope_unit_ft(wt->envelope, wt->qvecs->q + qstart, qhomenr, &ft_re, &ft_im);
        }

        if (wd->avAcorr == nullptr) {
            snew(wd->avAcorr, qhomenr);
        }

        for (int i = 0; i < qhomenr; i++) {
            wd->avAcorr[i].re = c * ft_re[i];
            wd->avAcorr[i].im = c * ft_im[i];
        }
    }
}

/*
 * Fixing the bulk solvent density to the value specified with mdp option waxs-solvdens.
 * See: Chen and Hub, Biophys. J., 107, 435-447 (2014), page 438
 */
static void fix_solvent_density(t_waxsrec* wr, const t_commrec* cr, int t) {
    t_waxsrecType* wt = &wr->wt[t];
    double elec2NSL_factor;

    /** Factor to translate electron density into NSL density at this deuterium concentration */
    if (wt->type == escatterXRAY) {
        elec2NSL_factor = 1;
    } else {
        /**
         * Water has 10 electrons per molecule, translating into NSL per molecule.
         *
         * NOTE: Strictly speaking, we would have to take this from the pure-solvent trajectory
         *       However, since the solvent is mainly water, this would make only a tiny difference.
         */
        double nslPerMol = NEUTRON_SCATT_LEN_O + 2 * ((1. - wt->deuter_conc) * NEUTRON_SCATT_LEN_1H + wt->deuter_conc * NEUTRON_SCATT_LEN_2H);
        elec2NSL_factor = nslPerMol / 10;
    }

    int qstart = wt->qvecs->qstart;
    int qhomenr = wt->qvecs->qhomenr;
    rvec* q = wt->qvecs->q;

    /**
     * Pick up Fourier transform of solvent electron density in A-system, that is of the solvent inside the envelope around the protein.
     * Important: ft_re and ft_im is always in unites of electron density (not NSL density)
     */
    real* ftSolvent_re;
    real* ftSolvent_im;
    swaxs::envelope_solvent_ft(wt->envelope, q + qstart, qhomenr, FALSE, &ftSolvent_re, &ftSolvent_im);

    /** Add a small density to solvent around protein, proportional to the density already present */
    if (PAR(cr)) {
        gmx_bcast(sizeof(double), &wr->solElecDensAv, cr->mpi_comm_mygroup);
    }

    /** Factor required to get *electron* density to the given density */
    real delta_rho_over_rho_bulk = (wr->inputParams->givenElecSolventDensity - wr->solElecDensAv) / wr->solElecDensAv;

    if (fabs(delta_rho_over_rho_bulk) > 0.05) {
        fprintf(stderr, "\nWARNING, you are correcting the density of the PROTEIN SYSTEM by more than 5%%.\n"
                "   delta_rho / rho = %g %%\n"
                "This may indicate that your waxs-solvent group is lacking atoms, that your\n"
                "waxs-solvdens is wrong, or that you have some other problem. The solvent density\n"
                "correction is meant for small corrections, typically by 1%% or less.\n\n", delta_rho_over_rho_bulk * 100);
    }

    if (wr->inputParams->bUseGPU == FALSE) {
        for (int i = 0; i < qhomenr; i++) {
            /** ftSolvent_re/ftSolvent_im are in units "number of electrons". For neutron scattering, translate to units number of NSL */
            wt->wd->A[i].re += delta_rho_over_rho_bulk * elec2NSL_factor * ftSolvent_re[i];
            wt->wd->A[i].im += delta_rho_over_rho_bulk * elec2NSL_factor * ftSolvent_im[i];
        }
    }

    int wstep = wr->waxsStep + 1;

    if (MASTER(cr) and norm2(wt->qvecs->q[0]) == 0. and wt->qvecs->qstart == 0) {
        wr->nElecAddedA = 1.0 / wstep * ((wstep - 1) * wr->nElecAddedA + delta_rho_over_rho_bulk * ftSolvent_re[0]);

        if (wt->type == escatterXRAY) {
            fprintf(wr->swaxsOutput->fpLog, "Solvent density correction to %g e/nm3:\n    Added %g electrons to solvation shell around solute"
                    " (added density of %g)\n", wr->inputParams->givenElecSolventDensity, delta_rho_over_rho_bulk * ftSolvent_re[0],
                    wr->inputParams->givenElecSolventDensity - wr->solElecDensAv);
        } else {
            fprintf(wr->swaxsOutput->fpLog, "Solvent density correction to %g e/nm3\n    Added %g electrons (%g NSLs) to solvation shell around solute"
                   " (added electron density of %g / NSL density of %g)\n", wr->inputParams->givenElecSolventDensity,
                    delta_rho_over_rho_bulk * ftSolvent_re[0], delta_rho_over_rho_bulk * elec2NSL_factor * ftSolvent_re[0],
                    wr->inputParams->givenElecSolventDensity - wr->solElecDensAv,
                    elec2NSL_factor * (wr->inputParams->givenElecSolventDensity - wr->solElecDensAv));
        }
    }

    real* ftEnv_re;
    real* ftEnv_im;

    /** In either of these cases, we will use the unit transform */
    if ((wr->bDoingSolvent or wt->wd->DerrSolvDens)) {
        swaxs::envelope_unit_ft(wt->envelope, q + qstart, qhomenr, &ftEnv_re, &ftEnv_im);
    }

    if (wr->bDoingSolvent) {
        /** Add a small constant density to the excluded solvent droplet */
        real delta_rho_B = (wr->inputParams->givenElecSolventDensity - wr->solElecDensAv_SysB);

        if (MASTER(cr) and fabs(delta_rho_B / wr->solElecDensAv_SysB) > 0.05) {
            fprintf(stderr, "\nWARNING, you are correcting the density of the SOLVENT SYSTEM by more than 5%%.\n"
                    "   delta_rho / rho = %g %%\n"
                    "This may indicate that your waxs-solvent group is lacking atoms, that your\n"
                    "waxs-solvdens is wrong, or that you have some other problem. The solvent density\n"
                    "correction is meant for small corrections, typically by 1%% or less.\n\n",
                    delta_rho_B / wr->solElecDensAv_SysB * 100);
        }

        if (MASTER(cr) and t == 0) {
            fprintf(wr->swaxsOutput->fpLog, "    Added %g electrons to excluded solvent (added density of %g)\n",
                    delta_rho_B * swaxs::envelope_volume(wt->envelope), delta_rho_B);
            wr->nElecAddedB = 1.0 / wstep * ((wstep - 1) * wr->nElecAddedB + delta_rho_B * swaxs::envelope_volume(wt->envelope));
        }

        /** Corrections in B takes the form of a pure homogenous background. */
        if (wr->inputParams->bUseGPU == FALSE) {
            for (int i = 0; i < qhomenr; i++) {
                /** ft_re/ft_im  are in units number of electrons. For neutron scattering, translate to units number of NSL  */
                wt->wd->B[i].re += delta_rho_B * elec2NSL_factor * ftEnv_re[i];
                wt->wd->B[i].im += delta_rho_B * elec2NSL_factor * ftEnv_im[i];
            }
        }
    }

    if (wt->wd->DerrSolvDens) {
        /**
         * Uncertainty in D(q) due to uncertainty in the relative uncertainty of the solvent density:
         * D[err] = 2 * drho / rho[bulk] * Re{ <A*-B*> . [ F(rho[s]) - rho[bulk].F(envelope) ] },
         * where  drho/rho[bulk] is the relative uncertainty of the solvent given by the mdp option waxs-solvdens-uncert.
         */
        for (int i = 0; i < qhomenr; i++) {
            /** In units electrons or NSL: */
            real AmB_re = wt->wd->avA[i].re - wt->wd->avB[i].re;
            real AmB_im = wt->wd->avA[i].im - wt->wd->avB[i].im;

            /** Always in units electrons, so translate to electrons OR NSL with factor elec2NSL_factor: */
            real FT_diff_re = (ftSolvent_re[i] - wr->inputParams->givenElecSolventDensity * ftEnv_re[i]) * elec2NSL_factor;
            real FT_diff_im = (ftSolvent_im[i] - wr->inputParams->givenElecSolventDensity * ftEnv_im[i]) * elec2NSL_factor;

            wt->wd->DerrSolvDens[i] = 2 * wr->inputParams->solventDensRelErr * (AmB_re * FT_diff_re + AmB_im * FT_diff_im);
        }
    }
}

static void compute_scattering_amplitude(t_complex_d* scatt_amplitude, rvec* atomEnvelope_coord, int* atomEnvelope_type,
        int isize, int nprot, rvec* q, int* iTable, int qhomenr, real** aff_table, real* nsl_table,
        t_complex* grad_amplitude_bar, double* timing_ms) {
    struct timespec t_start;
    clock_gettime(CLOCK_MONOTONIC_RAW, &t_start);

    swaxs_debug("Begin of compute_scattering_amplitude()\n");

    if (aff_table and nsl_table) {
        gmx_fatal(FARGS, "aff_table and nsl_table are both != nullptr !\n");
    }

    if (!aff_table and !nsl_table) {
        gmx_fatal(FARGS, "aff_table and nsl_table are both == nullptr !\n");
    }

    int i;

    for (i = 0; i < qhomenr; i++) {
        scatt_amplitude[i] = cnul_d;
    }

    int nReal = sizeof(gmx::SimdReal) / sizeof(real);

    /** nq2 >= qhomenr and multiple of nReal (the # of reals per register) */
    int nq2 = ((qhomenr - 1) / nReal + 1) * nReal;

    /**
     * IMPORTANT:
     * We must make sure that the float pointers are aligned in memory in same way as the
     * gmx::SimdReal pointers (e.g., gmx::SimdReal is __m256 for AVX_256 or __mm128 for FMA).
     * Otherwise, after casting a float* onto a gmx::SimdReal*, we may have in incorrect
     * aligntment of the gmx::SimdReal* variable, which may cause errors such as Segfaults.
     *
     * For instance, floats are 4-byte-aligned, where as __m256 are 16-byte-aligned.
     *
     * To make sure that the float arrays correctly aligned, they must be alloccated with
     * snew_aligned and freed with sfree_aligned.
     *
     * For an explanation, see e.g.:
     * https://stackoverflow.com/questions/25596379/simd-intrinsics-segmentation-fault
     */

    real* p_aff;
    real* p_qdotx;
    real* re;
    real* im;

    /** Allocate 32-byte alinged float real arrays */
    snew_aligned(p_aff,   nq2, 32);
    snew_aligned(p_qdotx, nq2, 32);
    snew_aligned(re,      nq2, 32);
    snew_aligned(im,      nq2, 32);

    for (int p = 0; p < isize; p++) {
        if (atomEnvelope_type[p] == 10 and p == 0 and FALSE) {
            fprintf(stderr, "scattering Amplitude AT = 10 , atom %d !\n", i);
        }

        /** Put atomic form factors or NSL and phase qdotx in a linear (real*) array */
        if (aff_table) {
            /** Xray scattering, atomic form factor depends on q */
            for (i = 0; i < qhomenr; i++) {
                p_aff[i] = aff_table[atomEnvelope_type[p]][iTable[i]];
                p_qdotx[i] = iprod(q[i], atomEnvelope_coord[p]);
            }
        } else {
            /** Neutron scattering NSL does NOT depend on q */
            for (i = 0; i < qhomenr; i++) {
                /**
                 * To keep this function generic for Xray and Neutron (for now), write the identical NSL into an array.
                 * Because the SINCOS function below is by far the most expensive anyway, this will hardly make any difference.
                 */
                p_aff[i] = nsl_table[atomEnvelope_type[p]];
                p_qdotx[i] = iprod(q[i], atomEnvelope_coord[p]);
            }
        }

        /** Cast the real* to gmx::SimdReal* arrays */
        gmx::SimdReal* m_aff = (gmx::SimdReal*)p_aff;
        gmx::SimdReal* m_qdotx = (gmx::SimdReal*)p_qdotx;
        gmx::SimdReal* mRe = (gmx::SimdReal*)re;
        gmx::SimdReal* mIm = (gmx::SimdReal*)im;

        /**
         * the SSE or AVX loop
         * SSE or AVX128: float: 4 sincos per call. double: 2 sincos per call
         * AVX256:        float: 8 sincos per call. double: 4 sincos per call
         */
        gmx::SimdReal sinm, cosm;

        for (int k = 0; k < qhomenr; k += nReal) {
            gmx::sincos(*m_qdotx, &sinm, &cosm);
            *mRe = *m_aff * cosm;
            *mIm = *m_aff * sinm;

            m_qdotx++;
            m_aff++;
            mRe++;
            mIm++;
        }

        for (i = 0; i < qhomenr; i++) {
            /* Requires a real to double conversion */
            scatt_amplitude[i].re += re[i];
            scatt_amplitude[i].im += im[i];
        }

        if (grad_amplitude_bar and p < nprot) {
            int k0 = p * qhomenr;

            /** Store: [ i . f_k(q) . exp(iq*x) ]* = (i.SF)*   */
            for (i = 0; i < qhomenr; i++) {
                /** Use [(a+i.b).i]* = -b-i*a */
                int k = k0 + i;

                grad_amplitude_bar[k].re = -im[i];
                grad_amplitude_bar[k].im = -re[i];
            }
        }
    }

    sfree_aligned(p_aff);
    sfree_aligned(p_qdotx);
    sfree_aligned(re);
    sfree_aligned(im);

    #ifdef PRINT_A_dkI
        FILE* fp = fopen("./uncorrected_scattering_amplitude_CPU.txt", "w");

        fprintf(fp, "Atom-number HOST start is: %d\n", isize);
        fprintf(fp, "qhomenr HOST after loop is: %d\n", qhomenr);

        for (int i = 0; i < qhomenr; i++) {
            fprintf(fp, "Real-part of scattering amplitude [ %d ] :  %f \n", i, scatt_amplitude[i].re);
            fprintf(fp, "Im  -part of scattering amplitude [ %d ] :  %f \n", i, scatt_amplitude[i].im);
            fprintf(fp, " \n");
        }

        fclose(fp);
    #endif

    /** Return elapsed time in milliseconds */
    struct timespec t_end;
    clock_gettime(CLOCK_MONOTONIC_RAW, &t_end);

    long int delta_us = (t_end.tv_sec - t_start.tv_sec) * 1000000 + (t_end.tv_nsec - t_start.tv_nsec) / 1000;
    *timing_ms = delta_us / 1000.;

    swaxs_debug("End of compute_scattering_amplitude()\n");
}

#if 0
/**
 * return Cromer-Mann fit for the atomic scattering factor:
 * sin_theta is the sine of half the angle between incoming and scattered
 * vectors. See g_sq.h for a short description of CM fit.
 */
static double md_CMSF(t_cromer_mann cmsf, real lambda, real sin_theta, real alpha, real delta) {
    /**
     * f0[k] = c + [SUM a_i * EXP(-b_i * (k^2))]
     *             i = 1,4
     */

    /** k2 is in units A^-2 (according to parameters b[i]), whereas lambda in nm */
    real k2 = (gmx::square(sin_theta) / gmx::square(10.0 * lambda));
    real tmp = cmsf.c;

    for (int i = 0; (i < 4); i++) {
        tmp += cmsf.a[i] * exp(-cmsf.b[i] * k2);
    }

    /* scale AFF by [ 1 + alpha*exp(-Q^2/2delta^2) ]  --- only used for water.
       Now this is done on the force field level, however. Therefore, alpha is always == 0.
     * Note: we use alpha instead of the (alpha-1) used in eq. 5, Sorenson et al, JCP 113:9194, 2000
     * Note: Q2 is in units nm^-2, delta in nm^-1 */

    if (alpha != 0) {
        real Q2 = gmx::square(4 * M_PI * sin_theta / lambda);
        tmp *= (1 + alpha * exp(-Q2 / (2 * delta * delta)));
    }

    return tmp;
}
#endif

/** Make a table of NSLs found in the topology, and add NSLs of 1H and 2H */
static void fill_neutron_scatt_len_table(const t_scatt_types* scattTypes, t_waxsrecType* wt, real backboneDeuterProb) {
    wt->nnsl_table = scattTypes->nNeutronScatteringLengths + 4;
    real nslH = NEUTRON_SCATT_LEN_1H;
    real nslD = NEUTRON_SCATT_LEN_2H;

    snew(wt->nsl_table, wt->nnsl_table);

    int i;
    for (i = 0; i < scattTypes->nNeutronScatteringLengths; i++) {
        wt->nsl_table[i] = scattTypes->neutronScatteringLengths[i].cohb;
    }

    wt->nsl_table[i++] = nslH;
    wt->nsl_table[i++] = nslD;

    /**
     * Optionally, we may assign a mixed NSL of 1H and 2H, corresponding to the deuterium concentration.
     * These are used if wr->inputParams->bStochasticDeuteration == FALSE.
     */

    real probD, nsl;

    /** First: deuteratable groups: */
    probD = wt->deuter_conc;
    nsl = probD * nslD + (1.0 - probD) * nslH;
    wt->nsl_table[i++] = nsl;

    /** Next: deuteratable backbone hydrogens: */
    probD = wt->deuter_conc * backboneDeuterProb;
    nsl = probD * nslD + (1.0 - probD) * nslH;
    wt->nsl_table[i++] = nsl;
}

/**
 * Return the NSL type index, taking the deuterium concentration into account.
 *      wr->inputParams->bStochasticDeuteration == TRUE:
 *
 * If the NSl is NSL_H_DEUTERATABLE and NSL_H_DEUTERATABLE_BACKBONE, they are randomly assigned to 1H or 2H.
 *      wr->inputParams->bStochasticDeuteration == FALSE:
 *
 * If the NSl is NSL_H_DEUTERATABLE and NSL_H_DEUTERATABLE_BACKBONE, a linear interpolation between 1H and 2H is assigned.
 */
static int get_nsl_type(t_waxsrec* wr, int t, int nslTypeIn) {
    t_waxsrecType* wt = &wr->wt[t];

    /** Types of 1H, 2H, deuteratable, and deuteratalbe backbone are always at the end of the nsl_table,
        see fill_neutron_scatt_len_table() */
    int nslType_1H                 = wt->nnsl_table - 4;
    int nslType_2H                 = wt->nnsl_table - 3;
    int nslType_deuteratable_mixed = wt->nnsl_table - 2;
    int nslType_deutBackbone_mixed = wt->nnsl_table - 1;

    gmx::UniformRealDistribution<real> uniform_distribution;

    if (fabs(wt->nsl_table[nslTypeIn] - NSL_H_DEUTERATABLE_BACKBONE) < 1e-5) {
        /** Backbone hydrogen */
        if (wr->inputParams->bStochasticDeuteration) {
            double probDeuter = wt->deuter_conc * wr->inputParams->backboneDeuterProb;

            return (uniform_distribution(*(wr->rng)) < probDeuter) ? nslType_2H : nslType_1H;
        } else {
            return nslType_deutBackbone_mixed;
        }
    } else if (fabs(wt->nsl_table[nslTypeIn] - NSL_H_DEUTERATABLE) < 1e-5) {
        /** Other deuteratable hydrogen (typically polar hydrogen) */
        if (wr->inputParams->bStochasticDeuteration) {
            return (uniform_distribution(*(wr->rng)) < wt->deuter_conc) ? nslType_2H : nslType_1H;
        } else {
            return nslType_deuteratable_mixed;
        }
    } else {
        return nslTypeIn;
    }
}

static void compute_atomic_form_factor_table(const t_scatt_types* scattTypes, t_waxsrecType* wt, t_commrec* cr) {
    wt->naff_table = scattTypes->nCromerMannParameters;
    snew(wt->aff_table, wt->naff_table);

    if (!wt->qvecs) {
        gmx_fatal(FARGS, "Atomic form factors cannot be calculated without a defined set of q-vectors."
                " Please initialise them first!\n");
    }

    if (MASTER(cr)) {
        printf("Building atomic form factor table for %d SF-tytpes and %d |q|\n", wt->naff_table, wt->nq);
    }

    for (int i = 0; (i < wt->naff_table); i++) {
        snew(wt->aff_table[i], wt->nq);

        for (int j = 0; j < wt->nq; j++) {
            wt->aff_table[i][j] = CMSF_q(scattTypes->cromerMannParameters[i], wt->qvecs->abs[j]);
        }
    }
}

static void copy_ir2waxsrec(t_waxsrec* wr, t_inputrec* ir, t_commrec* cr) {
    wr->inputParams->nstlog = ir->waxs_nstlog;
    wr->inputParams->nfrsolvent = ir->waxs_nfrsolvent;
    wr->inputParams->bVacuum = FALSE;
    wr->inputParams->stepCalcNindep = 0;

    wr->inputParams->npbcatom = ir->waxs_npbcatom;
    snew(wr->pbcatoms, wr->inputParams->npbcatom);

    for (int i = 0; i < wr->inputParams->npbcatom; i++) {
        wr->pbcatoms[i] = ir->waxs_pbcatoms[i];
    }

    wr->inputParams->bScaleI0 = ir->ewaxs_bScaleI0;
    wr->inputParams->givenElecSolventDensity = ir->waxs_denssolvent;
    wr->inputParams->solventDensRelErr = ir->waxs_denssolventRelErr;

    if (wr->inputParams->givenElecSolventDensity == 0 and wr->inputParams->solventDensRelErr > 0) {
        gmx_fatal(FARGS, "To account for the uncertainty of the solvent density, you must specify the expected solvent density.\n"
                "Specify the solvent density in the mdp file, e.g. to 334 e/nm^3\n");
    }

    wr->inputParams->bCorrectBuffer = (ir->ewaxs_correctbuff == waxscorrectbuffYES);
    wr->inputParams->bFixSolventDensity = (ir->waxs_denssolvent > GMX_FLOAT_EPS);
    wr->inputParams->bBayesianSolvDensUncert = (ir->ewaxs_solvdensUnsertBayesian == ewaxsSolvdensUncertBayesian_YES);
    wr->inputParams->solv_warn_lay = ir->waxs_warn_lay;

    real tau = wr->inputParams->tau = ir->waxs_tau;
    wr->inputParams->nstcalc = ir->waxs_nstcalc;

    real dt = 1.0 * wr->inputParams->nstcalc * ir->delta_t;
    wr->inputParams->tausteps = wr->inputParams->tau / dt;

    if (dt <= 0.) {
        gmx_fatal(FARGS, "dt for WAXS intensity calculation %g is <= 0\n", dt);
    }

    if (tau > 0.0) {
        wr->inputParams->scale = exp(-1.0 * dt / tau);
        wr->inputParams->bSwitchOnForce = TRUE;
        wr->inputParams->stepCalcNindep = (int)((tau / dt)) / 2;
    } else if (tau == -1.0) {
        wr->inputParams->scale = 1.0;  /** non-weighted averaging */
        wr->inputParams->bSwitchOnForce = FALSE;
    } else if (tau == 0.0) {
        wr->inputParams->scale = 0.0;  /** no averaging, i.e., instantaneous coupling. */
        wr->inputParams->bSwitchOnForce = FALSE;
    } else {
        gmx_fatal(FARGS, "Illegal value for waxs_tau found (%g). Allowed: -1, 0, >0)\n", tau);
    }

    wr->inputParams->ewaxs_Iexp_fit      = ir->ewaxs_Iexp_fit;
    wr->inputParams->potentialType       = ir->ewaxs_potential;

    wr->inputParams->weightsType         = ir->ewaxs_weights;
    wr->inputParams->ewaxs_ensemble_type = ir->ewaxs_ensemble_type;
    wr->inputParams->t_target            = ir->waxs_t_target;

    wr->inputParams->kT = ir->opts.ref_t[0] * BOLTZ;

    if (MASTER(cr)) {
        fprintf(stderr, "WAXS-MD: kT = %g\n", wr->inputParams->kT);
        fprintf(stderr, "WAXS-MD: Computing # independent points every %d steps.\n", wr->inputParams->stepCalcNindep);
    }

    /** Init stuff that is specific to the type of scattering (xray or neutron) */
    wr->inputParams->nTypes = ir->waxs_nTypes;
    wr->inputParams->bDoingNeutron = FALSE;

    snew(wr->wt, wr->inputParams->nTypes);

    for (int i = 0; i < wr->inputParams->nTypes; i++) {
        wr->wt[i]             = init_t_waxsrecType();
        wr->wt[i].fc          = ir->waxs_fc         [i];
        wr->wt[i].type        = ir->escatter        [i];
        wr->inputParams->bDoingNeutron = (wr->inputParams->bDoingNeutron or ir->escatter[i] == escatterNEUTRON);
        wr->wt[i].nq          = ir->waxs_nq         [i];
        wr->wt[i].minq        = ir->waxs_start_q    [i];
        wr->wt[i].maxq        = ir->waxs_end_q      [i];
        wr->wt[i].deuter_conc = ir->waxs_deuter_conc[i];

        sprintf(wr->wt[i].saxssansStr, "%s", ir->escatter[i] == escatterXRAY ? "SAXS" : "SANS");
        sprintf(wr->wt[i].scattLenUnit, "%s", ir->escatter[i] == escatterXRAY ? "electrons" : "NSLs");
    }

    wr->inputParams->J = ir->waxs_J;

    if (tau > 0.0) {
        wr->inputParams->nAverI = 2 * tau / dt;
    } else {
        wr->inputParams->nAverI = 100;
    }

    /* Hardcoded - uh, ugly. Or should we depend this on tau? */
    if (MASTER(cr)) {
        fprintf(stderr, "WAXS-MD: Running sigma of I(q) computed from the last %d intensities (tau/dt = %g/%g) - NOT used at the moment.\n",
                wr->inputParams->nAverI, tau, dt);
    }

    /** In case of SAXS ensemble refinement: set initial weights of states */
    wr->inputParams->ensemble_nstates = ir->waxs_ensemble_nstates;
    snew(wr->ensemble_weights, wr->inputParams->ensemble_nstates);
    snew(wr->inputParams->ensemble_weights_init, wr->inputParams->ensemble_nstates);

    wr->inputParams->ensemble_weights_fc = ir->waxs_ensemble_fc;

    for (int i = 0; i < wr->inputParams->ensemble_nstates; i++) {
        wr->ensemble_weights[i] = ir->waxs_ensemble_init_w[i];
        wr->inputParams->ensemble_weights_init[i] = ir->waxs_ensemble_init_w[i];
    }

    if (MASTER(cr) and wr->inputParams->ensemble_nstates) {
        fprintf(stderr, "WAXS-MD: Found initial weights for %d states for ensemble refinement =", wr->inputParams->ensemble_nstates);

        for (int i = 0; i < wr->inputParams->ensemble_nstates; i++) {
            fprintf(stderr, " %g", wr->inputParams->ensemble_weights_init[i]);
        }

        fprintf(stderr, "\n");
        fprintf(stderr, "WAXS-MD: Force constant for ensemble weights = %g", wr->inputParams->ensemble_weights_fc);
    }

    /** The other structures to be initialised later. */
}

/** Returns a continuous array (size mtop->natoms) of Cromer-Mann or NSL (bNeutron = TRUE) types from mtop */
static int* mtop2scattTypeList(const gmx_mtop_t* mtop, gmx_bool bNeutron) {
    /**
     * Bookkeeping: natoms = Sum _i ^ nmolblock ( molblock[i]->nmol * molblock[i]->natoms_mol )
     * moltype.atoms->nr = molblock[i]->natoms_mol
     * Loop over mblocks instead and use its information to write
     */

    int* scattTypeId = nullptr;
    snew(scattTypeId, mtop->natoms);

    for (int i = 0; i < mtop->natoms; i++) {
        scattTypeId[i] = -1000;
    }

    int isum = 0;

    /** Loop over molecule types */
    for (std::size_t mb = 0; mb < mtop->molblock.size(); mb++) {
        const t_atoms* atoms = &mtop->moltype[mtop->molblock[mb].type].atoms;

        int nmols = mtop->molblock[mb].nmol;
        int natoms_mol = atoms->nr;

        /** Loop over atoms in this molecule type */
        for (int iatom = 0; iatom < natoms_mol; iatom++) {
            /** Pick NSL or Cromer-Mann type of this atom */
            int stype = bNeutron ? atoms->atom[iatom].nsltype : atoms->atom[iatom].cmtype;

            /** Loop over moleculs of this molecule type */
            for (int imol = 0; imol < nmols; imol++) {
                int itot = isum + imol * natoms_mol + iatom;
                scattTypeId[itot] = stype;
            }
        }

        isum += nmols * natoms_mol;
    }

    /** For debugging, make sure that Cromer-Mann or NSL type of *every* atom was set. */
    for (int i = 0; i < mtop->natoms; i++) {
        if (scattTypeId[i] == -1000) {
            gmx_fatal(FARGS, "scattTypeId[%d] was not set in mtop2sftype_list\n", i);
        }
    }

    return scattTypeId;
}

/**
 * Assume both solvent and solute .tpr files are set up identically, allowing matching of the atomic form factors.
 * Do not allow types that are only found in solvent, as this would indicate some buffer mismatch between solute
 * and solvent systems.
 */
static void redirect_md_solventtypes(gmx_mtop_t* solvent_top, const t_scatt_types* scattTypesSolute) {
    int nSolventGroups = countGroups(solvent_top->groups, SimulationAtomGroupType::egcWAXSSolvent);

    fprintf(stderr, "In water-background: nSolventGroups = %d. \n", nSolventGroups);

    /**
     * This section is somewhat shared with grompp's remap to unique atom-types. Do we combine later?
     *
     * Set up redirect map. Although there normally should be only one moltype and molblock,
     * we should make allowances for complex solvents in the future.
     */

    int j, k;
    int isum = 0;

    for (std::size_t mb = 0; mb < solvent_top->moltype.size(); mb++) {
        t_atoms* atoms = &solvent_top->moltype[solvent_top->molblock[mb].type].atoms;

        for (int iatom = 0; iatom < atoms->nr; iatom++) {
            int itot = iatom + isum;
            gmx_bool bSolvent = (getGroupType(solvent_top->groups, SimulationAtomGroupType::egcWAXSSolvent, itot) < nSolventGroups);

            if (bSolvent) {
                t_scatt_types* solvent_scattTypes = &solvent_top->scattTypes;

                /** Look for idenitcal Cromer-Mann parameters in solute topology */
                j = atoms->atom[iatom].cmtype;
                k = search_scattTypes_by_cm(scattTypesSolute, &(solvent_scattTypes->cromerMannParameters[j]));

                if (k == NOTSET) {
                    gmx_fatal(FARGS,"Cromer-Mann type %d of solvent not found in main system\n\n"
                            "We do not allow using waxs_solvents that has scattering types"
                            " not found in the solute system! Please use waxs_solvents molecules that are exactly like"
                            " or are subsets of the solvent in the main simulation.\n", j);
                } else if (k >= scattTypesSolute->nCromerMannParameters or k < 0) {
                    gmx_fatal(FARGS, "search_scattTypes_by_cm returned illegal sftype index (%d). (Allowed 0 to %d)",
                            k, scattTypesSolute->nCromerMannParameters);
                } else {
                    atoms->atom[iatom].cmtype = k;
                    fprintf(stderr, "Redirecting solvent Cromer-Mann type %d (atom %d) to main system Cromer-Mann type %d\n",
                            j, iatom, k);
                }

                /**
                 * Optionally: Look or neutron scattering length in solute topology. Only needed if we have
                 * NSL types in the solute system, otherwise we don't do neutron scattering anyway.
                 */
                j = atoms->atom[iatom].nsltype;

                if (j >= 0 and scattTypesSolute->nNeutronScatteringLengths > 0) {
                    k = search_scattTypes_by_nsl(scattTypesSolute, &(solvent_scattTypes->neutronScatteringLengths[j]));

                    if (k == NOTSET) {
                        gmx_fatal(FARGS, "Neutron scattering length type %d of solvent not found in main system (cohb = %g)\n\n"
                                "We do not allow using waxs_solvents that has NEUTRON scattering types\n"
                                "not found in the main system! Please use waxs_solvents molecules that are exactly like\n"
                                "or are subsets of the solvent in the main simulation.\n", j, solvent_scattTypes->neutronScatteringLengths[j].cohb);
                    } else if (k >= scattTypesSolute->nNeutronScatteringLengths or k < 0) {
                        gmx_fatal(FARGS, "search_scattTypes_by_nsl returned illegal NSL type index (%d). (Allowed 0 to %d)",
                                k, scattTypesSolute->nNeutronScatteringLengths);
                    } else {
                        atoms->atom[iatom].nsltype = k;

                        fprintf(stderr, "Redirecting solvent NSL type %d (atom %d) to main system NSL type %d\n", j, iatom, k);
                    }
                }
            }
        }

        isum += atoms->nr * solvent_top->molblock[mb].nmol;
    }
}

/** Return the number of unconnected molecules in the solute */
static int nSoluteMolecules(const gmx_mtop_t* mtop) {
    int mb_last = -1;
    int imol_last = -1;
    int nSoluteMols = 0;

    int isum = 0;
    int nSoluteGroups = countGroups(mtop->groups, SimulationAtomGroupType::egcWAXSSolute);

    for (std::size_t mb = 0; mb < mtop->molblock.size(); mb++) {
        const t_atoms* atoms = &mtop->moltype[mtop->molblock[mb].type].atoms;
        int nmols = mtop->molblock[mb].nmol;
        int natoms_mol = atoms->nr;

        /** molecules */
        for (int imol = 0; imol < nmols; imol++) {
            /** atoms in molecule */
            for (int iatom = 0; iatom < natoms_mol; iatom++) {
                int itot = isum + imol * natoms_mol + iatom;
                gmx_bool bSolute = (getGroupType(mtop->groups, SimulationAtomGroupType::egcWAXSSolute, itot) < nSoluteGroups);

                // TODO: The way this procedures detects last molecule block needs to be revisited. (remove cast, analyse the issue)

                if (bSolute and (static_cast<int>(mb) != mb_last or imol != imol_last)) {
                    nSoluteMols++;
                    mb_last = mb;
                    imol_last = imol;
                }
            }
        }

        isum += nmols*natoms_mol;
    }

    printf("Found %d unconnected molecule%s in the solute group.\n", nSoluteMols, nSoluteMols > 1 ? "s" : "");

    return nSoluteMols;
}


/** Setup indices of protein and solvent */
static void prep_md_indices(int** indA_prot, int* nindA_prot, int** indA_solv, int* nindA_solv, const gmx_mtop_t* mtop,
        gmx_bool bDoProt, gmx_bool bDoSolv) {
    int nSoluteGroups = countGroups(mtop->groups, SimulationAtomGroupType::egcWAXSSolute);
    int nSolventGroups = countGroups(mtop->groups, SimulationAtomGroupType::egcWAXSSolvent);

    int* prot = nullptr;
    int* solv = nullptr;
    int nprot = 0;
    int nsolv = 0;

    int natoms = mtop->natoms;

    for (int i = 0; i < natoms; i++) {
        if (bDoSolv and getGroupType(mtop->groups, SimulationAtomGroupType::egcWAXSSolvent, i) < nSolventGroups) {
            nsolv++;
            srenew(solv, nsolv);
            solv[nsolv - 1] = i;
        } else if (bDoProt and getGroupType(mtop->groups, SimulationAtomGroupType::egcWAXSSolute, i) < nSoluteGroups) {
            nprot++;
            srenew(prot, nprot);
            prot[nprot - 1] = i;
        }
    }

    if (bDoProt) {
        *nindA_prot = nprot;
        *indA_prot = prot;
        fprintf(stderr, "Created array of %d solute atoms.\n", *nindA_prot);
    }

    if (bDoSolv) {
        *nindA_solv = nsolv;
        *indA_solv = solv;
        fprintf(stderr, "Created array of %d solvent atoms.\n", *nindA_solv);
    }
}

static void prep_fitting_weights(t_waxsrec* wr, int** ind_RotFit, int* nind_RotFit, real* w_fit, const gmx_mtop_t* mtop) {
    printf("WAXS-MD: Preparing weights for %s fitting...", wr->inputParams->bMassWeightedFit ? "mass-weighted" : "non-weighted");
    fflush(stdout);

    int natoms = mtop->natoms;

    for (int itot = 0; itot < natoms; itot++) {
        w_fit[itot] = 0.;
    }

    int ngrps_fit = countGroups(mtop->groups, SimulationAtomGroupType::egcROTFIT);

    int isum = 0;
    int* fitgrp = nullptr;
    int nfit = 0;

    /* This is a loop over all atoms. Global atom id is itot */
    /* Molecule types */
    for (std::size_t mb = 0; mb < mtop->molblock.size(); mb++) {
        const t_atoms* atoms = &mtop->moltype[mtop->molblock[mb].type].atoms;
        int nMols = mtop->molblock[mb].nmol;
        int natoms_mol = atoms->nr;

        /* molecules */
        for (int iMol = 0; iMol < nMols; iMol++) {
            /* atoms in molecule */
            for (int iatom = 0; iatom < natoms_mol; iatom++) {
                int itot = isum + iMol * natoms_mol + iatom;

                gmx_bool bFit = (getGroupType(mtop->groups, SimulationAtomGroupType::egcROTFIT, itot) < ngrps_fit);

                if (bFit) {
                    nfit++;
                    srenew(fitgrp, nfit);
                    fitgrp[nfit - 1] = itot;
                    real m = atoms->atom[iatom].m;
                    w_fit[itot] = wr->inputParams->bMassWeightedFit ? m : 1.;
                }
            }
        }

        isum += nMols * natoms_mol;
    }

    *nind_RotFit = nfit;
    *ind_RotFit = fitgrp;

    printf("done. Using %d fitting atoms.\n", *nind_RotFit);
}


/**
 * Allocate memory for the local storage of the averages A(q), B(q)
 *   A(q) - B(q), grad[k](A * (q)), and A(q) * grad[k](A * (q))
 * Allocating for waxsType t
 */
static void alloc_waxs_datablock(t_commrec* cr, t_waxsrec* wr, int nindA_prot, int t) {
    t_waxsrecType* wt = &wr->wt[t];
    int qhomenr = wt->qvecs->qhomenr;

    waxs_datablock* wd;
    snew(wd, 1);

    int nIvalues = -1;

    /** Store intensity for each |q| */
    nIvalues = wt->qvecs->nabs;

    wd->nIvalues = nIvalues;
    int nabs = wt->qvecs->nabs;

    /** Init norm of cumulative average to zero */
    wd->normA = 0.;
    wd->normB = 0.;

    /** Init average overall densities */
    wd->avAsum = 0;
    wd->avBsum = 0;

    int64_t nByte = 0;

    if (wr->inputParams->bCalcPot) {
        snew(wd->vAver, nIvalues);
        snew(wd->vAver2, nIvalues);
        // nByte += 2 * nIvalues * sizeof(real);
        nByte += nIvalues * (sizeof(wd->vAver[0]) + sizeof(wd->vAver2[0]));
    }

    wd->B                           = nullptr;
    wd->IA = wd->IB = wd->Ibulk     = nullptr;
    wd->I_scaleI0                   = nullptr;
    wd->I_varA = wd->I_varB         = nullptr;
    wd->varI = wd->varIA            = nullptr;
    wd->varIB = wd->varIbulk        = nullptr;
    wd->I_errSolvDens               = nullptr;
    wd->varI_avAmB2                 = nullptr;
    wd->varI_varA = wd->varI_varB   = nullptr;
    wd->avA = wd->avB = wd->avAcorr = nullptr;
    wd->re_avBbar_AmB               = nullptr;
    wd->avBsq                       = nullptr;
    wd->Orientational_Av            = nullptr;
    wd->avAqabs_re = wd->avBqabs_re = nullptr;
    wd->avAqabs_im = wd->avBqabs_im = nullptr;
    wd->Nindep                      = nullptr;
    wd->Dglobal                     = nullptr;
    wd->DerrSolvDens                = nullptr;
    wd->avAglobal                   = nullptr;
    wd->avAsqglobal                 = nullptr;
    wd->avA4global                  = nullptr;
    wd->av_ReA_2global              = nullptr;
    wd->av_ImA_2global              = nullptr;
    wd->avBglobal                   = nullptr;
    wd->avBsqglobal                 = nullptr;
    wd->avB4global                  = nullptr;
    wd->av_ReB_2global              = nullptr;
    wd->av_ImB_2global              = nullptr;

    snew(wd->A,          qhomenr);
    snew(wd->avA,        qhomenr);
    snew(wd->av_ReA_2,   qhomenr);
    snew(wd->av_ImA_2,   qhomenr);
    snew(wd->avAsq,      qhomenr);
    snew(wd->avA4,       qhomenr);
    snew(wd->D,          qhomenr);

    snew(wd->I,          nIvalues);
    snew(wd->varI,       nIvalues);
    snew(wd->IA,         nIvalues);
    snew(wd->varIA,      nIvalues);
    snew(wd->avAqabs_re, nIvalues);
    snew(wd->avAqabs_im, nIvalues);

    if (wr->inputParams->solventDensRelErr > 0) {
        snew(wd->DerrSolvDens, qhomenr);
        snew(wd->I_errSolvDens, nIvalues);
    }

    nByte += qhomenr*(sizeof(wd->A[0]) + sizeof(wd->avA[0]) + sizeof(wd->av_ReA_2[0]) + sizeof(wd->av_ImA_2[0]) +
            sizeof(wd->avAsq[0]) + sizeof(wd->avA4[0]) + sizeof(wd->D[0]) );
    nByte += nIvalues*(sizeof(wd->I[0]) + sizeof(wd->varI[0]) + sizeof(wd->IA[0]) +
            sizeof(wd->varIA[0]) + sizeof(wd->avAqabs_re[0]) + sizeof(wd->avAqabs_im[0]));

    for (int i = 0; i < qhomenr; i++) {
        wd->avA     [i] = cnul_d;
        wd->avAsq   [i] = 0.;
        wd->avA4    [i] = 0.;
        wd->av_ReA_2[i] = 0.;
        wd->av_ImA_2[i] = 0.;
    }

    if (!wr->inputParams->bVacuum) {
        snew(wd->IB,          nIvalues);
        snew(wd->Ibulk,       nIvalues);
        snew(wd->varIB,       nIvalues);
        snew(wd->varIbulk,    nIvalues);
        snew(wd->avBqabs_re,  nIvalues);
        snew(wd->avBqabs_im,  nIvalues);
        snew(wd->I_avAmB2,    nIvalues);
        snew(wd->I_varA,      nIvalues);
        snew(wd->I_varB,      nIvalues);

        if (wr->inputParams->bScaleI0) {
            snew(wd->I_scaleI0, nIvalues);
        }

        snew(wd->varI_avAmB2,   nIvalues);
        snew(wd->varI_varA,     nIvalues);
        snew(wd->varI_varB,     nIvalues);
        snew(wd->B,             qhomenr);
        snew(wd->avB,           qhomenr);
        snew(wd->av_ReB_2,      qhomenr);
        snew(wd->av_ImB_2,      qhomenr);
        snew(wd->avBsq,         qhomenr);
        snew(wd->avB4,          qhomenr);
        snew(wd->re_avBbar_AmB, qhomenr);

        nByte += qhomenr
                * (sizeof(wd->B[0]) + sizeof(wd->avB[0]) + sizeof(wd->av_ReB_2[0]) + sizeof(wd->av_ImB_2[0])
                        + sizeof(wd->avBsq[0]) + sizeof(wd->avB4[0]) + sizeof(wd->re_avBbar_AmB[0]));
        nByte += nIvalues
                * (sizeof(wd->IB[0]) + sizeof(wd->Ibulk[0]) + sizeof(wd->varIB[0]) + sizeof(wd->varIbulk[0])
                        + sizeof(wd->avBqabs_re[0]) + sizeof(wd->avBqabs_im[0]) + sizeof(wd->I_avAmB2[0])
                        + sizeof(wd->I_varA[0]) + sizeof(wd->I_varB[0]) + sizeof(wd->varI_avAmB2[0])
                        + sizeof(wd->varI_varA[0]) + sizeof(wd->varI_varB[0]));

        for (int i = 0; i < qhomenr; i++) {
            wd->avB[i] = cnul_d;
            wd->avBsq[i] = 0;
            wd->avB4[i] = 0.;
            wd->av_ReB_2[i] = 0.;
            wd->av_ImB_2[i] = 0.;
            wd->re_avBbar_AmB[i] = 0.;
        }
    }

    int64_t nByteInten = nByte;
    int64_t nByteForce = 0;

    if (wr->inputParams->bCalcForces) {
        snew(wd->dkI, nIvalues * nindA_prot);
        nByteForce += nIvalues * nindA_prot * sizeof(wd->dkI[0]);

        /** Now of size qhomenr*nindA_prot for gradients */
        snew(wd->dkAbar, qhomenr * nindA_prot);
        nByteForce += qhomenr * nindA_prot * sizeof(wd->dkAbar[0]);

        snew(wd->Orientational_Av, nindA_prot * nabs);
        nByteForce += nindA_prot * DIM * nIvalues;

        nByte += nByteForce;
    }

    wd->nByteAlloc = nByte;
    wt->wd = wd;

    for (int i = 0; i < (cr->nnodes - cr->npmenodes); i++) {
        if (cr->nodeid == i) {
            if (wr->inputParams->bCalcForces) {
                fprintf(stderr, "Node %2d) Allocated %8g MB to store averages "
                        "(%4g MiB for intensities, %8g MB for forces) (scattering type %d).\n",
                        cr->nodeid, 1.0 * wd->nByteAlloc / 1024 / 1024, 1.0 * nByteInten / 1024 / 1024,
                        1.0 * nByteForce / 1024 / 1024, t);
            } else {
                fprintf(stderr, "Node %2d) Allocated %8g MiB to store averages (scattering type %d)\n",
                        cr->nodeid, 1.0 * wd->nByteAlloc / 1024 / 1024, t);
            }
        }

        if (PAR(cr)) {
            gmx_barrier(cr->mpi_comm_mygroup);
        }
    }
}

static void done_waxs_datablock(t_waxsrec* wr) {
    waxs_datablock* wd;

    for (int t = 0; t < wr->inputParams->nTypes; t++) {
        wd = wr->wt[t].wd;

        sfree(wd->A);
        sfree(wd->avA);
        sfree(wd->av_ReA_2);
        sfree(wd->av_ImA_2);
        sfree(wd->avAsq);
        sfree(wd->avA4);
        sfree(wd->D);
        sfree(wd->I);
        sfree(wd->varI);
        sfree(wd->IA);
        sfree(wd->varIA);
        sfree(wd->avAqabs_re);
        sfree(wd->avAqabs_im);

        if (!wr->inputParams->bVacuum) {
            sfree(wd->IB);
            sfree(wd->Ibulk);
            sfree(wd->varIB);
            sfree(wd->varIbulk);
            sfree(wd->avBqabs_re);
            sfree(wd->avBqabs_im);
            sfree(wd->I_avAmB2);
            sfree(wd->I_varA);
            sfree(wd->I_varB);
            sfree(wd->I_scaleI0);
            sfree(wd->varI_avAmB2);
            sfree(wd->varI_varA);
            sfree(wd->varI_varB);
            sfree(wd->B);
            sfree(wd->avB);
            sfree(wd->av_ReB_2);
            sfree(wd->av_ImB_2);
            sfree(wd->avBsq);
            sfree(wd->avB4);
            sfree(wd->re_avBbar_AmB);
        }

        if (wr->inputParams->bCalcForces) {
            sfree(wd->dkI);
            sfree(wd->dkAbar);
            sfree(wd->Orientational_Av);
        }

        sfree(wd);
        wr->wt[t].wd = nullptr;
    }
}

/* Changed to raw malloc becuase GROMACS doesn't like things being sfree'd
 * to give space then reallocated.

   Jochen: Strange, I don't think there is a problem with snew/sfree (Feb 2016)

 */
void waxs_alloc_globalavs(t_waxsrec* wr) {
    for (int t = 0; t < wr->inputParams->nTypes; t++) {
        int nq = wr->wt[t].qvecs->n;

        waxs_datablock* wd = wr->wt[t].wd;

        wd->Dglobal = (double*)calloc(nq, sizeof(*(wd->Dglobal)));
        wd->avAglobal = (t_complex_d*)calloc(nq, sizeof(*(wd->avAglobal)));
        wd->avAsqglobal = (double*)calloc(nq, sizeof(*(wd->avAsqglobal)));
        wd->avA4global = (double*)calloc(nq, sizeof(*(wd->avA4global)));
        wd->av_ReA_2global = (double*)calloc(nq, sizeof(*(wd->av_ReA_2global)));
        wd->av_ImA_2global = (double*)calloc(nq, sizeof(*(wd->av_ImA_2global)));

        if (!wr->inputParams->bVacuum) {
            wd->avBglobal = (t_complex_d*)calloc(nq, sizeof(*(wd->avBglobal)));
            wd->avBsqglobal = (double*)calloc(nq, sizeof(*(wd->avBsqglobal)));
            wd->avB4global = (double*)calloc(nq, sizeof(*(wd->avB4global)));
            wd->av_ReB_2global = (double*)calloc(nq, sizeof(*(wd->av_ReB_2global)));
            wd->av_ImB_2global = (double*)calloc(nq, sizeof(*(wd->av_ImB_2global)));
        }
    }
}

void waxs_free_globalavs(t_waxsrec* wr) {
    for (int t = 0; t < wr->inputParams->nTypes; t++) {
        waxs_datablock* wd = wr->wt[t].wd;

        free(wd->Dglobal);         wd->Dglobal        = nullptr;
        free(wd->avAglobal);       wd->avAglobal      = nullptr;
        free(wd->avAsqglobal);     wd->avAsqglobal    = nullptr;
        free(wd->avA4global);      wd->avA4global     = nullptr;
        free(wd->av_ReA_2global);  wd->av_ReA_2global = nullptr;
        free(wd->av_ImA_2global);  wd->av_ImA_2global = nullptr;

        if (!wr->inputParams->bVacuum) {
            free(wd->avBglobal);      wd->avBglobal      = nullptr;
            free(wd->avBsqglobal);    wd->avBsqglobal    = nullptr;
            free(wd->avB4global);     wd->avB4global     = nullptr;
            free(wd->av_ReB_2global); wd->av_ReB_2global = nullptr;
            free(wd->av_ImB_2global); wd->av_ImB_2global = nullptr;
        }
    }
}

void init_waxs_md(t_forcerec* fr, t_commrec* cr, t_inputrec* ir, const gmx_mtop_t* top_global,
        const gmx_output_env_t* oenv, double t0, const char* runFilePath, const char* trajectoryFilePath,
        const char* fnOut, const char* fnScatt, t_state* state_local, gmx_bool bRerunMD, gmx_bool bWaterOptSet,
        gmx_bool bReadI, gmx_bool started_from_checkpoint, gmx_bool use_gpu) {
    if (t0 > 0.) {
        gmx_fatal(FARGS, "Starting from t is currently not implemented.\n");
    }

    int nsltype;
    swaxs::Solvent* solvent = nullptr;

    if (started_from_checkpoint) {
        /* At present, you need to rerun grompp to restart a SAXS-driven MD. This is required
           to make sure that the posres reference coordinates are updated. Posres is used during
           tau < simtime < 2tau to make sure the structure is not drifting due to the
           absence of the SAXS-derived forces */
        gmx_fatal(FARGS, "With WAXS-MD, you cannot restart from a checkpoint file. Rerun grompp and start with a new tpr file.\n"
                "This is required to make sure that the posres reference coordinates are updated.\n");
    }

    fr->waxsrec = init_t_waxsrec();
    fr->waxsrec->inputParams = std::make_unique<swaxs::InputParams>();

    fr->waxsrec->inputParams->bUseGPU = use_gpu;

    if (ir->cutoff_scheme == ecutsVERLET) {
        fr->waxsrec->inputParams->bUseGPU = FALSE;
    }

#if GMX_GPU != GMX_GPU_CUDA
    fr->waxsrec->inputParams->bUseGPU = FALSE;
#else
    gmx_warning("GROMACS-SWAXS: %s\n", fr->waxsrec->inputParams->bUseGPU ? "going to use GPU" : "will not use GPU");
#endif

    t_waxsrec* wr = fr->waxsrec;

    wr->inputParams->bDoingMD = (ir != nullptr);

    /** As soon as we have a targt SAXS curve, we compute the SAXS potential */
    wr->inputParams->bCalcPot = bReadI;

    /** Compute forces only if waxs_tau is positive, we don't do a rerun, and we have a target SAXS curve */
    wr->inputParams->bCalcForces = (!bRerunMD and (ir->waxs_tau > 0) and bReadI);

#ifdef RERUN_CalcForces
    // JUST FOR TEST - use only in case you know what you are doing!
    bRerunMD = FALSE ;
    wr->inputParams->bCalcForces = TRUE;  // TEST!!!
#endif

    if (ir == nullptr) {
        gmx_fatal(FARGS, "Error in init_waxs_md(), input record not initialized\n");
    }

    if (MASTER(cr) and (!bRerunMD and ir->waxs_tau <= 0)) {
        fprintf(stderr, "WAXS-MD: Found waxs-tau <= 0 : will NOT calculate forces.\n");
    }

    if (MASTER(cr)) {
        fprintf(stderr, "WAXS-MD: Using SIMD level: %s\n", waxs_simd_string());
    }

    if (bReadI and !bRerunMD and !(ir->waxs_tau > 0)) {
        gmx_fatal(FARGS, "You are running an MD simulation with a target SAXS curve, but waxs-tau is <=0 (%g)\n"
                "For a SAXS-driven MD simulation, you should use a positive waxs-tau (e.g. 200ps)\n", ir->waxs_tau);
    }

    if (MASTER(cr)) {
        fprintf(stderr, "WAXS-MD: We do %scalculate the potential\n", wr->inputParams->bCalcPot ? "" : "NOT ");
        fprintf(stderr, "WAXS-MD: We do %scalculate forces.\n", wr->inputParams->bCalcForces ? "" : "NOT ");
        printf("WAXS-MD: Will compute WAXS patterns on %d nodes\n", cr->nnodes - cr->npmenodes);
    }

    if (cr->nodeid >= (cr->nnodes - cr->npmenodes)) {
        gmx_fatal(FARGS, "Inconsnsitency in init_waxs_md, nodeid = %d, but expected only PP %d nodes (nnodes = %d, npmenodes = %d)\n",
                cr->nodeid, cr->nnodes - cr->npmenodes, cr->nnodes, cr->npmenodes);
    }

    /* For the moment has to be implemented later */
    if (gmx_omp_nthreads_get(emntDefault) > 1) {
        printf("WAXS-MD: NOTE: OpenMP paralellization is supported but not fully optimized yet!\n");
    }

    /**
     * We compute the WAXS forces after calculating the virial in do_force(). Therefore,
     * it is anyway not added to the virial. No need to set fr->bF_NoVirSum=TRUE
     * Note: If we decide to compute WAXS forces before computing the virial, we must
     * add the WAXS force to f_novirsum and set fr->bF_NoVirSum = TRUE NOT here but in
     * forcerec.c. Otherwise a segfault would occur in do_force().
     */
    // fr->bF_NoVirSum = fr->bF_NoVirSum or wr->inputParams->bCalcForces;

    /** Store a pointer on the local state - required for dd_collect_state() within do_force */
    if (state_local) {
        wr->local_state = state_local;
    }

    /** Copy values to waxsrec to make the rest source-independent. */
    if (wr->inputParams->bDoingMD) {
        copy_ir2waxsrec(wr, ir, cr);
    }

    if (MASTER(cr)) {
        if (!wr->inputParams->bVacuum) {
            solvent = new swaxs::Solvent(runFilePath, trajectoryFilePath, oenv, TRUE, wr->inputParams->nfrsolvent);

            wr->solvent = solvent;
            wr->inputParams->nfrsolvent = solvent->nFrames;

            if (wr->inputParams->bDoingNeutron and !solvent->hasNeutron()) {
                gmx_fatal(FARGS, "Doing neutron scattering, but found no neutron scattering length in solvent tpr file.\n"
                        "Use `scatt-coupl = neutron` in your solvent .mdp file.");
            }
        } else if (wr->inputParams->bDoingMD and bWaterOptSet) {
            gmx_fatal(FARGS, "No waxs_solvent was specified in main simulation, but options\n"
                    " to provide excluded solvent was passed to mdrun\n");
        }
    }

    /* Broadcast values that were changed on MASTER after reading waxs solvent.
     * Required for all nodes to keep in step when determining bDoingSolvent */
    if (PAR(cr)) {
        gmx_bcast(sizeof(int), &wr->inputParams->nfrsolvent, cr->mpi_comm_mygroup);
    }

    /* read scattering curve to which we couple and broadcast it */
    if (wr->inputParams->bCalcPot) {
        read_waxs_curves(fnScatt, wr, cr);
    }

    /* Redirect the atomtypes in waxs-solv to existing types and allocate waxsrec to receive these types. */
    if (MASTER(cr) and solvent) {
        redirect_md_solventtypes(solvent->topology.get(), &top_global->scattTypes);
    }

    /* init envelope */
    if (getenv("GMX_ENVELOPE_FILE") == nullptr) {
        gmx_fatal(FARGS, "Environment varialbe GMX_ENVELOPE_FILE not found. Please define it to the file name of\n"
                "the evelope (such as envelope.py), written by g_genenv. In addition, define GMX_WAXS_FIT_REFFILE\n"
                "to the reference file written by g_genenv.\n");
    }

    for (int t = 0; t < wr->inputParams->nTypes; t++) {
        wr->wt[t].envelope = gmx_envelope_init_md(-1, cr, MASTER(cr));
    }

    swaxs::readEnvironmentVariables(wr, cr);

    /* setup q */
    /* boolean == TRUE means write debug output */
    for (int t = 0; t < wr->inputParams->nTypes; t++) {
        if (wr->wt[t].nq <= 0) {
            gmx_fatal(FARGS, "Number of q vales must be >= 1. Fixs waxs-nq mdp option.\n");
        }

        wr->wt[t].qvecs = gen_qvecs_map(wr->wt[t].minq, wr->wt[t].maxq, wr->wt[t].nq, wr->inputParams->J,
                t == 0 and wr->inputParams->verbosityLevel > 1, cr, wr->wt[t].envelope, wr->inputParams->Jmin, wr->inputParams->Jalpha, MASTER(cr) and t == 0);
    }

    if (wr->inputParams->nfrsolvent > 0) {
#if GMX_GPU_CUDA
        /** Unit FT is only calculated once. It is therefore also only copied once to GPU */
        if (wr->inputParams->bUseGPU == TRUE) {
            push_unitFT_to_GPU(wr);
        }
#endif
    }

    if (MASTER(cr)) {
        /** Init output stuff */
        if (fnOut) {
            wr->swaxsOutput = std::make_unique<swaxs::SwaxsOutput>(wr, fnOut, oenv);
        }
    }

    if (MASTER(cr)) {
        /* Memory for atom coordinates x, so we can overwrite the coordinates when making the solute whole, etc. */
        wr->x.resizeWithPadding(top_global->natoms);

        /* Short cut array with number of electrons per atom (size top_global->natoms) */
        wr->nElectrons = swaxs::countElectronsPerAtom(top_global);

        prep_md_indices(&wr->indA_prot, &wr->nindA_prot, &wr->indA_sol, &wr->nindA_sol, top_global, TRUE, !wr->inputParams->bVacuum);

        if (solvent) {
            prep_md_indices(nullptr, nullptr, &wr->indB_sol, &wr->nindB_sol, solvent->topology.get(), FALSE, TRUE);
        }

        /** Pick cmtypes and nsltypes from mtop (main simulation and solvent) into continuous array in waxsrec */
        wr->inputParams->cmtypeList = mtop2scattTypeList(top_global, FALSE);

        if (wr->inputParams->bDoingNeutron) {
            wr->inputParams->nsltypeList = mtop2scattTypeList(top_global, TRUE);
        }

        if (solvent) {
            solvent->cromerMannTypes = mtop2scattTypeList(solvent->topology.get(), FALSE);

            if (wr->inputParams->bDoingNeutron) {
                solvent->neutronScatteringLengthTypes = mtop2scattTypeList(solvent->topology.get(), TRUE);
            }
        }

        /**
         * Write protein atoms into beginning of waxsrec->indexA. These will not change
         * throughout the calculations, but solvation shell atoms will be added to indexA.
         */
        snew(wr->indexA, wr->nindA_prot);
        wr->isizeA = wr->nindA_prot;
        wr->indexA_nalloc = wr->nindA_prot;

        for (int i = 0; i < wr->nindA_prot; i++) {
            wr->indexA[i] = wr->indA_prot[i];
        }

        /** Array for atomic coordinates inside the envelope, update every each step */
        snew(wr->atomEnvelope_coord_A, wr->nindA_prot);

        for (int t = 0; t < wr->inputParams->nTypes; t++) {
            snew(wr->wt[t].atomEnvelope_scatType_A, wr->nindA_prot);
        }

        /** Arrays for solute and solvent atom shifts that are inside the envelope */
        snew(wr->shiftsInsideEnvelope_A_prot, wr->nindA_prot);
        snew(wr->shiftsInsideEnvelope_A_sol, wr->nindA_sol);

        if (solvent) {
            snew(wr->shiftsInsideEnvelope_B, wr->nindB_sol);
        }

        /** Init PBC atom. Number-wise center if not defined. If defined, -1 for mdrun-interal numbering */
        if (wr->inputParams->npbcatom == 0) {
            gmx_fatal(FARGS, "Something has gone wrong - pbcatom information is missing!\n");
        }

        /** Get # of molecules in Solute - if ==1, we can make the solute whole using gmx internal routines */
        wr->nSoluteMols = nSoluteMolecules(top_global);

        /** Check for the type of pbcatom fitting. */
        if (wr->pbcatoms[0] == -1) {
            wr->pbcatoms[0] = (wr->nindA_prot + 1) / 2 - 1;
        } else if (wr->pbcatoms[0] < -1) {
            if (wr->nSoluteMols > 1) {
                gmx_fatal(FARGS, "Found waxs-pbc atom = %d, meaning that the protein is made whole using\n"
                        "the distances between number-wise atomic neighbors. However, this is only possible\n"
                        "when there is only 1 molecule in the solute group (found %d)\n",
                        wr->pbcatoms[0] + 1, wr->nSoluteMols);
            }
        }

        fprintf(stderr, "Using %d PBC atoms for WAXS Solute. First atom index (global): %d\n", wr->inputParams->npbcatom,
                wr->pbcatoms[0]);

        /* fprintf(stderr,"Using PBC atom id (mdrun-internal) for WAXS solute: %d\n",wr->pbcatom); */

        /* Rotational-fitting requires the solute indices to be defined. */
        if (wr->inputParams->bRotFit) {
            snew(wr->x_ref, top_global->natoms);
            snew(wr->w_fit, top_global->natoms);

            for (int i = 0; i < top_global->natoms; i++) {
                clear_rvec(wr->x_ref[i]);
            }

            prep_fitting_weights(wr, &wr->ind_RotFit, &wr->nind_RotFit, wr->w_fit, top_global);

            if (wr->inputParams->bHaveRefFile) {
                read_fit_reference(wr->x_ref_file, wr->x_ref, top_global->natoms, wr->indA_prot, wr->nindA_prot,
                        wr->ind_RotFit, wr->nind_RotFit);

                reset_x(wr->nind_RotFit, wr->ind_RotFit, top_global->natoms, nullptr, wr->x_ref, wr->w_fit);
                fprintf(stderr, "Loaded reference file into x_ref.\n");
            } else {
                gmx_fatal(FARGS, "Environtment variable GMX_WAXS_FIT_REFFILE not defined. Define "
                        "it with the fit reference coordinate file written by g_genenv\n");
            }
        }

        /* TODO: Pass seed from command-line */

        uint64_t seed = 0;

        if (seed == 0) {
            seed = gmx::makeRandomSeed();
        }

        wr->rng = new gmx::DefaultRandomEngine(seed);

        if (!wr->inputParams->bVacuum) {
            wr->solvent->getDensity(wr->nindB_sol, wr->indB_sol, wr->shiftsInsideEnvelope_B, wr->wt[0].envelope,
                    wr->inputParams->bFixSolventDensity, wr->inputParams->givenElecSolventDensity, wr->inputParams->verbosityLevel);

            wr->solElecDensAv_SysB = wr->solvent->averageDropletDensity;
        }
    }

    if (PAR(cr)) {
        gmx_bcast(sizeof(real), &wr->solElecDensAv_SysB, cr->mpi_comm_mygroup);
    }

    for (int t = 0; t < wr->inputParams->nTypes; t++) {
        /* allocate memory for cumulative averages and init wd->norm (for averaging) to zero. */
        if (PAR(cr)) {
            gmx_bcast(sizeof(int), &wr->nindA_prot, cr->mpi_comm_mygroup);
        }

        alloc_waxs_datablock(cr, wr, wr->nindA_prot, t);

        t_waxsrecType* wt = &wr->wt[t];

        if (wt->type == escatterXRAY) {
            /** Build table of atomic form factors: 1st index: cmtype. 2nd index: absolute value of q. */
            compute_atomic_form_factor_table(&top_global->scattTypes, wt, cr);
        } else if (wt->type == escatterNEUTRON) {
            /** Build table of NSLs, taking deuterium concentration into account. */
            fill_neutron_scatt_len_table(&top_global->scattTypes, wt, wr->inputParams->backboneDeuterProb);
        }

        /** Store qstart and qhomenr on Master. Requires both alloc_datablock and gen_qvecs */
        gen_qvecs_accounting(wr, cr, cr->nnodes - cr->npmenodes, t);
    }

    if (wr->inputParams->bCalcForces) {
        if (PAR(cr)) {
            if (wr->indA_prot == nullptr) {
                snew(wr->indA_prot, wr->nindA_prot);
            }

            gmx_bcast(wr->nindA_prot * sizeof(int), wr->indA_prot, cr->mpi_comm_mygroup);
        }

        snew(wr->fLast, wr->nindA_prot);

        for (int t = 0; t < wr->inputParams->nTypes; t++) {
            snew(wr->wt[t].fLast, wr->nindA_prot);
        }
    }

    /** Count total number of electrons in A system */
    if (MASTER(cr)) {
        for (int i = 0; i < wr->nindA_prot; i++) {
            wr->nElecTotA += wr->nElectrons[wr->indA_prot[i]];
        }

        wr->nElecProtA = wr->nElecTotA;

        for (int i = 0; i < wr->nindA_sol; i++) {
            wr->nElecTotA += wr->nElectrons[wr->indA_sol[i]];
        }

        printf("There are %g electrons in total in solute + solvent (%g per atom on average)\n", wr->nElecTotA,
                wr->nElecTotA / (wr->nindA_prot + wr->nindA_sol));
    }

    /**
     * Save sum of scattering lengths of solute and solvent (# of electrons or # of NSLs).
     * This will be used to estimate the contrast of the solute
     */
    gmx_bool bStochDeuterBackup = wr->inputParams->bStochasticDeuteration;
    wr->inputParams->bStochasticDeuteration = FALSE;

    if (MASTER(cr) and solvent) {
        snew(solvent->averageScatteringLengthDensity, wr->inputParams->nTypes);
    }

    for (int t = 0; t < wr->inputParams->nTypes; t++) {
        if (MASTER(cr)) {
            t_waxsrecType* wt = &wr->wt[t];

            if (wt->type == escatterXRAY) {
                wt->soluteSumOfScattLengths = wr->nElecProtA;

                if (solvent) {
                    solvent->averageScatteringLengthDensity[t] = solvent->nElectrons * solvent->averageInverseVolume;
                }
            } else {
                /** Sum NSLs of solute without stochastic deuteration */
                wt->soluteSumOfScattLengths = 0;

                for (int i = 0; i < wr->nindA_prot; i++) {
                    int j = wr->indA_prot[i];

                    nsltype = get_nsl_type(wr, t, wr->inputParams->nsltypeList[j]);
                    wt->soluteSumOfScattLengths += wt->nsl_table[nsltype];
                }

                if (solvent) {
                    double sum = 0;

                    for (int i = 0; i < wr->nindB_sol; i++) {
                        int j = wr->indB_sol[i];

                        nsltype = get_nsl_type(wr, t, solvent->neutronScatteringLengthTypes[j]);
                        sum += wt->nsl_table[nsltype];
                    }

                    solvent->averageScatteringLengthDensity[t] = sum * solvent->averageInverseVolume;
                }
            }
        }
    }

    wr->inputParams->bStochasticDeuteration = bStochDeuterBackup;

    if (MASTER(cr)) {
        printf("\nSum of scattering lengths of solute:\n");

        for (int t = 0; t < wr->inputParams->nTypes; t++) {
            t_waxsrecType* wt = &wr->wt[t];

            printf("\tType %d (%s) sum = %10g %s\n", t, wt->saxssansStr, wt->soluteSumOfScattLengths,
                    (wt->type == escatterXRAY) ? "electrons" : "NSLs");
        }

        printf("\n");

        if (solvent) {
            printf("\nDensities of pure-solvent system:\n");

            for (int t = 0; t < wr->inputParams->nTypes; t++) {
                t_waxsrecType* wt = &wr->wt[t];

                printf("\tType %d (%s)  density = %10g %s/nm3\n",
                        t, wt->saxssansStr, solvent->averageScatteringLengthDensity[t], (wt->type == escatterXRAY) ? "electrons" : "NSLs");
            }

            printf("\n");
        }
    }

    /** Get scattering intensity of the pure buffer. Used to add back oversubtracted buffer */
    if (MASTER(cr)) {
        printf("WAXS-MD: %s back oversubtracted buffer intensity\n",
                (wr->inputParams->bCorrectBuffer and !wr->inputParams->bVacuum) ? "Adding" : "Not adding");
    }

    if (wr->inputParams->bCorrectBuffer and !wr->inputParams->bVacuum) {
        if (wr->inputParams->bDoingNeutron) {
            gmx_fatal(FARGS, "For neutron scattering, only the simple buffer subtraction scheme "
                    "I_sam - I_buf is supported so far.\nChange mdp option waxs-correct-buffer to no.");
        }

        swaxs::solventScatteringFromRdfs(wr, cr, trajectoryFilePath, top_global);
    }

    /** Consitency checks for ensembles */
    if (!(wr->inputParams->ewaxs_ensemble_type == ewaxsEnsembleNone or wr->inputParams->ewaxs_ensemble_type == ewaxsEnsemble_BayesianOneRefined)) {
        gmx_fatal(FARGS, "Only ensemble types 'none' or 'bayesian-one-refined' supported\n");
    }

    /*
    if (wr->inputParams->ewaxs_ensemble_type == ewaxsEnsemble_BayesianOneRefined and !wr->bBayesianMD) {
        gmx_fatal(FARGS, "Ensemble refinement so far only with Bayesian MD. wr->bBayesianMD == %d, wr->inputParams->ewaxs_ensemble_type = %d\n",
                wr->bBayesianMD, wr->inputParams->ewaxs_ensemble_type);
    }
    */

    if (WAXS_ENSEMBLE(wr) and wr->inputParams->ewaxs_ensemble_type == ewaxsEnsembleNone) {
        gmx_fatal(FARGS, "Found >1 number of states in enbemble (%d), but ewaxs_ensemble_type = none\n", wr->inputParams->ensemble_nstates);
    }

    if (WAXS_ENSEMBLE(wr)) {
        /** For ensemble refinement, read the intensities of the other fixed states */
        init_ensemble_stuff(wr, cr);
    }

    if (ir->cutoff_scheme == ecutsVERLET) {
#if GMX_GPU_CUDA
        /** Here all data is copied to GPU that will remain constantly there. It initializes the GPU for all scattering types. */
        if (wr->inputParams->bUseGPU) {
            init_gpudata_type(wr);
        }

        fprintf(stderr, "Initiated data for SAXS/SANS calculations on the GPU.\n");
#endif
    }

    /** Init computing time measurements */
    if (MASTER(cr)) {
        wr->timing = std::make_unique<swaxs::Timing>();
    }
}

void done_waxs_md(t_waxsrec* wr) {
    for (int t = 0; t < wr->inputParams->nTypes; t++) {
        free_qvecs_accounting(wr->wt[t].wd);

        if (wr->wt[t].wd) {
            done_waxs_datablock(wr);

            if (wr->solvent) {
                delete wr->solvent;
                wr->solvent = nullptr;
            }
        }
    }
}

#ifdef __cplusplus
extern "C" {
#endif

/** Update cumulative averages: cum_sum = fac1 * sum + fac2 * cum_sum */

#if 0
static real r_accum_avg(real sum, real cum_sum, real fac1, real fac2) {
    real tmp = fac1 * sum + fac2 * cum_sum;

    return tmp;
}
#endif

static double d_accum_avg(double sum, double cum_sum, double fac1, double fac2) {
    double tmp = fac1 * sum + fac2 * cum_sum;

    return tmp;
}

#if 0
static t_complex c_accum_avg(t_complex sum, t_complex cum_sum, real fac1, real fac2) {
    t_complex tmp = cadd(rcmul(fac1, sum), rcmul(fac2, cum_sum));

    return tmp;
}
#endif

static t_complex_d cd_accum_avg(t_complex_d sum, t_complex_d cum_sum, double fac1, double fac2) {
    t_complex_d tmp = cadd_d(rcmul_d(fac1, sum), rcmul_d(fac2, cum_sum));

    return tmp;
}

#if 0
static void cvec_accum_avg(cvec* sum, cvec cum_vec, real fac1, real fac2) {
    cvec tmp;

    for (int d = 0; d < DIM; ++d) {
        tmp[d] = c_accum_avg(*sum[d], cum_vec[d], fac1, fac2);
        *sum[d] = tmp[d];
    }
}
#endif

#ifdef __cplusplus
}
#endif

/**
 * Update the average values, or calculate the average values.
 * Initialized to zero in alloc_waxs_datablock()
 */
static void update_AB_averages(t_waxsrec* wr) {
    #ifdef RERUN_CalcForces
        gmx_bool bCalcForces = TRUE;
    #endif

    swaxs_debug("Entering update_AB_averages()");

    for (int t = 0; t < wr->inputParams->nTypes; t++) {
        t_waxsrecType* wt = &wr->wt[t];
        waxs_datablock* wd = wt->wd;

        int qhomenr = wt->qvecs->qhomenr;
        int qstart = wt->qvecs->qstart;
        int nabs = wt->qvecs->nabs;

        /**
         * Get fac1 and fac2 for running weighted average
         *   <X[i+1]> = fac1 * <X[i]> + fac2 * x[i + 1] =
         *            =  (N[i + 1] - 1) / N[i + 1]) * <X[i]> + 1 / N[i + 1] * x[i + 1]
         *  This is valid for either exponential or equally-weighted averaging.
         */
        wd->normA = 1.0 + wr->inputParams->scale * wd->normA;

        double fac1A = 1.0 * (wd->normA - 1) / wd->normA;
        double fac2A = 1.0 / wd->normA;

        /** For the B system (pure solvent), always use a non-weighted average (scale = 1) */
        double fac1B = 0.;
        double fac2B = 0.;

        if (wr->bDoingSolvent) {
            wd->normB = 1.0 + wd->normB;

            fac1B = 1.0 * (wd->normB - 1) / wd->normB;
            fac2B = 1.0 / wd->normB;
        }

        /**
         * Note on precision: A[i], B[i] and dkAbar[k] are real.
         *                    All averages are double.
         */

        /** For each local q vector */
        for (int i = 0; i < qhomenr; i++) {
            t_complex_d tmp_cd;

            tmp_cd.re = wd->A[i].re;
            tmp_cd.im = wd->A[i].im;

            wd->avA     [i] = cd_accum_avg(wd->avA    [i], tmp_cd,                      fac1A, fac2A);
            double dtmp = cabs2_d(wd->A[i]);
            wd->avAsq   [i] = d_accum_avg(wd->avAsq   [i], dtmp,                        fac1A, fac2A);
            wd->avA4    [i] = d_accum_avg(wd->avA4    [i], dtmp * dtmp,                 fac1A, fac2A);
            wd->av_ReA_2[i] = d_accum_avg(wd->av_ReA_2[i], gmx::square(wd->A[i].re), fac1A, fac2A);
            wd->av_ImA_2[i] = d_accum_avg(wd->av_ImA_2[i], gmx::square(wd->A[i].im), fac1A, fac2A);

            if (wr->bDoingSolvent) {
                tmp_cd.re = wd->B[i].re;
                tmp_cd.im = wd->B[i].im;

                /**
                 * We could pick the avB from the GPU if available.
                 * But due to the low calculation but expensive data transfer cost, avB is recalculated here.
                 */

                wd->avB     [i] = cd_accum_avg(wd->avB    [i], tmp_cd,                      fac1B, fac2B);
                double dtmp = cabs2_d(wd->B[i]);
                wd->avBsq   [i] = d_accum_avg(wd->avBsq   [i], dtmp,                        fac1B, fac2B);
                wd->avB4    [i] = d_accum_avg(wd->avB4    [i], dtmp * dtmp,                 fac1B, fac2B);
                wd->av_ReB_2[i] = d_accum_avg(wd->av_ReB_2[i], gmx::square(wd->B[i].re), fac1B, fac2B);
                wd->av_ImB_2[i] = d_accum_avg(wd->av_ImB_2[i], gmx::square(wd->B[i].im), fac1B, fac2B);
            }
        }

        /** If we use the GPU, the orientiational average for grad D(q) is already done on the GPU */
        if (wr->inputParams->bCalcForces and wr->inputParams->bUseGPU == FALSE) {
            int nprot = wr->nindA_prot;

            for (int p = 0; p < nprot; p++) {
                for (int n = 0; n < nabs; n++) {
                    /** 1st summand */
                    dvec tmp_dkAbar_A_avB;
                    clear_dvec(tmp_dkAbar_A_avB);

                    /** loop over q-vec which belong to this absolute value |q_i| */
                    for (int j = wt->qvecs->ind[n]; j < wt->qvecs->ind[n + 1]; j++) {
                        if (j >= qstart and j < (qstart + qhomenr)) {
                            int jj = j - qstart;  /** index of A and avB */

                            /** index of derivatives atoms */
                            int k = p * qhomenr + jj ;

                            /**
                             * Do product of A.grad[k]*.(A(q) - B_av)  (before time average)
                             *
                             * Calculate the difference A[k] - B_av
                             * Note that after a certain number of steps <B> is not changing anymore, therefore, it can be taken as a constant
                             */
                            t_complex_d A_avB_tmp;

                            A_avB_tmp.re = wd->A[jj].re - wd->avB[jj].re;
                            A_avB_tmp.im = wd->A[jj].im - wd->avB[jj].im;

                            if (wr->inputParams->bScaleI0) {
                                A_avB_tmp.re += wd->avAcorr[jj].re - wd->avB[jj].re;
                                A_avB_tmp.im += wd->avAcorr[jj].im - wd->avB[jj].im;
                            }

                            /** grad[k](A*(q)) . (A(q) - B_av)  (before time average). Only the real part remains: */
                            double tmpc1 = +wd->dkAbar[k].re * A_avB_tmp.re
                                           -wd->dkAbar[k].im * A_avB_tmp.im;

                            for (int d = 0; d < DIM; d++) {
                                /** Multiply with \vec{q} */
                                tmp_dkAbar_A_avB[d] += tmpc1 * wt->qvecs->q[j][d];
                            }
                        }
                    }

                    int l = p * nabs + n;

                    for (int d = 0; d < DIM; d++) {
                        /** Temporal running average */
                        wd->Orientational_Av[l][d] = d_accum_avg(wd->Orientational_Av[l][d], tmp_dkAbar_A_avB[d], fac1A, fac2A);
                    }
                }
            }
        }
    }

    swaxs_debug("Leaving update_AB_averages()");
}


/** Computes I(q) and grad[k] I(q) (in parallel) */
static void calculate_I_dkI(t_waxsrec* wr, const t_commrec* cr) {
    swaxs_debug("Entering calculate_I_dkI()");

    for (int t = 0; t < wr->inputParams->nTypes; t++) {
        t_waxsrecType* wt = &wr->wt[t];
        t_spherical_map* qvecs = wt->qvecs;
        waxs_datablock* wd = wt->wd;

        double Bprefact;

        if (wr->inputParams->tau < 0.) {
            /** Prefactor in front of <B^2>-<B>^2 to get an unbiased estimator. Park et al., eq. 31 */
            int nFramesSolvent = (wr->waxsStep < wr->inputParams->nfrsolvent) ? (wr->waxsStep + 1) : wr->inputParams->nfrsolvent;

            Bprefact = (nFramesSolvent > 1) ? (1. * nFramesSolvent + 1.) / (1. * nFramesSolvent - 1.) : 1;
        } else {
            Bprefact = 1.;
        }

        int nIvalues = wd->nIvalues;

        /** Clear I, grad[k] I */
        for (int i = 0; i < nIvalues; i++) {
            wd->I[i] = 0.;
            wd->IA[i] = 0.;
            wd->varIA[i] = 0.;
            wd->avAqabs_re[i] = 0.;
            wd->avAqabs_im[i] = 0.;

            if (!wr->inputParams->bVacuum) {
                wd->IB[i] = 0.;
                wd->Ibulk[i] = 0.;
                wd->varIB[i] = 0.;
                wd->varIbulk[i] = 0.;
                wd->avBqabs_re[i] = 0.;
                wd->avBqabs_im[i] = 0.;
                wd->I_avAmB2[i] = 0.;
                wd->I_varA[i] = 0.;
                wd->I_varB[i] = 0.;
                wd->varI_avAmB2[i] = 0.;
                wd->varI_varA[i] = 0.;
                wd->varI_varB[i] = 0.;

                if (wr->inputParams->bScaleI0) {
                    wd->I_scaleI0[i] = 0.;
                }
            }

            if (wd->I_errSolvDens) {
                wd->I_errSolvDens[i] = 0;
            }
        }

        int nprot = wr->nindA_prot;

        if (wr->inputParams->bCalcForces and wr->inputParams->bUseGPU == FALSE) {  /** do not clear, if dkI is calculated on GPU! */
            for (int i = 0; i < nIvalues * nprot; i++) {
                clear_dvec(wd->dkI[i]);
            }
        }

        /** Compute D(q) for home-qvalues */
        swaxs_debug("Compute D(q) for home-qvalues");

        int qhomenr = qvecs->qhomenr;

        for (int i = 0; i < qhomenr; i++) {
            if (!wr->inputParams->bVacuum) {
                /** Equivalent to equation below */
                t_complex_d this_avA;

                if (wr->inputParams->bScaleI0) {
                    this_avA = cadd_d(wd->avA[i], wd->avAcorr[i]);
                } else {
                    this_avA = wd->avA[i];
                }

                wd->D[i] = cabs2_d(csub_d(this_avA, wd->avB[i])) + (wd->avAsq[i] - cabs2_d(wd->avA[i]))
                        - Bprefact * (wd->avBsq[i] - cabs2_d(wd->avB[i]));
            } else {
                wd->D[i] = wd->avAsq[i];
            }
        }

        if (wr->inputParams->stepCalcNindep > 0
                and ((wr->waxsStep + 1) % wr->inputParams->stepCalcNindep == 0 or wr->waxsStep == 0 or wr->waxsStep == 5 or wr->waxsStep == 10 or wr->waxsStep == 20)) {
            waxsEstimateNumberIndepPoints(wr, cr, FALSE, FALSE);
        }

        int* ind = qvecs->ind;
        int nabs = qvecs->nabs;

        const int qstart = qvecs->qstart;
        // const int qend = qstart + qhomenr;

        /**
         * - spherical averging of D(\vec{q})
         * - Here we have one intensity per absolute q-value: I[0..nabs)
         */

        /** Loop over absolute values of q, |q| */
        for (int i = 0; i < nabs; i++) {
            /** Loop over q-vectors with this absolute value of q */
            for (int j = ind[i]; j < ind[i + 1]; j++) {
                /** is this q-vector on this node? */
                if (j >= qstart and j < (qstart + qhomenr)) {
                    int jj = j - qstart;
                    wd->I[i] += wd->D[jj];

                    /**
                     * Also get intensity from only atoms A or B and the third term, that is
                     *   <|A|^2>, <|B|^2>, and -2Re[ <B>.<A-B>*]
                     */
                    int stepA = wr->waxsStep + 1;
                    wd->IA[i] += wd->avAsq[jj];
                    wd->varIA[i] += (wd->avA4[jj] - gmx::square(wd->avAsq[jj])) / stepA;

                    /** Also get < A(|q|)> := int_dOmega[q] < A(q) > */
                    wd->avAqabs_re[i] += wd->avA[jj].re;
                    wd->avAqabs_im[i] += wd->avA[jj].im;

                    if (wd->I_errSolvDens) {
                        /** Uncertainty in I(q) due to uncertainty in solvent density */
                        wd->I_errSolvDens[i] += wd->DerrSolvDens[jj];
                    }

                    if (!wr->inputParams->bVacuum) {
                        int stepB = (wr->waxsStep + 1 < wr->inputParams->nfrsolvent) ? wr->waxsStep + 1 : wr->inputParams->nfrsolvent;

                        wd->IB[i] += wd->avBsq[jj];
                        wd->varIB[i] += (wd->avB4[jj] - gmx::square(wd->avBsq[jj])) / stepB;

                        t_complex_d tmpc = csub_d(wd->avA[jj], wd->avB[jj]);
                        tmpc = conjugate_d(tmpc);
                        tmpc = cmul_d(wd->avB[jj], tmpc);

                        wd->Ibulk[i] -= 2 * tmpc.re;

                        /** Also get < B(|q|)> := int_dOmega[q] < B(q) > */
                        wd->avBqabs_re[i] += wd->avB[jj].re;
                        wd->avBqabs_im[i] += wd->avB[jj].im;

                        wd->varIbulk[i] +=
                                  gmx::square( 2 * wd->avB[jj].re                     ) * (wd->av_ReA_2[jj] - gmx::square(wd->avA[jj].re)) / stepA
                                + gmx::square( 2 * wd->avB[jj].im                     ) * (wd->av_ImA_2[jj] - gmx::square(wd->avA[jj].im)) / stepA
                                + gmx::square(-2 * wd->avA[jj].re + 4 * wd->avB[jj].re) * (wd->av_ReB_2[jj] - gmx::square(wd->avB[jj].re)) / stepB
                                + gmx::square(-2 * wd->avA[jj].im + 4 * wd->avB[jj].im) * (wd->av_ImB_2[jj] - gmx::square(wd->avB[jj].im)) / stepB;

                        /**
                         * Finally, we comute the contributions using the alternative fomula by Park et al., that is
                         *   D(q) = |<A-B>|^2 + var(A) - var(B)
                         * These there terms are stored in variables AmB2, varA, varB
                         */
                        wd->I_avAmB2[i] += cabs2_d(csub_d(wd->avA[jj], wd->avB[jj]));

                        wd->I_varA[i] += wd->avAsq[jj] - cabs2_d(wd->avA[jj]);
                        wd->I_varB[i] += wd->avBsq[jj] - cabs2_d(wd->avB[jj]);

                        /**
                         * And the respective variances:
                         * Note: |<A-B>|^2 = <Re A>^2 + <Im A>^2 + <Re B>^2 - 2*<Re B>.<Re A> (using <Im B> = 0)
                         */
                        wd->varI_avAmB2[i] +=
                                  gmx::square(2 * wd->avA[jj].re - 2 * wd->avB[jj].re ) * (wd->av_ReA_2[jj] - gmx::square(wd->avA[jj].re)) / stepA
                                + gmx::square(2 * wd->avA[jj].im                      ) * (wd->av_ImA_2[jj] - gmx::square(wd->avA[jj].im)) / stepA
                                + gmx::square(2 * wd->avB[jj].re - 2 * wd->avA[jj].re ) * (wd->av_ReB_2[jj] - gmx::square(wd->avB[jj].re)) / stepB;

                        /**
                         * The standard error of the variance estimate is sigma^2 * sqrt(2 / k),
                         * where k is the number of data points used to compute the variance.
                         * Will devide by # of waxs steps (k) when writing the contrib file.
                         */
                        wd->varI_varA[i] += 2 * gmx::square(wd->av_ReA_2[jj] - gmx::square(wd->avA[jj].re)) / stepA
                                + 2 * gmx::square(wd->av_ImA_2[jj] - gmx::square(wd->avA[jj].im)) / stepA;
                        wd->varI_varB[i] += 2 * gmx::square(wd->av_ReB_2[jj] - gmx::square(wd->avB[jj].re)) / stepB;

                        if (wr->inputParams->bScaleI0) {
                            /**
                             * Additional intensity due to scaling of I(q=0)
                             *   Iscale_I0 := |<A+dA-B>|^2 - |<A-B>|^2
                             */
                            t_complex_d tmpc = csub_d(wd->avA[jj], wd->avB[jj]);
                            tmpc = cmul_d(conjugate_d(tmpc), wd->avAcorr[jj]);
                            wd->I_scaleI0[i] = 2 * tmpc.re + cabs2_d(wd->avAcorr[jj]);
                        }
                    }
                }
            }
        }

        /** Sum over the nodes and normalize */
        if (PAR(cr)) {
            gmx_sumd(nIvalues, wd->I,          cr);
            gmx_sumd(nIvalues, wd->IA,         cr);
            gmx_sumd(nIvalues, wd->varIA,      cr);
            gmx_sumd(nIvalues, wd->avAqabs_re, cr);
            gmx_sumd(nIvalues, wd->avAqabs_im, cr);

            if (!wr->inputParams->bVacuum) {
                gmx_sumd(nIvalues, wd->IB,          cr);
                gmx_sumd(nIvalues, wd->Ibulk,       cr);
                gmx_sumd(nIvalues, wd->varIB,       cr);
                gmx_sumd(nIvalues, wd->varIbulk,    cr);
                gmx_sumd(nIvalues, wd->avBqabs_re,  cr);
                gmx_sumd(nIvalues, wd->avBqabs_im,  cr);
                gmx_sumd(nIvalues, wd->I_avAmB2,    cr);
                gmx_sumd(nIvalues, wd->I_varA,      cr);
                gmx_sumd(nIvalues, wd->I_varB,      cr);
                gmx_sumd(nIvalues, wd->varI_avAmB2, cr);
                gmx_sumd(nIvalues, wd->varI_varA,   cr);
                gmx_sumd(nIvalues, wd->varI_varB,   cr);
            }

            if (wd->I_errSolvDens) {
                gmx_sumd(nIvalues, wd->I_errSolvDens, cr);
            }
        }

        /** In case with averaged over all q orientiations: divide by nr of q vectors per |q| */
        for (int i = 0; i < nabs; i++) {
            int N = ind[i + 1] - ind[i];

            wd->I         [i] /= N;
            wd->IA        [i] /= N;
            wd->varIA     [i] /= N;
            wd->avAqabs_re[i] /= N;
            wd->avAqabs_im[i] /= N;

            /** Get final variance of I based on error propagation */
            if (wr->inputParams->bVacuum) {
                wd->varI[i] = fabs(wd->varIA[i]);
            }

            if (!wr->inputParams->bVacuum) {
                wd->IB         [i] /= N;
                wd->Ibulk      [i] /= N;
                wd->varIB      [i] /= N;
                wd->varIbulk   [i] /= N;
                // wd->varI[i] += wd->varIB[i] + wd->varIbulk[i];
                wd->avBqabs_re [i] /= N;
                wd->avBqabs_im [i] /= N;
                wd->I_avAmB2   [i] /= N;
                wd->I_varA     [i] /= N;
                wd->I_varB     [i] /= N;
                wd->varI_avAmB2[i] /= N;
                wd->varI_varA  [i] /= N;
                wd->varI_varB  [i] /= N;

                wd->varI[i] = fabs(wd->varI_avAmB2[i] + wd->varI_varA[i] + wd->varI_varB[i]);
            }

            if (wr->waxsStep <= 1) {
                /**
                 * In the first step, we have no reasonable estimate for varI - just use 50% of I(q) for stddev
                 * Otherwise, we get a numercal error in the Bayesian WAXS-MD .
                 */
                if (MASTER(cr) and i == 0) {
                    printf("WAXS step %d: Using 50%% of I(q) as stddev of I(q)\n", wr->waxsStep);
                }

                wd->varI[i] = gmx::square(0.5 * wd->I[i]);
            }

            if (wd->I_errSolvDens) {
                wd->I_errSolvDens[i] /= N;
            }

            if (wr->bHaveNindep) {
                swaxs_debug("Divide by # of independent q-vectors if we have it already");

                /** Divide by # of independent q-vectors if we have it already */
                wd->varI[i] /= wd->Nindep[i];
            }
        }

        if (wr->inputParams->bCorrectBuffer) {
            if (t > 0) {
                gmx_fatal(FARGS, "In calculate_I_dkI(): correcting for oversubtracted buffer only "
                        "implemented for *one* scattering group, which must be X-ray");
            }

            /** Correcting for oversubtracted buffer, eq. 2 in Koefinger & Hummer, Phys Rev E 87 052712 (2013) */
            for (int i = 0; i < nabs; i++) {
                wd->I[i] += wr->soluteVolAv * wt->Ipuresolv[i];
            }
        }

        if (WAXS_ENSEMBLE(wr)) {
            if (MASTER(cr)) {
                /**
                 * Compute ensemble-averaged Icalc and stddev of Icalc.
                 * Note: weights are at present normalized - but let's be sure and normalize again.
                 */
                double sum = 0.;

                for (int m = 0; m < wr->inputParams->ensemble_nstates; m++) {
                    sum += wr->ensemble_weights[m];
                }

                for (int i = 0; i < nabs; i++) {
                    wt->ensembleSum_I   [i] = 0;
                    wt->ensembleSum_Ivar[i] = 0;

                    /** Sum over fixed states */
                    for (int m = 0; m < wr->inputParams->ensemble_nstates - 1; m++) {
                        wt->ensembleSum_I[i] += wr->ensemble_weights[m] * wt->ensemble_I[m][i];
                        wt->ensembleSum_Ivar[i] += wr->ensemble_weights[m] * wt->ensemble_Ivar[m][i];
                    }

                    /** Add the state of the MD simulation */
                    int m = wr->inputParams->ensemble_nstates - 1;
                    wt->ensembleSum_I   [i] += wr->ensemble_weights[m] * wd->I   [i];
                    wt->ensembleSum_Ivar[i] += wr->ensemble_weights[m] * wd->varI[i];

                    /** Normalize */
                    wt->ensembleSum_I   [i] /= sum;
                    wt->ensembleSum_Ivar[i] /= sum;
                }

            }

            if (PAR(cr)) {
                gmx_bcast(nabs * sizeof(double), wt->ensembleSum_I, cr->mpi_comm_mygroup);
                gmx_bcast(nabs * sizeof(double), wt->ensembleSum_Ivar, cr->mpi_comm_mygroup);
            }
        }

        /** Compute I(q) and gradients of I(q)  atomic coordinates if not already done on GPU */
        swaxs_debug("Compute I(q) and gradients of I(q) atomic coordinates if not already done on GPU");

        if (wr->inputParams->bCalcForces) {
            swaxs_debug("wr->bCalcForces is true");

            if (wr->inputParams->bUseGPU) {
                swaxs_debug("wr->bCalcForces with GPU");

                #if GMX_GPU_CUDA
                    double GPU_time;

                    calculate_dkI_GPU(t, wd->dkI, wr->atomEnvelope_coord_A, wt->atomEnvelope_scatType_A, qstart, qhomenr, nabs, nprot, &GPU_time, cr);

                    wr->timing->add(wr->waxsStep, swaxs::Timing::Stage::dkI, GPU_time / 1000, *cr);
                #endif
            } else {
                swaxs_debug("wr->bCalcForces without gpu");

                wr->timing->start(wr->waxsStep, swaxs::Timing::Stage::dkI, *cr);

                for (int i = 0; i < nabs; i++) {
                    int J = (ind[i + 1] - ind[i]);

                    /** Calculate grad[k] D(vec{q}) and sum immediately into grad[k] I(q) */
                    for (int p = 0; p < nprot; p++) {
                        int l = p * nabs + i;  /** index of dkI */

                        /** Now calculate dkI, the J denotes the number of q-vectors for this absolute value of q */
                        for (int d = 0; d < DIM; d++) {
                            wd->dkI[l][d] = 2. * wd->Orientational_Av[l][d] / J;
                        }
                    }
                }

                swaxs_debug("dKi done");

                /** Globally sum rvec array and divide by nr of qvecs on q-sphere */
                if (PAR(cr)) {
                    gmx_sumd(nabs * nprot * DIM, wd->dkI[0], cr);
                    swaxs_debug("after gmx_sumd");
                }

                wr->timing->end(wr->waxsStep, swaxs::Timing::Stage::dkI, *cr);
            }

            swaxs_debug("end if bCalcForces");
        }

        #ifdef PRINT_A_dkI
            if (wr->inputParams->bCalcForces and wr->inputParams->debugLvl > 1) {
                char fn[STRLEN];
                sprintf(fn, "fn_dkI_%s_%s.txt", wr->inputParams->bUseGPU ? "GPU" : "CPU", wt->type == escatterXRAY ? "XRAY" : "Neutron");

                FILE* fdkI;
                fdkI = fopen(fn, "w");
                fprintf(fdkI, "nprot is: %d\n", nprot);
                fprintf(fdkI, "nabs  is: %d\n", nabs);

                for (int i = 0; i < nabs; i++) {
                    fprintf(fdkI, "q_abs = %f \n", wt->qvecs->abs[i]);

                    for (int p = 0; p < nprot; p++) {
                        int l = i * nprot + p; /* index of dkI */

                        for (int d = 0; d < DIM; d++) {
                            fprintf(fdkI, "dkI [ %d ][%d] =  %f \n", l, d, wd->dkI[l][d]);
                        }

                        fprintf(fdkI, "\n");
                    }
                }

                fclose(fdkI);
                printf("Debugging level %d: Wrote dkI to %s\n", wr->inputParams->debugLvl, fn);
            }

            /** Test code for comparing CPU- vs. GPU-computed scattering amplitudes */
            if (qstart > 0 and wr->inputParams->debugLvl > 1) {
                char fn_p[STRLEN], fn_b[STRLEN];
                sprintf(fn_p, "scattering_amplitude_%s_%s.txt", wr->inputParams->bUseGPU ? "GPU" : "CPU",
                        wt->type == escatterXRAY ? "XRAY" : "Neutron");
                sprintf(fn_b, "water_scattering_amplitude_%s_%s.txt", wr->inputParams->bUseGPU ? "GPU" : "CPU",
                        wt->type == escatterXRAY ? "XRAY" : "Neutron");
                FILE* fp = fopen(fn_p, "w");
                FILE* fb = fopen(fn_b, "w");

                fprintf(fp, "Atom-number HOST start is: %d\n", wr->isizeA);
                fprintf(fp, "qhomenr HOST after loop is: %d\n", qhomenr);
                fprintf(fb, "Atom-number HOST start is: %d\n", wr->isizeB);
                fprintf(fb, "qhomenr HOST after loop is %d \n", qhomenr);

                for (int i = 0; i < qhomenr; i++) {
                    fprintf(fp, "Real-part of scattering amplitude [ %d ] :  %f \n", i, wd->A[i].re);
                    fprintf(fp, "Im  -part of scattering amplitude [ %d ] :  %f \n", i, wd->A[i].im);
                    fprintf(fp, " \n");

                    fprintf(fb, "Real-part of scattering amplitude [ %d ] :  %f \n", i, wd->B[i].re);
                    fprintf(fb, "Im  -part of scattering amplitude [ %d ] :  %f \n", i, wd->B[i].im);
                    fprintf(fb, " \n");
                }

                fclose(fp);
                fclose(fb);
                printf("Debugging Level %d: Wrote some scattering amplitudes to %s and %s\n", wr->inputParams->debugLvl, fn_p, fn_b);
            }
        #endif
    }

    swaxs_debug("Leaving calculate_I_dkI()");
}


/** Compute total force and torque on solute atoms and write to log file */
static void write_total_force_torque(t_waxsrec* wr, gmx::ArrayRef<gmx::RVec> x) {
    rvec fsum, torque;
    clear_rvec(fsum);
    clear_rvec(torque);

    int nprot = wr->nindA_prot;
    real faver = 0.;

    for (int p = 0; p < nprot; p++) {
        faver += sqrt(norm2(wr->fLast[p]));
        rvec_inc(fsum, wr->fLast[p]);

        int ix = wr->indA_prot[p];
        rvec c;
        cprod(x[ix], wr->fLast[p], c);
        rvec_inc(torque, c);
    }

    faver /= nprot;

    fprintf(wr->swaxsOutput->fpLog, "Average absolute WAXS force on atoms [kJ/(mol nm)] = %8.4g\n", faver);
    fprintf(wr->swaxsOutput->fpLog, "Sum of WAXS forces [kJ/(mol nm)] = ( %10.5g  %10.6g  %10.5g )\n",
            fsum[XX], fsum[YY], fsum[ZZ]);
    fprintf(wr->swaxsOutput->fpLog, "Total torque [kJ/(mol nm) nm]    = ( %10.5g  %10.6g  %10.5g )\n",
            torque[XX], torque[YY], torque[ZZ]);
}

/** Return difference between calculated and experimental intensity on log or linear scale */
static inline double waxs_intensity_Idiff(t_waxsrec* wr, t_waxsrecType* wt, double* Icalc, int iq,
        gmx_bool bAllowNegativeIcalc, gmx_bool bVerbose) {
    /** Works with or without fitting. Without fitting, f=1 and c=0, with fitting only the scale, c=0 */
    double IexpFitted = wt->f_ml * wt->Iexp[iq] + wt->c_ml;
    double Idiff = 0;

    switch (wr->inputParams->potentialType) {
        case ewaxsPotentialLOG:
            if (IexpFitted <= 0 and bAllowNegativeIcalc == FALSE) {
                gmx_fatal(FARGS, "After fitting the experimental curve I(fitted) = f*I(exp)+c to the calculated curve,\n"
                        "we found negative fitted I(q) (%g) at q = %g. This will not work with a WAXS potential on the "
                        "log scale.\n"
                        "Use either a linear scale, try a lower q-range, or fit only the scale f but not the offset c. "
                        "(f = %g  c = %g)", IexpFitted, wt->qvecs->abs[iq], wt->f_ml, wt->c_ml);
            }

            if (Icalc[iq] <= 0 and bAllowNegativeIcalc == FALSE) {
                gmx_fatal(FARGS, "Found a negative computed intensity at q = %g (iq = %d): Icalc = %g.\n"
                        "This will not work when coupling on a log scale. Choose a linear coupling scale instead.\n",
                        wt->qvecs->abs[iq], iq, Icalc[iq]);
            }

            if ((Icalc[iq] <= 0 or IexpFitted <= 0) and bAllowNegativeIcalc) {
                if (bVerbose) {
                    printf("\nNOTE!!!\nFound a negative Icalc = %g or negative fitted Iexp = %g at q = %g (iq = %d), "
                           "while coupling on the log scale.\n"
                           "This is ok at the beginning of the simulation (t < waxs_tau), when Icalc might not yet be "
                           "converged.\n\n", Icalc[iq], IexpFitted, wt->qvecs->abs[iq], iq);
                }

                Idiff = 0;
            } else {
                Idiff = log(Icalc[iq] / IexpFitted);
            }

            break;
        case ewaxsPotentialLINEAR: Idiff = Icalc[iq] - IexpFitted; break;
        default:
            gmx_fatal(FARGS, "Unknown type of WAXS potential (type %d, should be < %d)\n", wr->inputParams->potentialType, ewaxsPotentialNR);
    }

    if (std::isnan(Idiff) or std::isinf(Idiff)) {
        gmx_fatal(FARGS, "Error in waxs_intensity_Idiff(), with potential type = %s :\n"
                "Found for iq = %d: I = %g -- Idiff = %g (NaN/inf) -- IexpFitted = %g -- f_ml = %g -- c_ml = %g\n",
                EWAXSPOTENTIAL(wr->inputParams->potentialType), iq, wt->wd->I[iq], Idiff, IexpFitted, wt->f_ml, wt->c_ml);
    }

    return Idiff;
}

/**
 * Return sigma that goes into the WAXS potential, depending on scale (log or linear) and weights type
 * This function is only used for non-bayesian MD.
 */
static inline double waxs_intensity_sigma(t_waxsrec* wr, t_waxsrecType* wt, double* varIcalc, int iq) {
    double sigma = 0;

    /** Choose the variance that goes into Ewaxs */
    switch (wr->inputParams->weightsType) {
        case ewaxsWeightsUNIFORM: sigma = 1; break;
        case ewaxsWeightsEXPERIMENT: sigma = wt->f_ml * wt->Iexp_sigma[iq]; break;
        case ewaxsWeightsEXP_plus_CALC: sigma = sqrt(gmx::square(wt->f_ml * wt->Iexp_sigma[iq]) + varIcalc[iq]); break;
        case ewaxsWeightsEXP_plus_SOLVDENS:
            sigma = sqrt(gmx::square(wt->f_ml * wt->Iexp_sigma[iq]) + gmx::square(wr->epsilon_buff * wt->wd->I_errSolvDens[iq]));
            break;
        case ewaxsWeightsEXP_plus_CALC_plus_SOLVDENS:
            sigma = sqrt(gmx::square(wt->f_ml * wt->Iexp_sigma[iq]) + varIcalc[iq] + gmx::square(wr->epsilon_buff * wt->wd->I_errSolvDens[iq]));
            break;
        default:
            gmx_fatal(FARGS, "Unknown type of WAXS weights type (type %d, should be < %d)\n", wr->inputParams->weightsType, ewaxsWeightsNR);
    }

    if (wr->inputParams->potentialType == ewaxsPotentialLOG and wr->inputParams->weightsType != ewaxsWeightsUNIFORM) {
        /* With a potential on a log scale, the uncertainty is delta[log I] = (delta I)/I */
        sigma /= wt->Iexp[iq];
    }

    return sigma;
}

/**
 * Maxiumum-likelihodd estimate for f and c, for fitting f * Iexp + c to Icalc.
 * Eqs. 13 and 14 in Shevchuk & Hub, Plos Comp Biol https://doi.org/10.1371/journal.pcbi.1005800
 */
static void maximum_likelihood_est_fc(double* Icalc, double* Iexp, double* tau, int nq, double* f_ml, double* c_ml,
        double* sumSquaredResiduals) {
    double av_c, av_e, sig_c, sig_e;

    /** These functions also work with tau == nullptr */
    average_stddev_d(Icalc, nq, &av_c, &sig_c, tau);
    average_stddev_d(Iexp, nq, &av_e, &sig_e, tau);
    double P = pearson_d(nq, Icalc, Iexp, av_c, av_e, sig_c, sig_e, tau);

    /** Maximum likelihood estimates for f and c */
    *f_ml = P * sig_c / sig_e;
    *c_ml = av_c - (*f_ml) * av_e;

    if (sumSquaredResiduals) {
        double sum_tau = tau ? sum_array_d(nq, tau) : nq;
        /** This sumSquaredResiduals calculation is accurate up to at least 10 digits, checked. */
        *sumSquaredResiduals = sum_tau * (sig_c * sig_c * (1.0 - P * P));
    }
}

/**
 * ML estimate for f only (but not c), for fitting f*Iexp to Icalc.
 * Eq. above Eq. 23 in Shevchuk & Hub, Plos Comp Biol https://doi.org/10.1371/journal.pcbi.1005800
 */
static void maximum_likelihood_est_f(double* Icalc, double* Iexp, double* tau, int nq, double* f_ml, double* sumSquaredResiduals) {
    double avY2, avXY, avX2, sum_tau;

    /** These functions also work with tau == nullptr */
    average_x2_d(Iexp, nq, &avY2, tau);
    average_xy_d(Iexp, Icalc, nq, &avXY, tau);

    /**
     * Maximum likelihood estimates for scale
     * Note that we here fit the experiment to the calculated curve. Then we have:
     */
    *f_ml = avXY / avY2;

    if (sumSquaredResiduals) {
        average_x2_d(Icalc, nq, &avX2, tau);
        sum_tau = tau ? sum_array_d(nq, tau) : nq;
        *sumSquaredResiduals = sum_tau * (avX2 - (*f_ml) * avXY);
    }
}

static void maximum_likelihood_est_fc_generic(t_waxsrec* wr, double* Icalc, double* Iexp, double* tau, int nq,
        double* f_ml, double* c_ml, double* sumSquaredResiduals) {
    switch (wr->inputParams->ewaxs_Iexp_fit) {
        case ewaxsIexpFit_NO:
            if (sumSquaredResiduals) {
                sum_squared_residual_d(Icalc, Iexp, nq, sumSquaredResiduals, tau);
            }

            *f_ml = 1;
            *c_ml = 0;

            break;
        case ewaxsIexpFit_SCALE:
            maximum_likelihood_est_f(Icalc, Iexp, tau, nq, f_ml, sumSquaredResiduals);
            *c_ml = 0;
            break;
        case ewaxsIexpFit_SCALE_AND_OFFSET:
            maximum_likelihood_est_fc(Icalc, Iexp, tau, nq, f_ml, c_ml, sumSquaredResiduals);
            break;
        default: gmx_fatal(FARGS, "Invalid value for wr->inputParams->ewaxs_Iexp_fit (%d)\n", wr->inputParams->ewaxs_Iexp_fit); break;
    }
}

/**
 * Computes the log Likelihood, after marginalizing the fitting parameters (f and c, only f, or none).
 * - tau-array must be allocated before entry.
 * - Icalc / IcalcVar is the overall calculated intensities, which may contain contributions
 *   from a heterogenous ensmble or not.
 */
static double bayesian_md_calc_logLikelihood(t_waxsrec* wr, int t, double* Icalc, double* IcalcVar, double* tau) {
    if (wr->inputParams->nTypes > 1) {
        gmx_fatal(FARGS, "Do now allow more than one scattering type with Bayeisan stuff\n");
    }

    if (!tau) {
        gmx_fatal(FARGS, "Found tau == nullptr in bayesian_md_calc_logLikelihood()\n");
    }

    if (t >= wr->inputParams->nTypes) {
        gmx_fatal(FARGS, "Invalid waxs type number (%d)\n", t);
    }

    t_waxsrecType* wt = &wr->wt[t];
    double zeta = wt->nShannon / wt->nq;

    for (int i = 0; i < wt->nq; i++) {
        /** Compute the statistical "precisions", or 1 / sigma^2 */
        tau[i] = 1. / gmx::square(waxs_intensity_sigma(wr, wt, IcalcVar, i));
    }

    /** Get \hat{chi}^2, that is the sum of squared residuals (weighted by tau) */
    double sumSquaredResiduals = 0;
    maximum_likelihood_est_fc_generic(wr, Icalc, wt->Iexp, tau, wt->nq, &wt->f_ml, &wt->c_ml, &sumSquaredResiduals);

    double logL = -0.5 * zeta * sumSquaredResiduals;

    if (std::isnan(logL) or std::isinf(logL)) {
        gmx_fatal(FARGS, "Encountered an invalid log-likilihood (logL = %g)\n", logL);
    }

    return logL;
}


/**
 * Do Gibbs sampling of the uncertainty epsilon_buff in the buffer density
 *
 *  epsilon_buff = 0  -> means no uncertainty due to the buffer density
 *  epsilon_buff = 1  -> means the uncertainty is the one given by the mdp option "waxs-solvdens-uncert"
 *  epsilon_buff = 2  -> uncertainty 2 times waxs-solvdens-uncert, etc.
 *
 * As prior for epsilon_buff, we use a Gaussian with width = 1
 */
#define WAXS_BAYESIAN_EPSBUFF_MAX 6  /** Maximum epsilon (or number of sigmas) sampled */
#define GIBBS_NST_OUTPUT 10          /** Write to waxs_gibbs.dat every XX steps */

static int bayesian_sample_buffer_uncertainty(t_waxsrec* wr, double simtime, double* Icalc, double* IcalcVar, int nMCmove) {
    if (wr->inputParams->nTypes > 1) {
        gmx_fatal(FARGS, "Do now allow more than one scattering type with Bayesian stuff\n");
    }

    int t = 0;
    t_waxsrecType* wt = &wr->wt[t];

    double* tau = nullptr;
    snew(tau, wt->nq);

    double logLprev = bayesian_md_calc_logLikelihood(wr, t, Icalc, IcalcVar, tau);
    double eps_prev = wr->epsilon_buff;

    FILE* fp = wr->swaxsOutput->fpGibbsSolvDensRelErr[t];
    fprintf(fp, "%8g", simtime);

    gmx::UniformRealDistribution<real> uniform_distribution;

    int nAccept = 0;

    for (int i = 0; i < nMCmove; i++) {
        /** Propose update of epsilon_buff */
        wr->epsilon_buff = uniform_distribution(*(wr->rng)) * WAXS_BAYESIAN_EPSBUFF_MAX;

        /** Compute new likelihood */
        double logL = bayesian_md_calc_logLikelihood(wr, t, Icalc, IcalcVar, tau);

        /** Prior for epsilon is a Gaussian of width 1: pi(eps) = exp(-eps^2/2) */
        double priorFact = exp(-0.5 * (gmx::square(wr->epsilon_buff) - gmx::square(eps_prev)));
        double pjointRel = exp(logL - logLprev) * priorFact;

        gmx_bool bAccept;

        /** Accept according to Metropolis criterium */
        if (pjointRel > 1) {
            bAccept = TRUE;
        } else {
            double r = uniform_distribution(*(wr->rng));
            bAccept = (r < pjointRel);
        }

        if (bAccept) {
            logLprev = logL;
            eps_prev = wr->epsilon_buff;
            nAccept++;
        } else {
            /** Put back the old value */
            wr->epsilon_buff = eps_prev;
            logL = logLprev;
        }

        if ((i % GIBBS_NST_OUTPUT) == 0) {
            /* Writing the current relative uncertainty of the solvent density (e.g. 0.01 measn 1% uncertainty) */
            fprintf(fp, " %g", wr->epsilon_buff * wr->inputParams->solventDensRelErr);
        }
    }

    fprintf(fp, "\n");

    sfree(tau);

    return nAccept;
}

/**
 * Ensemble-average intensities, averaged over M-1 fixed states plus the state of the MD simulation .
 * Weights don't have to be normalized - however, so far they are already normalized upon entry.
 */
static void intensity_ensemble_average(int nq, int M, double* w, double** I_fixed, double** Ivar_fixed, double* I_MD,
        double* Ivar_MD, double* Icalc, double* IcalcVar) {
    double wsum = 0;

    for (int m = 0; m < M; m++) {
        wsum += w[m];
    }

    /** Loop over q-vectors */
    for (int i = 0; i < nq; i++) {
        Icalc[i] = 0;
        IcalcVar[i] = 0;

        /** Loop over fixed states */
        for (int m = 0; m < M - 1; m++) {
            Icalc[i] += w[m] * I_fixed[m][i];
            IcalcVar[i] += gmx::square(w[m]) * Ivar_fixed[m][i];  /** Gaussian error propagation */
        }

        /** Add I / varI of MD simulation state */
        int m = M - 1;
        Icalc[i] += w[m] * I_MD[i];
        IcalcVar[i] += gmx::square(w[m]) * Ivar_MD[i];

        /** Normalize */
        Icalc[i] /= wsum;
        IcalcVar[i] /= gmx::square(wsum);
    }
}

#if 0
static int compare_double(const void* a, const void* b) {
    double diff = *(double*)a - *(double*)b;

    if (diff > 0) {
        return 1;
    } else if (diff < 0) {
        return -1;
    } else {
        return 0;
    }
}
#endif

#define GMX_WAXS_ENSWEIGHT_MOVE_MAX 0.1

/** Allow weights also < 0 or > 1, so to avoid artifacts at the 0/1 boundary in weights space. */
static int bayesian_sample_ensemble_weights(t_waxsrec* wr, double simtime, int nMCmove) {
    gmx::UniformRealDistribution<real> uniform_distribution;

    if (wr->inputParams->nTypes > 1) {
        gmx_fatal(FARGS, "Do now allow more than one scattering type with Bayeisan stuff\n");
    }

    int t = 0;
    t_waxsrecType* wt = &wr->wt[t];
    int M = wr->inputParams->ensemble_nstates;

    double* Icalc = nullptr;
    double* IcalcVar = nullptr;
    snew(Icalc, wt->nq);
    snew(IcalcVar, wt->nq);

    double* R = nullptr;
    snew(R, M + 1);

    /** Get placeholder for weights, and initiate it with the old weights */
    double* wNew;
    snew(wNew, M);
    memcpy(wNew, wr->ensemble_weights, M * sizeof(double));

    intensity_ensemble_average(wt->nq, M, wr->ensemble_weights, wt->ensemble_I, wt->ensemble_Ivar, wt->wd->I,
            wt->wd->varI, Icalc, IcalcVar);

    double* tau = nullptr;
    snew(tau, wt->nq);

    double logL = bayesian_md_calc_logLikelihood(wr, t, Icalc, IcalcVar, tau);

    /**
     * We need to draw random weights, such that the sum of the weigths is one
     *
     * To generate such N uniformly distributed random numbers with a sum of 1, one typically does the following:
     *  1) Draw N-1 random numbers R between 0 and 1 and put them in a list L.
     *  2) Also add 0 and 1 to the list.
     *  3) Sort the list.
     *  4) The N random numbers are given by L1-L0, L2-L1, ... L[N]-L[N-1]
     *
     *  Note: I tested this against the Python code given on the Wikipedia site, all fine.
     *
     *  So, in terms of a MC step, we can randomly move the N-1 random numbers R.
     *
     *  Addition: Due to the memory effect in our calculated SAXS curves, we get artifacts
     *            at the hard boundaries at 0 and 1 of the weights. Therefore, extend the
     *            flat Dirichlet prior such that weights slightly smalle 0 and larger 1
     *            are possible as well. This is achieved as follows:
     *
     *            1) Compute the weights \vec{w0} according to the Dirichelet distribution, see above.
     *            2) Multiply the weights vector \vec{w0} by (1+xi*N) and shift it back along the vector
     *               -xi-(1,....1), such that ||w0||_1 = 1, so the weights remain normalized:
     *
     *               \vec{wc} = (1+xi*N) * \vec{w0} - xi*(1,...,1)
     *
     *              Then, the new weight vector \vec{wc} is still normalized, but its elements are within
     *              the element:
     *
     *                  wc_i \in [-xi, 1 + N*xi - xi]
     *
     *              for example with N = 2, wc_i \in [-xi, 1 + xi]  (instead of [0,1])
     */

    R[0] = 0;
    R[M] = 1;

    int nAccept = 0;

    /** Allow state weights in interval [-xi, 1 + M*xi - xi] */
    double xi = wr->inputParams->stateWeightTolerance;

    for (int i = 0; i < nMCmove; i++) {
        /** Set back weights */
        memcpy(wNew, wr->ensemble_weights, M * sizeof(double));

        /**
         * Set the list of random numbers between 0 and 1
         *   R[1]      is the weight of state 0
         *   R[2]-R[1] is the weight of state 1, etc
         */

        for (int m = 0; m < M - 1; m++) {
            /** Weights wNew[m] can be <0 or >1 if wr->inputParams->stateWeightTolerance > 0, so map back to the [0,1] interval. */
            double wNonScaled = (wNew[m] + xi) / (1 + M * xi);
            R[m + 1] = R[m] + wNonScaled;

            if (R[m + 1] > 1.001 or R[m + 1] < -0.001) {
                gmx_fatal(FARGS, "Error while initiating R (should be 0<= R <= 1), found %g\n", R[m + 1]);
            }
        }

        /** Do random moves of the R */
        gmx_bool bOutsideBounds = FALSE;

        for (int m = 0; m < M - 1; m++) {
            R[m + 1] += (2 * uniform_distribution(*(wr->rng)) - 1) * GMX_WAXS_ENSWEIGHT_MOVE_MAX;

            if (R[m + 1] >= 1. or R[m + 1] < 0.) {
                bOutsideBounds = TRUE;
            }
        }

        if (bOutsideBounds) {
            /** Reject move when one of the R[] is outside [0,1] */
            continue;
        }

        /** Sort R - use qsort_threadsafe() defined in gmx_sort.h */

        // `gmx_qsort_threadsafe` have been removed in Gromacs 2019.
        // I am not sure if _threadsafe_ version have been used here on purpose or _just in case_.
        // Calculations give expected result with STL sort.

        // TODO: Verify that there is no need for more sophisticated approach, then remove this comment.
        // `qsort_threadsafe.cpp/h` can be found in Gromacs 2018 if needed.

        // gmx_qsort_threadsafe(R, M+1, sizeof(double), compare_double);
        std::sort(R, R + M + 1, std::less<double>());

        /** Pick new weights as differences between neighboring elements in sorted R */
        for (int m = 0; m < M; m++) {
            wNew[m] = R[m + 1] - R[m];
        }

        /**
         * Allow weights slightly larger 1 or smaller 0, so to avoid artifacts
         * at the boundary. This still yields a normalized weight vector (sum_i w_i = 1),
         * that is still uniformly distributed.
         */
        for (int m = 0; m < M; m++) {
            wNew[m] = (1 + M * xi) * wNew[m] - xi;
        }

        /** Compute new calculated intensity */
        intensity_ensemble_average(wt->nq, M, wNew, wt->ensemble_I, wt->ensemble_Ivar, wt->wd->I, wt->wd->varI, Icalc, IcalcVar);

        /** Compute new likelihood */
        double logLNew = bayesian_md_calc_logLikelihood(wr, t, Icalc, IcalcVar, tau);

        /** Relative probabilities = P[new]/P[old] */
        double pjointRel = exp(logLNew - logL);

        /** Add contribution to P[new]/P[old] from umbrella potential on weights */
        if (wr->inputParams->ensemble_weights_fc > 0) {
            double deltaVweights = 0;

            /** We sum over M-1 states only, since the M'th state is fixed by the other M-1 states anyway */

            for (int m = 0; m < M - 1; m++) {
                /** Change in umbrella potential fc / 2 * (w - w0)^2 on weights */
                deltaVweights += gmx::square(wNew[m] - wr->inputParams->ensemble_weights_init[m])
                        - gmx::square(wr->ensemble_weights[m] - wr->inputParams->ensemble_weights_init[m]);
            }

            deltaVweights *= 0.5 * wr->inputParams->ensemble_weights_fc;
            pjointRel *= exp(-deltaVweights / wr->inputParams->kT);
        }

        gmx_bool bAccept;

        /** Accept according to Metropolis criterium */
        if (pjointRel > 1) {
            bAccept = TRUE;
        } else {
            double r = uniform_distribution(*(wr->rng));
            bAccept = (r < pjointRel);
        }

        if (bAccept) {
            logL = logLNew;
            memcpy(wr->ensemble_weights, wNew, M * sizeof(double));
            nAccept++;
        }

        if ((i % GIBBS_NST_OUTPUT) == 0) {
            FILE* fp = wr->swaxsOutput->fpGibbsEnsW[t];

            fprintf(fp, "%g ", simtime);

            for (int m = 0; m < M; m++) {
                fprintf(fp, " %g", wr->ensemble_weights[m]);
            }

            fprintf(fp, "\n");
        }

        // printf("\tGibbs weights: %g %g | %g - %d | w =", logL, logLNew, pjointRel, bAccept);
        //
        // for (int m = 0; m < M; m++) {
        //     printf(" %g", wr->ensemble_weights[m]);
        // }
        //
        // printf("\n");
        // printf("\tTry was: ");
        //
        // for (int m = 0; m < M; m++) {
        //     printf(" %g", wNew[m]);
        // }
        //
        // printf("\n");
    }

    sfree(tau);
    sfree(Icalc);
    sfree(IcalcVar);
    sfree(wNew);
    sfree(R);

    return nAccept;
}

/** Specify number of Monte-Carlo Moves */
#define WAXS_GIBBS_N_MONTECARLO_MOVES 100  /** Buffer density only or ensemble only */
#define WAXS_GIBBS_BOTH_N_ROUNDS       20  /** Rounds of both density and ensemble */

static void bayesian_gibbs_sampling(t_waxsrec* wr, double simtime, const t_commrec* cr) {
    if (wr->inputParams->nTypes > 1) {
        gmx_fatal(FARGS, "Do now allow more than one scattering type for a Bayesian treatment of\n"
                "systematic errors and/or for refining heterogeneous ensembles.");
    }

    wr->timing->start(wr->waxsStep, swaxs::Timing::Stage::Gibbs, *cr);

    gmx_bool bDoEnsmbleW = (wr->inputParams->ewaxs_ensemble_type == ewaxsEnsemble_BayesianOneRefined);
    gmx_bool bDoSolvDens = wr->inputParams->bBayesianSolvDensUncert;

    int t = 0;
    t_waxsrecType* wt = &wr->wt[t];

    if (wt->wd->I_errSolvDens == nullptr and bDoSolvDens) {
        gmx_incons("bayesian_gibbs_sampling(): I_errSolvDens not set, but requesting bDoSolvDens");
    }

    if (bDoSolvDens and wr->inputParams->weightsType != ewaxsWeightsEXP_plus_CALC_plus_SOLVDENS) {
        gmx_fatal(FARGS, "Requested Bayesian sampling of solvent density uncertainty, but not using solvent\n"
                "density uncertiainty for potential and likelihood. Use waxs-weights = exp+calc+solvdens\n");
    }

    int nAcceptW, nAcceptEps;
    double relAcceptEps, relAcceptW;

    if (MASTER(cr)) {
        if (bDoSolvDens and !bDoEnsmbleW) {
            /** Gibbs sampling of the uncertainty of the buffer density */
            nAcceptEps = bayesian_sample_buffer_uncertainty(wr, simtime, wt->wd->I, wt->wd->varI, WAXS_GIBBS_N_MONTECARLO_MOVES);
            relAcceptEps = 1.0 * nAcceptEps / WAXS_GIBBS_N_MONTECARLO_MOVES;
            printf("Gibbs sampling, final bufEps = %12g - (acceptance rate %g %%)\n", wr->epsilon_buff, 100. * relAcceptEps);
        } else if (!bDoSolvDens and bDoEnsmbleW) {
            /** Gibbs sampling of only ensemble weights */
            nAcceptW = bayesian_sample_ensemble_weights(wr, simtime, WAXS_GIBBS_N_MONTECARLO_MOVES);
            relAcceptW = 1.0 * nAcceptW / WAXS_GIBBS_N_MONTECARLO_MOVES;
            printf("Gibbs sampling of ensemble weigths (acceptance rate %g %%). Final weights =", 100. * relAcceptW);

            for (int m = 0; m < wr->inputParams->ensemble_nstates; m++) {
                printf(" %10g", wr->ensemble_weights[m]);
            }

            printf("\n");
        } else if (bDoSolvDens and bDoEnsmbleW) {
            /** Doing several rounds of Gibbs sampling of ensemble weights and buffer density uncertainty */
            nAcceptEps = 0;
            nAcceptW = 0;

            for (int i = 0; i < WAXS_GIBBS_BOTH_N_ROUNDS; i++) {
                /** Doing a few rounds of Gibbs sampling of the buffer uncertainty */
                intensity_ensemble_average(wt->nq, wr->inputParams->ensemble_nstates, wr->ensemble_weights, wt->ensemble_I,
                        wt->ensemble_Ivar, wt->wd->I, wt->wd->varI, wt->ensembleSum_I, wt->ensembleSum_Ivar);

                nAcceptEps += bayesian_sample_buffer_uncertainty(wr, simtime, wt->ensembleSum_I, wt->ensembleSum_Ivar,
                        WAXS_GIBBS_BOTH_N_ROUNDS);

                /** Doing a few rounds of Gibbs sampling on the weights of the states of the ensemble */
                nAcceptW += bayesian_sample_ensemble_weights(wr, simtime, WAXS_GIBBS_BOTH_N_ROUNDS);
            }

            relAcceptEps = 1.0 * nAcceptEps / (WAXS_GIBBS_BOTH_N_ROUNDS * WAXS_GIBBS_BOTH_N_ROUNDS);
            relAcceptW = 1.0 * nAcceptW / (WAXS_GIBBS_BOTH_N_ROUNDS * WAXS_GIBBS_BOTH_N_ROUNDS);

            printf("Gibbs sampling. Accept. rates: bufEps: %6.2f %%   Weights: %6.2f %%\n",
                    100. * relAcceptEps, 100. * relAcceptW);
            printf("Final bufEps = %10g      Final weights =", wr->epsilon_buff);

            for (int m = 0; m < wr->inputParams->ensemble_nstates; m++) {
                printf(" %10g", wr->ensemble_weights[m]);
            }

            printf("\n");
        } else {
            gmx_incons("Inconsistency in bayesian_gibbs_sampling()");
        }

        if (bDoEnsmbleW) {
            /** Update the ensemble average with the new weights */
            intensity_ensemble_average(wt->nq, wr->inputParams->ensemble_nstates, wr->ensemble_weights, wt->ensemble_I,
                    wt->ensemble_Ivar, wt->wd->I, wt->wd->varI, wt->ensembleSum_I, wt->ensembleSum_Ivar);
        }
    }

    /** Broadcast final variables after Gibbs sampling */
    if (PAR(cr)) {
        if (bDoEnsmbleW) {
            gmx_bcast(wr->inputParams->ensemble_nstates * sizeof(double), wr->ensemble_weights, cr->mpi_comm_mygroup);
            gmx_bcast(wt->nq * sizeof(double), wt->ensembleSum_I, cr->mpi_comm_mygroup);
            gmx_bcast(wt->nq * sizeof(double), wt->ensembleSum_Ivar, cr->mpi_comm_mygroup);
        }

        if (bDoSolvDens) {
            gmx_bcast(sizeof(double), &wr->epsilon_buff, cr->mpi_comm_mygroup);
        }
    }

    wr->timing->end(wr->waxsStep, swaxs::Timing::Stage::Gibbs, *cr);
}

static void clear_vLast_fLast(t_waxsrec* wr) {
    /** Clear waxs potential */
    wr->vLast = 0;

    for (int t = 0; t < wr->inputParams->nTypes; t++) {
        wr->wt[t].vLast = 0;
    }

    int nprot = wr->nindA_prot;

    /** Clear waxs forces */
    if (wr->fLast) {
        for (int p = 0; p < nprot; p++) {
            clear_rvec(wr->fLast[p]);

            for (int t = 0; t < wr->inputParams->nTypes; t++) {
                clear_rvec(wr->wt[t].fLast[p]);
            }
        }
    }
}

/** Compute the SAXS/SANS-derived potentials and forces */
static void waxs_md_pot_forces(const t_commrec* cr, t_waxsrec* wr, real simtime, matrix* Rinv) {
    swaxs_debug("Entering waxs_md_pot_forces()");

    if (WAXS_ENSEMBLE(wr) and wr->inputParams->nTypes > 1) {
        gmx_fatal(FARGS, "Ensemble refinemnt so far only with one scattering type (found wr->inputParams->nTypes = %d)\n", wr->inputParams->nTypes);
    }

    if (!wr->inputParams->bCalcPot) {
        gmx_incons("wr->inputParams->bCalcPot false in waxs_md_pot_forces()");
    }

    for (int t = 0; t < wr->inputParams->nTypes; t++) {
        t_waxsrecType* wt = &wr->wt[t];
        waxs_datablock* wd = wt->wd;

        int nprot = wr->nindA_prot;

        /**
         * Depending on whether we do ensemble refinement, the calculated intensity
         * is the ensemble-averaged I(q) or the simply I(q) computed from the MD.
         * So let's define two pointer on the respective arrays.
         */

        double* Icalc;
        double* IcalcVar;

        if (WAXS_ENSEMBLE(wr)) {
            Icalc = wt->ensembleSum_I;
            IcalcVar = wt->ensembleSum_Ivar;
        } else {
            Icalc = wd->I;
            IcalcVar = wd->varI;
        }

        /** For cumulative (non-weighted) average of potentials vAver */
        double fac1 = 1.0 * (wr->waxsStep) / (wr->waxsStep + 1);
        double fac2 = 1.0 / (wr->waxsStep + 1);

        /** Smoothly switching on the force between tau < simtime < 2tau. This
            makes sure that we don't apply force while the I(q) are not yet converged */

        double this_simtime = simtime;

        real fForceSwitch;

        if (this_simtime > 2.0 * wr->inputParams->tau or !wr->inputParams->bSwitchOnForce) {
            fForceSwitch = 1.;
        } else if (this_simtime < wr->inputParams->tau) {
            fForceSwitch = 0.;
        } else {
            fForceSwitch = 0.5 * (1. - cos(M_PI * (this_simtime - wr->inputParams->tau) / (wr->inputParams->tau)));
        }

        if (MASTER(cr) and wr->swaxsOutput->fpLog and fForceSwitch < 1.0) {
            fprintf(wr->swaxsOutput->fpLog, "While switching on potential and force: using factor of %g\n", fForceSwitch);
        }

        /**
         * Get relative weights of current I(q) and target I(q), which enter the I(q) to which we couple.
         * Here we use the total simulation time, not the simulation time from the last restart.
         */
        real fact_target_switch;

        if (simtime > wr->inputParams->t_target or wr->inputParams->t_target <= 0.) {
            /** Only use input pattern */
            fact_target_switch = 1.;
        } else {
            /**
             * Linearly switch from current to target pattern.
             * Note:  I_couple = (1 - f)I_sim + f * I_target.
             *        -> V = k * (I_sim - I_couple)^2 = k * f^2 * (I_sim - I_target)^2
             *        -> So we only get an additional factor of f^2
             */
            fact_target_switch = simtime / wr->inputParams->t_target;
        }

        if (MASTER(cr) and wr->swaxsOutput->fpLog and fact_target_switch < 1.0) {
            fprintf(wr->swaxsOutput->fpLog, "Relative weight of target pattern vs. current pattern %g\n", fact_target_switch);
        }

        /**
         * Get maximum likelihood estimate for f (overall scale) and c (offset)
         *
         * At the beginning, we don't have any estimate for f yet, so we cannot compute the scaling factor entering
         * the experimental errors. Therefore, get a first good guess for f using uniform weights:
         */
        if (wt->f_ml == 0) {
            maximum_likelihood_est_fc_generic(wr, Icalc, wt->Iexp, nullptr, wt->nq, &wt->f_ml, &wt->c_ml, nullptr);
        }

        int nabs = wt->nq;

        double* tau = nullptr;
        snew(tau, nabs);

        /** Now we have a reasonable estimate for f, and we can compute the statistical weights (or "precisions")
            tau_i = 1 / sigma_i^2 */
        for (int i = 0; i < nabs; i++) {
            tau[i] = 1. / gmx::square(waxs_intensity_sigma(wr, wt, IcalcVar, i));
        }

        /** Get fitting parameters f/c, or purely f, or no fitting (dependig on waxs-Iexp-fit) */
        double sumSquaredResiduals = -1;
        maximum_likelihood_est_fc_generic(wr, Icalc, wt->Iexp, tau, wt->nq, &wt->f_ml, &wt->c_ml, &sumSquaredResiduals);

        double nShannonFactorUsed = (wr->inputParams->bDoNotCorrectForceByNindep ? 1. : wt->nShannon);

        /**
         * Overall factor for pot and forces
         * Update in Dec. 2017. Multiply with Nindep, such that we follow a Bayesian logic if the force constant is fc == 1
         */
        real fact0 = fForceSwitch * wt->fc * wr->inputParams->kT * gmx::square(fact_target_switch) * nShannonFactorUsed / wt->nq;

        /**
         * At the beginning of the simulation (t < waxs_tau), the Icalc may be negative simply because Icalc is not yet
         * converged. However, negative Icalc would lead to an error when coupling on a log scale. To avoid this error,
         * allow negative Icalc while t < waxs_tau, while fact0 is anyway still zero:
         */
        gmx_bool bAllowNegativeIcalc = (fForceSwitch == 0.);

        double* diffI = nullptr;
        double* sigma = nullptr;
        snew(diffI, nabs);
        snew(sigma, nabs);

        /** Get delta_I = I_calc - I_exp and sigmas */
        for (int i = 0; i < nabs; i++) {
            diffI[i] = waxs_intensity_Idiff(wr, wt, Icalc, i, bAllowNegativeIcalc, TRUE);
            sigma[i] = waxs_intensity_sigma(wr, wt, IcalcVar, i);
        }

        /** WAXS potential energy is only computed on the master and =0 on all slave nodes */
        if (MASTER(cr) and wr->inputParams->bCalcPot) {
            if (wr->swaxsOutput->fpPot[t]) {
                fprintf(wr->swaxsOutput->fpPot[t], "%8g", simtime);
            }

            /** Recomputing the residuals, to make sure they agree with the result from maximum_likelihood_est_fc_generic() */
            double sumSquaredResidualsRecomputed = 0;

            for (int i = 0; i < nabs; i++) {
                /**
                 * The SAXS/SANS potential, and cumulative averages
                 *
                 * The factor of 0.5 comes from the 1/2 in Eq. 17, Shevchuk & Hub, Plos Comp Biol 2017.
                 * This factor ensures that we follow the Bayesian refinemnt with a force constant of fc=1.
                 */
                real thisv = 0.5 * fact0 * gmx::square(diffI[i] / sigma[i]);

                wt->vLast += thisv;
                wd->vAver[i] = d_accum_avg(wd->vAver[i], (double)thisv, fac1, fac2);
                wd->vAver2[i] = d_accum_avg(wd->vAver2[i], gmx::square(thisv), fac1, fac2);

                sumSquaredResidualsRecomputed += gmx::square(diffI[i] / sigma[i]);

                if (wr->swaxsOutput->fpPot[t]) {
                    fprintf(wr->swaxsOutput->fpPot[t], "  %8g", thisv);
                }
            }

            if (wr->swaxsOutput->fpPot[t]) {
                fprintf(wr->swaxsOutput->fpPot[t], "\n");
                fflush(wr->swaxsOutput->fpPot[t]);
            }

            printf("Type %1d (%s): Fit params: f = %8g  c = %8g  mean residual = %8g  V [kJ/mol] = %8g\n", t,
                    wt->saxssansStr, wt->f_ml, wt->c_ml, sqrt(sumSquaredResiduals / wt->nq), wt->vLast);

            if (wr->inputParams->potentialType == ewaxsPotentialLINEAR
                    and fabs(sumSquaredResidualsRecomputed / nabs - sumSquaredResiduals / nabs) > 0.2 and wr->waxsStep > 30) {
                /** Residuals computed in two ways may slightly differ, because we update the f_ml * sigma[exp] after fitting f_ml */
                fprintf(stderr, "\nWARNING, the residuals computed in two ways are inconsistent. This should not happen:\n"
                        "Via max-likelihood calculation: %g  ---  directly: %g\n",
                        sqrt(sumSquaredResidualsRecomputed / nabs), sqrt(sumSquaredResiduals / nabs));
            }

            /** Add potential of this scattering type to total potential */
            wr->vLast += wt->vLast;
        }

        /** Each node computes all forces at the moment */
        if (wr->inputParams->bCalcForces) {
            /**
             * At negative contrast, moving an atom by +deltaR will shift the contrast by -deltaR,
             * since the empty space will be filled by water. This should approximately lead to
             * a reduction of the force relative to the contrast. For more details, see the comment
             * at the function updateSoluteContrast().
             */
            real contrastFactorUsed;

            if (wr->inputParams->bDoNotCorrectForceByContrast) {
                contrastFactorUsed = 1.;
            } else {
                contrastFactorUsed = wt->contrastFactor;
            }

            /** Should this be parallelized? - But then we need another communication. What is faster? */
            for (int p = 0; p < nprot; p++) {
                rvec ftmp;
                clear_rvec(ftmp);

                for (int i = 0; i < nabs; i++) {
                    /**
                     * We used to have a factor of 2 here - this was removed to follow the Bayesian formalism
                     * if the force constant is fc=1.
                     */
                    real fact1 = -fact0 * contrastFactorUsed * diffI[i] / gmx::square(sigma[i]);

                    if (wr->inputParams->potentialType == ewaxsPotentialLOG) {
                        fact1 /= Icalc[i];
                    }

                    int l = p * nabs + i;

                    for (int d = 0; d < DIM; d++) {
                        ftmp[d] += fact1 * wd->dkI[l][d];
                    }
                }

                if (wr->inputParams->bRotFit) {
                    rvec ftmp2;
                    copy_rvec(ftmp, ftmp2);
                    mvmul(*Rinv, ftmp2, ftmp);
                }

                /** Store current force of this scattering type, and add to the total force */
                copy_rvec(ftmp, wt->fLast[p]);
                rvec_inc(wr->fLast[p], ftmp);
            }
        }

        sfree(diffI);
        sfree(sigma);
        sfree(tau);
    }

    swaxs_debug("Leaving waxs_md_pot_forces()");
}

void do_waxs_md_low(const t_commrec* cr, gmx::ArrayRef<gmx::RVec> x, double simtime, int64_t step, t_waxsrec* wr,
        const gmx_mtop_t* mtop, const matrix box, PbcType pbcType, gmx_bool bDoLog) {
    #ifdef RERUN_CalcForces
        wr->inpatParams.bCalcForces = TRUE;
    #endif

    swaxs_debug("Entering do_waxs_md_low()");

    /** Doing this step? */
    gmx_bool bTimeOK = ((wr->inputParams->calcWAXS_begin == -1) or ((simtime + 1e-5) >= wr->inputParams->calcWAXS_begin))
            and ((wr->inputParams->calcWAXS_end == -1) or ((simtime - 1e-5) <= wr->inputParams->calcWAXS_end));

    if (PAR(cr)) {
        gmx_bcast(sizeof(gmx_bool), &bTimeOK, cr->mpi_comm_mygroup);
    }

    if (!bTimeOK) {
        if (MASTER(cr)) {
            fprintf(wr->swaxsOutput->fpLog, "Skipping time %f - outside of time boundaries: begin %g, end %g\n",
                    simtime, wr->inputParams->calcWAXS_begin, wr->inputParams->calcWAXS_end);
            printf("Skipping time %f - outside of time boundaries: begin %g, end %g\n",
                    simtime, wr->inputParams->calcWAXS_begin, wr->inputParams->calcWAXS_end);
        }

        clear_vLast_fLast(wr);

        return;
    }

    wr->timing->start(wr->waxsStep, swaxs::Timing::Stage::Step, *cr);

    /** Doing solvent in this frame? */
    wr->bDoingSolvent = (!wr->inputParams->bVacuum and wr->waxsStep < wr->inputParams->nfrsolvent);

    if (!wr->inputParams->bVacuum and wr->waxsStep == wr->inputParams->nfrsolvent and MASTER(cr)) {
        printf("\nNote: Reached the requested nr of pure solvent frames (%d)."
               "\n      Will not compute any more pure solvent scattering amplitudes.\n\n", wr->inputParams->nfrsolvent);
    }

    /* = = = = = MASTER does all the preparations before bcast. = = = = = */

    real nelecA = 0.;
    real nelecB = 0.;
    matrix Rinv;
    double time_ms;

    if (MASTER(cr)) {
        wr->timing->start(wr->waxsStep, swaxs::Timing::Stage::PrepareSolute, *cr);

        printf("\nSWAXS/SANS step %d at MD step ", wr->waxsStep);
        printf("%" PRId64, step);
        printf(" (time %6g ps)\n", simtime);
        fprintf(wr->swaxsOutput->fpLog, "\nSWAXS/SANS step %d at MD step ", wr->waxsStep);
        fprintf(wr->swaxsOutput->fpLog, "%" PRId64, step);
        fprintf(wr->swaxsOutput->fpLog, " (time %g ps)\n", simtime);

        /**
         * Do the required steps with the solute before getting the solvation shell:
         *  1. Make solute/protein whole.
         *  2. Get bounding sphere around solute
         *  3. Move this sphere to the center of the box - this maximises the solvent coverage within the unit cell.
         *  3. Put all solvent atoms into the compact box.
         *  4. Obtain the vector between box_center and solute COG. This will become the newBoxCent a the end.
         *  5. Shift fit-group COG to origin.
         *  6. If requested, do a rotation of the solute (using the fitgroup atoms such as Backbone),
         *     and return the inverse rotation matrix.
         *  7. After rotation, move the solute COG to the origin.
         */
        waxs_prepareSoluteFrame(wr, mtop, x, box, pbcType, wr->swaxsOutput->fpLog, Rinv);

        wr->timing->end(wr->waxsStep, swaxs::Timing::Stage::PrepareSolute, *cr);

        /** Pick the pure water box and shift coordinates it to the center of the protein box */

        if (wr->bDoingSolvent) {
            wr->timing->start(wr->waxsStep, swaxs::Timing::Stage::PrepareSolvent, *cr);

            /** Shift pure-solvent frame (frame nr iFrameUsedThisStep) onto envelope and store in 'ws->xPrepared' */
            wr->solvent->prepareNextFrame(wr->wt[0].envelope, wr->inputParams->verbosityLevel);

            wr->timing->end(wr->waxsStep, swaxs::Timing::Stage::PrepareSolvent, *cr);
        }

        /* = = = = = All atoms in place for buffer subtraction = = = = = */

        /* = = = = = Obtain the atoms inside the envelope = = = = =
           Search solvent molecules in solvation layer and in the excluded volume and
           store in index_A (protein+solvation layer) and index_B (excluded solvent),
           respectively. */
        if (!wr->inputParams->bVacuum) {
            auto xExclSolventRef = gmx::arrayRefFromArray(reinterpret_cast<gmx::RVec*>(wr->solvent->xPrepared),
                    wr->solvent->topology->natoms);

            // The new way of getting the solvation shell. Included because some routines already use it.
            // Do not write PDB files (prot+solvationlayer.pdb, ...) since this is still done by the
            // old get_solvation_shell
            //
            // wr->waxs_solv->box[wr->waxsStep%wr->waxs_solv->nframes] picks the box of this solvent frame. Ugly, improve later.
            get_solvation_shell_periodic(x, xExclSolventRef, wr, mtop, wr->solvent->topology.get(), pbcType, box,
                    wr->solvent->boxPrepared, FALSE);

            // The old way of getting solvent inside the envelope. This will be fully replaced by
            // get_solvation_shell_periodic()
            get_solvation_shell(x, xExclSolventRef, wr, mtop, wr->solvent->topology.get(), pbcType, box);

            fprintf(wr->swaxsOutput->fpLog, "Atoms in solute          %5d\n", wr->nindA_prot);
            fprintf(wr->swaxsOutput->fpLog, "Atoms in solvation shell %5d\n", wr->isizeA - wr->nindA_prot);
            fprintf(wr->swaxsOutput->fpLog, "Atoms in excluded volume %5d\n", wr->isizeB);

            /** Check if solvation layer and excluded solvent fill roughly the same volume */
            if (wr->bDoingSolvent) {
                check_selected_solvent(x, xExclSolventRef, wr->indexA, wr->isizeA, wr->indexB, wr->isizeB, TRUE);
            }
        }

        /** For wr->inputParams->bVacuum == TRUE, nothing must be done because indexA and isizeA were initialized to the protein
            in prep_md_scattering_data() */

        /** Get number of electrons and electron density of A and B systems */
        real densA = droplet_density(wr, wr->indexA, wr->isizeA, wr->nElectrons, &nelecA);

        fprintf(wr->swaxsOutput->fpLog, "Density of A [e/nm3] = %10g -- %8g electrons -- A(q=0) = %8g\n",
                densA, nelecA, nelecA * nelecA);

        if (wr->bDoingSolvent) {
            real densB = droplet_density(wr, wr->indexB, wr->isizeB, wr->solvent->nElectronsPerAtom, &nelecB);
            fprintf(wr->swaxsOutput->fpLog, "Density of B [e/nm3] = %10g -- %8g electrons -- B(q=0) = %g\n",
                    densB, nelecB, nelecB * nelecB);
        }

        /* = = = = = Statistics: MASTER calculates solvation shell stats assuming equal averages = = = = = */

        /** Get Radius of Gyration of Solute (without solvation layer) */
        double Rg = soluteRadiusOfGyration(wr, x);
        UPDATE_CUMUL_AVERAGE(wr->RgAv, wr->waxsStep + 1, Rg);

        /** Averages of nr of atoms in surface layer and excluded volume */
        UPDATE_CUMUL_AVERAGE(wr->nAtomsLayerAver, wr->waxsStep + 1, wr->isizeA - wr->nindA_prot);
        UPDATE_CUMUL_AVERAGE(wr->nAtomsExwaterAver, wr->waxsStep + 1, wr->isizeB);
        UPDATE_CUMUL_AVERAGE(wr->nElecAvA, wr->waxsStep + 1, nelecA);
        UPDATE_CUMUL_AVERAGE(wr->nElecAv2A, wr->waxsStep + 1, nelecA * nelecA);
        UPDATE_CUMUL_AVERAGE(wr->nElecAv4A, wr->waxsStep + 1, nelecA * nelecA * nelecA * nelecA);

        if (wr->bDoingSolvent) {
            UPDATE_CUMUL_AVERAGE(wr->nElecAvB, wr->waxsStep + 1, nelecB);
            UPDATE_CUMUL_AVERAGE(wr->nElecAv2B, wr->waxsStep + 1, nelecB * nelecB);
            UPDATE_CUMUL_AVERAGE(wr->nElecAv4B, wr->waxsStep + 1, nelecB * nelecB * nelecB * nelecB);
        }

        if (!wr->inputParams->bVacuum) {
            // printf("Current volume of box = %g nm3\n", det(wr->local_state->box));
            real solDen = (wr->nElecTotA - nelecA) / (det(wr->local_state->box) - droplet_volume(wr));

            UPDATE_CUMUL_AVERAGE(wr->solElecDensAv, wr->waxsStep + 1, solDen);
            UPDATE_CUMUL_AVERAGE(wr->solElecDensAv2, wr->waxsStep + 1, solDen * solDen); /* needed for fix_solvent_density */
            UPDATE_CUMUL_AVERAGE(wr->solElecDensAv4, wr->waxsStep + 1, solDen * solDen * solDen * solDen);

            wr->soluteVolAv = droplet_volume(wr) - (wr->nElecAvA - wr->nElecProtA) / wr->solElecDensAv;
        }

        /* = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = */
    }

    /* = = = = = END of MASTER preparations = = = = = */

    /** Distribute atoms inside the envelope over the nodes */
    if (PAR(cr)) {
        gmx_bcast(sizeof(int), &wr->isizeA, cr->mpi_comm_mygroup);
        gmx_bcast(sizeof(int), &wr->isizeB, cr->mpi_comm_mygroup);
        gmx_bcast(sizeof(int), &wr->indexA_nalloc, cr->mpi_comm_mygroup);
        gmx_bcast(sizeof(int), &wr->indexB_nalloc, cr->mpi_comm_mygroup);
        gmx_bcast(sizeof(double), &wr->soluteVolAv, cr->mpi_comm_mygroup);
    }

    /** Broadcast the Rotational Matrix for force rotation. */
    if (wr->inputParams->bRotFit and PAR(cr)) {
        gmx_bcast(sizeof(matrix), Rinv, cr->mpi_comm_mygroup);
    }

    /** Possibly expand allocated memory for atomic coordinates and scattering types */
    srenew(wr->atomEnvelope_coord_A, wr->indexA_nalloc);

    for (int t = 0; t < wr->inputParams->nTypes; t++) {
        srenew(wr->wt[t].atomEnvelope_scatType_A, wr->indexA_nalloc);
    }

    if (wr->bDoingSolvent) {
        srenew(wr->atomEnvelope_coord_B, wr->indexB_nalloc);

        for (int t = 0; t < wr->inputParams->nTypes; t++) {
            srenew(wr->wt[t].atomEnvelope_scatType_B, wr->indexB_nalloc);
        }
    }

    for (int t = 0; t < wr->inputParams->nTypes; t++) {
        /* Compute Fourier Transforms of solvent density, used for solvent density correction in
           solute system */
        if (wr->inputParams->bFixSolventDensity or wr->inputParams->bScaleI0) {
            update_envelope_solvent_density(wr, cr, x, t);
        }
    }

    #if GMX_GPU_CUDA
        /**
         * The envelope of the solvent density is needed on the GPU already in the first WAXS-step.
         * Attention: On the GPU we never recalculate the FT we only pass the values of the solvent FT by reference!
         */
        if (wr->inputParams->bUseGPU and wr->bRecalcSolventFT_GPU) {
            update_envelope_solvent_density_GPU(wr);
        }
    #endif

    wr->timing->start(wr->waxsStep, swaxs::Timing::Stage::ScattAmplitude, *cr);

    /** The main loop over the scattering groups (xray, neutron1, neutron2,...) */
    for (int t = 0; t < wr->inputParams->nTypes; t++) {
        t_waxsrecType* wt = &wr->wt[t];
        waxs_datablock* wd = wt->wd;

        if (MASTER(cr)) {
            int nDeuter = 0;
            int nHyd = 0;

            for (int i = 0; i < wr->isizeA; i++) {
                int j = wr->indexA[i];

                if (wt->type == escatterXRAY) {
                    assert_scattering_type_is_set(wr->inputParams->cmtypeList[j], wt->type, j);
                    wt->atomEnvelope_scatType_A[i] = wr->inputParams->cmtypeList[j];
                } else {
                    /* get the NSL type, taking this deuterium conc into account */
                    assert_scattering_type_is_set(wr->inputParams->nsltypeList[j], wt->type, j);

                    wt->atomEnvelope_scatType_A[i] = get_nsl_type(wr, t, wr->inputParams->nsltypeList[j]);
                    nDeuter += (wt->atomEnvelope_scatType_A[i] == wt->nnsl_table - 1);
                    nHyd += (wt->atomEnvelope_scatType_A[i] == wt->nnsl_table - 2);

                    // printf("atom %d, nsltype = %d - t = %d\n", j, wr->redA[i].t, t);
                }

                copy_rvec(x[j], wr->atomEnvelope_coord_A[i]);
            }

            UPDATE_CUMUL_AVERAGE(wt->n2HAv_A, wr->waxsStep + 1, nDeuter);
            UPDATE_CUMUL_AVERAGE(wt->n1HAv_A, wr->waxsStep + 1, nHyd);
            UPDATE_CUMUL_AVERAGE(wt->nHydAv_A, wr->waxsStep + 1, nHyd + nDeuter);
        }

        if (PAR(cr)) {
            gmx_bcast(wr->isizeA * sizeof(rvec), wr->atomEnvelope_coord_A, cr->mpi_comm_mygroup);
            gmx_bcast(wr->isizeA * sizeof(int), wt->atomEnvelope_scatType_A, cr->mpi_comm_mygroup);
        }

        int qstart = wt->qvecs->qstart;
        int qhomenr = wt->qvecs->qhomenr;

        /** B-part: scattering of excluded solvent */
        if (wr->bDoingSolvent) {
            if (MASTER(cr)) {
                int nDeuter = 0;
                int nHyd = 0;

                for (int i = 0; i < wr->isizeB; i++) {
                    int j = wr->indexB[i];

                    if (wt->type == escatterXRAY) {
                        assert_scattering_type_is_set(wr->solvent->cromerMannTypes[j], wt->type, j);
                        wt->atomEnvelope_scatType_B[i] = wr->solvent->cromerMannTypes[j];
                    } else {
                        /** Get the NSL type, taking this deuterium conc into account */
                        assert_scattering_type_is_set(wr->solvent->neutronScatteringLengthTypes[j], wt->type, j);

                        wt->atomEnvelope_scatType_B[i] = get_nsl_type(wr, t, wr->solvent->neutronScatteringLengthTypes[j]);
                        nDeuter += (wt->atomEnvelope_scatType_B[i] == wt->nnsl_table - 1);
                        nHyd += (wt->atomEnvelope_scatType_B[i] == wt->nnsl_table - 2);
                    }

                    copy_rvec(wr->solvent->xPrepared[j], wr->atomEnvelope_coord_B[i]);
                }

                UPDATE_CUMUL_AVERAGE(wt->n2HAv_B, wr->waxsStep + 1, nDeuter);
                UPDATE_CUMUL_AVERAGE(wt->n1HAv_B, wr->waxsStep + 1, nHyd);
                UPDATE_CUMUL_AVERAGE(wt->nHydAv_B, wr->waxsStep + 1, nHyd + nDeuter);
            }

            if (PAR(cr)) {
                gmx_bcast(wr->isizeB * sizeof(rvec), wr->atomEnvelope_coord_B, cr->mpi_comm_mygroup);
                gmx_bcast(wr->isizeB * sizeof(int), wt->atomEnvelope_scatType_B, cr->mpi_comm_mygroup);
            }

            /** Compute instantanous scattering amplitude of pure-solvent sytem, B(\vec{q}) */
            if (wr->inputParams->bUseGPU) {
                #if GMX_GPU_CUDA
                    compute_scattering_amplitude_cuda(wr, cr, t, wd->B, wr->atomEnvelope_coord_B, wt->atomEnvelope_scatType_B,
                            wr->isizeB, 0, qhomenr, wt->type == escatterXRAY ? wt->aff_table : nullptr,
                            wt->type == escatterNEUTRON ? wt->nsl_table : nullptr, wt->qvecs->nabs, wd->normA, wd->normB,
                            wr->inputParams->scale, wr->inputParams->bCalcForces, wr->inputParams->bFixSolventDensity, &time_ms);
                #endif
            } else {
                compute_scattering_amplitude(wd->B, wr->atomEnvelope_coord_B, wt->atomEnvelope_scatType_B, wr->isizeB, 0,
                        wt->qvecs->q + qstart, wt->qvecs->iTable + qstart, qhomenr,
                        wt->type == escatterXRAY ? wt->aff_table : nullptr,
                        wt->type == escatterNEUTRON ? wt->nsl_table : nullptr, nullptr, &time_ms);
            }

            wr->timing->add(wr->waxsStep, swaxs::Timing::Stage::OneScattAmplitude, time_ms / 1000, *cr);
        }

        /** Compute instantanous scattering amplitude of protein + hydration layer, A(\vec{q}) */
        if (wr->inputParams->bUseGPU) {
            #if GMX_GPU_CUDA
                compute_scattering_amplitude_cuda(wr, cr, t, wd->A, wr->atomEnvelope_coord_A, wt->atomEnvelope_scatType_A,
                        wr->isizeA, wr->nindA_prot, qhomenr, wt->type == escatterXRAY ? wt->aff_table : nullptr,
                        wt->type == escatterNEUTRON ? wt->nsl_table : nullptr, wt->qvecs->nabs, wd->normA, wd->normB,
                        wr->inputParams->scale, wr->inputParams->bCalcForces, wr->inputParams->bFixSolventDensity, &time_ms);
            #endif
        } else {
            compute_scattering_amplitude(wd->A, wr->atomEnvelope_coord_A, wt->atomEnvelope_scatType_A, wr->isizeA,
                    wr->nindA_prot, wt->qvecs->q + qstart, wt->qvecs->iTable + qstart, qhomenr,
                    wt->type == escatterXRAY ? wt->aff_table : nullptr,
                    wt->type == escatterNEUTRON ? wt->nsl_table : nullptr, wd->dkAbar, &time_ms);
        }

        wr->timing->add(wr->waxsStep, swaxs::Timing::Stage::OneScattAmplitude, time_ms / 1000, *cr);

        /** Extra-check: Make sure that A[0]/B[0] is in agreement with the total number of electrons in the envelope */
        if (wt->qvecs->qstart == 0 and norm2(wt->qvecs->q[0]) == 0.0 and wt->type == escatterXRAY and wr->inputParams->bUseGPU == FALSE) {
            if (fabs(wd->A[0].re - nelecA) / nelecA > 1e-6 and MASTER(cr)) {
                gmx_fatal(FARGS, "A(q=0) = %g is inconsistent with # of electrons in A: %g\n", wd->A[0].re, nelecA);
            }

            if (wr->bDoingSolvent and MASTER(cr) and fabs(wd->B[0].re - nelecB) / nelecB > 1e-6) {
                gmx_fatal(FARGS, "B(q=0) is inconsistent with # of electrons in B: %g / %g\n", wd->B[0].re, nelecB);
            }
        }

        /** Update total number of electrons or NSLs of A and B systems */
        real tmp = number_of_electrons_or_NSLs(wt, wt->atomEnvelope_scatType_A, wr->isizeA, mtop);
        UPDATE_CUMUL_AVERAGE(wd->avAsum, wr->waxsStep + 1, tmp);

        if (wr->bDoingSolvent) {
            tmp = number_of_electrons_or_NSLs(wt, wt->atomEnvelope_scatType_B, wr->isizeB, mtop);
            UPDATE_CUMUL_AVERAGE(wd->avBsum, wr->waxsStep + 1, tmp);
        }

        wd = nullptr;
        wt = nullptr;
    }

    wr->timing->end(wr->waxsStep, swaxs::Timing::Stage::ScattAmplitude, *cr);

    wr->timing->start(wr->waxsStep, swaxs::Timing::Stage::SolvDensCorr, *cr);

    for (int t = 0; t < wr->inputParams->nTypes; t++) {
        if (wr->inputParams->bFixSolventDensity and !wr->inputParams->bVacuum) {
            /**
             * This function we need also to execute in case of GPU calculation to get estimation of added electrons etc..
             * However, in case of GPU calculation the actual fix_solvent_density correction will be done on the GPU!"
             */
            fix_solvent_density(wr, cr, t);
        }
    }

    wr->timing->end(wr->waxsStep, swaxs::Timing::Stage::SolvDensCorr, *cr);

    if (MASTER(cr) and wr->inputParams->bGridDensity) {
        update_envelope_griddensity(wr, x);
    }

    if (wr->inputParams->bScaleI0) {
        scaleI0_getAddedDensity(wr, cr);
    }

    wr->timing->start(wr->waxsStep, swaxs::Timing::Stage::ScattUpdates, *cr);

    /** Update average of scattering amplitudes A(q) and B(q) */
    update_AB_averages(wr);

    wr->timing->end(wr->waxsStep, swaxs::Timing::Stage::ScattUpdates, *cr);

    wr->timing->start(wr->waxsStep, swaxs::Timing::Stage::ComputeIdkI, *cr);

    /** Compute I(q) and gradients of I(q) wrt. atomic positions */
    calculate_I_dkI(wr, cr);

    wr->timing->end(wr->waxsStep, swaxs::Timing::Stage::ComputeIdkI, *cr);

    /** Updating estimates for the solute contrast wrt. to the buffer */
    updateSoluteContrast(wr, cr);

    #ifndef RERUN_CalcForces

    if (wr->inputParams->bCalcForces or wr->inputParams->bCalcPot) {
        wr->timing->start(wr->waxsStep, swaxs::Timing::Stage::PotForces, *cr);

        /* Clear old potential and forces */
        clear_vLast_fLast(wr);

        /* Update number of independent data points (Shannon-Nyquist theorem) */
        nIndep_Shannon_Nyquist(wr, x, cr, wr->waxsStep == 0 and (MASTER(cr)));

        /**
         * GIBBS SAMPLING:
         *    - Sample the uncertainty factor for the buffer density using Gibbs sampling
         *    - With ensemble refinment, in addition sample the weights of the different states
         *
         * On exit: New buffer epsilon and state weights present, already broadcastet over the nodes
         *          wr->ensembleSum_I and wr->ensembleSum_Ivar are updated.
         */
        if (wr->inputParams->ewaxs_ensemble_type == ewaxsEnsemble_BayesianOneRefined or wr->inputParams->bBayesianSolvDensUncert) {
            bayesian_gibbs_sampling(wr, simtime, cr);
        }

        /** Compute SAXS-drived potential and forces */
        waxs_md_pot_forces(cr, wr, simtime, &Rinv);

        if (MASTER(cr) and wr->inputParams->bCalcForces) {
            write_total_force_torque(wr, x);
        }

        if (wr->inputParams->verbosityLevel > 1 and wr->inputParams->bRotFit) {
            fprintf(stderr, "Node rotations: Rinv(row1) = %g %g %g\n", Rinv[0][0], Rinv[0][1], Rinv[0][2]);
        }

        wr->timing->end(wr->waxsStep, swaxs::Timing::Stage::PotForces, *cr);
    }

    #endif

    /** Output to log file */
    if (bDoLog and MASTER(cr)) {
        for (int t = 0; t < wr->inputParams->nTypes; t++) {
            t_waxsrecType* wt = &wr->wt[t];

            fprintf(wr->swaxsOutput->fpSpectra[t], "\n&\n# Intensity %d at simulation step ", wr->waxsStep);
            fprintf(wr->swaxsOutput->fpSpectra[t], "%" PRId64, step);

            write_intensity(wr->swaxsOutput->fpSpectra[t], wr, t);

            if (wr->inputParams->bCalcPot and wr->inputParams->weightsType != ewaxsWeightsUNIFORM) {
                write_stddevs(wr, step, t);
            }

            if ((wr->waxsStep % 10) == 0) {
                fflush(wr->swaxsOutput->fpSpectra[t]);
            }

            if (WAXS_ENSEMBLE(wr)) {
                /* Write ensemble-averaged I(q) */
                fprintf(wr->swaxsOutput->fpSpecEns[t], "\n&\n# Intensity %d at simulation step ", wr->waxsStep);
                fprintf(wr->swaxsOutput->fpSpecEns[t], "%" PRId64, step);
                fprintf(wr->swaxsOutput->fpSpecEns[t], "\n@type xydy\n");

                for (int i = 0; i < wt->nq; i++) {
                    fprintf(wr->swaxsOutput->fpSpecEns[t], "%8g  %12g %12g\n", wt->qvecs->abs[i], wt->ensembleSum_I[i],
                            sqrt(wt->ensembleSum_Ivar[i]));
                }

                if ((wr->waxsStep % 10) == 0) {
                    fflush(wr->swaxsOutput->fpSpecEns[t]);
                }
            }
        }

        if (wr->inputParams->bPrintForces) {
            rvec* x_red = nullptr;
            snew(x_red, wr->nindA_prot);

            if (wr->inputParams->bRotFit) {
                for (int j = 0; j < wr->nindA_prot; j++) {
                    rvec tmpvec;
                    rvec tmpvec2;

                    /** Rotate x to the original frame */
                    rvec_sub(x[wr->indA_prot[j]], wr->origin, tmpvec);

                    /** v' = R(v0 - c0) + c' ; Rotate and re-center. */
                    mvmul(Rinv, tmpvec, tmpvec2);
                    rvec_add(tmpvec2, wr->origin, x_red[j]);
                }
            } else {
                for (int j = 0; j < wr->nindA_prot; j++) {
                    copy_rvec(x[wr->indA_prot[j]], x_red[j]);
                }
            }

            fprintf(stderr, "Writing positions and forces of frame %i into trr file\n", wr->waxsStep);

            for (int t = 0; t < wr->inputParams->nTypes; t++) {
                /* Write coordinate and forces from all scattering groups into separate trr files */
                gmx_trr_write_frame(wr->swaxsOutput->xfout[t], wr->waxsStep, simtime, wr->wt[t].vLast, box, wr->nindA_prot,
                        x_red, nullptr, wr->wt[t].fLast);
            }

            sfree(x_red);
        }
    }

    if (PAR(cr)) {
        gmx_barrier(cr->mpi_comm_mygroup);
    }

    wr->timing->end(wr->waxsStep, swaxs::Timing::Stage::Step, *cr);

    if (MASTER(cr) and wr->swaxsOutput->fpLog and wr->waxsStep >= SWAXS_STEPS_RESET_TIME_AVERAGES) {
        wr->timing->writeLast(wr->swaxsOutput->fpLog);
    }

    wr->waxsStep++;

    swaxs_debug("Leaving do_waxs_md_low()");
}
