#include "./solvent.h"

#include <gromacs/fileio/confio.h>  // write_sto_conf_mtop
#include <gromacs/fileio/oenv.h>  // gmx_output_env_t
#include <gromacs/fileio/tpxio.h>  // read_tpx_state
#include <gromacs/fileio/trxio.h>  // t_trxstatus
#include <gromacs/mdtypes/inputrec.h>  // t_inputrec
#include <gromacs/mdtypes/state.h>  // t_state
#include <gromacs/trajectory/trajectoryframe.h>  // t_trxframe
#include <gromacs/utility/fatalerror.h>  // FARGS, gmx_fatal, gmx_incons
#include <gromacs/utility/smalloc.h>  // snew, sfree, srenew

#include <waxs/debug.h>  // swaxs_debug
#include <waxs/envelope/envelope.h>  // gmx_envelope
#include <waxs/envelope/volume.h>  // swaxs::envelope_volume
#include <waxs/gmxlib/gmx_envelope.h>  // gmx_envelope_bInsideCompactBox
#include <waxs/gmxlib/pbcshifts.h>  // N_SWAXS_BOX_SHIFTS, ONE_UI32, setAtomShiftsInsideEnvelope, summarizeAtomShifts
#include <waxs/gmxlib/sftypeio.h>  // countGroups, print_cmsf
#include <waxs/gmxlib/waxsmd_utils.h>  // swaxs::countElectronsPerAtom, mv_boxcenter_to_rvec

static void ensureTopologyHasScatteringParams(const gmx_mtop_t* topology, const char* runFilePath, const gmx_bool bVerbose) {
    int nSolventGroups = countGroups(topology->groups, SimulationAtomGroupType::egcWAXSSolvent);

    fprintf(stderr, "Read water topology %s file successfully:\n\t"
            "Contains %d Cromer-Mann types, %d NSL types, and %d solvent groups)\n",
            runFilePath, topology->scattTypes.nCromerMannParameters, topology->scattTypes.nNeutronScatteringLengths, nSolventGroups);

    if (nSolventGroups != 1) {
        gmx_fatal(FARGS, "Found %d solvent groups in %s. Please define one group as `waxs-solvent` and regenerate a\n"
                "`.tpr` file for the pure-buffer system with `grompp`.", nSolventGroups, runFilePath);
    }

    if (topology->scattTypes.nCromerMannParameters == 0) {
        gmx_fatal(FARGS, "No Cromer-Mann definitions found in %s\n"
                "Regenerate the `.tpr` file with `.mdp` option `waxs-coupl = yes` and `waxs-solvent` defined.\n",
                runFilePath);
    }

    if (bVerbose) {
        fprintf(stderr, "Found these Cromer-Mann / NSL types in %s:\n", runFilePath);
        fprintf(stderr, "\t    %-36s%-36s%-8s\n", "a1-a4", "b1-b4", "c");

        for (int i = 0; i < topology->scattTypes.nCromerMannParameters; i++) {
            fprintf(stderr, "%3d) ", i);
            print_cmsf(stderr, &topology->scattTypes.cromerMannParameters[i], 1);
        }

        if (topology->scattTypes.nNeutronScatteringLengths) {
            fprintf(stderr, "\t    %-36s\n", "cohb");

            for (int i = 0; i < topology->scattTypes.nNeutronScatteringLengths; i++) {
                fprintf(stderr, "%3d) %g", i, topology->scattTypes.neutronScatteringLengths[i].cohb);
            }
        }
    }
}

static void ensureSupportedPbcType(PbcType& pbcType) {
    if (pbcType != PbcType::Unset and pbcType != PbcType::Xyz) {
        gmx_fatal(FARGS, "The PBC of your pure-solvent system should be %s, but I found %s\n",
                c_pbcTypeNames[PbcType::Xyz].c_str(), c_pbcTypeNames[pbcType].c_str());
    }
}

static void ensureTrajectoryMatchesTopology(const t_trxframe& firstFrame, const gmx_mtop_t& topology, const char* runFilePath, const char* trajectoryFilePath) {
    if (firstFrame.natoms != topology.natoms) {
        gmx_fatal(FARGS, "Error while reading solvent for SWAXS analysis\n"
                "%d atoms in %s, but %d atoms in %s\n", topology.natoms, runFilePath, firstFrame.natoms, trajectoryFilePath);
    }

    if (!firstFrame.bBox) {
        gmx_fatal(FARGS, "No box (unit cell) information found in %s\n", trajectoryFilePath);
    }
}

void swaxs::Solvent::readTrajectory(const char* trajectoryFilePath, const gmx_output_env_t* oenv, const int frameLimit) {
    t_trxstatus* status;
    t_trxframe frame;
    read_first_frame(oenv, &status, trajectoryFilePath, &frame, TRX_NEED_X);

    int nAtoms = frame.natoms;

    /** Keep reading trajectory until `waxs-nfrsolvent` frames were found */
    int j = 0;

    gmx_bool bHaveEnough;

    do {
        srenew(this->x, j + 1);
        srenew(this->box, j + 1);
        snew(this->x[j], nAtoms);

        for (int i = 0; i < nAtoms; i++) {
            copy_rvec(frame.x[i], this->x[j][i]);
        }

        copy_mat(frame.box, this->box[j]);
        j++;

        bHaveEnough = ((frameLimit > 0) and (j >= frameLimit));

        if (bHaveEnough) {
            printf("\n\nStopped reading from %s after %d frames,\n\tas requested by `.mdp` option `waxs_nfrsolvent = %d`\n",
                    trajectoryFilePath, j, frameLimit);
        }
    } while (!bHaveEnough and read_next_frame(oenv, status, &frame));

    this->nFrames = j;

    close_trx(status);

    real memoryUsageMegaBytes = 1.0 * nAtoms * j * sizeof(rvec) / 1024 / 1024;
    fprintf(stderr, "Read %d frames from %s (%d atoms) (%4g MiByte)\n\n", j, trajectoryFilePath, nAtoms, memoryUsageMegaBytes);
}

swaxs::Solvent::Solvent(const char* runFilePath, const char* trajectoryFilePath, const gmx_output_env_t* oenv,
        const gmx_bool bVerbose, const int frameLimit) : topology{std::make_unique<gmx_mtop_t>()} {
    /** Using topology from the run file */

    t_inputrec ir; t_state state;  /** Unused */
    read_tpx_state(runFilePath, &ir, &state, this->topology.get());

    ensureTopologyHasScatteringParams(this->topology.get(), runFilePath, bVerbose);

    this->nElectronsPerAtom = swaxs::countElectronsPerAtom(this->topology.get());

    /** Using the first frame of the trajectory */

    t_trxframe firstFrame;

    t_trxstatus* status = nullptr;
    read_first_frame(oenv, &status, trajectoryFilePath, &firstFrame, TRX_NEED_X);
    close_trx(status);

    ensureTrajectoryMatchesTopology(firstFrame, *topology, runFilePath, trajectoryFilePath);

    ensureSupportedPbcType(firstFrame.pbcType);

    this->pbcType = firstFrame.pbcType;

    snew(this->xPrepared, firstFrame.natoms);

    /** Reading entire trajectory */

    readTrajectory(trajectoryFilePath, oenv, frameLimit);
}

void swaxs::Solvent::prepareNextFrame(gmx_envelope* envelope, int verbosityLevel) {
    assert(this->xPrepared != nullptr);

    /**
     * xtc frame ID of pure-water frame, relevant if mdp option waxs-nfrsolvent is
     * larger than the number of frames in xtc file
     */
    int iXtcFrame = this->iFrame % this->nFrames;

    /** Make a copy of the solvent coordinates of the used frame */
    memcpy(this->xPrepared, this->x[iXtcFrame], this->topology->natoms * sizeof(rvec));

    /** ...and a copy of the box */
    copy_mat(this->box[iXtcFrame], this->boxPrepared);

    /** Shift water box when iFrame >= nFrames, (to get independent pure-water frame) */
    if (this->iFrame >= this->nFrames) {
        real rshift = (this->iFrame / this->nFrames) * WAXS_SHIFT_WATERFRAME_BOXRATIO;
        rshift -= floor(rshift);

        rvec tmpVec;

        for (int d = 0; d < DIM; d++) {
            tmpVec[d] = rshift * this->boxPrepared[d][d];
        }

        // printf("Note: shifting water frame # %d coodinates by %g, %g, %g to get independent coordinates\n",
        //       iWaterframe, tmpvec[XX], tmpvec[YY], tmpvec[ZZ]);

        for (int i = 0; i < this->topology->natoms; i++) {
            rvec_inc(this->xPrepared[i], tmpVec);
        }
    }

    rvec envelopeCenter;
    matrix idM = { { 1, 0, 0 }, { 0, 1, 0 }, { 0, 0, 1 } };

    /**
     * The envelope is always located around the origin. Hence, we need to shift the pure-solvent frame
     * (which is in the unit cell) onto the envelope, such that the pure-solvent frame encloses the envelope
     * as good as possible.
     */
    if (TRICLINIC(this->boxPrepared)) {
        /** With a tricilinic box, we shift the 'center of the solvent box' to the
            'center of the bounding sphere' of the envelope */
        real rBoundingSphere;
        gmx_envelope_bounding_sphere(envelope, envelopeCenter, &rBoundingSphere);
    } else {
        /** With a cuboid box, we shift the 'center of the solvent box' to the 'center in x/y/z' of the envelope */
        gmx_envelope_center_xyz(envelope, idM, envelopeCenter);
    }

    auto xPreparedRef = gmx::arrayRefFromArray(reinterpret_cast<gmx::RVec *>(this->xPrepared), this->topology->natoms);

    /** All solvent atoms into the compact box */
    put_atoms_in_compact_unitcell(this->pbcType, ecenterTRIC, this->boxPrepared, xPreparedRef);

    /** Shift pure-solvent atoms to the center of the envelope */
    mv_boxcenter_to_rvec(this->topology->natoms, xPreparedRef, envelopeCenter, this->boxPrepared);

    /** Check if envelope fits into the compact box */
    t_pbc pbc;
    set_pbc(&pbc, this->pbcType, this->boxPrepared);

    rvec box_center;
    rvec waterRefInBox;
    calc_box_center(ecenterTRIC, this->boxPrepared, box_center);
    rvec_sub(box_center, envelopeCenter, waterRefInBox);

    if (!gmx_envelope_bInsideCompactBox(envelope, idM, this->boxPrepared, waterRefInBox, &pbc, TRUE, 0.)) {
        // This fatal error will be removed when the filling the envelope with PBC shifts is fully implemented
        gmx_fatal(FARGS, "The envelope does not fit into the compact unitcell for the PURE-SOLVENT box.\n"
                "Provide a larger PURE-SOLVENT simulation system with -sw and -fw\n");
        printf("NOTE: The envelope does not fit into the compact box. Will use PBC shifts of atoms to fill the envelope.\n");
    }

    /** Write the shifted solvent coordinates for debugging */
    if (this->iFrame == 0 and verbosityLevel > 1) {
        write_sto_conf_mtop("water_in_center_step0.pdb", *(this->topology->name), this->topology.get(), this->xPrepared,
                nullptr, this->pbcType, this->boxPrepared);
    }

    /** Increase water-frame counter */
    this->iFrame++;

    swaxs_debug("Centering of solvent box done.\n");
}

void swaxs::Solvent::getDensity(int nindB_sol, int* indB_sol, uint32_t* shiftsInsideEnvelope_B, gmx_envelope* envelope,
        gmx_bool bFixSolventDensity, real givenElecSolventDensity, int verbosityLevel) {
    real inv_v = 0.;
    real inv_v2 = 0.;

    for (int i = 0; i < this->nFrames; i++) {
        double tmp = 1. / det(this->box[i]);
        inv_v += tmp;
        inv_v2 += gmx::square(tmp);
    }

    inv_v /= this->nFrames;
    inv_v2 /= this->nFrames;
    real inv_vstddev = inv_v2 - gmx::square(inv_v);

    double nelec_sum = 0.;
    double nelec2_sum = 0.;

    for (int i = 0; i < nindB_sol; i++) {
        double nel = this->nElectronsPerAtom[indB_sol[i]];

        if (nel < 0) {
            gmx_fatal(FARGS, "Found illegal number of Electrons (%g) of solvent box atom %d.\n", nel, indB_sol[i]);
        }

        nelec_sum += nel;
    }

    this->nElectrons = nelec_sum;
    this->averageInverseVolume = inv_v;
    this->averageDensity = nelec_sum * inv_v;

    printf("\nAnalyzing complete solvent system (from %d frames):\n"
           "\t# of atoms          = %d\n"
           "\t# of electrons      = %g\n"
           "\tav. volume [nm3]    = %g +- %g\n"
           "\tav. density [e/nm3] = %g\n\n",
            this->nFrames, nindB_sol, nelec_sum, 1. / inv_v, inv_vstddev / gmx::square(inv_v), this->averageDensity);

    if (this->averageDensity > 334 * 1.1 or this->averageDensity < 334 * 0.9) {
        fprintf(stderr, "\n\nWARNING, you have a solvent density of %g e/nm3 (for water, this should"
                " be ~334 e/nm3)\n\n", this->averageDensity);
    }

    /** Now get density and variance of density inside the envelope */
    double nat_sum = 0.;

    nelec_sum = 0.;
    nelec2_sum = 0.;

    this->resetPureSolventFrameCounter();

    for (int iWaterframe = 0; iWaterframe < this->nFrames; iWaterframe++) {
        if ((iWaterframe % 20) == 0 or iWaterframe == (this->nFrames - 1)) {
            printf("\rComputing density (average + variance) of excluded volume - %6.1f%% done",
                    100. * (iWaterframe + 1) / this->nFrames);
            fflush(stdout);
        }

        /** Shift pure-solvent frame number 'iWaterframe' onto the envelope. On exit, ws->xPrepared is on the envelope */
        this->prepareNextFrame(envelope, verbosityLevel);

        /** Fill shiftsInsideEnvelope_B, specify which PBC images of solvent atoms are inside the envelope */
        auto xPreparedRef = gmx::arrayRefFromArray(reinterpret_cast<gmx::RVec *>(this->xPrepared), this->topology->natoms);

        setAtomShiftsInsideEnvelope(envelope, xPreparedRef, this->box[iWaterframe], nindB_sol, indB_sol, shiftsInsideEnvelope_B);

        if (verbosityLevel > 1) {
            printf("\nWaterframe %d\n", iWaterframe);
            summarizeAtomShifts(stdout, nindB_sol, indB_sol, shiftsInsideEnvelope_B);
        }

        /** Count electrons and atoms within the envelope */
        double nat_frame = 0;
        double nelec_frame = 0;

        #pragma omp parallel
        {
            double nelec_loc = 0;
            double nat_loc = 0;

            #pragma omp for
            for (int j = 0; j < nindB_sol; j++) {
                /** Loop over 27 (3x3x3) possible shifts */
                for (int s = 0; s < N_SWAXS_BOX_SHIFTS; s++) {
                    /** Is atom with shift s inside envelope? */
                    if (shiftsInsideEnvelope_B[j] & (ONE_UI32 << s)) {
                        nelec_loc += this->nElectronsPerAtom[indB_sol[j]];
                        nat_loc += 1.;
                    }
                }
            }

            #pragma omp critical
            {
                /** Sum over threads */
                nat_frame += nat_loc;
                nelec_frame += nelec_loc;
            }
        }

        /** Store for computing variance of nelec */
        nelec_sum  += nelec_frame;
        nelec2_sum += gmx::square(nelec_frame);
        nat_sum    += nat_frame;
    }

    printf("\n\n");

    this->resetPureSolventFrameCounter();

    nelec_sum  /= this->nFrames;
    nelec2_sum /= this->nFrames;
    nat_sum    /= this->nFrames;

    double var_nelec = nelec2_sum - gmx::square(nelec_sum);

    real vDroplet = swaxs::envelope_volume(envelope);
    real varDropletDensity = var_nelec / gmx::square(vDroplet);

    this->averageDropletDensity = nelec_sum / vDroplet;

    if (this->averageDropletDensity > 334 * 1.1 or this->averageDropletDensity < 334 * 0.9) {
        fprintf(stderr, "\n\nWARNING, you have a solvent density of %g e/nm3 (for water, this should"
                " be ~334 e/nm3)\n\n", this->averageDropletDensity);
    }

    if (fabs(this->averageDropletDensity / this->averageDensity - 1) > 0.01) {
        gmx_fatal(FARGS, "The solvent density estimated from the whole box (%g e/nm3) differs by more than 1%%"
                "from the density estimated from the solvent inside the envelope (%g e/nm3). This might indicate that"
                "don't have enough frames, or your pure-solvent systems does not contain only solvent.",
                this->averageDensity, this->averageDropletDensity);
    }

    printf("Solvent droplet inside the envelope (computed from %d frames):\n"
            "\t# of atoms           = %g\n"
            "\t# of electrons       = %g  +-  %g\n"
            "\tvolume [nm3]         = %g\n"
            "\tdensity [e/nm3]      = %g  +- %g \n",
            this->nFrames, nat_sum, nelec_sum, sqrt(var_nelec), vDroplet, this->averageDropletDensity, sqrt(varDropletDensity));

    if (bFixSolventDensity) {
        printf("Adding a constant electron density of %g e/nm3 to the solvent (to reach requested density of %g e/nm3).\n\n",
                givenElecSolventDensity - this->averageDropletDensity, givenElecSolventDensity);
    } else {
        printf("Will not change the solvent density but simply use density in xtc file (because waxs-solvdens = 0)\n\n");
    }
}
