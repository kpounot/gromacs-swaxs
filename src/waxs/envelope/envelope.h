#pragma once

#include <gromacs/math/vec.h>  // dvec
#include <gromacs/utility/basedefinitions.h>  // gmx_bool

#include <waxs/envelope/icosphere.h>  // t_icosphere
#include <waxs/envelope/grid_density.h>  // grid_density_t

struct t_triangle {
    int ind[3]; /* ray indices of the corners */
    int iface; /* Index of icosphere face */
    gmx_bool bDefined; /* triangle defined, that is, inner/outer of all 3 rays is defined? */
    dvec center; /* Geometric center of the triangle */
    dvec normal; /* normal vector pointint outwards */
    double c; /* constant c of the plane: vec{x}*normal = c */
    double area_r1; /* area of the triangle at radius 1 */
    double spaceangle; /* space angle covered by this triangle */
    double volPyramid; /* Volume of the pyramide */
    double rmin, rmax; /* distance of corner with the shortest/largest distance from the origin */
};

struct t_spherical {
    double theta, phi;
};

struct grid_density_t;

struct gmx_envelope {
    int nrec; /* # of recursion levels for icosphere. */
    int nrays; /* # of directions of envelope */
    double d; /* Build envelope at this distance from atoms */

    t_spherical* s; /* spherical coordinates of envelope rays (theta/phi) */
    dvec* r; /* vectors in direction of envelope rays */

    /* Ray params. Written in gmx_envelope_setStats */
    double *inner, *outer; /* inner and outer surface of envelope along rays, size nrays */
    gmx_bool* isDefined; /* are inner and outer defined?, size nrays */
    int nDefined; /* # of rays with defined surface */
    gmx_bool bHaveEnv; /* Have envelope constructed? */
    double minInner, maxInner, minOuter, maxOuter;
    double vol; /* Volume of the envelope */

    /* Written in gmx_envelope_setStats. Not needed for checkpointing. */
    /* surface elements (triangles of envelope) - each triangle refers to one face of the ikosphere */
    int nSurfElems; /* # of faces/surface elements of icosphere */
    gmx_bool bOriginInside; /* Is the origin within the envelope? -> surfElemInner = nullptr */
    t_triangle* surfElemOuter; /* The outer and inner surfaces of the envelope */
    t_triangle* surfElemInner; /* size nSurfElems */

    /* Partly set in gmx_envelope_binVolumes, called from setStats. ft_re and ft_im calculated later */
    /* Binning the envelope into small volume elements */
    int ngrid; /* radial grid number for volume elements */
    double** binVol; /* volumes of ngrid volume bins for each surface element, ngrid*nSurfElem */
    rvec** xBinVol; /* central coordinates of the volume elements (required for Fourier Transform) */

    real *ftunit_re, *ftunit_im; /* Fourier transform of a unit density in the envelope.
                                  * Used for the pure solvent sphere
                                  * The q vectors are provided externally and not known here.
                                  * Thus, the FT(q) of the local qs are stored */
    gmx_bool bHaveFourierTrans; /* FT already computed? */

    /* solvent density inside the envelope, around the solute in WAXS. */
    double** solventNelec; /* # electrons in solvent array of: [# surfElems][ngrid] (essentially density) */
    int nSolventStep; /* # of frames over which we averaged the density */
    real *ftdens_re, *ftdens_im; /* Fourier Transform of the density, nq(qhomenr on each node)
                                  * Used for the A solvent. */
    gmx_bool bHaveSolventFT; /* FT of density already already computed? */
    double solventNelecNorm; /* Norm for cumulative average of density */
    double solventNelec_fac1, solventNelec_fac2; /* factors for cumulative average */
    double solventNelecTotal; /* Total # of electrons in the solvent */

    t_icosphere* ico; /* The icosphere */

    grid_density_t* grid_density; /* The averaged electron density on a normal grid */

    /* Called in gmx_envelope_setStats. */
    /* bounding sphere of envelope */
    dvec bsphereCent; /* Center */
    double bsphereR2; /* Radius^2 */

    gmx_bool bVerbose;
};

gmx_envelope* gmx_envelope_init(int n, gmx_bool bVerbose);
