#include "./volume.h"

#include <cstdio>  // printf, fflush

#include <gromacs/math/vec.h>  // DIM, dsvmul
#include <gromacs/utility/fatalerror.h>  // FARGS, gmx_fatal
#include <gromacs/utility/gmxomp.h>  // gmx_omp_get_thread_num, gmx_omp_get_max_threads
#include <gromacs/utility/smalloc.h>  // snew, sfree

#include <waxs/envelope/envelope.h>  // gmx_envelope
// TODO: This should be removed, replaced by native Gromacs implementation. Then `random.cpp/random.h` removed.
#include <waxs/gmxlib/random.h>  // gmx_rng_t, gmx_rng_make_seed, gmx_rng_init_array, gmx_rng_uniform_real

// TODO: This should be removed.
#include <waxs/gmxlib/gmx_envelope.h>  // gmx_envelope_isInside

namespace swaxs {

void envelope_calculate_volume(gmx_envelope* envelope, double nTry_d, int seed0) {
    if (!envelope->bHaveEnv) {
        gmx_fatal(FARGS, "Error, trying to compute envelope volume using Monte-Carlo, but envelope is not defined\n");
    }

    std::size_t envelope_nseed;

    /* Use more seeds for RNG in case of many insertions - gives higher entropy in the RNG */
    if (nTry_d < 1e9) {
        envelope_nseed = 2;
    } else {
        envelope_nseed = 4;
    }

    dvec x, min = { 1e20, 1e20, 1e20 }, max = { -1e20, -1e20, -1e20 };

    for (int i = 0; i < envelope->nSurfElems; i++) {
        if (envelope->surfElemOuter[i].bDefined) {
            for (int j = 0; j < 3; j++) {
                int iray = envelope->surfElemOuter[i].ind[j];
                dsvmul(envelope->outer[iray], envelope->r[iray], x);

                for (int d = 0; d < DIM; d++) {
                    if (x[d] > max[d]) {
                        max[d] = x[d];
                    }

                    if (x[d] < min[d]) {
                        min[d] = x[d];
                    }
                }
            }
        }
    }

    dvec L;

    for (int d = 0; d < DIM; d++) {
        L[d] = max[d] - min[d];
    }

    const int NROUNDS = 50;
    double* vol_round;
    int64_t* nInside_round;

    snew(vol_round, NROUNDS);
    snew(nInside_round, NROUNDS);

    int nThreads = gmx_omp_get_max_threads();
    int64_t nTry = nTry_d;
    int64_t nTryPerThread;

    /* Make sure nTry can be divided by the number of threads */
    nTry = nTry_d / NROUNDS;
    nTry = (nTry / nThreads) * nThreads;
    nTryPerThread = nTry / nThreads;

    printf("\nComputing envelope volume from Monte Carlo using %g insertion trials, %d rounds:\n", nTry_d, NROUNDS);
    printf("\tmin coordinates of verticies = %12g %12g %12g\n", min[XX], min[YY], min[ZZ]);
    printf("\tmax coordinates of verticies = %12g %12g %12g\n", max[XX], max[YY], max[ZZ]);

    #pragma omp parallel shared(L, min)
    {
        unsigned int seed[envelope_nseed];
        int threadID = gmx_omp_get_thread_num();

        /* Init the random number generator with different seeds in every thread */
        if (seed0 < 0) {
            for (unsigned int is = 0; is < envelope_nseed; is++) {
                seed[is] = gmx_rng_make_seed();
            }
        } else {
            for (unsigned int is = 0; is < envelope_nseed; is++) {
                seed[is] = seed0 + is + threadID * envelope_nseed;
            }
        }

        gmx_rng_t rng = gmx_rng_init_array(seed, envelope_nseed);

        if (threadID == 0) {
            printf("\tRandom numbers initiated\n");
            fflush(stdout);
        }

        rvec R;
        int64_t nInside_loc[NROUNDS];

        for (int iround = 0; iround < NROUNDS; iround++) {
            nInside_loc[iround] = 0;

            if (threadID == 0) {
                printf("\r\t%5.0f %% done", 100.0 * (iround + 1) / NROUNDS);
                fflush(stdout);
            }

            /* The long loop over the Monte Carlo trials */
            for (int64_t imc = 0; imc < nTryPerThread; imc++) {
                for (unsigned int dl = 0; dl < DIM; dl++) {
                    R[dl] = gmx_rng_uniform_real(rng) * L[dl] + min[dl];
                }

                if (gmx_envelope_isInside(envelope, R)) {
                    nInside_loc[iround]++;
                }
            }
        }

        #pragma omp critical
        for (int iround = 0; iround < NROUNDS; iround++) {
            nInside_round[iround] += nInside_loc[iround];
        }
    }

    printf("\n");

    for (int iround = 0; iround < NROUNDS; iround++) {
        vol_round[iround] = 1.0 * nInside_round[iround] / nTry * L[XX] * L[YY] * L[ZZ];
    }

    double vol = 0;

    for (int iround = 0; iround < NROUNDS; iround++) {
        vol += vol_round[iround];
    }

    vol /= NROUNDS;

    double tmp = 0;

    for (int iround = 0; iround < NROUNDS; iround++) {
        tmp += gmx::square(vol_round[iround] - vol);
    }

    double volerr = sqrt(tmp / NROUNDS) / sqrt(1.0 * NROUNDS);

    printf("Volume = %12.8g +- %12g\n", vol, volerr);

    sfree(vol_round);
}

double envelope_volume(gmx_envelope* envelope) {
    if (!envelope->bHaveEnv) {
        gmx_fatal(FARGS, "envelope not defined.\n");
    }

    return envelope->vol;
}

}
