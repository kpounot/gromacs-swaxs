#pragma once

#include <gromacs/math/vec.h>  // dvec, DIM
#include <gromacs/utility/basedefinitions.h>  // gmx_inline

void copy_rdvec(const rvec in, dvec out);

void copy_drvec(const dvec in, rvec out);

inline double ddet(dvec a[DIM]) {
    return (  a[XX][XX] * (a[YY][YY] * a[ZZ][ZZ] - a[ZZ][YY] * a[YY][ZZ])
            - a[YY][XX] * (a[XX][YY] * a[ZZ][ZZ] - a[ZZ][YY] * a[XX][ZZ])
            + a[ZZ][XX] * (a[XX][YY] * a[YY][ZZ] - a[YY][YY] * a[XX][ZZ]));
}
