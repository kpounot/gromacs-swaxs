#include "./grid_density.h"

#include <cstdio>  // printf
#include <cstring>  // strlen

#include <gromacs/math/vec.h>  // dvec, rvec
#include <gromacs/utility/fatalerror.h>  // FARGS, gmx_fatal
#include <gromacs/utility/futil.h>  // gmx_ffopen, gmx_ffclose
#include <gromacs/utility/smalloc.h>  // snew, sfree

#include <waxs/envelope/envelope.h>  // gmx_envelope
#include <waxs/envelope/rvec.h>  // copy_rdvec

// TODO: It doesn't belong here.
#include <waxs/gmxlib/gmx_envelope.h>  // gmx_envelope_isInside

#define WAXS_ENVELOPE_GRID_DENSITY_SPACING 0.1

namespace swaxs {

// TODO: It should be a constructor of related structure.
static grid_density_t* grid_density_init(dvec min, dvec max, double spacing) {
    grid_density_t* g = new grid_density_t;

    for (int d = 0; d < DIM; d++) {
        g->min[d] = min[d];
        g->n[d] = ceil(max[d] - min[d]) / spacing;
        g->max[d] = g->min[d] + g->n[d] * spacing;
    }

    g->N = g->n[XX] * g->n[YY] * g->n[ZZ];
    g->dx = spacing;
    g->norm = 0;
    g->nstep = 0;
    g->fac1 = g->fac2 = 0;
    g->bOpen = FALSE;

    snew(g->dens, g->N);

    printf("Initiated grid density (required %g MB), spacing %g nm\n", 1.0 * g->N * sizeof(double) / 1024 / 1024, spacing);
    printf("\tDensity grid range: x (nm): %12g - %12g : %d bins\n", min[0], max[0], g->n[0]);
    printf("\t                    y (nm): %12g - %12g : %d bins\n", min[1], max[1], g->n[1]);
    printf("\t                    z (nm): %12g - %12g : %d bins\n\n", min[2], max[2], g->n[2]);

    return g;
}

static void grid_density_nextFrame(grid_density_t* g, double scale) {
    if (g->bOpen) {
        gmx_fatal(FARGS, "Trying to prepare grid density for next frame, but the frame is already open - did you"
                "not call grid_density_closeFrame()?\n");
    }

    if (!g) {
        gmx_fatal(FARGS, "Error, trying update grid-based electron density, but the grid was not yet initiated\n");
    }

    /* Norm and factors for cumulative averaging: D[n] = fac1 * D[n-1] + fac2 * d[n] */
    g->norm = 1. + scale * g->norm;
    g->fac1 = 1.0 * (g->norm - 1.) / g->norm;
    g->fac2 = 1 / g->norm;

    for (int j = 0; j < g->N; j++) {
        g->dens[j] *= g->fac1;
    }

    g->nstep++;
    g->bOpen = TRUE;
}

/** Just a wrapper functions visible from outside to use the grid density */
void envelope_grid_density_next_frame(gmx_envelope* envelope, double scale) {
    /** Init density grid if not done yet */
    if (!envelope->grid_density) {
        dvec xmin = { 1e20, 1e20, 1e20 }, xmax = { -1e20, -1e20, -1e20 }, x;
        double spacing = WAXS_ENVELOPE_GRID_DENSITY_SPACING;

        if (getenv("WAXS_ENVELOPE_GRID_DENSITY_SPACING") != nullptr) {
            spacing = atof(getenv("WAXS_ENVELOPE_GRID_DENSITY_SPACING"));

            printf("Found environment variable WAXS_ENVELOPE_GRID_DENSITY_SPACING. Using grid spacing of %g for "
                    "density in evelope.\n", spacing);
        }

        /* First get largest and smallest corners enclosing the envelope */
        for (int j = 0; j < envelope->nrays; j++) {
            if (envelope->isDefined[j]) {
                dsvmul(envelope->outer[j], envelope->r[j], x);
                for (int d = 0; d < DIM; d++) {
                    xmax[d] = (x[d] > xmax[d]) ? x[d] : xmax[d];
                    xmin[d] = (x[d] < xmin[d]) ? x[d] : xmin[d];
                }
            }
        }

        envelope->grid_density = grid_density_init(xmin, xmax, spacing);
    }

    /** Now open the grid for adding atoms */
    grid_density_nextFrame(envelope->grid_density, scale);
}

static void grid_density_closeFrame(grid_density_t* g) {
    if (!g->bOpen) {
        gmx_fatal(FARGS, "Trying to close the grid density for adding atoms, by the grid is not open.\n");
    }

    g->bOpen = FALSE;
}

void envelope_grid_density_close_frame(gmx_envelope* envelope) {
    if (!envelope->grid_density) {
        gmx_fatal(FARGS, "Inconsitency, trying to use grid_density but grid_density has not been initialized\n");
    }

    grid_density_closeFrame(envelope->grid_density);
}

static void grid_density_addAtom(grid_density_t* g, const rvec xin, double nelec) {
    if (!g->bOpen) {
        gmx_fatal(FARGS, "The grid density is not open for adding atsom. Call function grid_density_nextFrame() first.\n");
    }

    dvec x;
    copy_rdvec(xin, x);

    ivec ix;
    /* index of volume bin */
    gmx_bool bInside = TRUE;

    for (int d = 0; d < DIM; d++) {
        ix[d] = floor((x[d] - g->min[d]) / g->dx);
        bInside &= (0 <= ix[d] and ix[d] < g->n[d]);
    }

    if (bInside) {
        int j = ix[XX] * g->n[YY] * g->n[ZZ] + ix[YY] * g->n[ZZ] + ix[ZZ];

        if (j < 0 or j >= g->N) {
            gmx_fatal(FARGS, "Invalid bin while adding atom to grid density (ix = %d / %d / %d)\n", ix[XX], ix[YY], ix[ZZ]);
        }

        g->dens[j] += g->fac2 * nelec;
    }
}

void envelope_grid_density_add_atom(gmx_envelope* envelope, const rvec x, const double nElec) {
    if (!envelope->grid_density) {
        gmx_fatal(FARGS, "Inconsitency, trying to use grid_density but grid_density has not been initialized\n");
    }

    if (gmx_envelope_isInside(envelope, x)) {
        grid_density_addAtom(envelope->grid_density, x, nElec);
    }
}

static void grid_density_write(grid_density_t* g, const char* fn) {
    if (g->bOpen) {
        gmx_fatal(FARGS, "Cannot write grid density to file while the grid is still open. Did you not call grid_density_closeFrame() ?\n");
    }

    const double BohrRadius = 0.0529177210903;  /** Bohr Radius in nanometers, used for Cube format */
    FILE* fp = gmx_ffopen(fn, "w");

    dvec x;

    for (int ix = 0; ix < g->n[XX]; ix++) {
        for (int iy = 0; iy < g->n[YY]; iy++) {
            for (int iz = 0; iz < g->n[ZZ]; iz++) {
                int j = ix * g->n[YY] * g->n[ZZ] + iy * g->n[ZZ] + iz;

                x[XX] = g->min[XX] + (1. * ix + 0.5) * g->dx;
                x[YY] = g->min[YY] + (1. * iy + 0.5) * g->dx;
                x[ZZ] = g->min[ZZ] + (1. * iz + 0.5) * g->dx;
                double densNorm = g->dens[j] / (g->dx * g->dx * g->dx);
                fprintf(fp, "%8.3f %8.3f %8.3f  %12.5g\n", x[XX], x[YY], x[ZZ], densNorm);
            }
        }
    }

    gmx_ffclose(fp);

    /* Writing in cube format */
    char* fnCube;
    snew(fnCube, strlen(fn) + 10);

    sprintf(fnCube, "%s.cube", fn);
    fp = gmx_ffopen(fnCube, "w");

    fprintf(fp, "CUBE FORMAT, DENSITY INSIDE ENVELOPE\n");
    fprintf(fp, "NOTE: THE FOLLOWING COORDINATES ARE IN UNITS OF BOHR RADIUS\n");

    fprintf(fp, "%5d  %12.6f %12.6f %12.6f\n", 1, g->min[XX] / BohrRadius, g->min[YY] / BohrRadius, g->min[ZZ] / BohrRadius);
    fprintf(fp, "%5d  %12.6f %12.6f %12.6f\n", g->n[XX], g->dx / BohrRadius, 0., 0.);
    fprintf(fp, "%5d  %12.6f %12.6f %12.6f\n", g->n[YY], 0., g->dx / BohrRadius, 0.);
    fprintf(fp, "%5d  %12.6f %12.6f %12.6f\n", g->n[ZZ], 0., 0., g->dx / BohrRadius);

    /* Add one atom in corner, otherwise PyMol crashes */
    fprintf(fp, "%5d  %12.6f %12.6f %12.6f\n", 1, g->min[XX] / BohrRadius, g->min[YY] / BohrRadius, g->min[ZZ] / BohrRadius);

    /* Now write all denities */
    int count = 0;

    for (int ix = 0; ix < g->n[XX]; ix++) {
        for (int iy = 0; iy < g->n[YY]; iy++) {
            for (int iz = 0; iz < g->n[ZZ]; iz++) {
                int j = ix * g->n[YY] * g->n[ZZ] + iy * g->n[ZZ] + iz;
                double densNorm = g->dens[j] / (g->dx * g->dx * g->dx);
                fprintf(fp, "%12.6f ", densNorm);

                if (((count++) % 6) == 0) {
                    fprintf(fp, "\n");
                }
            }
        }
    }

    gmx_ffclose(fp);
}

void envelope_grid_density_write(gmx_envelope* envelope, const char* fn) {
    if (!envelope->grid_density) {
        gmx_fatal(FARGS, "Inconsitency, trying to use grid_density but grid_density has not been initialized\n");
    }

    grid_density_write(envelope->grid_density, fn);
}

}
