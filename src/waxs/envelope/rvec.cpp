#include "./rvec.h"  // copy_rdvec, copy_drvec

#include <gromacs/math/vec.h>  // dvec, rvec

void copy_rdvec(const rvec in, dvec out) {
    out[XX] = in[XX];
    out[YY] = in[YY];
    out[ZZ] = in[ZZ];
}

void copy_drvec(const dvec in, rvec out) {
    out[XX] = in[XX];
    out[YY] = in[YY];
    out[ZZ] = in[ZZ];
}
