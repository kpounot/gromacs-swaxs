#pragma once

#include <waxs/envelope/envelope.h>  // gmx_envelope

namespace swaxs {

/**
 * Computing the volume of the envelope using Monte-Carlo Integration. The function creates  a cuboid box that encloses
 * the envelope, gerates nTry_d random points in this box, and uses gmx_envelope_isInside() to test if the point is inside.
 *
 * Introduced to test if the volume is correct, otherwise not needed.
 *
 * Result: Seems like we get excellent agreement with the analytic result within the statistical uncertainty.
 */
void envelope_calculate_volume(gmx_envelope* envelope, double nTry_d, int seed0);

/**
 * It crashes if the volume have not been calculated yet.
 */
double envelope_volume(gmx_envelope* envelope);

}
