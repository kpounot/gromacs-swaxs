#pragma once

#include <gromacs/math/vec.h>  // rvec

#include <waxs/envelope/envelope.h>  // gmx_envelope

/**
 * Array to average the electron density on a grid enclosing the envelope,
 * such we can visualize the electron density inside the envelope.
 */
struct grid_density_t {
    dvec min, max; /* lower-left & upper-right corners of cuboid volume */
    ivec n; /* number of volume bins in x/y/z */
    int N; /* total number of volume bins */
    double dx; /* grid spacing */
    double* dens; /* density (array of size n[0]*n[1]*n[2]) */
    double norm; /* normalization for cumulative averaging */
    double fac1, fac2; /* factors for cumulative average */
    int nstep;
    gmx_bool bOpen; /* The grid is open for adding atoms - toggled by nextFrame / closeFrame functions */
};

struct gmx_envelope;

/* Functions to use the electron density on a regular grid (which can be visualized in VMD or so) */

namespace swaxs {

void envelope_grid_density_next_frame(gmx_envelope* envelope, double scale);

void envelope_grid_density_add_atom(gmx_envelope* envelope, const rvec x, double nElec);

void envelope_grid_density_close_frame(gmx_envelope* envelope);

/* Write density as x/y/z density to a file */
void envelope_grid_density_write(gmx_envelope* envelope, const char* fn);

}
