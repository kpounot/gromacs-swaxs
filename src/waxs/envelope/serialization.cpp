#include "./serialization.h"  // gmx_envelope_readFromFile

#include <cstdio>  // sscanf

#include <gromacs/math/vec.h>  // dcprod, diprod, dnorm2, dsvmul, dvec_*, dvec_add, dvec_sub, copy_ivec
#include <gromacs/utility/fatalerror.h>  // FARGS, gmx_fatal
#include <gromacs/utility/futil.h>  // gmx_ffopen, gmx_ffclose

#include <waxs/envelope/envelope.h>  // gmx_envelope*
#include <waxs/gmxlib/random.h>  // gmx_rng_t, gmx_rng_make_seed, gmx_rng_init_array, gmx_rng_uniform_real

// TODO: It doesn't belong here
#include <waxs/gmxlib/gmx_envelope.h>  // gmx_envelope_setStats

namespace swaxs {

void envelope_write_vmd_cgo(gmx_envelope* envelope, const char* fn, const rvec rgb, real alpha) {
    if (envelope->nSurfElems == 0) {
        gmx_fatal(FARGS, "Trying to write envelope triangles to file, but no triangles were defined\n");
    }

    double r, g, b;

    if (rgb == nullptr) {
        /* magenta */
        r = 212 / 256.0;
        g = 31 / 256.0;
        b = 123 / 256.0;
    } else {
        r = rgb[0];
        g = rgb[1];
        b = rgb[2];
    }

    FILE* fp = gmx_ffopen(fn, "w");

    /* Prepare molecule material.*/
    fprintf(fp, "proc draw-envelope {} {\n");
    fprintf(fp, "graphics top color magenta\n");

    if (alpha < 1.0 and alpha > 0.0) {
        fprintf(fp, "material change opacity Glossy %g\n", alpha);
        fprintf(fp, "graphics top material Glossy\n");
    } else {
        fprintf(fp, "graphics top material Glossy\n");
    }

    /* Write origin */
    double rad = 1.0;
    fprintf(fp, "graphics top sphere {0 0 0} radius %g\n", rad);

    /* Set colour and material */
    fprintf(fp, "color change rgb 32 %g %g %g\n", r, g, b);
    fprintf(fp, "graphics top color 32\n");

    dvec x[3];

    /* Write triangles */
    for (int i = 0; i < envelope->nSurfElems; i++) {
        if (envelope->surfElemOuter[i].bDefined) {
            fprintf(fp, "graphics top triangle ");

            for (int j = 0; j < 3; j++) {
                int iray = envelope->surfElemOuter[i].ind[j];
                dsvmul(envelope->outer[iray] * 10, envelope->r[iray], x[0]);
                fprintf(fp, "{%g %g %g} ", x[0][XX], x[0][YY], x[0][ZZ]);
            }

            fprintf(fp, "\n");
        }

        if (envelope->bOriginInside == FALSE and envelope->surfElemInner[i].bDefined) {
            fprintf(fp, "graphics top triangle ");

            for (int j = 0; j < 3; j++) {
                int iray = envelope->surfElemInner[i].ind[j];
                dsvmul(envelope->inner[iray] * 10, envelope->r[iray], x[0]);
                fprintf(fp, "{%g %g %g} ", x[0][XX], x[0][YY], x[0][ZZ]);
            }

            fprintf(fp, "\n");
        }
    }

    /* Now print cylinders on the edges of the surface elements */

    gmx_bool bDrawCyl = FALSE;

    if (bDrawCyl) {
        const double rcyl = 0.06;
        dvec ccyl;
        ccyl[0] = 0.8 * 256;
        ccyl[1] = 0.8 * 256;
        ccyl[2] = 0.8 * 256;
        const int res = 10;

        fprintf(fp, "color change rgb 31 %g %g %g\n", ccyl[0], ccyl[1], ccyl[2]);
        fprintf(fp, "graphics top color 31\n");

        for (int i = 0; i < envelope->nSurfElems; i++) {
            if (envelope->surfElemOuter[i].bDefined) {
                for (int j = 0; j < 3; j++) {
                    int iray = envelope->surfElemOuter[i].ind[j];
                    dsvmul(envelope->outer[iray] * 10, envelope->r[iray], x[j]);
                }

                for (int j = 0; j < 3; j++) {
                    int jj = (j + 1) % 3;

                    fprintf(fp, "graphics top cylinder {%g %g %g} {%g %g %g} radius %g resolution %i filled no\n",
                            x[j][XX], x[j][YY], x[j][ZZ], x[jj][XX], x[jj][YY], x[jj][ZZ], rcyl, res);
                }
            }

            if (envelope->bOriginInside == FALSE and envelope->surfElemInner[i].bDefined) {
                for (int j = 0; j < 3; j++) {
                    int iray = envelope->surfElemInner[i].ind[j];
                    dsvmul(envelope->inner[iray] * 10, envelope->r[iray], x[j]);
                }

                for (int j = 0; j < 3; j++) {
                    int jj = (j + 1) % 3;

                    fprintf(fp, "graphics top cylinder {%g %g %g} {%g %g %g} radius %g resolution %i filled no\n",
                            x[j][XX], x[j][YY], x[j][ZZ], x[jj][XX], x[jj][YY], x[jj][ZZ], rcyl, res);
                }
            }

            /* Draw the lines in radial direction to connect inner and outer surface elements.
               If the surface element is not defined but the ray on a corner is, then draw a cylinder */
            if (!envelope->bOriginInside and !envelope->surfElemOuter[i].bDefined) {
                for (int j = 0; j < 3; j++) {
                    /* Check if the ray is defined */
                    int iray = envelope->surfElemInner[i].ind[j];

                    if (envelope->isDefined[iray]) {
                        dsvmul(envelope->inner[iray] * 10, envelope->r[iray], x[0]);
                        dsvmul(envelope->outer[iray] * 10, envelope->r[iray], x[1]);
                        fprintf(fp, "graphics top cylinder {%g %g %g} {%g %g %g} radius %g resolution %i filled no\n",
                                x[0][XX], x[0][YY], x[0][ZZ], x[1][XX], x[1][YY], x[1][ZZ], rcyl, res);
                    }
                }
            }
        }
    }

    fprintf(fp, "\nputs \"Done drawing.\"\n}\ndraw-envelope\n");
    gmx_ffclose(fp);
    printf("Wrote envelope as VMD-tcl into %s\n", fn);
}

void envelope_write_pymol_cgo(gmx_envelope* envelope, const char* fn, const char* name, rvec rgb, rvec rgb_inside, real alpha) {
    int nDefined, iDefined[3];
    dvec v1, v2, normal, x[4], faceCent;
    const double rcyl = 0.06;
    const double mvInnerTriangles = 0.01; /* Distance in Angstroem of the inner surface triangles */

    if (envelope->nSurfElems == 0) {
        gmx_fatal(FARGS, "Trying to write envelope triangles to file, but no triangles were defined\n");
    }

    gmx_bool bTrianglesInside = (rgb_inside != nullptr);

    double r, g, b, a;

    if (rgb == nullptr) {
        /* magenta */
        r = 202. / 256;
        g = 31. / 256;
        b = 123. / 256;
    } else {
        r = rgb[0];
        g = rgb[1];
        b = rgb[2];
    }

    if (alpha < 0) {
        alpha = 1;
    }

    FILE* fp = gmx_ffopen(fn, "w");

    /* Write origin */
    fprintf(fp, "from pymol.cgo import *\nfrom pymol import cmd\n\n");
    fprintf(fp, "obj3 = [\nCOLOR, %g, %g, %g,\n", 0.9, 0.0, 0.9);
    fprintf(fp, "SPHERE, %g,%g,%g, %g,\n", 0., 0., 0., 0.5);
    fprintf(fp, "]\n\n");
    fprintf(fp, "cmd.load_cgo(obj3, '%s_origin')\n\n", name);

    /* Write triangles */
    fprintf(fp, "obj= [\n\nBEGIN, TRIANGLES,\n\n\n");

    for (int i = 0; i < envelope->nSurfElems; i++) {
        if (envelope->surfElemOuter[i].bDefined) {
            fprintf(fp, "COLOR, %g, %g, %g,\n", r, g, b);
            fprintf(fp, "NORMAL, %g, %g, %g,\n", envelope->surfElemOuter[i].normal[XX],
                    envelope->surfElemOuter[i].normal[YY], envelope->surfElemOuter[i].normal[ZZ]);

            for (int j = 0; j < 3; j++) {
                int iray = envelope->surfElemOuter[i].ind[j];
                dsvmul(envelope->outer[iray] * 10, envelope->r[iray], x[0]);
                fprintf(fp, "VERTEX, %g, %g, %g,\n", x[0][XX], x[0][YY], x[0][ZZ]);
            }

            if (bTrianglesInside) {
                /* Add triangle for the inner surface of the envelope - otherwise it's just black in Pymol */
                fprintf(fp, "COLOR, %g, %g, %g,\n", rgb_inside[XX], rgb_inside[YY], rgb_inside[ZZ]);
                fprintf(fp, "NORMAL, %g, %g, %g,\n", -envelope->surfElemOuter[i].normal[XX], -envelope->surfElemOuter[i].normal[YY],
                        -envelope->surfElemOuter[i].normal[ZZ]);

                for (int j = 0; j < 3; j++) {
                    int iray = envelope->surfElemOuter[i].ind[j];
                    /* Move vertices slightly inside, by 0.05 Angstroem */
                    dsvmul(envelope->outer[iray] * 10 - mvInnerTriangles, envelope->r[iray], x[0]);
                    fprintf(fp, "VERTEX, %g, %g, %g,\n", x[0][XX], x[0][YY], x[0][ZZ]);
                }
            }
        }

        if (envelope->bOriginInside == FALSE and envelope->surfElemInner[i].bDefined) {
            dsvmul(-1., envelope->surfElemInner[i].normal, normal);
            fprintf(fp, "COLOR, %g, %g, %g,\n", r, g, b);
            fprintf(fp, "NORMAL, %g, %g, %g,\n", normal[XX], normal[YY], normal[ZZ]);

            for (int j = 0; j < 3; j++) {
                int iray = envelope->surfElemInner[i].ind[j];
                dsvmul(envelope->inner[iray] * 10, envelope->r[iray], x[0]);
                fprintf(fp, "VERTEX, %g, %g, %g,\n", x[0][XX], x[0][YY], x[0][ZZ]);
            }

            if (bTrianglesInside) {
                /* Add triangle for the inner surface of the envelope - otherwise it's just black in Pymol */
                fprintf(fp, "COLOR, %g, %g, %g,\n", rgb_inside[XX], rgb_inside[YY], rgb_inside[ZZ]);
                fprintf(fp, "NORMAL, %g, %g, %g,\n", -normal[XX], -normal[YY], -normal[ZZ]);

                for (int j = 0; j < 3; j++) {
                    int iray = envelope->surfElemInner[i].ind[j];
                    /* Move vertices slightly outside, by 0.05 Angstroem */
                    dsvmul(envelope->inner[iray] * 10 + mvInnerTriangles, envelope->r[iray], x[0]);
                    fprintf(fp, "VERTEX, %g, %g, %g,\n", x[0][XX], x[0][YY], x[0][ZZ]);
                }
            }
        }

        /* Draw quadrilaterals on the surfaces that conect the outer with the inner surface.
           If this triangle is not defined, but two of its corerns are defined, then draw the quadrilateral */
        if (!envelope->bOriginInside and !envelope->surfElemOuter[i].bDefined) {
            nDefined = 0;

            for (int j = 0; j < 3; j++) {
                /* Check if the ray is defined */
                int iray = envelope->ico->face[i].v[j];

                if (envelope->isDefined[iray]) {
                    iDefined[nDefined++] = iray;
                }
            }

            if (nDefined == 2) {
                dsvmul(envelope->inner[iDefined[0]] * 10, envelope->r[iDefined[0]], x[0]);
                dsvmul(envelope->outer[iDefined[0]] * 10, envelope->r[iDefined[0]], x[1]);
                dsvmul(envelope->inner[iDefined[1]] * 10, envelope->r[iDefined[1]], x[2]);
                dsvmul(envelope->outer[iDefined[1]] * 10, envelope->r[iDefined[1]], x[3]);
                dvec_sub(x[1], x[0], v1);
                dvec_sub(x[2], x[0], v2);
                dcprod(v1, v2, normal);

                /* Get direction of normal right - is normal pointing the direction of the center of this face? */
                clear_dvec(faceCent);

                for (int j = 0; j < 3; j++) {
                    int iray = envelope->ico->face[i].v[j];
                    dvec_inc(faceCent, envelope->ico->vertex[iray]);
                }

                dsvmul(1. / 3, faceCent, faceCent);
                dvec_sub(faceCent, x[0], v1);

                if (diprod(normal, v1) < 0.) {
                    dsvmul(-1, normal, normal);
                }

                fprintf(fp, "COLOR, %g, %g, %g,\n", r, g, b);
                fprintf(fp, "NORMAL, %g, %g, %g,\n", normal[XX], normal[YY], normal[ZZ]);
                fprintf(fp, "VERTEX, %g, %g, %g,\n", x[0][XX], x[0][YY], x[0][ZZ]);
                fprintf(fp, "VERTEX, %g, %g, %g,\n", x[1][XX], x[1][YY], x[1][ZZ]);
                fprintf(fp, "VERTEX, %g, %g, %g,\n", x[2][XX], x[2][YY], x[2][ZZ]);
                fprintf(fp, "NORMAL, %g, %g, %g,\n", normal[XX], normal[YY], normal[ZZ]);
                fprintf(fp, "VERTEX, %g, %g, %g,\n", x[1][XX], x[1][YY], x[1][ZZ]);
                fprintf(fp, "VERTEX, %g, %g, %g,\n", x[2][XX], x[2][YY], x[2][ZZ]);
                fprintf(fp, "VERTEX, %g, %g, %g,\n", x[3][XX], x[3][YY], x[3][ZZ]);

                if (bTrianglesInside) {
                    /* Add triangles for the inner surface */
                    a = mvInnerTriangles;

                    fprintf(fp, "COLOR, %g, %g, %g,\n", r, g, b);
                    fprintf(fp, "NORMAL, %g, %g, %g,\n", -normal[XX], -normal[YY], -normal[ZZ]);
                    fprintf(fp, "VERTEX, %g, %g, %g,\n", x[0][XX] - normal[XX] * a, x[0][YY] - normal[YY] * a, x[0][ZZ] - normal[ZZ] * a);
                    fprintf(fp, "VERTEX, %g, %g, %g,\n", x[1][XX] - normal[XX] * a, x[1][YY] - normal[YY] * a, x[1][ZZ] - normal[ZZ] * a);
                    fprintf(fp, "VERTEX, %g, %g, %g,\n", x[2][XX] - normal[XX] * a, x[2][YY] - normal[YY] * a, x[2][ZZ] - normal[ZZ] * a);
                    fprintf(fp, "NORMAL, %g, %g, %g,\n", -normal[XX], -normal[YY], -normal[ZZ]);
                    fprintf(fp, "VERTEX, %g, %g, %g,\n", x[1][XX] - normal[XX] * a, x[1][YY] - normal[YY] * a, x[1][ZZ] - normal[ZZ] * a);
                    fprintf(fp, "VERTEX, %g, %g, %g,\n", x[2][XX] - normal[XX] * a, x[2][YY] - normal[YY] * a, x[2][ZZ] - normal[ZZ] * a);
                    fprintf(fp, "VERTEX, %g, %g, %g,\n", x[3][XX] - normal[XX] * a, x[3][YY] - normal[YY] * a, x[3][ZZ] - normal[ZZ] * a);
                }
            }
        }
    }

    fprintf(fp, "\nEND ]\n\n");
    fprintf(fp, "cmd.load_cgo(obj,'%s')\n\n", name);

    /* Now print cylinders on the edges of the surface elements */
    fprintf(fp, "obj2 = [\n\nCOLOR, %g, %g, %g,\n\n", 0.8, 0.8, 0.8);

    for (int i = 0; i < envelope->nSurfElems; i++) {
        if (envelope->surfElemOuter[i].bDefined) {
            for (int j = 0; j < 3; j++) {
                int iray = envelope->surfElemOuter[i].ind[j];
                dsvmul(envelope->outer[iray] * 10, envelope->r[iray], x[j]);
            }

            for (int j = 0; j < 3; j++) {
                int jj = (j + 1) % 3;

                fprintf(fp, "CYLINDER, %g,%g,%g, %g,%g,%g, %g, %g,%g,%g,%g,%g,%g,\n", x[j][XX], x[j][YY], x[j][ZZ],
                        x[jj][XX], x[jj][YY], x[jj][ZZ], rcyl, .1, .7, .7, .1, .7, .7);
            }
        }

        if (envelope->bOriginInside == FALSE and envelope->surfElemInner[i].bDefined) {
            for (int j = 0; j < 3; j++) {
                int iray = envelope->surfElemInner[i].ind[j];
                dsvmul(envelope->inner[iray] * 10, envelope->r[iray], x[j]);
            }

            for (int j = 0; j < 3; j++) {
                int jj = (j + 1) % 3;

                fprintf(fp, "CYLINDER, %g,%g,%g, %g,%g,%g, %g, %g,%g,%g,%g,%g,%g,\n", x[j][XX], x[j][YY], x[j][ZZ],
                        x[jj][XX], x[jj][YY], x[jj][ZZ], rcyl, .1, .7, .7, .1, .7, .7);
            }
        }

        /* Draw the lines in radial direction to connect inner and outer surface elements.
           If the surface element is not defined but the ray on a corner is, then draw a cylinder */
        if (!envelope->bOriginInside and !envelope->surfElemOuter[i].bDefined) {
            for (int j = 0; j < 3; j++) {
                /* Check if the ray is defined */
                int iray = envelope->surfElemInner[i].ind[j];

                if (envelope->isDefined[iray]) {
                    dsvmul(envelope->inner[iray] * 10, envelope->r[iray], x[0]);
                    dsvmul(envelope->outer[iray] * 10, envelope->r[iray], x[1]);

                    fprintf(fp, "CYLINDER, %g,%g,%g, %g,%g,%g, %g, %g,%g,%g,%g,%g,%g,\n", x[0][XX], x[0][YY], x[0][ZZ],
                            x[1][XX], x[1][YY], x[1][ZZ], rcyl, .1, .7, .7, .1, .7, .7);
                }
            }
        }
    }

    fprintf(fp, "]\n\n");
    fprintf(fp, "cmd.load_cgo(obj2, '%s_mesh')\n\n", name);

    gmx_ffclose(fp);
    printf("Wrote envelope as pymol CGO into %s\n", fn);
}

void envelope_dump_to_pdb(gmx_envelope* envelope, int nHighlight, int* high, gmx_bool bExtra, rvec x_extra, double R, const char* fn) {
    int j, res = 1;
    gmx_bool bHigh;

    FILE* fp = fopen(fn, "w");

    for (j = 0; j < envelope->nrays; j++) {
        bHigh = FALSE;

        for (int i = 0; i < nHighlight; i++) {
            bHigh |= (j == high[i]);
        }

        if (!bHigh) {
            fprintf(fp, "ATOM  %5d %4s %3s %1s%4d    %8.3f%8.3f%8.3f%6.2f%6.2f\n",
                    j + 1, "C", "MOL", "", res, R * envelope->r[j][XX], R * envelope->r[j][YY], R * envelope->r[j][ZZ], 1.0, 0.0);
        } else {
            res++;
            fprintf(fp, "ATOM  %5d %4s %3s %1s%4d    %8.3f%8.3f%8.3f%6.2f%6.2f\n",
                    j + 1, "O", "HIG", "", res, R * envelope->r[j][XX], R * envelope->r[j][YY], R * envelope->r[j][ZZ], 1.0, 0.0);
            res++;
        }
    }

    if (bExtra) {
        res++;
        fprintf(fp, "ATOM  %5d %4s %3s %1s%4d    %8.3f%8.3f%8.3f%6.2f%6.2f\n",
                j + 1, "FE", "FE", "", res, R * x_extra[XX], R * x_extra[YY], R * x_extra[ZZ], 1.0, 0.0);
    }

    fclose(fp);
    printf("\n ** Dumped envelope with error to %s **\n", fn);
}

gmx_envelope* envelope_read_from_file(const char* fn) {
    char line[200];
    char fm[10] = "%lf %lf";

    FILE* fp = gmx_ffopen(fn, "r");

    if (fgets(line, 200, fp) == nullptr) {
        gmx_fatal(FARGS, "Error while reading the first line of file %s\n", fn);
    }

    int nrec;

    if (sscanf(line, "%d", &nrec) != 1) {
        gmx_fatal(FARGS, "Expected exactly one number in the first line of %s\n", fn);
    }

    gmx_envelope* envelope = gmx_envelope_init(nrec, TRUE);

    int nrays = envelope->nrays;

    printf("Initiating envelope from file %s with %d rays (recusion level %d)\n", fn, nrays, nrec);

    int j = 0;

    while (fgets(line, 200, fp) != nullptr) {
        if (j >= nrays) {
            gmx_fatal(FARGS, "Found (at least) %d inner / outer surfaces in %s. Expected only %d\n", j + 1, fn, nrays);
        }

        if (sscanf(line, fm, &envelope->inner[j], &envelope->outer[j]) != 2) {
            gmx_fatal(FARGS, "Error, could not read 2 numers (innter/outer surface) from %s (j=%d). Line was:\n%s\n", fn, j, line);
        }

        j++;
    }

    if (j != nrays) {
        gmx_fatal(FARGS, "Expected %d inner / outer surfaces in envelope file %s. Found %d\n", nrays, fn, j);
    }

    gmx_ffclose(fp);
    printf("Read surfaces for %d envelope rays from file %s\n", j, fn);

    gmx_envelope_setStats(envelope);

    printf("\tInner surface spanning radii from %g to %g nm.\n"
           "\tOuter surface spanning radii from %g to %g nm\n"
           "\t%d of %d directions defined.\n\n",
            envelope->minInner, envelope->maxInner, envelope->minOuter, envelope->maxOuter, envelope->nDefined,
            envelope->nrays);

    envelope->bHaveEnv = TRUE;

    return envelope;
}

void envelope_write_to_file(gmx_envelope* envelope, const char* fn) {
    FILE* fp = gmx_ffopen(fn, "w");
    fprintf(fp, "%d\n", envelope->nrec);

    for (int j = 0; j < envelope->nrays; j++) {
        fprintf(fp, "%.12g %.12g\n", envelope->inner[j], envelope->outer[j]);
    }

    gmx_ffclose(fp);
}

void envelope_triangle_to_cgo(gmx_envelope* envelope, FILE* fp, int f, double rcyl, double r, double g, double b) {
    dvec x, x1, y, y1;

    for (int i = 0; i < 3; i++) {
        int j = envelope->surfElemOuter[f].ind[i];
        int j1 = envelope->surfElemOuter[f].ind[(i + 1) % 3];

        dsvmul(envelope->outer[j], envelope->r[j], x);
        dsvmul(envelope->outer[j1], envelope->r[j1], x1);
        printf("\nenvelope->bOriginInside = %d\n", envelope->bOriginInside);

        if (envelope->bOriginInside) {
            clear_dvec(y);
            clear_dvec(y1);
        } else {
            dsvmul(envelope->inner[j], envelope->r[j], y);
            dsvmul(envelope->inner[j1], envelope->r[j1], y1);
            printf("\ninner %d = %g  outer %g\n", j, envelope->inner[j], envelope->outer[j]);
        }

        printf("\n x = %g %g %g\n", x[XX], x[YY], x[ZZ]);
        printf("\n y = %g %g %g\n", y[XX], y[YY], y[ZZ]);

        fprintf(fp, "CYLINDER, %8g,%8g,%8g, %8g,%8g,%8g, %8g, %8g,%8g,%8g,%8g,%8g,%8g,\n",
                10 * x[XX], 10 * x[YY], 10 * x[ZZ], 10 * y[XX], 10 * y[YY], 10 * y[ZZ], rcyl, r, g, b, r, g, b);

        fprintf(fp, "CYLINDER, %8g,%8g,%8g, %8g,%8g,%8g, %8g, %8g,%8g,%8g,%8g,%8g,%8g,\n",
                10 * x[XX], 10 * x[YY], 10 * x[ZZ], 10 * x1[XX], 10 * x1[YY], 10 * x1[ZZ], rcyl, r, g, b, r, g, b);

        if (!envelope->bOriginInside) {
            fprintf(fp, "CYLINDER, %8g,%8g,%8g, %8g,%8g,%8g, %8g, %8g,%8g,%8g,%8g,%8g,%8g,\n",
                    10 * y[XX], 10 * y[YY], 10 * y[ZZ], 10 * y1[XX], 10 * y1[YY], 10 * y1[ZZ], rcyl, r, g, b, r, g, b);
        }
    }
}

void envelope_dvec_to_cgo(FILE* fp, const dvec x, double radius) {
    fprintf(fp, "SPHERE, %g,%g,%g, %g,\n", 10 * x[XX], 10 * x[YY], 10 * x[ZZ], radius);
}

void envelope_dump_triangle_to_cgo_file(gmx_envelope* envelope, int f, const dvec x, const char* fn) {
    FILE* fp = fopen(fn, "w");

    fprintf(fp, "from pymol.cgo import *\nfrom pymol import cmd\n\n");
    fprintf(fp, "obj = [\n\nCOLOR, %g, %g, %g,\n\n", 0.8, 0.8, 0.8);

    envelope_triangle_to_cgo(envelope, fp, f, 0.05, 1.0, 0., 0.);
    envelope_dvec_to_cgo(fp, x, 0.1);

    fprintf(fp, "]\n\ncmd.load_cgo(obj, 'error')\n\n");
    printf("\n\nDumped surface element %d and x = %g %g %g to CGO file %s\n\n", f, x[0], x[1], x[2], fn);

    fclose(fp);
}

}
