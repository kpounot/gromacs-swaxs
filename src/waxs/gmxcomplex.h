#pragma once

#include <math.h>  // cos, sin

#include <gromacs/math/gmxcomplex.h>  // t_complex

typedef struct {
    double re, im;
} t_complex_d;

typedef t_complex_d cdvec[DIM];

#pragma GCC diagnostic ignored "-Wunused-variable"
static t_complex_d cnul_d = { 0.0, 0.0 };
#pragma GCC diagnostic pop

static inline t_complex_d rcmul_d(double r, t_complex_d c) {
    t_complex_d d;

    d.re = r * c.re;
    d.im = r * c.im;

    return d;
}

#if 0
static inline t_complex_d rcexp_d(double r) {
    t_complex_d c;

    c.re = cos(r);
    c.im = sin(r);

    return c;
}
#endif

static inline t_complex_d cadd_d(t_complex_d a, t_complex_d b) {
    t_complex_d c;

    c.re = a.re + b.re;
    c.im = a.im + b.im;

    return c;
}

static inline t_complex_d csub_d(t_complex_d a, t_complex_d b) {
    t_complex_d c;

    c.re = a.re - b.re;
    c.im = a.im - b.im;

    return c;
}

static inline t_complex_d cmul_d(t_complex_d a, t_complex_d b) {
    t_complex_d c;

    c.re = a.re * b.re - a.im * b.im;
    c.im = a.re * b.im + a.im * b.re;

    return c;
}

#if 0
static inline t_complex_d cmul_rd(t_complex a, t_complex_d b) {
    t_complex_d c;

    c.re = a.re * b.re - a.im * b.im;
    c.im = a.re * b.im + a.im * b.re;

    return c;
}
#endif

static inline t_complex_d conjugate_d(t_complex_d c) {
    t_complex_d d;

    d.re = c.re;
    d.im = -c.im;

    return d;
}

static inline real cabs2_d(t_complex_d c) {
    double abs2;
    abs2 = (c.re * c.re) + (c.im * c.im);

    return abs2;
}

#if 0
static inline t_complex_d cdiv_d(t_complex_d teller, t_complex_d noemer) {
    t_complex_d res, anoemer;

    anoemer = cmul_d(conjugate_d(noemer), noemer);
    res = cmul_d(teller, conjugate_d(noemer));

    return rcmul_d(1.0 / anoemer.re, res);
}
#endif
