#pragma once

#include <gromacs/math/vec.h>  // rvec
#include <gromacs/mdlib/forcerec.h>  // t_forcerec
#include <gromacs/mdtypes/commrec.h>  // t_commrec
#include <gromacs/mdtypes/mdatom.h>  // t_mdatoms
#include <gromacs/pbcutil/pbc.h>  // PbcType
#include <gromacs/timing/wallcycle.h>  // gmx_wallcycle_t
#include <gromacs/topology/topology.h>  // gmx_mtop_t

/* Compute SWAXS / Neutron scattering curve and possibly do coupling. */

/* Compute WAXS intensity, potential, & forces.
   These are computed here because the forces should not go into the virial.
*/
void do_waxs_md(const t_commrec* cr, const t_mdatoms* mdatoms, gmx::ArrayRef<gmx::RVec> xlocal, double simtime,
        int64_t step, t_forcerec* fr, const gmx_mtop_t* mtop, const matrix box, rvec f_novirsum[], real* enerXray, real* enerNeutron);
